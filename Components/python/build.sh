pip3 install -r requirements.txt
shopt -s nullglob
for f in *.py; do
  cd $( dirname $f)
  python -m py_compile $f
done