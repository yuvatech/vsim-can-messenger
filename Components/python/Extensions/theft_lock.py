import builtins as __builtin__
import os, sys
import json
import pickle
import time
from datetime import datetime
import logging
import argparse

# Basic logging setup
logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = GMVehicleSim()

def close(sig, frame):  
  _gmVehicleSim.close()
signal.signal(signal.SIGINT, close)

# Properties
_running = True
_fc_wait = 0.25
_id_tester = '14DA80F1' # test tool #1 id


def _cycle_handler(cycle = None):
  """ Wakeup and diagnostics mode """
  payload = []
  # payload.append({'Type': 'Raw', 'Mode': 'RAW', 'Id': '145', 'Value': '4540000000000020'})  # PN Wakeup Message
  payload.append({'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '023E800000000000'})  # Tester Present Message

  # cycle
  count = 0;
  while _running:
    count = count + 1
    _gmVehicleSim.send(payload) 
    if cycle and cycle <= count:
      break
    time.sleep(2)

def _clear_lock():
  # Enter Diagnostic Extended Session
  entry = {'Type': 'Raw', 'Mode': 'MF', 'Id': _id_tester, 'Value': '1003'} 
  _gmVehicleSim.send([entry])
  time.sleep(1)

  # Unlock - Get Seed
  entry = {'Type': 'Raw', 'Mode': 'MF', 'Id': _id_tester, 'Value': '2701'}
  _gmVehicleSim.send([entry])
  time.sleep(_fc_wait)
  # flow control on the response (to the above command)!
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '3000000000000000'}
  _gmVehicleSim.send([entry])
  time.sleep(1)

  # Unlock - Send Key  
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '100E270220202020'}
  _gmVehicleSim.send([entry])
  time.sleep(_fc_wait)
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '2121202020202020'}
  _gmVehicleSim.send([entry])
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '2220000000000000'}
  _gmVehicleSim.send([entry])
  time.sleep(1)

  """ Clear Theft-lock """  
  # clear theft-lock ?
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '10142EF190000000'}
  _gmVehicleSim.send([entry])
  time.sleep(0.025)
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '2100000000000000'}
  _gmVehicleSim.send([entry])
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '2200000000000000'}
  _gmVehicleSim.send([entry])

def _set_MEC(value='FF'):
  # Enter Diagnostic Extended Session
  entry = {'Type': 'Raw', 'Mode': 'MF', 'Id': _id_tester, 'Value': '1003'} 
  _gmVehicleSim.send([entry])
  time.sleep(1)

  # Unlock - Get Seed
  entry = {'Type': 'Raw', 'Mode': 'MF', 'Id': _id_tester, 'Value': '270D'}
  _gmVehicleSim.send([entry])
  time.sleep(_fc_wait)
  # flow control on the response (to the above command)!
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '3000000000000000'}
  _gmVehicleSim.send([entry])
  time.sleep(1)

  # Unlock - Send Key  
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '100E270EAAAAAAAA'}
  _gmVehicleSim.send([entry])
  time.sleep(_fc_wait)
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '21AAAAAAAAAAAAAA'}
  _gmVehicleSim.send([entry])
  entry = {'Type': 'Raw', 'Mode': 'RAW', 'Id': _id_tester, 'Value': '22AA000000000000'}
  _gmVehicleSim.send([entry])
  time.sleep(1)

  """ Set MEC counter """
  entry = {'Type': 'Raw', 'Mode': 'MF', 'Id': _id_tester, 'Value': '3101F0008001'}  # Enter AME Mode
  _gmVehicleSim.send([entry]) 
  time.sleep(1)
  entry = {'Type': 'Raw', 'Mode': 'MF', 'Id': _id_tester, 'Value': '3101F0008041'+str(value)}  # Set MEC Command
  _gmVehicleSim.send([entry]) 
  time.sleep(1)


def main():
  global _running

  parser = argparse.ArgumentParser(description='')
  parser.add_argument('--MEC', action='store_true', help="set MEC to max. value", default=False)
  args = parser.parse_args()

  _gmVehicleSim.open(True) # block until ready

  _running = True
  threading.Thread(target=_cycle_handler).start()  
  time.sleep(6)
 
  if hasattr(args, 'MEC') and args.MEC == True:    
    _set_MEC()
  else:
    _clear_lock()

  time.sleep(6)
  _running = False
  

''' Main Entry '''
if __name__== "__main__":
  main()
  close(None,None)
