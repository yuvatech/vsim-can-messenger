import builtins as __builtin__
import os, sys
import tkinter as tk
from tkinter import ttk
import json
import pickle
import time
from threading import Thread
import threading
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *


class AnonObject(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class Application(tk.Frame):
  def __init__(self, master=None):       
    super().__init__(master)    

    self._gmVehicleSim = GMVehicleSim()    

    self._shutdown = threading.Event()
    self._shutdown.clear()

    self._threads = []  # reset?
    self._threads.append(Thread(target=self._heartbeat_handler , args=[])) 

    for thread in self._threads:
      thread.start()

    self.master = master
    self.pack()
    self.widgets()


  def _close(self):
    self._shutdown.set()
    self.master.destroy()
    self._gmVehicleSim.close()


  def _heartbeat_handler(self):
    while not self._shutdown.is_set():
      if not self._gmVehicleSim.is_connected():
        self._gmVehicleSim.open();
      self._init_vsim()
      time.sleep(1*3)


  def widgets(self):
    self.master.title('Plant Programmer')

    self._info_lbl = tk.StringVar()
    self.send_info_lbl = tk.Label(self, fg="red", width=25, textvariable=self._info_lbl)
    self.send_info_lbl.grid(row=1, column=2, padx=5, sticky=tk.W)
    self._info_lbl.set("")

    self.send_btn = tk.Button(self)
    self.send_btn["text"] = "Off Mode & Park"
    self.send_btn["command"] = self._off_mode_park
    self.send_btn.grid(row=1, column=1, sticky=tk.N+tk.S+tk.W+tk.E)

    self.send_btn = tk.Button(self)
    self.send_btn["text"] = "Off & Clear VIN"
    self.send_btn["command"] = self._off_clear_vin
    self.send_btn.grid(row=3, column=1, sticky=tk.N+tk.S+tk.W+tk.E)

    self._repeat = tk.IntVar()
    self.send_repeat_chk = tk.Checkbutton(self, text="Repeat",variable=self._repeat)
    self.send_repeat_chk.grid(row=1000, column=1, sticky=tk.W)

    self.quit = tk.Button(self, text="QUIT", fg="red",command=self._close, width = 10)
    self.quit.grid(row=1000, column=1000, sticky=tk.E) # 1000 big number to me right most column and bottom most position

    self.pack()

  def _init_vsim(self):
    payload = []
    payload.append(AnonObject(entry = {'Type': 'VehicleSim', 'Name': 'mode', 'Value': 'passive'}, delay = 0))
    self._send_delayed(payload)

  def _off_clear_vin(self):
    thread = Thread(target=self._off_clear_vin_handler, args=[])
    thread.start()
    #thread.join()


  def _off_clear_vin_handler(self):
    payload = []
    # Info VLAN Continue
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '621', 'Value': '0040000000000000'}, delay = 0))
    # System Power Mode Off
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '242000', 'Value': '00'}, delay= 1000))  
    # Clear VIN
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'HS', 'Id': '252', 'Value': '07AEFE8000000000'}, delay= 100)) 
    # System Power Mode Off
    for i in range(2):    
      payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '242000', 'Value': '00'}, delay= 100))  
    # Door Status Open
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '630000', 'Value': '81'}, delay= 100))
    # System Power Mode Off
    for i in range(5):    
      payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '242000', 'Value': '00'}, delay= 100))
    # Clear VIN
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'HS', 'Id': '252', 'Value': '07AEFE8000000000'}, delay= 100)) 
    # System Power Mode Off
    for i in range(9):    
      payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '242000', 'Value': '00'}, delay= 100))
    # Door Status Closed
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '630000', 'Value': '80'}, delay= 0))
    # System Power Mode Off
    for i in range(4):    
      payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '242000', 'Value': '00'}, delay= 100))
    
    while True:
      time.sleep(0.250)
      self._send_delayed(payload)
      time.sleep(0.250)
      for i in range(75): 
        self._info_lbl.set(str(75-i-1))
        time.sleep(1) # ~1 sec.
      if self._repeat.get() is 0:
        break
      time.sleep(0.250)


  def _off_mode_park(self):
    thread = Thread(target=self._off_mode_park_handler, args=[])
    thread.start()
    #thread.join()


  def _off_mode_park_handler(self):
    payload = []
    # Info VLAN Active
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '621', 'Value': '0140000000000000'}, delay = 50))
    # System Power Mode Off
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '242000', 'Value': '00'}, delay= 50))    
    # Door Status Closed
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'LS', 'Id': '630000', 'Value': '80'}, delay= 0))  
    # Park 1
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'HS', 'Id': '1F5', 'Value': '0F0F000100000300'}, delay= 50)) 
    # Park 2
    payload.append(AnonObject(entry = {'Type': 'Raw', 'Mode': 'HS', 'Id': '4D6', 'Value': '0000000000'}, delay= 1000)) 

    while True:
      self._send_delayed(payload)
      if self._repeat.get() is 0:
        break
      time.sleep(0.250)


  def _send_delayed(self, payload):
    for item in payload:
      if self._gmVehicleSim.send([item.entry]) or True: # if sent then show
        print(item.entry)
        self._gmVehicleSim.delay(item.delay)

def main():
  root = tk.Tk()
  app = Application(master=root)
  app.mainloop()


""" Main Entry """
if __name__== "__main__":
  main()
  sys.exit(0)
