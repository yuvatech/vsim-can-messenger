import builtins as __builtin__
import os, sys, signal
import json
import pickle
import time
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.INFO)
log = logging.getLogger()
log.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = GMVehicleSim()

microsecond_now = lambda: int(round(datetime.now().microsecond))

def close(sig, frame):
  # Note: will finally close when log queue is empty (since is slower and has to catch-up)
  _gmVehicleSim.close()
  sys.exit(0)
signal.signal(signal.SIGINT, close)

def on_receive(packet):
  if packet['Name'] and "SPMP_SysPwrModeAuth" == packet['Name']:
    print( datetime.now().strftime('%H:%M:%S.%f')[:-3] + "," + packet['Value']) #_gmVehicleSim.log('Rx('+str(microsecond_now())+'): ' + str(item))


def main():
  
  db = GMVehicleSim.db();  # ARXML data object! :)
  
  # Example:
  payload = []
  #entry  = {'Type': 'VehicleSim', 'Name': 'mode', 'Value': 'run'}
  #entry  = {'Type': 'VehicleSim', 'Name': 'mode', 'Value': 'reboot'}
  entry  = {'Type': 'Signal', 'Name': 'CustMdBrkPdlCstChngSetReq', 'Value': '6'}
  payload.append(entry)
  # set periodic 
  payload.append({'Type': 'Signal', 'Name': 'SPMP_SysPwrModeAuth', 'Value': '1', 'Periodic': 'true'})

  _gmVehicleSim.open() # defualts to localhost and correct port, also waits until socket is ready
  print("Ready...")

  _gmVehicleSim.on_receive(on_receive) # register for on rx event

  # send
  if len(payload) > 0 and _gmVehicleSim.send(payload):
    _gmVehicleSim.log('Tx ('+str(microsecond_now())+'): ' + str(payload))

  while True:
    try:
      time.sleep(0.250)
    except Exception as ex:
      # Todo: better with reconnect and timeout and ....
      log.error(ex)
      close(None, None)
      break


""" Main Entry """
if __name__== "__main__":
  main()
  close(None,None)
