
import builtins as __builtin__
import os, sys, signal
import json
import pickle
import time
from datetime import datetime
import logging
import argparse
import binascii

# basic logging setup
logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = None
_db = None

_play = True

def close(sig, frame):
  print("please wait...")
  if _gmVehicleSim is not None:
    _gmVehicleSim.close()
  print("done.")
  os._exit(0)
  #sys.exit(0)
signal.signal(signal.SIGINT, close) # register OS signal
signal.signal(signal.SIGTERM, close)

time_loop = None # initial init. later

def main():
  parser = argparse.ArgumentParser(description='')
  parser.add_argument('--record', action='store_true', help="", default=(not _play))
  parser.add_argument('--play', action='store_true', help="", default=(_play))
  parser.add_argument('--file', nargs='?', help="capture file", default="capture.log")
  args = parser.parse_args()
 
  if hasattr(args, 'file'):
    capture_file = str(args.file)

  if hasattr(args, 'record') and args.record == True:
    if os.path.exists(capture_file):
      os.remove(capture_file)  # remove old file if starting again

  global _gmVehicleSim 
  _gmVehicleSim = GMVehicleSim(log_file=capture_file)
  global _db 
  _db = GMVehicleSim.db();

  _gmVehicleSim.on_receive(_receive_handler)

  _from_CSM_init() # try to cache values for quicker searching...

  if hasattr(args, 'record') and args.record == True: 
    print("recording...")
  elif hasattr(args, 'play') and args.play== True: 
    print("playing...")
  
  while True:
    try:
      if not _gmVehicleSim.is_connected(): # open if closed   
        _gmVehicleSim.open(True)

      if hasattr(args, 'record') and args.record == True:        
        while True:  # forever
          time.sleep(1)
      elif hasattr(args, 'play') and args.play == True:          
        with open(capture_file) as fp:                  
          for line in fp:
            #line = fp.readline()
            if ',' not in line :
              continue # yucky line?

            split =  [x.strip() for x in line.split(',')]  # by comma
            if len(split) < 4:
              continue  # should never be here
            
            payload_tx = []
            entry  = {'Type': 'Raw', 'Id': split[2], 'Value': split[3]}
            payload_tx.append(entry)

            # try to mimic timing...
            time.sleep(float(split[1]) * 0.001 * _gmVehicleSim.lag_factor());
            _gmVehicleSim.send(payload_tx)            
        break  # done
    except Exception as ex:
      # Todo: better with reconnect and timeout and ...
      logger.error(ex)
      break; # leave if issue...

def _receive_handler(packet):
  global time_loop
  if packet:
    # init. time stamp
    if time_loop is None:
      time_loop = datetime.now()
    time_now = datetime.now()

    # raw data sent to CSM
    CSM_rx_any = False
    item = packet
    if item['Type'] is not None:
      if 'raw' in item['Type'].lower():
        if not from_CSM(item['Id']): # if this id is sent to CSM (not from CSM)
            CSM_rx_any = True
            milli_sec = float(round((time_now - time_loop).total_seconds() * 1000.0 * _gmVehicleSim.lag_factor(), 3)) # 3 decimal places is enough, 
            # write capture file
            _gmVehicleSim.log('rx,' + str(milli_sec) + ',' + str(item['Id']) + ',' + str(item['Value']) ) # non-blocking uses queue underneath!
    if CSM_rx_any:  # only care if logging a message else keep counting time
      time_loop = datetime.now() # mark time since last payload

def from_CSM(id):
  if len(_from_CSM_Ids) <= 0:
    _from_CSM_init()
  CSM_Ids = _from_CSM_Ids

  for CSM_id in CSM_Ids:
    if CSM_id == id: # exact match!
      return True
  return False

_from_CSM_Ids = []
def _from_CSM_init():
  db = _db
  messages = db['messages'];
  for key, value in messages.items():
    # Todo: do this smarter, more pythonic!
    id_match = None
    rx_csm = True
    for sub_key, sub_value in value.items():
      if sub_key.lower() == 'identifier':
        # convert to hex
        temp = hex(sub_value)
        temp = temp[2:]       
        id_match = temp
      if sub_key.lower() == 'tx':
        rx_csm = not True

    if not rx_csm:
      _from_CSM_Ids.append(id_match.upper())


""" Main Entry """
if __name__== "__main__":
  main()
  close(None,None)