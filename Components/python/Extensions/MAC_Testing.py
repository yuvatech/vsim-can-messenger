import builtins as __builtin__
import os, sys, signal
import json
import pickle
import time
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()
log.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = GMVehicleSim()

def close(sig, frame):
  # Note: will finally close when log queue is empty (since is slower and has to catch-up)
  _gmVehicleSim.close()
  sys.exit(0)
signal.signal(signal.SIGINT, close)

def on_receive(packet):
  pass

def main():
  
  db = GMVehicleSim.db();  # ARXML data object! :)
  

  _gmVehicleSim.open()
  print("Ready...")

  _gmVehicleSim.on_receive(on_receive) # register for on rx event

  while True:
    try:
      inc = 0
      for i in range(5):
        inc += 1
        _gmVehicleSim.send([{'Type': 'Signal', 'Name': 'SPMP_SysPwrModeAuth', 'Value': str(i)}])
        time.sleep(1)

      print(".", end ="") 
      time.sleep(0.250)
    except Exception as ex:
      # Todo: better with reconnect and timeout and ....
      log.error(ex)
      close(None, None)
      break


""" Main Entry """
if __name__== "__main__":
  main()
  close(None,None)
