# -*- mode: python -*-

block_cipher = None


a = Analysis(['D:\\work\\Development\\New\\gmvehiclesim\\Components\\python\\Extensions\\GMVehicleSim\\gm_vehicle_sim.py'],
             pathex=['D:\\work\\Development\\New\\gmvehiclesim\\Components\\python\\Extensions\\GMVehicleSim'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='gm_vehicle_sim',
          debug=True,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
