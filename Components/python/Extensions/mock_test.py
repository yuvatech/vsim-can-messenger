import builtins as __builtin__
import os, sys, signal
import json
import pickle
import time
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.INFO)
log = logging.getLogger()
log.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = GMVehicleSim()

logger = open("mock_test.csv", "w")

def close(sig, frame):
  logger.close()
  # Note: will finally close when log queue is empty (since is slower and has to catch-up)
  _gmVehicleSim.close()
  sys.exit(0)
signal.signal(signal.SIGINT, close)

_count = 0
def on_receive(packet):
  if not packet:
    return
  if "100" in packet['Id'] and "raw" in packet['Type'].lower():
    # _count = _count + 1
    now = datetime.now().strftime('%H:%M:%S.%f')[:-3]
    info_timing = now + "," + packet['Value']
    print(info_timing)
    # logger.write(info_timing + '\n')


def main():
  
  db = GMVehicleSim.db();  # ARXML data object! :)
  
  # Example:
  payload = []
  # payload.append({'Type': 'VehicleSim', 'Name': 'RX', 'Value': 'flush'})
  # payload.append({'Type': 'Signal', 'Name': 'SPMP_SysPwrModeAuth', 'Value': '1', 'Periodic': 'true'})
  _gmVehicleSim.open() # defualts to localhost and correct port, also waits until socket is ready
  print("Ready...")

  _gmVehicleSim.on_receive(on_receive) # register for on rx event

  # send
  if len(payload) > 0 and _gmVehicleSim.send(payload):
    _gmVehicleSim.log('Tx ('+str(microsecond_now())+'): ' + str(payload))

  while True:
    try:

      input("FLUSH <enter>")
      _gmVehicleSim.send([{'Type': 'VehicleSim', 'Name': 'RX', 'Value': 'flush'}])

      time.sleep(0.025) # super small...
    except Exception as ex:
      # Todo: better with reconnect and timeout and ....
      log.error(ex)
      close(None, None)
      break


""" Main Entry """
if __name__== "__main__":
  main()
  close(None,None)
