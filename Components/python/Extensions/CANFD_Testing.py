import builtins as __builtin__
import os, sys
import tkinter as tk
from tkinter import ttk
import json
import pickle
import time
from threading import Thread
import threading
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

microsecond_now = lambda: int(round(datetime.now().microsecond))

class Application(tk.Frame):
  def __init__(self, master=None):       
    super().__init__(master)    

    self._gmVehicleSim = GMVehicleSim()

    self._gmVehicleSim.on_receive(self._rx_handler)

    self._shutdown = threading.Event()
    self._shutdown.clear()

    self._threads = []  # reset?
    self._threads.append(Thread(target=self._heartbeat_handler , args=[])) 

    for thread in self._threads:
      thread.start()

    self.master = master
    self.pack()
    self.widgets()

  def _close(self):
    self._shutdown.set()
    self.master.destroy()
    self._gmVehicleSim.close()

  def _heartbeat_handler(self):
    while not self._shutdown.is_set():
      if not self._gmVehicleSim.is_connected():
        self._gmVehicleSim.open();
      time.sleep(1*3)

  def widgets(self):
    self.master.title('CAN Rx & Tx')

    self.send_id_lbl = tk.Label(self, text="Id (hex):")
    self.send_id_lbl.grid(row=0, column=1, padx=5, sticky=tk.W)
    self.send_id_txt = tk.Text(self, height=1, width=16)
    self.send_id_txt.grid(row=0, column=2, sticky=tk.W)

    self.send_data_lbl = tk.Label(self, text="Data (hex bytes):")
    self.send_data_lbl.grid(row=1, column=1, sticky=tk.W)
    self.send_data_txt = tk.Text(self, height=1, width=40)
    self.send_data_txt.grid(row=1, column=2, rowspan=2, sticky=tk.W)

    self.send_btn = tk.Button(self)
    self.send_btn["text"] = "Send"
    self.send_btn["command"] = self.send
    self.send_btn.grid(row=3, column=2, sticky=tk.N+tk.S+tk.W+tk.E)

    self._canfd = tk.IntVar()
    self.send_fd_chk = tk.Checkbutton(self, text="FD",variable=self._canfd)
    self.send_fd_chk.grid(row=3, column=3, sticky=tk.N+tk.S+tk.W+tk.E)

    self.monitor_tree = ttk.Treeview(self, columns=('Mode', 'Time', 'Id', 'Data'))
    self.monitor_tree.heading('#0', anchor=tk.W, text='Direction')
    self.monitor_tree.heading('#1', anchor=tk.W, text='Mode')
    self.monitor_tree.heading('#2', anchor=tk.W, text='Time')
    self.monitor_tree.heading('#3', anchor=tk.W, text='Id')
    self.monitor_tree.heading('#4', anchor=tk.W, text='Data')
    self.monitor_tree.column('#0', anchor=tk.W, minwidth=50, width=75, stretch=tk.NO)
    self.monitor_tree.column('#1', anchor=tk.W, minwidth=50, width=75, stretch=tk.NO)
    self.monitor_tree.column('#2', anchor=tk.W, minwidth=50, width=75, stretch=tk.NO)
    self.monitor_tree.column('#3', anchor=tk.W, minwidth=50, width=75, stretch=tk.NO)
    self.monitor_tree.column('#4', anchor=tk.W, minwidth=200, width=400, stretch=tk.NO)
    self.monitor_tree.grid(row=4, columnspan=16, padx=10, pady=10, sticky=tk.N+tk.S+tk.W+tk.E)
    self.rx_tree_index = 0 # init
    # add scroll bars
    # y
    self.monitor_tree_ybar = ttk.Scrollbar(self, orient=tk.VERTICAL, command=self.monitor_tree.yview)
    self.monitor_tree_ybar.grid(row=4, column=17, sticky=tk.N+tk.S+tk.W+tk.E)
    self.monitor_tree.configure(yscrollcommand=self.monitor_tree_ybar.set)
    # x
    self.monitor_tree_xbar = ttk.Scrollbar(self, orient=tk.HORIZONTAL, command=self.monitor_tree.xview)
    self.monitor_tree_xbar.grid(row=5, columnspan=16, sticky=tk.N+tk.S+tk.W+tk.E)
    self.monitor_tree.configure(xscrollcommand=self.monitor_tree_xbar.set)

    self.quit = tk.Button(self, text="QUIT", fg="red",command=self._close, width = 10)
    self.quit.grid(row=1000, column=1000, sticky=tk.E) # 1000 big number to me right most column and bottom most position

    self.pack()#expand=tk.YES, fill=tk.BOTH)

    #self._test(); # temp!

  def _test(self):
    '''
    Hardcoded testing for now... (yes lazy, should use unit tests)
    '''
    timestamp = str(microsecond_now())
    for x in range(50):
      self._rx_tree_insert({'Direction': 'Test', 'Mode': 'Test', 'Time': timestamp, 'Id': '01FF', 'Data': '0102030405060708090A0B0C0D0E0F'})

  def send(self):
    id = self.send_id_txt.get('1.0', 'end-1c') # ingore newline at end
    data = self.send_data_txt.get('1.0', 'end-1c') # ignore newline at end
    #entry  = {'Type': 'Raw', 'Mode': 'HSCANFD', 'Name': str(id), 'Value': str(data)}
    mode = 'HS'
    if self._canfd.get() is not 0:
      mode = 'FD'    
    timestamp = str(microsecond_now())
    entry  = {'Type': 'Raw', 'Mode': str(mode), 'Id': str(id), 'Value': str(data), 'Port': str(2)}
    if self._gmVehicleSim.send([entry]) or True: # if sent then show
      self._rx_tree_insert({'Direction': 'Tx', 'Mode': str(mode), 'Time': timestamp, 'Id': str(id), 'Data': str(data)})

  def _rx_tree_insert(self, item):
    try:
      if self._canfd and self._canfd.get() is not 0 and 'FD' not in item['Mode'].upper():
          return
    except:
      pass

    # insert at top else set to 'END'
    self.monitor_tree.insert('', 0, text=item['Direction'], values=(item['Mode'], item['Time'], item['Id'], item['Data']))    
    self.rx_tree_index = self.rx_tree_index + 1

  def _rx_handler(self, packet):
    entry = packet
    timestamp = str(microsecond_now())
    if str(entry['Type']).upper() in ('RAW') and entry['Port'] is '2':
      if str(entry['Mode']).upper() in ('HS', 'FD'):
        self._rx_tree_insert({'Direction': 'Rx', 'Mode': str(entry['Mode']) , 'Time': timestamp, 'Id': str(entry['Id']), 'Data': str(entry['Value'])})
   

def main():
  root = tk.Tk()
  app = Application(master=root)
  app.mainloop()

""" Main Entry """
if __name__== "__main__":
  main()
  sys.exit(0)
