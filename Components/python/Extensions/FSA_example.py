import builtins as __builtin__
import os
import sys
import signal
import json
import pickle
import time
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.INFO)
log = logging.getLogger()
log.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = GMVehicleSim()

def close(sig, frame):
  # Note: will finally close when log queue is empty (since is slower and has
  # to catch-up)
  _gmVehicleSim.close()
  sys.exit(0)
signal.signal(signal.SIGINT, close)

def on_receive(packet):
  if packet['Type'] and "Get" == packet['Type']:
    print( datetime.now().strftime('%H:%M:%S.%f')[:-3] + "," + packet['Value'])

def main():
    
  _gmVehicleSim.open() # defualts to localhost and correct port, also waits until socket is ready
  print("Ready...")

  _gmVehicleSim.on_receive(on_receive) # register for on rx event

  ### FSA ###
  #Init
  _gmVehicleSim.send([{'Service': 'Init','Id':'1016','Value':'192.168.1.102:9005'}])
  # get
  _gmVehicleSim.send([{'Service': 'OnStarFunctions', 'Type': 'Get', 'Name': 'OnStarMessage', 'Value': ''}])
  # set
  _gmVehicleSim.send([{'Service': 'OnStarFunctions', 'Type': 'Set', 'Name': 'TTYMode', 'Value': 'HEARING_MODE'}])
  # subscribe
  _gmVehicleSim.send([{'Service': 'OnStarFunctions', 'Type': 'Subscribe', 'Name': 'OnStarMessage', 'Value': ''}])
  # unsubscribe
  _gmVehicleSim.send([{'Service': 'OnStarFunctions', 'Type': 'UnSubscribe', 'Name': 'OnStarMessage', 'Value': ''}])
  # request
  _gmVehicleSim.send([{'Service': 'OnStarFunctions', 'Type': 'Request', 'Name': 'NeedLocationSharingEnabledResponse', 'Value': 'true'}])
  # request & response
  _gmVehicleSim.send([{'Service': 'OnStarFunctions', 'Type': 'RequestResponse', 'Name': 'SendTTYText', 'Value': '{ "Text": "Some SendTTYText", "Encoding": "UTF8"}' }])

  while True:
    try:
      time.sleep(0.250)
    except Exception as ex:
      # Todo: better with reconnect and timeout and ....
      log.error(ex)
      close(None, None)
      break


""" Main Entry """
if __name__ == "__main__":
  main()
  close(None,None)
