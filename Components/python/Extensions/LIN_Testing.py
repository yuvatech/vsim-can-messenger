import builtins as __builtin__
import os, sys, signal
import json
import pickle
import time
from datetime import datetime
import logging

# Basic logging setup
logging.basicConfig(level=logging.INFO)
log = logging.getLogger()
log.addHandler(logging.StreamHandler())  # pass logging to console

# include module (in path)
sys.path.append("/GMVehicleSim")
from GMVehicleSim import *

_gmVehicleSim = GMVehicleSim()

def close(sig, frame):
  # Note: will finally close when log queue is empty (since is slower and has to catch-up)
  _gmVehicleSim.close()
  sys.exit(0)
signal.signal(signal.SIGINT, close)

def on_receive(packet):
  print(packet)

def main():
    
  _gmVehicleSim.open() # defualts to localhost and correct port, also waits until socket is ready
  print("Ready...")

  _gmVehicleSim.on_receive(on_receive) # register for on rx event

  ### LIN ###  
  # LIN Frame
  _gmVehicleSim.send([{'Mode': 'LIN', 'Id': '01', 'Port': '1'}]) # master (no data) sync poll slave Id, on LIN port/channel 1
  # master-slave, where master polls, but then also answers as a slave
  _gmVehicleSim.send([{'Mode': 'LIN', 'Id': '01', 'Port': '1', 'Value': 'FF02030405060708'}]) # slave (has data), on LIN port/channel 1
  #

  while True:
    try:
      time.sleep(0.250)
    except Exception as ex:
      # Todo: better with reconnect and timeout and ....
      log.error(ex)
      close(None, None)
      break


""" Main Entry """
if __name__== "__main__":
  main()
  close(None,None)
