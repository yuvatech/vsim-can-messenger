SETLOCAL

::SET PATH="%LOCALAPPDATA%\Programs\Python\Python35-32\";%PATH%;
SET PATH="C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64";%PATH%;
::python -O -m PyInstaller flasher.spec
:: --onefile --noupx --onedir
::python -m PyInstaller --onefile --debug --log-level=DEBUG --add-data "Data;Data" flasher.py

CD /d %~dp0

FOR /R %%f in (*.py) do (
  CD %%~pf
  python -m PyInstaller --onefile --debug --log-level=DEBUG %%f
)

ENDLOCAL