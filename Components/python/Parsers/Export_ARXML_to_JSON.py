# Usage: python Export_ARXML_to_JSON.py <Input file name>

from xml.etree import ElementTree as ET
import json
import re
import os
import sys

# hard-code testing...
# sys.argv.append("D:\Temp\GlobalB_SystemDescriptionFile_22_22_144R3_422.arxml")

# Open XML document using minidom parser
if(len(sys.argv)< 2):
   print("command not proper. Exiting generation")
   sys.exit(0)

INPUT_FILENAME = sys.argv[1]

if not os.path.isfile(INPUT_FILENAME):
  print('ARXML file not found. Aborting.')
  exit()
  
# function for converting string to int or float depending on the text
def num(s):
  try:
    return int(s)
  except ValueError:
    return float(s)

# Read the contents of the XML file into xmlstring
with open(INPUT_FILENAME) as arxmlFile:
    arxmlStr = arxmlFile.read()

# Strip the UTF-8 BOM and Remove the default namespace definition (xmlns="http://some/namespace")
if arxmlStr[0:2] != '<?':
  arxmlStr = re.sub('.+<\?', '<?', arxmlStr, count=1)
arxmlStr = re.sub('\\sxmlns="[^"]+"', '', arxmlStr, count=1)
 
# Parse the XML string
Arxml = ET.fromstring(arxmlStr)
  
filePrefix = re.sub('((.*\\\)|(\..*))', '', INPUT_FILENAME)
JSON_FILENAME = 'arxml' + '.json'
print( "Parsing ARXML - " + filePrefix )

Signals = {}
ISignals = {}
Messages = {}
SignalGroups = {}

# create a dictionary of all the signal names as the keys 
for node in Arxml.iter('SYSTEM-SIGNAL'):
  signalShortName = node.find('SHORT-NAME').text
  
  Signals[signalShortName] = {}
  Signals[signalShortName]['name'] = signalShortName
  
  try:
    signalLongName = node.find('DESC/L-2').text
    Signals[signalShortName]['longname'] = re.sub('\n', '', signalLongName.strip() )
  except:
     pass

# add I-Signals as signals because that's what we do now in 131 apparently
for node in Arxml.iter("I-SIGNAL"):

  signalShortName = node.find("SYSTEM-SIGNAL-REF")
  if signalShortName is None:
    # fix for 131
    signalShortName = node.find("SHORT-NAME").text
  else:
    signalShortName = signalShortName.text

  signalShortName = re.sub(".*/", "", signalShortName)
  if signalShortName not in Signals:
    Signals[signalShortName] = {}
    Signals[signalShortName]['name'] = signalShortName
  
  # map this to the new signal
  try:
    iSignalShortName = node.find("SHORT-NAME").text
    ISignals[iSignalShortName] = signalShortName
  except:
    pass
  
  if 'longname' not in Signals[signalShortName]:
    try:
      signalLongName = node.find('DESC/L-2').text
      Signals[signalShortName]['longname'] = re.sub('\n', '', signalLongName.strip() )
    except:
      Signals[signalShortName]['longname'] = signalShortName
    
    
# create a dictionary of all the signal groups with names as the keys 
for node in Arxml.iter("SYSTEM-SIGNAL-GROUP"):

  groupName = node.find('SHORT-NAME').text
  SignalGroups[groupName] = {}
  SignalGroups[groupName]['signals'] = []
  
  for signalRef in node.findall(".//SYSTEM-SIGNAL-REF"):
    signalName = signalRef.text
    signalName = re.sub(".*/", "", signalName)
    
    SignalGroups[groupName]['signals'].append(signalName)
    
  SignalGroups[groupName]['signals'].sort()

#add starting bit to signal dictionary
for node in Arxml.iter("I-SIGNAL-TO-I-PDU-MAPPING"):

  signalShortName = node.find("SHORT-NAME").text
  
  if signalShortName in Signals:
    try:
      isignalShortName = node.find("I-SIGNAL-REF").text
      isignalShortName = re.sub(".*/", "", isignalShortName)
      ISignals[isignalShortName] = signalShortName
      Signals[signalShortName]['isignal'] = isignalShortName
    except:
      pass
      
    try:
      Signals[signalShortName]['byteorder'] = node.find("PACKING-BYTE-ORDER").text
    except:
      pass
    
    try:
      Signals[signalShortName]['startbit'] = int(node.find("START-POSITION").text)
    except:
      pass
    
    try:
      Signals[signalShortName]['transferproperty'] = node.find("TRANSFER-PROPERTY").text
    except:
      pass

#add signal PDU Group
triggeredSigs = []

for node in Arxml.iter("I-SIGNAL-I-PDU"):
    
    pduName = node.find("SHORT-NAME").text  #pdu group name for all signals under this particular element

    for signal in node.findall(".//I-SIGNAL-TO-I-PDU-MAPPING"):
      
      signalShortName = signal.find("SHORT-NAME").text

      if signalShortName in Signals:
        Signals[signalShortName]['pdugroup'] = pduName
        
      # if it's a signal group, add all the contained signals to the PDU
      elif signalShortName in SignalGroups:
        for signame in SignalGroups[signalShortName]['signals']:
          Signals[signame]['pdugroup'] = pduName
    
    # search for triggering
    for signal in node.findall(".//I-SIGNAL-IN-I-PDU-REF"):
          
      signalShortName = signal.text
      signalShortName = re.sub(".*/", "", signalShortName)
      
      if signalShortName in Signals:
        triggeredSigs.append( signalShortName )
        
      # if it's a signal group, add all the contained signals to the PDU
      elif signalShortName in SignalGroups:
        for signame in SignalGroups[signalShortName]['signals']:
          triggeredSigs.append( signame )


# find the triggering info to make sure it is received by CSM
for node in Arxml.iter("I-SIGNAL-TRIGGERING"):
  
  for signal in node.findall(".//I-SIGNAL-REF"):
    iSignalName = signal.text
    
    iSignalName = re.sub(".*/", "", iSignalName)
    if iSignalName not in ISignals:
      print( 'ISignal not found: ' + iSignalName)
    elif ISignals[iSignalName] not in triggeredSigs:
      triggeredSigs.append( ISignals[iSignalName] )

# delete signals without a pdugroup (un-mapped signals)
signalsToDelete = []
for sig in Signals:
  if sig not in triggeredSigs:
    signalsToDelete.append( sig )
    
for signal in signalsToDelete:
  del Signals[signal]

#add signal CAN Frame name
for node in Arxml.iter("CAN-FRAME"):

    frameName = node.find("SHORT-NAME").text
    pduName = node.find(".//PDU-REF").text
    pduName = re.sub(".*/", "", pduName) 

    for signal in Signals:
        
        if 'pdugroup' not in Signals[signal]:
            continue

        if Signals[signal]['pdugroup'] == pduName:
            Signals[signal]['canframe'] = frameName   

#create table of units
Units = {}
for node in Arxml.iter("UNIT"):

  unitId   = node.find("SHORT-NAME")
  unitName = node.find("DISPLAY-NAME")
  if unitName and unitId:
    Units[unitId.text.rstrip()] = unitName.text.rstrip()


#add compu method items (factor, offset, enumerations, sign)
CompuMethods = {}
for node in Arxml.iter("COMPU-METHOD"):

   COMPUMethodName = node.find("SHORT-NAME").text
   COMPUCategory   = node.find("CATEGORY").text

   if COMPUCategory == "TEXTTABLE":  #enumerations
   
       CompuMethods[COMPUMethodName] = {}
       CompuMethods[COMPUMethodName]['type'] = 'enum'
       CompuMethods[COMPUMethodName]['enumerations'] = {}
       
       value = 0
       for compuscale in node.iter("COMPU-SCALE"):
         try:
           name_key = compuscale.find("SHORT-LABEL").text
         except:
           name_key = compuscale.find("COMPU-CONST/VT").text
         try:
           tempVal = compuscale.find("LOWER-LIMIT").text
           # check if not None
           if tempVal:
             value = tempVal
         except:
           pass
           # print( 'No Enum lower limit for ' + COMPUMethodName + '.' + name_key )
         CompuMethods[COMPUMethodName]['enumerations'][name_key] = int(value)
         value = int(value) + 1

   elif COMPUCategory == "LINEAR":  #values
       
       numerators = node.findall(".//COMPU-NUMERATOR/V")

       # changed in extract 131
       if len(numerators) > 1:
         offset = num(numerators[0].text) #offset 
         factor = num(numerators[1].text) #factor
       else:
         offset = 0 #offset 
         factor = num(numerators[0].text) #factor
       
       denominator = num(node.find(".//COMPU-DENOMINATOR/V").text) #denominator 

       if denominator != 1:
         offset = offset / denominator
         factor = factor / denominator
       
       minvalue = num(node.find(".//LOWER-LIMIT").text)
       maxvalue = num(node.find(".//UPPER-LIMIT").text)

       # convert min/max to engineering units
       minvalue = minvalue * factor + offset
       maxvalue = maxvalue * factor + offset
       
       CompuMethods[COMPUMethodName] = {}
       
       #add the unit if it exists
       try:
         unitRef = node.find("UNIT-REF").text

         unitRef = re.sub(".*/", "", unitRef)
         if unitRef in Units and unitRef != ' ':
           CompuMethods[COMPUMethodName]['units'] = Units[unitRef]
       except:
         pass

       CompuMethods[COMPUMethodName]['type'] = 'linear'
       CompuMethods[COMPUMethodName]['offset'] = offset
       CompuMethods[COMPUMethodName]['factor'] = factor
       CompuMethods[COMPUMethodName]['min'] = minvalue
       CompuMethods[COMPUMethodName]['max'] = maxvalue


#add bitlength and enumerations to signal dictionary
for node in Arxml.iter("I-SIGNAL"):

    signalShortName = node.find("SYSTEM-SIGNAL-REF")
    if signalShortName is None:
      # fix for 131
      signalShortName = node.find("SHORT-NAME").text
    else:
      signalShortName = signalShortName.text

    signalShortName = re.sub(".*/", "", signalShortName)
    if signalShortName not in Signals:
      continue
      
    Signals[signalShortName]['bitlength'] = int(node.find("LENGTH").text)
    
    #isignalShortName = node.find("SHORT-NAME").text

    try:
      iCOMPUMethodRef = node.find(".//COMPU-METHOD-REF").text
    except:
      # raw signals don't have compumethods
      # print( 'No CompuMethod found for ' + signalShortName )
      continue
    iCOMPUMethodName = re.sub(".*/", "", iCOMPUMethodRef)

    # skip if not in compumethod list
    if iCOMPUMethodName not in CompuMethods:
      continue
    
    # otherwise find the matching signal and add compumethod info
    #signal = ISignals[isignalShortName]
    
    if CompuMethods[iCOMPUMethodName]['type'] == 'enum':
      Signals[signalShortName]['enumerations'] = CompuMethods[iCOMPUMethodName]['enumerations']
     
    elif CompuMethods[iCOMPUMethodName]['type'] == 'linear':
      if 'units' in CompuMethods[iCOMPUMethodName]:
        Signals[signalShortName]['units']  = CompuMethods[iCOMPUMethodName]['units']
      Signals[signalShortName]['offset'] = CompuMethods[iCOMPUMethodName]['offset']
      Signals[signalShortName]['factor'] = CompuMethods[iCOMPUMethodName]['factor']
      Signals[signalShortName]['min']    = CompuMethods[iCOMPUMethodName]['min']
      Signals[signalShortName]['max']    = CompuMethods[iCOMPUMethodName]['max']

#create a json message list
for node in Arxml.iter("CAN-FRAME-TRIGGERING"):
    
    FrameTriggering = node.find("SHORT-NAME").text
    
    FrameName = node.find("FRAME-REF").text
    FrameName = re.sub(".*/", "", FrameName)

    AddressingMode = node.find("CAN-ADDRESSING-MODE").text

    Identifier =  int(node.find("IDENTIFIER").text)

    frame_port_ref = node.find(".//FRAME-PORT-REF").text
    frame_port_ref = re.sub(".*/", "", frame_port_ref)

    Messages[FrameName] = {}
    Messages[FrameName]['msgname'] = FrameName
    Messages[FrameName]['addressingmode'] = AddressingMode
    Messages[FrameName]['Identifier'] = Identifier
    Messages[FrameName]['frameportref'] = frame_port_ref
    
    # if Message is an NM message, create a signal for the data
    if re.search('NM_Frame_On_CAN', FrameName):
      # delete previous NM signal if exists
      if 'NM_Signal' in Signals:
        del Signals['NM_Signal']

      # create NM-frame-specific signal
      ecuName = re.sub('_.*$', '', FrameName)
      nmSignalName = ecuName + '_NM_Signal'
      Signals[nmSignalName] = {
        'name': nmSignalName,
        'longname': ecuName + ' Network Management Data',
        'startbit': 0,
        'min': 0,
        'bitlength': 64,
        'max': 0xFFFFFFFFFFFFFFFF,
        'factor': 1,
        'offset': 0,
        'transferproperty': 'PENDING',
        'canframe': FrameName,
        'byteorder': 'OPAQUE' }


#add signals to the message JSON
for eachcanframe in Messages:
    
    list_signals = [] #a list that will hold all signals for each message
    Messages[eachcanframe]['signals'] = []

    for eachsignal in sorted(Signals.keys()):
        
      if 'canframe' not in Signals[eachsignal]:
        continue

      if Signals[eachsignal]['canframe'] == Messages[eachcanframe]['msgname']:
        Messages[eachcanframe]['signals'].append(eachsignal)
        # messages[eachcanframe]['signals'][eachsignal] = signals[eachsignal]

    if Messages[eachcanframe]['signals'] == [] and not re.match('(USDT|UUDT|Tp)_.*', eachcanframe):
      print( 'No signal contained in Message: ' + eachcanframe )

#add message direction
for node in Arxml.iter("FRAME-PORT"):

    frameportName = node.find("SHORT-NAME").text
    direction = node.find("COMMUNICATION-DIRECTION").text

    # scan messages to find matchin frameport
    for eachframe in Messages:

        if Messages[eachframe]['frameportref'] == frameportName:
              if direction == 'OUT':
                Messages[eachframe]['Tx'] = 1
              if direction == 'IN':
                Messages[eachframe]['Rx'] = 1

#add frame length
for node in Arxml.iter("CAN-FRAME"):

    messagename = node.find("SHORT-NAME").text
    framelength = node.find("FRAME-LENGTH").text
    canframepduname = node.find(".//PDU-REF").text
    canframepduname = re.sub(".*/", "", canframepduname)

    for eachframe in Messages:

        if Messages[eachframe]["msgname"] == messagename:
            Messages[eachframe]["length"] = framelength
            Messages[eachframe]["pdu"] = canframepduname


#add cyclic information
for node in Arxml.iter("I-SIGNAL-I-PDU"):

    pduName = node.find("SHORT-NAME").text
    
    # Add PDU timing info if it exists
    for eachframe in Messages:

        if Messages[eachframe]["pdu"] == pduName:
          try:
            timeoffsetvalue = node.find(".//TIME-OFFSET/VALUE").text
            Messages[eachframe]["timeoffset"] = timeoffsetvalue
          except:
            pass
            
          try:
            timeperiodvalue = node.find(".//TIME-PERIOD/VALUE").text
            Messages[eachframe]["timeperiod"] = timeperiodvalue
          except:
            pass
            
          try:
            timedelayvalue  = node.find(".//MINIMUM-DELAY").text
            Messages[eachframe]["timedelay"]  = timedelayvalue
          except:
            pass

fileData = {}

fileData['messages'] = Messages
fileData['signalGroups'] = SignalGroups
fileData['signals'] = Signals

JSONFilePtr = open(JSON_FILENAME, "w")

JSONFilePtr.write("%s" % json.dumps(fileData, sort_keys=True, indent=4))

print( "ARXML Export complete: " + str(len(Signals)) + " Signals, " + str(len(Messages)) + " Messages, "  + str(len(SignalGroups)) + " Signal Groups" )

JSONFilePtr.close()

