# Usage: python <Script Name> <Input file name>

from xml.dom.minidom import *
import json
import re
import os
import sys
import csv
from io import StringIO

JSON_SUFFIX = ".json"

CSM_NODES = ["CSM_LS", "CSM_HS"]

cmdLineParam = sys.argv

# Open DBC file
if(len(cmdLineParam)!= 2):
   print("command not proper. Exiting generation")
   sys.exit(0)

INPUT_FILENAME = sys.argv[1]

if not os.path.isfile(INPUT_FILENAME):
  print('DBC file not found. Aborting.')
  exit()

# function for converting string to int or float depending on the text
def num(s):
  try:
    return int(s)
  except ValueError:
    return float(s)

with open(INPUT_FILENAME) as dbcFile:
  dbcData = dbcFile.readlines( )

filePrefix = re.sub('((.*\\\)|(\..*))', '', INPUT_FILENAME)

JSON_FILENAME = filePrefix + JSON_SUFFIX
print( "Parsing DBC - " + filePrefix )

Signals = {}
Messages = {}
SignalGroups = {}
ValueTables = {}

SIGNAL_IGNORE_LIST = []
MESSAGE_IGNORE_LIST = []


#append ^ $ to signal ignore list
for i in range(0, len(SIGNAL_IGNORE_LIST)):
  SIGNAL_IGNORE_LIST[i] = '(^' + SIGNAL_IGNORE_LIST[i] + '$)'
  
SIGNAL_IGNORE_LIST = '|'.join(SIGNAL_IGNORE_LIST)

for i in range(0, len(MESSAGE_IGNORE_LIST)):
  MESSAGE_IGNORE_LIST[i] = '(^' + MESSAGE_IGNORE_LIST[i] + '$)'
  
MESSAGE_IGNORE_LIST = '|'.join(MESSAGE_IGNORE_LIST)

#loop through the dbc and pull out value tables

# VAL_TABLE_ vt_BooleanValues 1 "true" 0 "false" ;
# VAL_ 2153406464 RrClsrInrAjrSwAct 1 "true" 0 "false" ;

for dbcLine in dbcData:

  # match either VAL_ or VAL_TABLE_  
  valTable = re.match('^\s*VAL_TABLE_\s+(.+)', dbcLine)

  if not valTable:
    valTable = re.match('^\s*VAL_\s+\d+\s+(.+)', dbcLine)
  
  if valTable:

    # parse the single dbc line
    enumLine = csv.reader( StringIO(valTable.group(1)), delimiter=' ', quotechar='"' )
    for row in enumLine:
      enumVals = row[:len(row)-1]
      break

    enumName = enumVals[0]
    
    ValueTables[enumName] = {}
    
    for i in range(1, len(enumVals) - 1, 2):
      ValueTables[enumName][enumVals[i+1]] = int(enumVals[i])

# Message TX relationships
# BO_TX_BU_ 605 : CSM_LS,CGM_LS,TCP_LS,ONSTAR_LS,TestTool_LS;
#            1      2
#     1: message Identifier
#     2: TX nodes
msgSendList = []
for dbcLine in dbcData:
  
  # match
  sendRel = re.match('^\s*BO_TX_BU_\s*(.+)', dbcLine)
  
  if sendRel:
  
    relData = sendRel.group(1).split()
    
    #strip trailing semicolon
    nodeList = relData[2][:-1]
    
    if set(CSM_NODES).intersection(nodeList.split(',')):
      msgSendList.append( int(relData[0]) )

# Message:
# BO_ 2154217472 Car_Sharing_Revok_Cert_Req1_LS: 8 ONSTAR_LS
#          1                   2                 3    4
#     1: "Identifier"
#     2: "msgname"
#     3: "length"
#     4: TX node
 
# Signal:
# SG_ EUSSE_ITEgUgScrMnScVal : 3|7@0- (5,0) [-320|315] ""  CSM_LS,RadioHead_LS,CGM_LS
#            1                 2 3 45  6 7     8   9    10  11 
#     1: short name             
#     2: "startbit"  (bits numbered: 7 6 5 4 3 2 1 0 | 15 14 13 12 11 10 9 8 | ... 
#     3: "bitlength"
#     4: "byteorder" (0: Motorola ("MOST-SIGNIFICANT-BYTE-FIRST"), 1: Intel ("LEAST-SIGNIFICANT-BYTE-FIRST"))
#     5: -: signed, +: unsigned
#     6: "factor",  7: "offset"   sig value = (raw * factor) + offset
#     8: "min",     9: "max"   (in engineering units)
#    10: units
#    11: RX nodes

# loop again, looking for messages TX by CSM, or messages containing signals received by CSM
sigCount = 1
dbcIter = iter(dbcData)
for dbcLine in dbcIter:

  msgTx = False
  msgRx = False
  tempMsg = {}
  
  # match BO_ for message
  msgLine = re.match('^\s*BO_\s+(.+)', dbcLine)
  
  if msgLine:
  
    msgData = msgLine.group(1).split()
    
    msgName = msgData[1][:-1]
    
    tempMsg['Identifier'] = int(msgData[0])
    tempMsg['msgname'] = msgName
    tempMsg['length'] = msgData[2]
    tempMsg['signals'] = []
    
    # check if message is in ignore list
    if MESSAGE_IGNORE_LIST != '' and re.match(MESSAGE_IGNORE_LIST, msgName):
      print( 'IGNORED ' + msgName )
      continue
    
    
    # check if message is in Sent messages list, or if CSM is marked as a sender of this message
    if (tempMsg['Identifier'] in msgSendList) or set(CSM_NODES).intersection(msgData[3].split(',')):
      msgTx = True
      tempMsg['Tx'] = 1
    
    # update the extended IDs
    if tempMsg['Identifier'] > 0x80000000:
      tempMsg['Identifier'] = tempMsg['Identifier'] - 0x70000000
    
    while True:
      sigLine = re.match('^\s*SG_\s+(.+)(".*")\s*(.+)', next(dbcIter) )
      if not sigLine:
        break
    
      sigData = sigLine.group(1).split()
      
      # check if this is a Tx message, or if the signal is explicitly marked as RX by CSM
      if msgTx or set(CSM_NODES).intersection(sigLine.group(3).split(',')):
         
         sigName = sigData[0]
         
         #check if signal is in ignore list
         if SIGNAL_IGNORE_LIST == '' or not re.match(SIGNAL_IGNORE_LIST, sigName):
            if set(CSM_NODES).intersection(sigLine.group(3).split(',')):
              tempMsg['Rx'] = 1
              msgRx = True
            
            # name
            Signals[sigName] = {}
            Signals[sigName]['name'] = sigName
            Signals[sigName]['longname'] = sigName
            
            Signals[sigName]['dbc_sig_id'] = sigCount
            sigCount = sigCount + 1
            
            tempMsg['signals'].append(sigName)
            
            # startbit, bitlength, byteorder
            sigInfo = re.split('[\|@]', sigData[2])
            Signals[sigName]['startbit'] = int(sigInfo[0])
            Signals[sigName]['bitlength'] = int(sigInfo[1])
            if sigInfo[2][0] == '0':
              Signals[sigName]['byteorder'] = 'MOST-SIGNIFICANT-BYTE-FIRST'
            else:
              Signals[sigName]['byteorder'] = 'LEAST-SIGNIFICANT-BYTE-FIRST'
            if sigInfo[2][1] == '-':
              Signals[sigName]['valuetype'] = 'signed'
            else:
              Signals[sigName]['valuetype'] = 'unsigned'
            
            # check for enumerations
            if sigName in ValueTables:
               Signals[sigName]['enumerations'] = ValueTables[sigName]
            
            else:
               # factor, offset
               factor = re.match('\((.+)\,(.+)\)', sigData[3])
               Signals[sigName]['factor'] = num(factor.group(1))
               Signals[sigName]['offset'] = num(factor.group(2))
               
               #min, max
               minmax = re.match('\[(.+)\|(.+)\]', sigData[4])
               Signals[sigName]['min'] = num(minmax.group(1))
               Signals[sigName]['max'] = num(minmax.group(2))
            
            # units
            units = sigLine.group(2).strip('"')
            if units:
              Signals[sigName]['units'] = units

    if (msgTx or msgRx) and len(tempMsg['signals']):
      Messages[tempMsg['msgname']] = tempMsg
 

# Signal Long Names
# BA_ "SignalLongName" SG_ 2158919680 ODDMI_DataID2 "Open Display Interface Dynamic Data Multi Request IPC : Data ID Two";
#                              1           2                 3
#     1: message Identifier
#     2: signal short name
#     3: "longname"
sigLongNames = {}
for dbcLine in dbcData:
  
  # match
  sigData = re.match('^\s*BA_\s*"SignalLongName"\s*SG_\s*(.+)', dbcLine)
  
  if sigData:
  
    relData = sigData.group(1).split(' ',2)
    
    shortName = relData[1]
    longName = relData[2]
    
    if shortName in Signals:
      Signals[shortName]['longname'] = longName[1:-2]

      
# Signal Long Names
# BA_ "SignalType" SG_ 2151194624 TrlrThftIndOn "BLN";
#                           1           2         3
#     1: message Identifier
#     2: signal short name
#     3: "datatype"
sigLongNames = {}
for dbcLine in dbcData:
  
  # match
  sigData = re.match('^\s*BA_\s*"SignalType"\s*SG_\s*(.+)', dbcLine)
  
  if sigData:
  
    relData = sigData.group(1).split(' ',2)
    
    shortName = relData[1]
    datatype = relData[2]
    
    if shortName in Signals:
      Signals[shortName]['datatype'] = datatype[1:-2]
      
      
# Signal Groups
# SIG_GROUP_ 2154651648 TeleAudCtl 1 : TAC_AudConctOutcm TAC_AudChConctStat;
#                 1         2      3           4
#     1: message Identifier
#     2: signal group name
#     3: I don't know
#     4: member signals
for dbcLine in dbcData:
  
  # match
  sigData = re.match('^\s*SIG_GROUP_\s(.+)', dbcLine)
  
  if sigData:
    relData = sigData.group(1).split(' ',4)
  
    #strip trailing semicolon from signal list and split into array of signal names
    sigList = relData[4][:-1].split(' ')
    
    if set(sigList).intersection(Signals):
    
      groupName = relData[1]
    
      SignalGroups[groupName] = {}
      SignalGroups[groupName]['signals'] = []
      
      # add the names of signals (only ones rx/tx by CSM)
      for signal in sigList:
        if signal in Signals:
          SignalGroups[groupName]['signals'].append(signal)

# # Create Signal Groups for Signal + Validity or mask combination
# for signal in Signals:
  # if (signal + 'V') in Signals or (signal + 'M') in Signals:
    # groupName = signal + 'Group'
    
    # SignalGroups[groupName] = {}
    # SignalGroups[groupName]['signals'] = []
    
    # SignalGroups[groupName]['signals'].append(signal)
    # if (signal + 'V') in Signals:
      # SignalGroups[groupName]['signals'].append(signal + 'V')
    # if (signal + 'M') in Signals:
      # SignalGroups[groupName]['signals'].append(signal + 'M')
  
  # # virtual device availabilty + VDM
  # if signal[-3:] == 'VDA' and (signal[:-3] + 'VDM') in Signals:
    # groupName = signal[:-3] + 'Group'
    
    # SignalGroups[groupName] = {}
    # SignalGroups[groupName]['signals'] = []
    
    # SignalGroups[groupName]['signals'].append(signal)
    # SignalGroups[groupName]['signals'].append(signal[:-3] + 'VDM')

# write the file
fileData = {}

fileData['messages'] = Messages
fileData['signalGroups'] = SignalGroups
fileData['signals'] = Signals

JSONFilePtr = open(JSON_FILENAME, "w")

JSONFilePtr.write("%s" % json.dumps(fileData, sort_keys=True, indent=4))

print( "DBC Export complete: " + str(len(Signals)) + " Signals, " + str(len(Messages)) + " Messages, "  + str(len(SignalGroups)) + " Signal Groups" )


JSONFilePtr.close()