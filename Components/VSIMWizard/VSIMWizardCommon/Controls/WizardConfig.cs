﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfCommon.Controls
{
	/// <summary>
	/// Represents the wizard configuration object.
	/// </summary>
	public class WizardConfig : Notifiable
	{
		#region fields

		bool showNavPanel = true;
		bool showContentBanner = true;
		bool showReset = true;
		bool showPrev = true;
		bool showNext = true;
		bool showFinish = true;
		bool showCancel = true;
		bool showBannerImage = false;
		bool showPage1OfN = false;
		double navListWidth = 150;
		double contentBannerHeight = 40;
#if __DEMO__
		string                            bannerBackgroundColorName  = "LightSteelBlue";
		string                            bannerBorderColorName      = "SteelBlue";
		string                            bannerTitleColorName       = "Black";
		string                            bannerSubtitleColorName    = "Black";
#else
		Color bannerBackgroundColor = Colors.LightSteelBlue;
		Color bannerBorderColor = Colors.SteelBlue;
		Color bannerTitleColor = Colors.White;
		Color bannerSubtitleColor = Colors.Black;
#endif
		double bannerBorderThicknessValue = 2d;
		ObservableCollection<WizardPageBase> pages = null;
		HorizontalAlignment bannerTextAlignment = HorizontalAlignment.Left;
		HorizontalAlignment bannerImageAlignment = HorizontalAlignment.Right;
		string bannerImageFilePath = string.Empty;

		#endregion fields

		#region properties

		/// <summary>
		/// Gets or sets a value indicating whether [show nav panel].
		/// </summary>
		public bool ShowNavPanel
		{
			get { return this.showNavPanel; }
			set
			{
				if (value != this.showNavPanel)
				{
					this.showNavPanel = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show content banner].
		/// </summary>
		public bool ShowContentBanner
		{
			get { return this.showContentBanner; }
			set
			{
				if (value != this.showContentBanner)
				{
					this.showContentBanner = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show reset button].
		/// </summary>
		public bool ShowResetButton
		{
			get { return this.showReset; }
			set
			{
				if (value != this.showReset)
				{
					this.showReset = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show previous button].
		/// </summary>
		public bool ShowPrevButton
		{
			get { return this.showPrev; }
			set
			{
				if (value != this.showPrev)
				{
					this.showPrev = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show next button].
		/// </summary>
		public bool ShowNextButton
		{
			get { return this.showNext; }
			set
			{
				if (value != this.showNext)
				{
					this.showNext = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show finish button].
		/// </summary>
		public bool ShowFinishButton
		{
			get { return this.showFinish; }
			set
			{
				if (value != this.showFinish)
				{
					this.showFinish = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show cancel button].
		/// </summary>
		public bool ShowCancelButton
		{
			get { return this.showCancel; }
			set
			{
				if (value != this.showCancel)
				{
					this.showCancel = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show cancel button].
		/// </summary>
		public bool ShowPage1OfN
		{
			get { return this.showPage1OfN; }
			set
			{
				if (value != this.showPage1OfN)
				{
					this.showPage1OfN = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the width of the nav list.
		/// </summary>
		public double NavListWidth
		{
			get { return this.navListWidth; }
			set
			{
				if (value != this.navListWidth)
				{
					this.navListWidth = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the height of the content banner.
		/// </summary>
		public double ContentBannerHeight
		{
			get { return this.contentBannerHeight; }
			set
			{
				if (value != this.contentBannerHeight)
				{
					this.contentBannerHeight = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the pages.
		/// </summary>
		public ObservableCollection<WizardPageBase> Pages
		{
			get { return this.pages; }
			set
			{
				if (value != this.pages)
				{
					this.pages = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets/sets the banner image alignment.
		/// </summary>
		public HorizontalAlignment BannerImageAlignment
		{
			get { return this.bannerImageAlignment; }
			set
			{
				if (value != this.bannerImageAlignment)
				{
					this.bannerImageAlignment = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets/sets the banner text alignment.
		/// </summary>
		public HorizontalAlignment BannerTextAlignment
		{
			get { return this.bannerTextAlignment; }
			set
			{
				if (value != this.bannerTextAlignment)
				{
					this.bannerTextAlignment = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets/sets the banner image path
		/// </summary>
		public string BannerImageFilePath
		{
			get { return this.bannerImageFilePath; }
			set
			{
				if (value != this.bannerImageFilePath)
				{
					this.bannerImageFilePath = value;
					this.NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets/sets the flag that indicates whether the banner image should be displayed
		/// </summary>
		public bool ShowBannerImage
		{
			//get { return (this.showBannerImage && !string.IsNullOrEmpty(this.BannerImageFilePath)); } 
			get { return this.showBannerImage; }
			set
			{
				if (value != this.showBannerImage)
				{
					this.showBannerImage = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("BannerImage");
				}
			}
		}

		/// <summary>
		/// Get the image to use as a source for the banner.
		/// </summary>
		public BitmapImage BannerImage
		{
			get
			{
				BitmapImage value = null;
				if (this.ShowBannerImage)
				{
					value = new BitmapImage(new Uri(this.BannerImageFilePath, UriKind.RelativeOrAbsolute));
				}
				return value;
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners background
		/// </summary>
		public SolidColorBrush BannerBackgroundBrush
		{
#if __DEMO__
			get { return new SolidColorBrush(this.NameToColor(this.bannerBackgroundColorName)); }
#else
			get { return new SolidColorBrush(this.BannerBackgroundColor); }
#endif
		}

		/// <summary>
		/// Gets/sets the color of the banners border line
		/// </summary>
		public SolidColorBrush BannerBorderBrush
		{
#if __DEMO__
			get { return new SolidColorBrush(this.NameToColor(this.bannerBorderColorName)); }
#else
			get { return new SolidColorBrush(this.BannerBorderColor); }
#endif
		}

		/// <summary>
		/// Gets/sets the color of the banners title text
		/// </summary>
		public SolidColorBrush BannerTitleBrush
		{
#if __DEMO__
			get { return new SolidColorBrush(this.NameToColor(this.bannerTitleColorName)); }
#else
			get { return new SolidColorBrush(this.BannerTitleColor); }
#endif
		}

		/// <summary>
		/// Gets/sets the color of the banners subtitle text
		/// </summary>
		public SolidColorBrush BannerSubtitleBrush
		{
			
				get { return new SolidColorBrush(this.BannerSubtitleColor); }

		}

#if __DEMO__
		/// <summary>
		/// Gets/sets the color of the banners background
		/// </summary>
		public string BannerBackgroundColorName
		{
			get { return this.bannerBackgroundColorName; }
			set
			{
				if (value != this.bannerBackgroundColorName)
				{
					this.bannerBackgroundColorName = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("ColorBannerBackground");
				}
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners border line
		/// </summary>
		public string BannerBorderColorName
		{
			get { return this.bannerBorderColorName; }
			set
			{
				if (value != this.bannerBorderColorName)
				{
					this.bannerBorderColorName = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("ColorBannerBorder");
				}
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners title text
		/// </summary>
		public string BannerTitleColorName
		{
			get { return this.bannerTitleColorName; }
			set
			{
				if (value != this.bannerTitleColorName)
				{
					this.bannerTitleColorName = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("ColorBannerTitle");
				}
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners subtitle text
		/// </summary>
		public string BannerSubtitleColorName
		{
			get { return this.bannerSubtitleColorName; }
			set
			{
				if (value != this.bannerSubtitleColorName)
				{
					this.bannerSubtitleColorName = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("ColorBannerSubtitle");
				}
			}
		}
#else
		/// <summary>
		/// Gets/sets the color of the banners background
		/// </summary>
		public Color BannerBackgroundColor
		{
			get { return this.bannerBackgroundColor; }
			set
			{
				if (value != this.bannerBackgroundColor)
				{
					this.bannerBackgroundColor = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("BannerBackgroundBrush");
				}
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners border line
		/// </summary>
		public Color BannerBorderColor
		{
			get { return this.bannerBorderColor; }
			set
			{
				if (value != this.bannerBorderColor)
				{
					this.bannerBorderColor = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("BannerBorderBrush");
				}
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners title text
		/// </summary>
		public Color BannerTitleColor
		{
			get { return this.bannerTitleColor; }
			set
			{
				if (value != this.bannerTitleColor)
				{
					this.bannerTitleColor = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("BannerTitleBrush");
				}
			}
		}

		/// <summary>
		/// Gets/sets the color of the banners subtitle text
		/// </summary>
		public Color BannerSubtitleColor
		{
			get { return this.bannerSubtitleColor; }
			set
			{
				if (value != this.bannerSubtitleColor)
				{
					this.bannerSubtitleColor = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("BannerSubtitleBrush");
				}
			}
		}

#endif

		/// <summary>
		/// Gets/sets the thickness of the border on the banner and button nav panels
		/// </summary>
		public double BannerBorderThicknessValue
		{
			get { return this.bannerBorderThicknessValue; }
			set
			{
				if (value != this.bannerBorderThicknessValue)
				{
					this.bannerBorderThicknessValue = value;
					this.NotifyPropertyChanged();
					this.NotifyPropertyChanged("BannerBorderThickness");
					this.NotifyPropertyChanged("NavPanelBorderThickness");
				}
			}
		}

		/// <summary>
		/// Gets the actual Thickness object for the banner based on the BorderBannerThicknessValue
		/// </summary>
		public Thickness BannerBorderThickness
		{
			get { return new Thickness(0, 0, 0, this.bannerBorderThicknessValue); }
		}

		/// <summary>
		/// Gets the actual Thickness object for the banner based on the BorderBannerThicknessValue
		/// </summary>
		public Thickness NavPanelBorderThickness
		{
			get { return new Thickness(0, this.bannerBorderThicknessValue, 0, 0); }
		}

		#endregion properties

		#region constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="WizardConfig"/> class.
		/// </summary>
		public WizardConfig()
		{
		}

		#endregion constructor

#if __DEMO__
		private Color NameToColor(string name)
		{
			Color color = (Color)(ColorConverter.ConvertFromString(name));
			return color;
		}
#endif

	}
}
