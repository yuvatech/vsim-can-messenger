﻿using System.Collections.ObjectModel;
using System.Linq;
using ObjectExtensions;

namespace WpfCommon.Controls
{

	/// <summary>
	/// 
	/// </summary>
	public class WizardSharedData : ObservableCollection<WizardSharedDataItem>
	{
		/// <summary>
		/// Checks to see if a WizSharedDataItem item with the specified case-sensitive name exists 
		/// in this collection.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool Contains(string key)
		{
			bool result = true;
			var found = this.Where(x=>x.Name == key).FirstOrDefault();
			result = (found != null);
			return result;
		}

		/// <summary>
		/// Attempts to add or update the specified item. 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <returns>The item that was added or updated.</returns>
		/// <remarks>It would be wise to check the returned item's UpdateStatus property to make sure it updated.</remarks>
		public WizardSharedDataItem AddUpdateItem(string name, object value)
		{
			WizardSharedDataItem found = this.Where(x=>x.Name == name).FirstOrDefault();
			if (found == null)
			{
				found = new WizardSharedDataItem(){Name=name, Value=value};
				this.Add(found);
			}
			else
			{
				found.Update(value);
			}
			return found;
		}

		/// <summary>
		/// Gets the specified item.
		/// </summary>
		/// <param name="name">The case-sensitive name.</param>
		/// <returns></returns>
		public WizardSharedDataItem GetItem(string name)
		{
			return this.Where(x=>x.Name == name).FirstOrDefault();
		}

		/// <summary>
		/// Removes the item from the collection.
		/// </summary>
		/// <param name="name"></param>
		public void DeleteItem(string name)
		{
			if (this.Contains(name))
			{
				WizardSharedDataItem item = this.GetItem(name);
				this.Remove(item);
				item = null;
			}
		}
	}
}
