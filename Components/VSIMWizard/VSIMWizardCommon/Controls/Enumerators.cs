﻿// no usings needed

namespace WpfCommon.Controls
{
	/// <summary>
	/// Enumerator that indicates the status of the last attempted pdate on a given 
	/// WizSharedDataItem object.
	/// </summary>
	public enum SharedDataUpdateResult { OriginalData, Updated, ValuesAreEqual, WrongType };
}
