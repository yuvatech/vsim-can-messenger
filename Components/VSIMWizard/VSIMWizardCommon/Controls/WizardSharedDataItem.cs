﻿using ObjectExtensions;

namespace WpfCommon.Controls
{
	/// <summary>
	/// 
	/// </summary>
	public class WizardSharedDataItem
	{
		/// <summary>
		/// Gets/sets the result of the last update attempt
		/// </summary>
		SharedDataUpdateResult UpdateResult { get; set; }
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public object Value { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public WizardSharedDataItem()
		{
			UpdateResult = SharedDataUpdateResult.OriginalData;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		public WizardSharedDataItem(string name, object value)
		{
			this.Name         = name;
			this.Value        = value;
			this.UpdateResult = SharedDataUpdateResult.OriginalData;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		/// <remarks>If the new value is not the same type as the existing this.Value, the value will not be updated. </remarks>
		public void Update(object value)
		{
			if (value.GetType() == this.Value.GetType())
			{
				if (!this.Value.CompareEquals(value))
				{
					this.Value = value;
					this.UpdateResult = SharedDataUpdateResult.Updated;
				}
				else
				{
					this.UpdateResult = SharedDataUpdateResult.ValuesAreEqual;
				}
			}
			else
			{
				this.UpdateResult = SharedDataUpdateResult.WrongType;
			}
		}
	}

}
