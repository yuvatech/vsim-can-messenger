﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfCommon.Controls
{
	/// <summary>
	/// 
	/// </summary>
	public interface IWizardPage
	{
		/// <summary>
		/// Gets or sets the instance of the parent wizard control.
		/// </summary>
		CtrlWizard ParentWiz { get; set; }

		/// <summary>
		/// Gets or sets the name of the page.
		/// </summary>
		string PageName { get; set; }

		/// <summary>
		/// Get/set the page's short name (used for conditional navigation with the next/prev button)
		/// </summary>
		string ShortName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		string Subtitle { get; set; }

		/// <summary>
		/// Performs handling on the currently active wizard page before the button 
		/// click is handled by the hosting wizard control.
		/// </summary>
		void ButtonClick(object sender, ref RoutedEventArgs e);

		/// <summary>
		/// Updates the wizard controls buttons from the active wizard page.
		/// </summary>
		void UpdateButtons();

		/// <summary>
		/// Resets this instance.
		/// </summary>
		void Reset();

		string NextPage { get; set; }

		string PrevPage { get; set; }
	}
}
