﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WpfCommon.Controls
{
	public class WpfColor
	{
		public string Name { get; set; }
		public SolidColorBrush Brush { get; set; }
	}
}
