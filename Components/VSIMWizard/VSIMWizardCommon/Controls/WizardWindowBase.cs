﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfCommon.Controls
{
    /// <summary>
    /// Interaction logic for WndWizard.xaml
    /// </summary>
    public abstract partial class WizardWindowBase : UserControl
    {
        protected bool isInitialized = false;

        protected Button wizBtnFinish;
        protected Button wizBtnCancel;
        protected Button wizBtnReset;
        protected Button wizBtnNext;
        protected Button wizBtnPrevious;

        #region properties

        public bool IsValid { get { return this.Wizard.IsValid; } }

        /// <summary>
        /// Gets or sets the wizard pages.
        /// </summary>
        protected ObservableCollection<WizardPageBase> Pages { get; set; }

        /// <summary>
        /// Gets or sets the wizard control.
        /// </summary>
        protected CtrlWizard Wizard { get; set; }
        protected WizardConfig WizConfig { get; set; }

        #endregion properties

        public WizardWindowBase()
        {
            this.Pages = new ObservableCollection<WizardPageBase>();
            this.WizConfig = new WizardConfig();
            //this.InitWizard();
        }

        protected abstract void InitWizard(WizardConfig config = null);
        /*
                // sample InitWizard body
                {
                    if (!this.isInitialized)
                    {
                        MyWizPage1 page1 = new MyWizPage1();
                        MyWizPage2 page2 = new MyWizPage2();
                        MyWizPage3 page3 = new MyWizPage3();
                        this.Pages.Add(page1);
                        this.Pages.Add(page2);
                        this.Pages.Add(page3);

                        // add the pages to the wizard control, and setup event hooks
                        this.ConfigureWizard(myContainerGrid);

                        // at this point, you can configure the WizardConfig object to setup general UI 
                        // appeaarance regarding header panel, buttons, and the page select listbox. Here 
                        // are the available properties
                        WizConfig.ShowContentBanner   = true;
                        WizConfig.ShowFinishButton    = true;
                        WizConfig.ShowCancelButton    = true;
                        WizConfig.ShowNavPanel        = true;
                        WizConfig.ShowNextButton      = true;
                        WizConfig.ShowPrevButton      = true;
                        WizConfig.ShowResetButton     = true;
                        WizConfig.ContentBannerHeight = 36;
                        WizConfig.NavListWidth        = 200;

                        // If you want to set a color with the #xxxxxx format, use the following line. Otherwise 
                        // you can just use the Colors enumerator to select the desired color. The two examples 
                        // below show each color being set to black.
                        WizConfig.ColorBannerTitle    = (Color)(ColorConverter.ConvertFromString("#000000"));
                        WizConfig.ColorBannerSubtitle = Colors.Black;

                        // add shared data - this is essentially an observable collection of key/value pairs, 
                        // where the key is a string that represents the name, and the value is any object
                        this.Wizard.SharedData.Add(new WizSharedDataItem("ItemName", myObject));
                    }
                }
        */

        /// <summary>
        /// Adds the defined pages to the config object, and sets the button objects as properties 
        /// so they're easier to work with (versus grabbing them each time from their parent 
        /// containers).
        /// </summary>
        /// <param name="grid"></param>
        protected virtual void ConfigureWizard(Grid grid)
        {
            this.WizConfig.Pages = this.Pages;

            this.Wizard = new CtrlWizard(this.WizConfig);

            this.wizBtnFinish = (Button)(this.Wizard.finishButtonPanel.Children[0]);
            this.wizBtnCancel = (Button)(this.Wizard.cancelButtonPanel.Children[0]);
            this.wizBtnReset = (Button)(this.Wizard.resetButtonPanel.Children[0]);
            this.wizBtnNext = (Button)(this.Wizard.nextButtonPanel.Children[0]);
            this.wizBtnPrevious = (Button)(this.Wizard.prevButtonPanel.Children[0]);

            this.SetupEvents();
            this.AddToUI(grid);

            this.isInitialized = true;
        }

        protected virtual void AddToUI(Grid grid)
        {
            foreach (WizardPageBase page in this.Wizard.Pages)
            {
                page.ParentWiz = this.Wizard;
            }
            grid.Children.Add(this.Wizard);
        }

        protected virtual void SetupEvents()
        {
            // add the pages to the visual control (next and prev are handled by the wizard 
            // control itself)
            this.wizBtnFinish.Click += this.WizFinish_Click;
            this.wizBtnCancel.Click += this.WizCancel_Click;
            this.wizBtnReset.Click += this.WizReset_Click;
            //this.Wizard.UpdateButtons();

        }

        #region UI event handlers

        /// <summary>
        /// Handles the Click event of the Reset button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void WizReset_Click(object sender, RoutedEventArgs e)
        {
            // perform whatever processing is reuired for your wizard in your derived window class
        }

        /// <summary>
        /// Handles the Click event of the Finish button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void WizFinish_Click(object sender, RoutedEventArgs e)
        {
            // this.DialogResult = true;
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Handles the Click event of the Cancel button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void WizCancel_Click(object sender, RoutedEventArgs e)
        {
            // this.DialogResult = false;
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Handles the Closing event of this Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        protected virtual void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.wizBtnFinish.Click -= this.WizFinish_Click;
            this.wizBtnCancel.Click -= this.WizCancel_Click;
            this.wizBtnReset.Click -= this.WizReset_Click;
        }

        #endregion UI event handlers
    }
}
