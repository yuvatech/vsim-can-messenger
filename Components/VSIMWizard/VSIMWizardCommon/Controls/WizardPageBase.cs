﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfCommon.Controls
{
	public abstract class WizardPageBase : UserControl, INotifyPropertyChanged
	{
		#region INotifyPropertyChanged

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

#if _NET_45_
		/// <summary>
		/// Notifies that the property changed, and sets IsModified to true.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
#else
		/// <summary>
		/// Notifies that the property changed, and sets IsModified to true.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
        protected virtual void NotifyPropertyChanged(String propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
#endif
		#endregion INotifyPropertyChanged

		/// <summary>
		/// Gets or sets the instance of the parent wizard control.
		/// </summary>
		public CtrlWizard ParentWiz           { get; set; }

		/// <summary>
		/// Gets or sets the name of the page.
		/// </summary>
		public string     PageName            { get; set; }

		public string     ShortName           { get; set; }

		public string     Subtitle            { get; set; }

		public Window     ParentWindow        { get { return this.ParentWiz.ParentWindow; } }

		public string     Title               { get; set; }

		/// <summary>
		/// Gets a value indicating whether or not this page is ready to handle UI events (the page 
		/// has to be visible, and the parent wizard cannot be null).
		/// </summary>
		protected bool IsReady { get { return (this.IsVisible && this.ParentWiz != null); } }
 
		public WizardSharedData SharedData 
		{ 
			get 
			{ 
				return (this.ParentWiz == null) ? null : this.ParentWiz.SharedData; 
			} 
		}

		public WizardPageBase()
		{
		}

		public WizardPageBase(string pageName, string shortName, string subtitle)
		{
			if (string.IsNullOrEmpty(pageName.Trim()))
			{
				throw new ArgumentNullException("The pageName must be specified.");
			}
			if (string.IsNullOrEmpty(shortName.Trim()))
			{
				throw new ArgumentNullException("The shortName must be specified.");
			}
			this.PageName  = pageName;
			this.ShortName = shortName;
			this.Subtitle  = subtitle;
			this.IsVisibleChanged += UserControl_IsVisibleChanged;
		}

		~WizardPageBase()
		{
			this.IsVisibleChanged -= UserControl_IsVisibleChanged;
		}

		#region Required wizard methods

		/// <summary>
		/// Updates the buttons on the parent Wizard control. You only need to populate this 
		/// method if you want to override what the parent wizard control does. Refer to the 
		/// CtrlWizard  class to see default button settings.
		/// </summary>
		public virtual void UpdateButtons()
		{
		}

		/// <summary>
		/// Interface method. Resets this data on this page (if necessary).
		/// </summary>
		public virtual void Reset()
		{
		}

		#endregion Required wizard methods

		/// <summary>
		/// Performs handling before the button click is handled by the hosting control.
		/// </summary>
		/// <param name="sender">The sender (button).</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		public virtual void ButtonClick(object sender, ref RoutedEventArgs e)
		{
/*			// If you need special handling of button clicks, use this code as an example of handling 
            // button clicks in your derived pages 
			if (sender is Button)
			{
				Button button = sender as Button;
				switch (button.Name)
				{
					case "btnNext" : break;
					case "btnPrev" : break;
					case "btnReset" : break;
					case "btnFinish" : break;
					case "btnCancel" : break;
				}
				e.Handled = false;
			}
			else
			{
			}
 */
		}

		/// <summary>
		/// Common code performed when the wizard page visibility changed
		/// </summary>
		protected abstract void OnVisibleChanged();
		//{
		//	throw new NotImplementedException("You must override this method in your derived class.");
		//}

		/// <summary>
		/// Handles the IsVisibleChanged event of the UserControl control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
		protected virtual void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			this.OnVisibleChanged();
		}

		public virtual string PrevPage { get; set; }

		public virtual string NextPage { get; set; }

		//protected void SetPageName()
		//{
		//	if (string.IsNullOrEmpty(this.PageName))
		//	{
		//		if (this.Tag != null)
		//		{
		//			this.PageName = (string)(this.Tag);
		//		}
		//		else
		//		{
		//			this.PageName = Guid.NewGuid().ToString("N");
		//		}
		//	}
		//}
	}
}
