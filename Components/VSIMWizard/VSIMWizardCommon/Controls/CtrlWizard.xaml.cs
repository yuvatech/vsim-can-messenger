﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ObjectExtensions;

namespace WpfCommon.Controls
{
	// The navigation panel is only viable on a wizard that has no restrictions on the Next/Finish 
	// buttons. This is controlled from the Window containing the wizard control.

	/// <summary>
	/// Interaction logic for UserControl1.xaml
	/// </summary>
	public partial class CtrlWizard : UserControl, INotifyPropertyChanged
	{
		#region INotifyPropertyChanged

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Notifies that the property changed, and sets IsModified to true.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

		#endregion INotifyPropertyChanged

		#region fields

		private int              pageCount        = 0;
		private int              activePageIndex  = 0;
		private int              activePageNumber = 0;
		private string           activePageName   = string.Empty;
		private WizardSharedData sharedData       = null;
		private WizardConfig     wizConfig        = null;

		#endregion fields

		#region properties

		public bool IsValid 
		{
			get
			{
				bool value = (this.SharedData != null);
				value &= (this.WizConfig != null);
				value &= (this.WizConfig.Pages.Count > 0);
				value &= (this.PagesAreDistinct());
				return value;
			}
		}

		/// <summary>
		/// Gets or sets the page count.
		/// </summary>
		public int PageCount 
		{ 
			get { return this.pageCount; }
			set
			{
				if (value != this.pageCount)
				{
					this.pageCount = value;
				}
				this.NotifyPropertyChanged("PageCount");
			}
		}

		/// <summary>
		/// Gets or sets the index of the active page.
		/// </summary>
		public int ActivePageIndex
		{
			get { return this.activePageIndex; }
			set
			{
				if (value != this.activePageIndex)
				{
					this.activePageIndex = value;
					this.ActivePageNumber = value + 1;
				}
				this.NotifyPropertyChanged("ActivePageIndex");
			}
		}

		/// <summary>
		/// Gets or sets the active page number.
		/// </summary>
		public int ActivePageNumber
		{
			get { return this.activePageNumber; }
			set
			{
				if (value != this.activePageNumber)
				{
					this.activePageNumber = value;
				}
				this.NotifyPropertyChanged("ActivePageNumber");
			}
		}

		/// <summary>
		/// Gets or sets the name of the active page.
		/// </summary>
		public string ActivePageName
		{
			get { return this.activePageName; }
			set
			{
				if (value != this.activePageName)
				{
					this.activePageName = value;
				}
				this.NotifyPropertyChanged("ActivePageName");
			}
		}

		/// <summary>
		/// Gets or sets the active page.
		/// </summary>
		public WizardPageBase ActivePage { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance can enable finish button.
		/// </summary>
		public bool CanEnableFinishButton { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance has children.
		/// </summary>
		public bool HasChildren { get { return (this.pageCount > 0); } }

		/// <summary>
		/// Gets or sets the pages.
		/// </summary>
		public ObservableCollection<WizardPageBase> Pages { get; set; }

		/// <summary>
		/// Data shared between wizard pages
		/// </summary>
		public WizardSharedData SharedData 
		{ 
			get { return this.sharedData; }
			set
			{
				if (value != this.sharedData)
				{
					this.sharedData = value;
					this.NotifyPropertyChanged("SharedData");
				}
			}
		}

		/// <summary>
		/// Gets/sets the wizard configuration object
		/// </summary>
		public WizardConfig WizConfig
		{
			get { return this.wizConfig; }
			set
			{
				if (value != this.wizConfig)
				{
					this.wizConfig = value;
					this.NotifyPropertyChanged("WizConfig");
				}
			}
		}

		/// <summary>
		/// Gets the current page's subtitle
		/// </summary>
		public string PageSubtitle 
		{ 
			get 
			{ 
				return (this.ActivePage == null) ? string.Empty : this.ActivePage.Subtitle; 
			} 
		}

		/// <summary>
		/// Gets the visibility of the current page's subtitle based on whether or not is is 
		/// null/empty. If there's no text, the element is collapsed.
		/// </summary>
		public Visibility PageSubtitleVisibility 
		{ 
			get 
			{ 
				return (this.ActivePage == null) ? Visibility.Collapsed : (string.IsNullOrEmpty(this.ActivePage.Subtitle) ? Visibility.Collapsed : Visibility.Visible); 
			} 
		}
		
		/// <summary>
		/// Gets the parent window for this control.
		/// </summary>
		public Window ParentWindow
		{
			get { return WpfCommon.Globals.FindParent<Window>(this); } 
		}

		#endregion properties

		#region constructor

		/// <summary>
		/// Constructor for the designer
		/// </summary>
		public CtrlWizard()
		{
			this.InitializeComponent();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CtrlWizard"/> class.
		/// </summary>
		/// <param name="config">The configuration object used to initialize the wizard.</param>
		/// <exception cref="System.ArgumentNullException">config</exception>
		/// <exception cref="System.InvalidOperationException">The WizardConfig.Pages collection cannot be null or empty.</exception>
		public CtrlWizard(WizardConfig config)
		{
			// sanity checks
			if (config == null)
			{
				throw new ArgumentNullException("config");
			}
			if (config.Pages == null || config.Pages.Count == 0)
			{
				throw new InvalidOperationException("The WizardConfig.Pages collection cannot be null or empty.");
			}

			// set the active page to -1 (no page is active)
			this.ActivePageIndex = -1;
			this.ActivePage      = null;

			this.WizConfig       = config;
			this.SharedData      = new WizardSharedData();

			this.InitializeComponent();
			this.DataContext     = this;

			// Initialize the wizard with the specified config object. This MUST happen AFTER 
			// calling InitializeComponent()
			this.InitWithConfig(config);
		}

		#endregion constructor

		#region methods

		/// <summary>
		/// Initializes the wizard with the specified configuration. 
		/// </summary>
		/// <param name="config">The configuration.</param>
		private void InitWithConfig(WizardConfig config)
		{
			this.CanEnableFinishButton        = config.ShowNavPanel;

			// add the wizard pages to the control, and collapse them
			if (this.Pages == null)
			{
				this.Pages = new ObservableCollection<WizardPageBase>();
				foreach(WizardPageBase page in config.Pages)
				{
					this.Pages.Add(page);
					((UserControl)(page)).Visibility = Visibility.Collapsed;
					page.ParentWiz = this;
					this.gridContentBody.Children.Add(((UserControl)(page)));
				}
				this.PageCount = this.Pages.Count;
				this.SharedData.AddUpdateItem("WizardPages", this.Pages);
			}

			// update the form to show the first page
			this.UpdateForm(0);
		}

		/// <summary>
		/// Updates the wizard control with the newly updated config object.
		/// </summary>
		public void UpdatedConfig()
		{
			this.InitWithConfig(this.WizConfig);
			this.NotifyPropertyChanged("WizConfig");
		}

		/// <summary>
		/// Updates the form in terms of active page and button states.
		/// </summary>
		/// <param name="newPageIndex">New index of the page.</param>
		public void UpdateForm(int newPageIndex)
		{
			if (this.ActivePageIndex != newPageIndex)
			{
				if (this.ActivePage != null )
				{
					((UserControl)(this.ActivePage)).Visibility = Visibility.Collapsed;
				}

				this.ActivePageIndex = newPageIndex;
				this.ActivePage      = this.GetCurrentPage();
				this.ActivePageName  = this.ActivePage.PageName;

				((UserControl)(this.ActivePage)).Visibility = Visibility.Visible;
				this.lbNavigation.SelectedIndex             = this.ActivePageIndex;
				this.NotifyPropertyChanged("PageSubtitle");
				this.NotifyPropertyChanged("PageSubtitleVisibility");
				this.UpdateButtons();
			}
		}

		/// <summary>
		/// This overload allows you to specify the new active page by its short name
		/// </summary>
		/// <param name="shortName"></param>
		public void UpdateForm(string shortName)
		{
			// we can't do anything if the pagename is null/empty
			if (string.IsNullOrEmpty(shortName))
			{
				throw new ArgumentNullException("A shortName was not returned by the current page. Navidation aborted" );
			}

			// Short names MUST be unique - this is sanity checked when the control is configured 
			// with the list of pages.
			WizardPageBase newPage = this.Pages.Where(x=>x.ShortName == shortName).FirstOrDefault();
			// we can't do anything if the named page can't be found
			if (newPage == null)
			{
				throw new Exception(string.Format("Could not find page '{0}'. Navigation aborted."));
			}

			WizardPageBase currentPage = this.GetCurrentPage();
			int index = this.Pages.IndexOf(newPage);
			this.UpdateForm(index);
		}

		/// <summary>
		/// Updates the buttons on the wizard control.
		/// </summary>
		public void UpdateButtons()
		{
			bool isFirstPage  = this.IsFirstPage(this.ActivePage);
			bool isLastPage   = this.IsLastPage(this.ActivePage);
			bool enableFinish = ((this.WizConfig.ShowFinishButton) && (isLastPage || this.CanEnableFinishButton));

			this.btnReset.IsEnabled  = this.WizConfig.ShowResetButton  ? true : false;
			this.btnPrev.IsEnabled   = isFirstPage      ? false : true;
			this.btnNext.IsEnabled   = isLastPage       ? false : true;
			this.btnFinish.IsEnabled = enableFinish;
			this.btnCancel.IsEnabled = this.WizConfig.ShowCancelButton ? true : false;

			this.ActivePage.UpdateButtons();
		}

		/// <summary>
		/// Updates the navigation panel.
		/// </summary>
		protected void UpdateNavigationPanel()
		{
			if (this.gridNavPanel.IsVisible)
			{
			}
		}

		/// <summary>
		/// Determines whether [is last page] [the specified page].
		/// </summary>
		/// <param name="page">The page.</param>
		/// <returns></returns>
		public bool IsLastPage(WizardPageBase page)
		{
			bool result = false;
			WizardPageBase last = this.Pages.Last<WizardPageBase>();
			result = (last.PageName == page.PageName && last.GetType().Name == page.GetType().Name);
			return result;
		}

		/// <summary>
		/// Determines whether [is first page] [the specified page].
		/// </summary>
		/// <param name="page">The page.</param>
		/// <returns></returns>
		public bool IsFirstPage(WizardPageBase page)
		{
			bool result = false;
			WizardPageBase first = this.Pages.First<WizardPageBase>();
			result = (first.PageName == page.PageName);
			return result;
		}

		private WizardPageBase GetCurrentPage()
		{
			WizardPageBase currentPage = null;
			if (this.ActivePageIndex >= 0)
			{
				currentPage = (WizardPageBase)(this.gridContentBody.Children[ActivePageIndex]);
			}
			return currentPage;
		}

		private bool PagesAreDistinct()
		{
			// sanity check for unique page names - case sensitivity is not a factor during this 
			// check because this code wants to promote truly unique names.
			int distinct = this.Pages.DistinctBy(x=>x.ShortName).Count();
			bool result = (this.Pages.Count > 0 && distinct == this.Pages.Count);
			return result;
		}

		#endregion methods

		#region UI event handlers

		/// <summary>
		/// Handles the SelectionChanged event of the lbNavigation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
		private void lbNavigation_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!e.Handled)
			{
				ListBox listbox = sender as ListBox;
				this.UpdateForm(listbox.SelectedIndex);
				e.Handled = true;
			}
		}

		/// <summary>
		/// Handles the Click event of the btnReset control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		private void btnReset_Click(object sender, RoutedEventArgs e)
		{
			this.UpdateForm(0);
		}

		/// <summary>
		/// Handles the Click event of the btnPrev control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		private void btnPrev_Click(object sender, RoutedEventArgs e)
		{
			if (this.HasChildren)
			{
				WizardPageBase currentPage = this.GetCurrentPage();
				currentPage.ButtonClick(sender, ref e); 

				if (!e.Handled)
				{
					string prevPage = currentPage.PrevPage;
					if (string.IsNullOrEmpty(prevPage))
					{
						this.UpdateForm(this.ActivePageIndex - 1);
					}
					else
					{
						this.UpdateForm(prevPage);
					}
					e.Handled = true;
				}
			}
			else
			{
				e.Handled = true;
			}
		}

		/// <summary>
		/// Handles the Click event of the btnNext control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		private void btnNext_Click(object sender, RoutedEventArgs e)
		{
			if (this.HasChildren)
			{
				WizardPageBase currentPage = this.GetCurrentPage();
				currentPage.ButtonClick(sender, ref e);

				if (!e.Handled)
				{
					string nextPage = currentPage.NextPage;
					if (string.IsNullOrEmpty(nextPage))
					{
						this.UpdateForm(this.ActivePageIndex + 1);
						currentPage = this.GetCurrentPage();
						currentPage.PrevPage = string.Empty;
					}
					else
					{
						string prevPage = currentPage.ShortName;
						this.UpdateForm(nextPage);
						currentPage = this.GetCurrentPage();
						// so we can find our way back if they hit the "Prev" button
						currentPage.PrevPage = prevPage;
					}
					e.Handled = true;
				}
				else
				{
					// do we need to add any processing here?
				}
			}
			else
			{
				e.Handled = true;
			}
		}

		/// <summary>
		/// Handles the Click event of the btnFinish control, and closes the parent control IF IT IS A WINDOW.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		private void btnFinish_Click(object sender, RoutedEventArgs e)
		{
			if (this.HasChildren)
			{
				//((WizardPageBase)(this.gridContentBody.Children[ActivePageIndex])).ButtonClick(sender, ref e);
				this.GetCurrentPage().ButtonClick(sender, ref e);
				if (this.Parent is Window)
				{
					((Window)(this.Parent)).Close();
					e.Handled = true;
				}
			}
			else
			{
				e.Handled = true;
			}
		}

		/// <summary>
		/// Handles the Click event of the btnCancel control, and closes the parent control IF IT IS A WINDOW.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			if (this.HasChildren)
			{
				//((WizardPageBase)(this.gridContentBody.Children[ActivePageIndex])).ButtonClick(sender, ref e);
				this.GetCurrentPage().ButtonClick(sender, ref e);

				if (this.Parent is Window)
				{
					((Window)(this.Parent)).Close();
					e.Handled = true;
				}
			}
			else
			{
				e.Handled = true;
			}
		}

		#endregion UI event handlers
	}
}
