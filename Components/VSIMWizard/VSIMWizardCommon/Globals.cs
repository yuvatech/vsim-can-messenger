﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

namespace WpfCommon
{
	/// <summary>
	/// 
	/// </summary>
	public static class Globals
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
		public delegate void RoutedEventAction(object sender, RoutedEventArgs e);

		/// <summary>
		/// Finds the parent.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="child">The child.</param>
		/// <returns></returns>
		public static T FindParent<T>(DependencyObject child) where T : DependencyObject
		 {
			 //get parent item
			 DependencyObject parentObject = VisualTreeHelper.GetParent(child);
 
			//we've reached the end of the tree
			 if (parentObject == null) return null;
 
			//check if the parent matches the type we're looking for
			 T parent = parentObject as T;
			 if (parent != null)
			 {
				 return parent;
			 }
			 else
			 {	 
				 return FindParent<T>(parentObject);
			 }
		 }

		/// <summary>
		/// Finds the visual child.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj">The object.</param>
		/// <returns></returns>
		public static T FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
		{
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
 				if (child is T)
				{
					return (T)child;
				}
				else
				{
					child = FindVisualChild<T>(child);
					if (child != null)
					{
						return (T)child;
					}
				}
			}
			return null;
		}

		/// <summary>
		/// Brushes from octet string.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public static SolidColorBrush BrushFromOctetString(string color)
		{
			//TO-DO: add error trapping
			SolidColorBrush newBrush = (SolidColorBrush)(new BrushConverter().ConvertFromString(color));
			return newBrush;
		}

		/// <summary>
		/// Converts the wpf color to a GDI color.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public static System.Drawing.Color ConvertToDrawingColor(System.Windows.Media.Color color)
		{
			return (System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B));
		}


		/// <summary>
		/// Rebinds the specified control's property.
		/// </summary>
		/// <param name="ctrl">The control.</param>
		/// <param name="dp">The dp.</param>
		public static void ReBind(FrameworkElement ctrl, DependencyProperty dp)
		{
			BindingExpression bindingExpr = ctrl.GetBindingExpression(dp);
			bindingExpr.UpdateTarget();
		}

		/// <summary>
		/// Adds the named routed event handler to the event manager object.
		/// </summary>
		/// <param name="classType">Type of the class that owns the event.</param>
		/// <param name="eventName">Name of the event (this method uses string.Contains to determine a match and ignores case, so you don't necessarily need to include the class name)</param>
		/// <param name="method">The method (a standard routed event handler.</param>
		public static void AddRoutedEventHandler(Type classType, string eventName, RoutedEventAction method )
		{
			var events = EventManager.GetRoutedEvents();
			RoutedEvent routedEvent = (from item in events where item.Name.ToUpper().Contains(eventName.ToUpper()) select item).FirstOrDefault<RoutedEvent>();
			if (routedEvent != null)
			{
				EventManager.RegisterClassHandler(classType, 
												  routedEvent, 
												  new RoutedEventHandler(method));
			}
		}

		/// <summary>
		/// Adds the named routed event handler to the event manager object.
		/// </summary>
		/// <param name="classType">Type of the class that owns the event.</param>
		/// <param name="eventName">Name of the event (the class name MUST be included, and case must be correct)</param>
		/// <param name="method">The method (a standard routed event handler.</param>
		public static void AddRoutedEventHandlerExact(Type classType, string eventName, RoutedEventAction method )
		{
			var events = EventManager.GetRoutedEvents();
			RoutedEvent routedEvent = (from item in events where item.Name == eventName select item).FirstOrDefault<RoutedEvent>();
			if (routedEvent != null)
			{
				EventManager.RegisterClassHandler(classType, 
												  routedEvent, 
												  new RoutedEventHandler(method));

			}
		}

		/// <summary>
		/// Builds a xaml object from the specified valid XAML text".
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static object GetXamlObject(string text)
		{
			object result = null;
			using (StringReader stream = new StringReader(text))
			{
				using (XmlReader xml = XmlReader.Create(stream))
				{
					result= XamlReader.Load(xml);
				}
			}
			return result;
		}

		public static string GetImageResourceName(string psAssemblyName, string psResourceName)
		{
			string value = string.Format("pack://application:,,,/{0};component/{1}", psAssemblyName, psResourceName);
			return value;
		}

		public static ImageSource GetImageSource(string path)
		{
			//Uri uri = new Uri(path, UriKind.RelativeOrAbsolute);
			//BitmapFrame bitFrame = BitmapFrame.Create(oUri);
			//return bitFrame;
			BitmapImage img = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));
			return img;
		}
	}
}
