﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfCommon
{
	/// <summary>
	/// Base class for all objects that require notifiablity and data validation
	/// </summary>
	public class Notifiable : INotifyPropertyChanged, IDataErrorInfo
	{
		#region INotifyPropertyChanged

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

#if _NET_45_
		/// <summary>
		/// Notifies that the property changed, and sets IsModified to true.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
				if (propertyName != "IsModified")
				{
					this.IsModified = true;
				}
            }
        }
#else
		/// <summary>
		/// Notifies that the property changed, and sets IsModified to true.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
        protected virtual void NotifyPropertyChanged(String propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
				if (propertyName != "IsModified")
				{
					this.IsModified = true;
				}
            }
        }
#endif
		#endregion INotifyPropertyChanged

		#region IDataErrorInfo Code

		/// <summary>
		/// Gets an error message indicating what is wrong with this object.
		/// </summary>
		public string Error
		{
			get { return "Error"; }
		}

		/// <summary>
		/// Gets the error message for the property with the given name.
		/// </summary>
		/// <param name="columnName">Name of the column.</param>
		/// <returns>The generated error message (if any).</returns>
		public string this[string columnName]
		{
			get 
			{
				return Validate(columnName);
			}
		}

		/// <summary>
		/// Validates the specified propery.
		/// </summary>
		/// <param name="properyName">Name of the propery.</param>
		/// <returns>Empty string if valid, otherwise, appropriate error message.</returns>
		protected virtual string Validate(string propertyName)
		{
			//Retun error message if there is error, otherwise return empty string
			string validationMsg = string.Empty;
			return validationMsg;
		}

		#endregion IDataErrorInfo Code

		#region Fields

		private bool isModified;
		private bool isSelected;
		private bool isVisible;
		private bool isEnabled;
		private bool isNew;
		private bool isClone;
		private bool isDeleted;
		private bool isValid;

		#endregion Fields

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether this instance is modified.
		/// </summary>
		public virtual bool IsModified
		{
			get { return this.isModified; }
			set
			{
				if (this.isModified != value)
				{
					this.isModified = value;
					this.NotifyPropertyChanged("IsModified");
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is selected.
		/// </summary>
		public virtual bool IsSelected
		{
			get { return this.isSelected; }
			set
			{
				if (this.isSelected != value)
				{
					this.isSelected = value;
					this.NotifyPropertyChanged("IsSelected");
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is visible.
		/// </summary>
		public virtual bool IsVisible
		{
			get { return this.isVisible; }
			set
			{
				if (value != this.isVisible)
				{
					this.isVisible = value;
					this.NotifyPropertyChanged("IsVisible");
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is enabled.
		/// </summary>
		public virtual bool IsEnabled
		{
			get { return this.isEnabled; }
			set
			{
				if (value != this.isEnabled)
				{
					this.isEnabled = value;
					this.NotifyPropertyChanged("IsEnabled");
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is new.
		/// </summary>
		public virtual bool IsNew
		{
			get { return this.isNew; }
			set
			{
				if (value != this.isNew)
				{
					this.isNew = value;
					this.NotifyPropertyChanged("IsNew");
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is a clone of an existing object.
		/// </summary>
		public virtual bool IsClone
		{
			get { return this.isClone; }
			set
			{
				if (value != this.isClone)
				{
					this.isClone = value;
					this.NotifyPropertyChanged("IsClone");
				}
			}
		}
		/// <summary>
		/// Get/set the flag indicating whether or not this item is marked for delete.
		/// </summary>
		public virtual bool IsDeleted
		{
			get { return this.isDeleted; }
			set
			{
				if (value != this.isDeleted)
				{
					this.isDeleted = value;
					this.NotifyPropertyChanged("IsDeleted");
				}
			}
		}

		/// <summary>
		/// Get or set a flag indicating whether or not this item has an error
		/// </summary>
		public virtual bool IsValid
		{
			get { return this.isValid; }
			set
			{
				if (value != this.isValid)
				{
					this.isValid = value;
					this.NotifyPropertyChanged("IsValid");
				}
			}
		}

		#endregion Properties

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="Notifiable"/> class.
		/// </summary>
		public Notifiable(bool isNew=false)
		{
			this.IsNew      = isNew;
			this.IsModified = false;
			this.IsVisible  = true;
			this.IsEnabled  = true;
			this.isValid    = true;
			this.IsClone    = false;
			this.IsDeleted  = false;
		}

		#endregion Constructor
	}
}
