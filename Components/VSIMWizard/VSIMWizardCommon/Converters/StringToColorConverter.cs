﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace WpfCommon.Converters
{
	public class StringToColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Color color = (Color)(ColorConverter.ConvertFromString((string)value));
			return color;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Type type = typeof(System.Windows.Media.Colors); 
			PropertyInfo[] colors = type.GetProperties(BindingFlags.Public | BindingFlags.Static); 
			return Colors.Black;
		}
	}
}
