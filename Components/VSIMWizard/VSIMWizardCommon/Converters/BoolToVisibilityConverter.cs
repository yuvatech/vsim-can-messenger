﻿using System;
using System.Windows;
using System.Windows.Data;

namespace WpfCommon.Converters
{
	/// <summary>
	/// 
	/// </summary>
	public class BoolToVisibilityConverter: IValueConverter
	{
		/// <summary>
		/// Gets or sets the value if false.
		/// </summary>
		public Visibility ValueIfFalse { get; set; }
		/// <summary>
		/// Gets or sets the value if true.
		/// </summary>
		public Visibility ValueIfTrue  { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="BoolToVisibilityConverter"/> class.
		/// </summary>
		public BoolToVisibilityConverter()
		{
			ValueIfTrue = Visibility.Visible;
			ValueIfFalse = Visibility.Hidden;
		}

		/// <summary>
		/// Converts a value.
		/// </summary>
		/// <param name="value">The value produced by the binding source.</param>
		/// <param name="targetType">The type of the binding target property.</param>
		/// <param name="parameter">The converter parameter to use.</param>
		/// <param name="culture">The culture to use in the converter.</param>
		/// <returns>
		/// A converted value. If the method returns null, the valid null value is used.
		/// </returns>
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Visibility result = ((bool)value) ? this.ValueIfTrue : this.ValueIfFalse;
			return result;
		}

		/// <summary>
		/// Converts a value.
		/// </summary>
		/// <param name="value">The value that is produced by the binding target.</param>
		/// <param name="targetType">The type to convert to.</param>
		/// <param name="parameter">The converter parameter to use.</param>
		/// <param name="culture">The culture to use in the converter.</param>
		/// <returns>
		/// A converted value. If the method returns null, the valid null value is used.
		/// </returns>
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			bool result = ((Visibility)value == this.ValueIfTrue);
			return result;
		}
	}
}
