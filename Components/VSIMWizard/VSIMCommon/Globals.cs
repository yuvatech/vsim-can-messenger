﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Linq;
using ObjectExtensions;
using System.Security;
using System.Security.Principal;

namespace Common
{
	/// <summary>
	/// Static cklass containing methods that are accessible to all apps and libraries
	/// </summary>
    public static class Globals
    {
		public static bool   IsInDesignMode        { get { return (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime); } }

		static Globals()
		{
		}

		/// <summary>
		/// Gets the property value.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetPropertyValue(object item, string propertyName)
		{
			return (item.GetType().GetProperty(propertyName).GetValue(item, null));
		}

		/// <summary>
		/// Determines whether the specified enumerator contains the specified ordinal value.
		/// </summary>
		/// <param name="enumType">Type of the enum.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static bool IsFlagsValid(Type enumType, int value)
		{
			bool valid = false;
			if (enumType.IsEnum)
			{
				valid = true;
				int maxBit = Convert.ToInt32(Math.Pow(2, Math.Ceiling(Math.Log(value)/Math.Log(2)))) >> 2;
				int i = 1;
				do
				{
					int ordinalValue = (1 << i);
					if (0 != (value & ordinalValue))
					{
						valid = (Enum.IsDefined(enumType, ordinalValue));
						if (!valid)
						{
							break;
						}
					}
					i++;
				} while (maxBit > i);
			}
			return valid;
		}

		/// <summary>
		/// Ints to enum.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static T IntToEnum<T>(int value, T defaultValue)
		{
			T enumValue = (Enum.IsDefined(typeof(T), value)) ? (T)(object)value : defaultValue;
			return enumValue;
		}

		/// <summary>
		/// Strings to enum.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static T StringToEnum<T>(string value, T defaultValue)
		{
			T enumValue = (Enum.IsDefined(typeof(T), value)) ? (T)Enum.Parse(typeof(T), value) : defaultValue;
			return enumValue;
		}



		/// <summary>
		/// Gets the name of the property (based on calling property name).
		/// </summary>
		/// <returns></returns>
		public static string GetPropertyName()
		{
			string name = new StackTrace().GetFrame(1).GetMethod().Name.Replace("get_","");
			return name;
		}

		/// <summary>
		/// Gets the calling method's name (and optionally the parent class) for use in a 
		/// string (usually when an exception needs to be thrown).
		/// </summary>
		/// <param name="withClassName"></param>
		/// <returns></returns>
		public static string GetCurrentMethodName(bool withClassName=true)
		{
			StackTrace st = new StackTrace ();
			StackFrame sf = st.GetFrame(1);
			string className = (withClassName) ? string.Format("{0}.",sf.GetMethod().DeclaringType.Name) : string.Empty;
			return string.Format("{0}{1}", className, sf.GetMethod().Name);
		}
	}
}
