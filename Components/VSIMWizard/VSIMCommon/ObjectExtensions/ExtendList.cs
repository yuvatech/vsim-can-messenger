﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ObjectExtensions
{
	/// <summary>
	/// 
	/// </summary>
	public static class ExtendList
	{
		/// <summary>
		/// Caluclates the Modes for the list of values.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <returns></returns>
        public static List<double> Modes(this List<double> list)
        {
            var modesList = list
                .GroupBy(values => values)
                .Select(valueCluster =>
                        new
                            {
                                Value = valueCluster.Key,
                                Occurrence = valueCluster.Count(),
                            })
                .ToList();
 
            int maxOccurrence = modesList
                .Max(g => g.Occurrence);
 
            return modesList.Where(x => x.Occurrence == maxOccurrence && maxOccurrence > 1).Select(x => x.Value).ToList();
        }

		/// <summary>
		/// Calculates the median value of the list.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="roundPlaces">The round places.</param>
		/// <returns></returns>
		public static double Median(this List<double> list, int roundPlaces=0)
        {
            List<double> orderedList = list.OrderBy(numbers => numbers).ToList();
 
            int listSize = orderedList.Count;
            double result;
 
            if (listSize % 2 == 0) // even
            {
                int midIndex = listSize/2;
                result = ((orderedList.ElementAt(midIndex - 1) + orderedList.ElementAt(midIndex))/2);
            }
            else // odd
            {
                double element = (double) listSize/2;
                element = Math.Round(element, MidpointRounding.AwayFromZero);
 
                result = orderedList.ElementAt((int) (element - 1));
            }
 
            return Math.Round(result, roundPlaces);
        }

		public static double Average(this List<double> list, int start, int end=0)
		{
			double value = 0d;
			if (start < 0)
			{
				throw new Exception("Starting index cannot be less than 0");
			}
			if (list.Count > 0)
			{
				end = (end==0) ? list.Count : end;
				int range = Math.Max(0, Math.Min(end-start, list.Count));
				if (range > 1)
				{
					List<double> temp = list.GetRange(start, range);
					value = temp.Average();
				}
				else
				{
					value = list[0];
				}
			}
			return value;
		}

		public static double Variance(this List<double> values)
		{
			return values.Variance(values.Average(), 0, values.Count);
		}

		public static double Variance(this List<double> values, double avg)
		{
			return values.Variance(avg, 0, values.Count);
		}

		public static double Variance(this List<double> values, double avg, int start, int end)
		{
			double variance = 0;

			for (int i = start; i < end; i++)
			{
				variance += Math.Pow((values[i] - avg), 2);
			}

			int n = end - start;
			if (start > 0) n -= 1;

			return variance / (n);
		}

		public static double StandardDeviation(this List<double> values)
		{
			return values.Count == 0 ? 0 : values.StandardDeviation(0, values.Count);
		}

		public static double StandardDeviation(this List<double> values, int start, int end)
		{
			double mean = values.Average(start, end);
			double variance = values.Variance(mean, start, end);
			return Math.Sqrt(variance);
		}

		public static bool PropertiesEqual<T>(this List<T> list, List<T> that, string[] properties, bool includeNonPublic = false)
		{
			bool result = true;
			if (properties != null && properties.Length > 0)
			{
				Type fromType = list.GetType();
				BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
				if (includeNonPublic)
				{
					flags |= BindingFlags.NonPublic;
				}

				PropertyInfo[] props = typeof(T).GetProperties(flags);
				foreach (PropertyInfo prop in props)
				{
					object dataFromCompare = prop.GetValue(list, null);
					object dataToCompare = prop.GetValue(that, null);
					Type type = prop.GetValue(list,null).GetType();
					//Type type = objectFromCompare.GetType().GetProperty(prop.Name).GetValue(objectToCompare,null).GetType();
					result = (Convert.ChangeType(dataFromCompare, type)).CompareEquals(Convert.ChangeType(dataToCompare, type));
					// no point in continuing beyond the first property that isn't equal.
					if (!result)
					{
						break;
					}
				}
			}
			return result;
		}

		public static bool ContentsEqual<T>(this List<T> thisList, List<T> thatList)
		{
			bool result = (thisList.Count == thatList.Count);
			if (result)
			{
				bool result2 = false;
				foreach(T thisItem in thisList)
				{
					foreach(T thatItem in thatList)
					{
						result2 = thisItem.CompareEquals(thatItem);
						if (result2)
						{
							break;
						}
					}
				}
				result &= result2;
			}
			return result;
		}

		/// <summary>
		/// Returns the contents of this string list as a semi-colon delimited list (for building 
		/// a set of email addresses).
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static string AsSemiColonDelimited(this List<string> list)
		{
			return list.AsDelimitedString(";");
		}

		public static string AsDelimitedString(this List<string> list, string delimiter, int start=0, int count=-1)
		{
			StringBuilder result = new StringBuilder();
			//int count = list.Count - 1;
			//foreach(string item in list)
			//{
			//	result.AppendFormat("{0}{1}", item, (list.IndexOf(item) < count) ? delimiter : string.Empty);
			//}

			// if the list contains items
			if (list.Count > 0)
			{
				// make sure the user didn't specify a starting index lower than 0
				start = Math.Max(0, start);

				// if count == -1, make count the length of the list
				if (count == -1)
				{
					count = list.Count-1;
				}

				// if start is lower than the length of the list
				if (start < list.Count - 1)
				{
					// calculate our stopping point
					int stop = Math.Min(start+count, list.Count-1);
					for( int i = start; i <= stop; i++)
					{
						result.AppendFormat("{0}{1}", list[i], (start < stop) ? delimiter : string.Empty);
					}
				}
			}
			return result.ToString();
		}

		/// <summary>
		/// Determines if the specified string (or one like it) is contained within this list. This 
		/// method calls the string.IsLike extension method (for a description of how that method 
		/// works, refer to the comments for that method) to determine whether this collection 
		/// contains an item like the specified value.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ContainsLike(this List<string> list, string value)
		{
			bool result = false;
			string found = list.Where(x=>x.IsLike(value)).FirstOrDefault<string>();
			result = (!string.IsNullOrEmpty(found));
			return result;
		}

		/// <summary>
		/// Determines if the specified string (or one like it) is contained within this list. This 
		/// method calls the string.IsLike extension method (for a description of how that method 
		/// works, refer to the comments for that method) to determine whether this collection 
		/// contains an item like the specified value.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ContainsLike(this string[] list, string value)
		{
			bool result = false;
			string found = list.Where(x=>x.IsLike(value)).FirstOrDefault<string>();
			result = (!string.IsNullOrEmpty(found));
			return result;
		}

		/// <summary>
		/// Gets the index of the first value in this string list that is "like" the specified 
		/// string value (refer to comments for the Common.IsLike method). It is the responsibility 
		/// of the programmer to ensure that the list is in the proper/desired sorted order before 
		/// calling this method.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static int IndexOfLike(this List<string> list, string value)
		{
			int index = -1;
			string found = list.Where(x=>x.IsLike(value)).FirstOrDefault<string>();
			if (!string.IsNullOrEmpty(found))
			{
				index = list.IndexOf(found);
			}
			return index;
		}

		/// <summary>
		/// Searches the specified string array, and returns the zero-based index of the first 
		/// occurrence of the specified value (comparison is case-sesnsitive).
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static int IndexOf(this string[] list, string value)
		{
			int index = -1;
			for (int i = 0; i < list.Length; i++)
			{
				if (list[i] == value)
				{
					index = i;
					break;
				}
			}
			return index;
		}

		/// <summary>
		/// Gets the index of the first value in this string list that is "like" the specified 
		/// string value (refer to comments for the Common.IsLike method). It is the responsibility 
		/// of the programmer to ensure that the list is in the proper/desired sorted order before 
		/// calling this method.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static int IndexOfLike(this string[] list, string value)
		{
			int index = -1;
			string found = list.Where(x=>x.IsLike(value)).FirstOrDefault<string>();
			if (!string.IsNullOrEmpty(found))
			{
				index = list.IndexOf(found);
			}
			return index;
		}
	}
}
