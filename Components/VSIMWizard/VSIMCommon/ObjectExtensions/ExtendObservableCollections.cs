﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectExtensions
{
	/// <summary>
	/// 
	/// </summary>
	public static class ExtendObservableCollections
	{
		/// <summary>
		/// Adds the list of items to this collection. There is no check for uniqueness 
		/// other than to make sure the object itsel is not already part of the collection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="collection">The collection.</param>
		/// <param name="list">The list.</param>
		public static void AddRange<T>(this ObservableCollection<T> collection, List<T> list)
		{
			foreach(T item in list)
			{
				//if (!collection.Contains(item))
				{
					collection.Add(item);
				}
			}
		}

		/// <summary>
		/// Sorts an observable collection by the specified property.
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TKey"></typeparam>
		/// <param name="source"></param>
		/// <param name="keySelector"></param>
		public static void Sort<TSource, TKey>(this ObservableCollection<TSource> source, Func<TSource, TKey> keySelector)
		{
			if (source != null && source.Count > 0)
			{
				Comparer<TKey> comparer = Comparer<TKey>.Default;

				for (int i = source.Count - 1; i >= 0; i--)
				{
					for (int j = 1; j <= i; j++)
					{
						TSource obj1 = source[j - 1];
						TSource obj2 = source[j];
						if (comparer.Compare(keySelector(obj1), keySelector(obj2)) > 0)
						{
							source.Remove(obj1);
							source.Insert(j, obj1);
						}
					}
				}
			}
		}
	}
}
