﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectExtensions
{
	/// <summary>
	/// 
	/// </summary>
	public static class ExtendInteger
	{
		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static bool IsEven(this int value)
		{
			int remainder = 0;
			if (value > 0)
			{
				Math.DivRem(value, 2, out remainder);
			}
			return (remainder == 0);
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static bool IsEven(this long value)
		{
			long remainder = 0;
			if (value > 0)
			{
				Math.DivRem(value, 2, out remainder);
			}
			return (remainder == 0);
		}

	}

}
