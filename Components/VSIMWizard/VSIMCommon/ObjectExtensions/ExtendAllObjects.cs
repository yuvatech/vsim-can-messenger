﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Common.Attributes;

namespace ObjectExtensions
{
	/// <summary>
	/// Object extensions that apply to all objects.
	/// </summary>
	public static class Extensions
	{
		/// <summary>
		/// Compares this object to the specified object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectFromCompare"></param>
		/// <param name="objectToCompare"></param>
		/// <returns></returns>
		/// <exception>To compare Dictionaries or HashSets, use the object's ToList() method 
		/// ('fromObj.ToList().CompareEquals(toObj.ToList()')</exception>
		public static bool CompareEquals<T>(this T objectFromCompare, T objectToCompare)
		{
			bool result = (objectFromCompare == null && objectToCompare == null);
			if (!result)
			{	
				try
				{
					Type fromType = objectFromCompare.GetType();
					if (fromType.IsPrimitive)
					{
						result = objectFromCompare.Equals(objectToCompare);
					}
					else if (fromType.FullName.StartsWith("System.String"))
					{
						result = ((objectFromCompare as string) == (objectToCompare as string));
					}
					else if (fromType.FullName.StartsWith("DateTime"))
					{
						result = (DateTime.Parse(objectFromCompare.ToString()).Ticks == DateTime.Parse(objectToCompare.ToString()).Ticks);
					}
					else if (fromType.FullName.StartsWith("System.Text.StringBuilder"))
					{
						result = ((objectFromCompare as StringBuilder).ToString() == (objectToCompare as StringBuilder).ToString());
					}
					else if (fromType.Name.StartsWith("Dictionary") || fromType.Name.StartsWith("HashSet"))
					{
						throw new Exception("To compare Dictionaries or HashSets, use the object's ToList() method ('fromObj.ToList().CompareEquals(toObj.ToList()')");
					}
					else if (objectFromCompare.IsSimpleCollection())
					{
						result = objectFromCompare.CompareCollection(objectToCompare);
					}
					else
					{
						PropertyInfo[] props = fromType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
						foreach (PropertyInfo prop in props)
						{
							
							if (Extensions.IgnoreProperty(prop))
							{
								result = true;
							}
							else
							{
								Type type = fromType.GetProperty(prop.Name).GetValue(objectToCompare, null).GetType();
								object dataFromCompare = fromType.GetProperty(prop.Name).GetValue(objectFromCompare, null);
								object dataToCompare = fromType.GetProperty(prop.Name).GetValue(objectToCompare, null);
								result = CompareEquals(Convert.ChangeType(dataFromCompare, type), Convert.ChangeType(dataToCompare, type));
								// no point in continuing beyond the first property that isn't equal.
								if (!result)
								{
									break;
								}
							}
						}
						if (result && objectFromCompare.IsComplexCollection())
						{
							result = objectFromCompare.CompareCollection(objectToCompare);
						}
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex);
				}
			}
			return result;
		}    

		/// <summary>
		/// Checks to see if the CompareIgnoreAttribute attribute is set for the specified 
		/// property, and then if the Ignore flag is true.
		/// </summary>
		/// <param name="property"></param>
		/// <returns>true if we can ignore this property</returns>
		private static bool IgnoreProperty(PropertyInfo property)
		{
			bool ignore = false;
			//this class is defined in the Common.Attributes namespace (in another assembly)
			CompareIgnoreAttribute attr = (CompareIgnoreAttribute)property.GetCustomAttribute(typeof(CompareIgnoreAttribute));
			if (attr != null)
			{
				ignore = attr.Ignore;
			}
			return ignore;
		}

		/// <summary>
		/// Determine if the spcified object is a "simple" collection
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool IsSimpleCollection<T>(this T obj)
		{
			bool result = false;
			Type fromType = obj.GetType();
			result = (fromType.IsGenericType || fromType.IsArray || fromType.Name.StartsWith("List") || fromType.Name.StartsWith("ObservableCollection"));
			return result;
		}

		///// <summary>
		///// 
		///// </summary>
		///// <typeparam name="T"></typeparam>
		///// <param name="obj"></param>
		///// <returns></returns>
		//public static bool IsKeyCollection<T>(this T obj)
		//{
		//	bool result = false;
		//	Type fromType = obj.GetType();
		//	result = (fromType.Name.StartsWith("KeyCollection"));
		//	return result;
		//}

		/// <summary>
		/// Determine if the spcified object is a "complex" collection
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		/// <remarks>A "complex" collection is a n object derived from a collection type that contains its own properties.</remarks>
		public static bool IsComplexCollection<T>(this T obj)
		{
			bool result = false;
			Type fromType = obj.GetType();
			result = (fromType.BaseType.Name.StartsWith("List") || fromType.BaseType.Name.StartsWith("ObservableCollection"));
			return result;
		}

		/// <summary>
		/// Compares two collections as if they're simple collections (compares only the objects 
		/// contained therein).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fromObj"></param>
		/// <param name="toObj"></param>
		/// <returns></returns>
		public static bool CompareCollection<T>(this T fromObj, T toObj)
		{
			bool result = (fromObj == null && toObj == null);

			if (!result)
			{
				try
				{
					Type fromType = fromObj.GetType();
					Type baseType = fromType.BaseType;
					string propName = (fromType.IsGenericType || baseType.IsGenericType) ? "Count" : "Length";
					string methName = (fromType.IsGenericType || baseType.IsGenericType) ? "get_Item" : "Get";
					PropertyInfo propInfo = fromType.GetProperty(propName);
					MethodInfo methInfo = fromType.GetMethod(methName);

					if (propInfo != null && methInfo != null)
					{
						int fromCount = (int)propInfo.GetValue(fromObj, null); 
						int toCount = (int)propInfo.GetValue(toObj, null); 
						result = (fromCount == toCount);
						if (result && fromCount > 0)
						{
							for (int index = 0; index < fromCount; index++) 
							{ 
								// Get an instance of the item in the list object 
								object fromItem = methInfo.Invoke(fromObj, new object[] { index });
								object toItem = methInfo.Invoke(toObj, new object[] { index });
								result = fromItem.CompareEquals(toItem);
								if (!result)
								{
									break;
								}
							}
						}
					}
					else
					{
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex);
				}
			}
			return result;
		}

		/// <summary>
		/// Compares a single property from the specified object for equality, ignoring all 
		/// other properties in the specified objects
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectFromCompare"></param>
		/// <param name="objectToCompare"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public static bool CompareEquals<T>(this T objectFromCompare, T objectToCompare, string propertyName)
		{
			bool result = (objectFromCompare == null && objectToCompare == null);
			if (!result)
			{
				try
				{
					Type fromType = objectFromCompare.GetType();
					PropertyInfo prop = fromType.GetProperty(propertyName);
					if (prop != null)
					{
						if (Extensions.IgnoreProperty(prop))
						{
							result = true;
						}
						else
						{
							Type type = prop.GetValue(objectToCompare,null).GetType();
							object dataFromCompare = prop.GetValue(objectFromCompare, null);
							object dataToCompare = prop.GetValue(objectToCompare, null);
							result = CompareEquals(Convert.ChangeType(dataFromCompare, type), Convert.ChangeType(dataToCompare, type));
						}
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex);
				}
			}
			return result;
		}

		/// <summary>
		/// Compares a list of properties for equality within the specified objects.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectFromCompare"></param>
		/// <param name="objectToCompare"></param>
		/// <param name="propertyNames"></param>
		/// <returns></returns>
		public static bool CompareEquals<T>(this T objectFromCompare, T objectToCompare, string[] propertyNames)
		{
			bool result = (objectFromCompare == null && objectToCompare == null);
			if (!result)
			{
				try
				{
					foreach (string propertyName in propertyNames)
					{
						result = CompareEquals(objectFromCompare, objectToCompare, propertyName);
						if (!result)
						{
							break;
						}
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex);
				}
			}
			return result;
		}

		/// <summary>
		/// Compares all public properties in the two objects except the properties named in the 
		/// ignorePropertes list.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectFromCompare"></param>
		/// <param name="objectToCompare"></param>
		/// <param name="ignoreProperties"></param>
		/// <returns></returns>
		public static bool CompareEqualsExcept<T>(this T objectFromCompare, T objectToCompare, string[] ignoreProperties)
		{
			bool result = (objectFromCompare == null && objectToCompare == null);
			//Type test = objectFromCompare.GetType();
			//MethodInfo[] testmi = test.GetMethods();
			//PropertyInfo[] testpi = test.GetProperties();
			if (!result)
			{
				try
				{
					Type fromType = objectFromCompare.GetType();
					List<PropertyInfo> properties = new List<PropertyInfo>();
					properties.AddRange(fromType.GetProperties());
					for (int i = properties.Count-1; i>=0; i--)
					{
						if (ignoreProperties.Contains(properties[i].Name) || Extensions.IgnoreProperty(properties[i]))
						{
							properties.RemoveAt(i);
						}
					}
					foreach (PropertyInfo property in properties)
					{
						Type type = property.GetValue(objectToCompare,null).GetType();
						object dataFromCompare = property.GetValue(objectFromCompare, null);
						object dataToCompare = property.GetValue(objectToCompare, null);
						result = CompareEquals(Convert.ChangeType(dataFromCompare, type), Convert.ChangeType(dataToCompare, type));
						if (!result)
						{
							break;
						}
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex);
				}
			}
			return result;
		}
	
	}
}
