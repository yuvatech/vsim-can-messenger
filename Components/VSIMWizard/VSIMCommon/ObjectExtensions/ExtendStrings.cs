﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ObjectExtensions
{
	/// <summary>
	/// 
	/// </summary>
	public static class ExtendStrings
	{
		/// <summary>
		/// <para>Determines whether this string is contained in the specified text. If the check </para>
		/// <para>is case-insensitive, both this string and the container text are uppercased for </para>
		/// <para>the comparison.</para>
		/// </summary>
		/// <param name="str">The string.</param>
		/// <param name="container">The container.</param>
		/// <param name="caseSensitive">if set to <c>true</c> [case sensitive].</param>
		/// <returns>True if this string is in the specified container</returns>
		public static bool IsIn(this string str, string container, bool caseSensitive=true)
		{
			bool result = !(string.IsNullOrEmpty(str));
			if (result)
			{
				result = (caseSensitive) ? container.IndexOf(str) >= 0 : container.ToUpper().IndexOf(str.ToUpper()) >= 0;
			}
			return result;
		}

		/// <summary>
		/// Converts the specified byte array to a string.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <param name="bytes">The bytes.</param>
		/// <returns></returns>
		public static string FromByteArray(this string str, byte[] bytes)
		{
			string result = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
			return result;
		}

		/// <summary>
		/// Converts the string to a byte array.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns>A byte array representing the string</returns>
		public static byte[] ToByteArray(this string str)
		{
			byte[] result = Encoding.ASCII.GetBytes(str);
			return result;
		}

		/// <summary>
		/// <para>Determines whether this string is "like" the specified string. Performs </para>
		/// <para>similarly to the SQL Server "LIKE" clause, and supports strings formatted </para>
		/// <para>with % wildcard symbols, like so: "string", "%string", "string%", and </para>
		/// <para>"%string%". The comparison is also case-insensitive.</para>
		/// </summary>
		/// <param name="str">This string.</param>
		/// <param name="like">The string to compare it against.</param>
		/// <returns></returns>
		public static bool IsLike(this string str, string like)
		{
			bool   result    = false;
			bool   wildStart = false;
			bool   wildEnd   = false;
			string actual    = like.ToUpper();
			string upperStr  = str.ToUpper();
			if (actual.StartsWith("%"))
			{
				actual = actual.Substring(1);
				wildStart = true;
			}
			if (actual.EndsWith("%"))
			{
				actual = actual.Substring(0,actual.Length-1);
				wildEnd = true;
			}

			if (wildStart && wildEnd)
			{
				result = (upperStr.Contains(actual));
			}
			else if (wildStart)
			{
				result = (upperStr.EndsWith(actual));
			}
			else if (wildEnd)
			{
				result = (upperStr.StartsWith(actual));
			}
			else
			{
				result = (upperStr == actual);
			}

			return result;
		}

		/// <summary>
		/// Get the index of the of first number in the string.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns>The found index, or -1 there is no number in the string. </returns>
		public static int IndexOfFirstNumber(this string str)
		{
			int index = -1;
			string numbers = "0123456789";
			foreach(char ch in str)
			{
				if (ch.ToString().IsIn(numbers))
				{
					index = str.IndexOf(ch);
				}
			}
			return index;
		}

		/// <summary>
		/// Get the index the of first alpha (a through z) character.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns>The found index, or -1 there is no alpha character in the string.</returns>
		public static int IndexOfAlpha(this string str)
		{
			int index = -1;
			string alphas = "abcdefghijklmnopqrstuvwxyz";
			foreach(char ch in str)
			{
				if (!ch.ToString().IsIn(alphas, false))
				{
					index = str.IndexOf(ch);
				}
			}
			return index;
		}

		/// <summary>
		/// Get the index the of first non-alpha (anything but a number or a letter) character.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns>The found index, or -1 there is no alpha character in the string.</returns>
		public static int IndexOfNonAlpha(this string str)
		{
			int index = -1;
			foreach(char ch in str)
			{
				if (str.IndexOfAlpha() == -1 && str.IndexOfFirstNumber() == -1)
				{
					index = str.IndexOf(ch);
				}
			}
			return index;
		}

		/// <summary>
		/// Determines whether the specified string is integer.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns></returns>
		public static bool IsInteger(this string str)
		{
			bool result = false;
			int value;
			result = int.TryParse(str, out value);
			return result;
		}

		/// <summary>
		/// Determines whether the specified string is double.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns></returns>
		public static bool IsDouble(this string str)
		{
			bool result = false;
			double value;
			result = double.TryParse(str, out value);
			return result;
		}

		/// <summary>
		/// Determines whether the specified string is decimal.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns></returns>
		public static bool IsDecimal(this string str)
		{
			bool result = false;
			decimal value;
			result = decimal.TryParse(str, out value);
			return result;
		}

		/// <summary>
		/// Determines whether the specified string is numeric.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns></returns>
		public static bool IsNumeric(this string str)
		{
			bool result = false;
			result = str.IsInteger() || str.IsDouble() || str.IsDecimal();
			return result;
		}

		/// <summary>
		/// Determines if this string represents a valid email address
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static bool IsValidEmail(this string text)
		{
			bool result = false;
			if (!string.IsNullOrEmpty(text))
			{
				try 
				{
					result = Regex.IsMatch(text, 
											@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
											@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
											RegexOptions.IgnoreCase, 
											TimeSpan.FromMilliseconds(250));
				}
				catch (Exception) 
				{
					result = false;
				}
			}
			return result;

		}

		/// <summary>
		/// This is intended for names, but will capitalize the first letter in every word in the 
		/// string.
		/// </summary>
		/// <param name="text"></param>
		public static string Capitalize(this string text)
		{
			return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text.ToLower());
		}

	}
}
