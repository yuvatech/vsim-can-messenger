﻿using System;

namespace Common.Attributes
{
	/// <summary>
	/// Indicates whether a property is included when the Globals.CompareEquals method is invoked.
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = false)]
	public class CompareIgnoreAttribute : System.Attribute
	{
		/// <summary>
		/// Get/set a flag indicationg whether or not to ignore the property in an equality comparison
		/// </summary>
		public bool Ignore { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ignore"></param>
		public CompareIgnoreAttribute(bool ignore=true)
		{
			this.Ignore = ignore;
		}
	}
}

