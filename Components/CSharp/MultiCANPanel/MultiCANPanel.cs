﻿using Extension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiCANPanel
{
    public partial class MultiCANPanel : Form
    {
        private Adapter _adapter = null;
        private readonly dynamic _db = Adapter.Database();
        public MultiCANPanel()
        {
            InitializeComponent();
            _adapter = new Adapter();
            _adapter.Open(false);
        }

        int A = 1;
        int B = 1;
        int C = 1;
        int D = 1;
        int E = 1;
        private void btnAddMessage_Click(object sender, EventArgs e)
        {
            //AddTextBox();
            //AddLabel();
            AddButton();
            //createButton();
        }


        private void createButton()
        {
            this.Controls.Clear();

            for (int i = 0; i < 4; i++)
            {
                Button b = new Button();
                b.Name = i.ToString();
                b.Click += delegate (object sender, EventArgs e) {/* any action */};
                b.Text = "Button" + i.ToString();
                this.Controls.Add(b);
            }

        }

        public System.Windows.Forms.TextBox AddTextBox()
        {
            System.Windows.Forms.TextBox txt = new TextBox();
            this.Controls.Add(txt);
            txt.Top = A * 28;
            txt.Left = 15;
            txt.Text = "TxtBox" + this.A.ToString();
            A = A + 1;
            return txt;
        }

        public System.Windows.Forms.Label AddLabel()
        {
            System.Windows.Forms.Label lbl = new System.Windows.Forms.Label();
            this.Controls.Add(lbl);
            lbl.Top = B * 28;
            lbl.Left = 150;
            lbl.Text = "Label" + this.B.ToString();
            B = B + 1;
            return lbl;
        }

        public System.Windows.Forms.Button AddButton()
        {
            System.Windows.Forms.Label lbl = new System.Windows.Forms.Label();
            this.Controls.Add(lbl);
            lbl.Top = B * 28;
            lbl.Left = 15;
            lbl.Text = "Message" + this.B.ToString();
            B = B + 1;

            System.Windows.Forms.TextBox txt = new TextBox();
            this.Controls.Add(txt);
            txt.Top = A * 28;
            txt.Left = 150;
            txt.Text = "";
            A = A + 1;

            System.Windows.Forms.TextBox txtVal = new TextBox();
            this.Controls.Add(txtVal);
            txtVal.Top = D * 28;
            txtVal.Left = 275;
            txtVal.Size = new System.Drawing.Size(300, 35);
            txtVal.Text = "";
            D = D + 1;

            System.Windows.Forms.TextBox txtValType = new TextBox();
            this.Controls.Add(txtValType);
            txtValType.Top = E * 28;
            txtValType.Left = 650;
            txtValType.Text = "Raw";
            E = E + 1;

            System.Windows.Forms.Button btn = new System.Windows.Forms.Button();
            this.Controls.Add(btn);
            btn.Top = C * 28;
            btn.Left = 800;
            btn.Text = "Send" + this.C.ToString();

            btn.Click += delegate (object sender, EventArgs e) {

                _adapter.Send(new Adapter.Packet() { Id = txt.Text, Type = txtValType.Text, Value = txtVal.Text });

                //var packets = new List<Adapter.Packet>();
                //packets.Add(new Adapter.Packet { Id = "4EF", Type = "Raw", Value = "0000000000000001",Periodic ="true" });
                ////packets.Add(new Adapter.Packet { Id = "256", Type = "Raw", Value = "C4BC000000000000" });
                //_adapter.Send(packets.ToArray());
            };
            C = C + 1;
            return btn;
        }

        private void MultiCANPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            e.Cancel = true;
            this.Hide();
        }
    }
}
