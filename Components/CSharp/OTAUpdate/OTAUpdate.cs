﻿using Extension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OTAUpdate
{
    public partial class OTAUpdate : Form
    {
        private Adapter _adapter = null;
        private readonly dynamic _db = Adapter.Database();
        Multimedia.Timer OTATimer = new Multimedia.Timer();
        public OTAUpdate()
        {
            InitializeComponent();
            _adapter = new Adapter();
            _adapter.Open(false);
        }


        private class Packeter
        {
            public Adapter.Packet Packet { get; set; }

            // ms.
            public int Delay { get; set; }

            public Packeter()
            {
                Delay = 25; //default
            }
        }

        /********************Timer Counters******************/
        int twelveMilliSeconds = 0;
        int twentyFiveMilliSeconds = 0;
        int hundredMilliSeconds = 0;
        int twoHundredandFiftyMilliSeconds = 0;
        int oneSecond = 0;
        /*************************************************************/
        private void periodicMessages()
        {
            OTATimer.Mode = Multimedia.TimerMode.Periodic;
            OTATimer.Period = 25;
            OTATimer.Resolution = 1;
            OTATimer.Tick += new System.EventHandler(Timer_Tick);
            OTATimer.Start();
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            twelveMilliSeconds++;
            twentyFiveMilliSeconds++;
            hundredMilliSeconds++;
            twoHundredandFiftyMilliSeconds++;
            oneSecond++;

            if (twelveMilliSeconds >= 0.5)
            {
                var packeters = new List<Adapter.Packet>();
                packeters.Add(new Adapter.Packet { Id = "35", Type = "Raw", Value = "CC79A8E4000010" });
                packeters.Add(new Adapter.Packet { Id = "31", Type = "Raw", Value = "AAC2D5AE7F8440" });
                _adapter.Send(packeters.ToArray());
                twelveMilliSeconds = 0;
            }
            if (twentyFiveMilliSeconds >= 1)
            {
                var packeters = new List<Adapter.Packet>();
                packeters.Add(new Adapter.Packet { Id = "29D", Type = "Raw", Value = "03DD100000000000" });
                packeters.Add(new Adapter.Packet { Id = "238", Type = "Raw", Value = "CD26100000000000" });
                _adapter.Send(packeters.ToArray());
                twentyFiveMilliSeconds = 0;
            }

            if (hundredMilliSeconds >= 4)
            {
                var packeters = new List<Adapter.Packet>();
                packeters.Add(new Adapter.Packet { Id = "415", Type = "Raw", Value = "BCE571870240" });
                packeters.Add(new Adapter.Packet { Id = "229", Type = "Raw", Value = "55C30391000002" });
                _adapter.Send(packeters.ToArray());
                hundredMilliSeconds = 0;
            }

            if (twoHundredandFiftyMilliSeconds >= 10)
            {
                var packeters = new List<Adapter.Packet>();
                packeters.Add(new Adapter.Packet { Id = "418", Type = "Raw", Value = "C5F5010000000000" });
                packeters.Add(new Adapter.Packet { Id = "227", Type = "Raw", Value = "2022CDAE0008" });
                packeters.Add( new Adapter.Packet { Id = "253", Type = "Raw", Value = "DC3B0EA711" });
                packeters.Add(new Adapter.Packet { Id = "70F", Type = "Raw", Value = "EDA4000000000000" });
                _adapter.Send(packeters.ToArray());

                twoHundredandFiftyMilliSeconds = 0;
            }

            if (oneSecond >= 40)
            {
                var packets = new List<Adapter.Packet>();
                packets.Add(new Adapter.Packet { Id = "236", Type = "Raw", Value = "2BB5000000000000" });
                packets.Add(new Adapter.Packet { Id = "256", Type = "Raw", Value = "C4BC000000000000" });
                _adapter.Send(packets.ToArray());
                oneSecond = 0;
            }

        }
        volatile bool _OTASignals = false;
        private void btnOTASignals_Click(object sender, EventArgs e)
        {
            _OTASignals = !_OTASignals;
            if (_OTASignals)
            {
                btnOTASignals.Text="Sending";
                btnOTASignals.BackColor = System.Drawing.Color.PaleGreen;

                periodicMessages();
            }
            else
            {
                OTATimer.Stop();
                btnOTASignals.Text = "Stopped";
                btnOTASignals.BackColor = System.Drawing.Color.PaleVioletRed;
            }
        }

        private void OTAUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            e.Cancel = true;
            this.Hide();
        }
    }
}
