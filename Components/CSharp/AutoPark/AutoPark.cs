﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Extension;

namespace AutoPark
{
    public partial class AutoPark : Form
    {
        private Adapter _adapter = null;
        private readonly dynamic _db = Adapter.Database();
        public AutoPark()
        {
            this.Show();
            InitializeComponent();

            _adapter = new Adapter();
            _adapter.Open(false);
            //_adapter.OnReceived += AdapterOnReceived;


            //_adapter.Open();
        }

        private class Packeter
        {
           public  Adapter.Packet Packet { get; set; }

            // ms.
           public int Delay { get; set; }

           public Packeter()
            {
                Delay = 25; //default
            }
        }

        private void btnAutoPark_Click(object sender, EventArgs e)
        {
            var packeters = new List<Packeter>();

            //#VMMP_VehMtnMvmtStatAuth= 0(Park)
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "VMMP_VehMtnMvmtStatAuth", Value = "0" } });

            // # TEGP_TrnsShftLvrPstnAuth, TEGP_TrnsShftLvrPstnAuth_Inv = 1,0 
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "TEGP_TrnsShftLvrPstnAuth", Value = "1" } });
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "TEGP_TrnsShftLvrPstnAuth_Inv", Value = "0" } });

            //# VSADP_VehSpdAvgDrvnAuth, VSADP_VehSpdAvgDrvnAuth_Inv = 0,0 
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "VSADP_VehSpdAvgDrvnAuth", Value = "0" } });
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "VSADP_VehSpdAvgDrvnAuth_Inv", Value = "0" } });

            //#PrkBrkSwAtv = True 
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "PrkBrkSwAtv", Value = "1" } });

            //#EPBSP_ElecPrkBrkApplStsAuth= 1(Applied)
            packeters.Add(new Packeter() { Packet = new Adapter.Packet { Name = "EPBSP_ElecPrkBrkApplStsAuth", Value = "1" } });            


            foreach(var temp in packeters)
            {
                _adapter.Send(temp.Packet);
                System.Threading.Thread.Sleep(25);
            }

        }

        //private void AdapterOnReceived(object sender, Adapter.Packet packet)
        //{

        //}

        private void AutoPark_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            e.Cancel = true;
            this.Hide();
        }
    }
}
