﻿namespace AutoPark
{
    partial class AutoPark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoPark));
            this.btnAutoPark = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAutoPark
            // 
            this.btnAutoPark.Location = new System.Drawing.Point(80, 74);
            this.btnAutoPark.Name = "btnAutoPark";
            this.btnAutoPark.Size = new System.Drawing.Size(187, 65);
            this.btnAutoPark.TabIndex = 0;
            this.btnAutoPark.Text = "Android_Auto_Park";
            this.btnAutoPark.UseVisualStyleBackColor = true;
            this.btnAutoPark.Click += new System.EventHandler(this.btnAutoPark_Click);
            // 
            // AutoPark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 237);
            this.Controls.Add(this.btnAutoPark);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AutoPark";
            this.Text = "AutoPark";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutoPark_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAutoPark;
    }
}

