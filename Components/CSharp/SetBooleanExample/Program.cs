﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Extension;
using SetBooleanExample;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {            
            var adapter = new Adapter();
            adapter.OnReceived += Adapter_OnReceived;

            var db = Adapter.Database();

            adapter.Open();  // blocking, wait until can actually connect

            var signals = BooleanSignals.GetNames(db);
            foreach (var signal in signals)
            {
                var packet = new Adapter.Packet() { Name = signal , Value = "1" };  // Turn ALL on!
                adapter.Send(packet);
            }
        }

        private static void Adapter_OnReceived(object sender, Adapter.Packet packet)
        {
            // Todo: check if received signal(s) after sending to know when onto the bus.
        }
    }
}
