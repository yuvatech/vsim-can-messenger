﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetBooleanExample
{
    class BooleanSignals
    {

        public static string[] GetNames(dynamic db)
        {
            var signals = db["signals"];
            var result = new List<string>();
            foreach (var signal in signals)
            {
                foreach (var key in signal)
                {
                    if (key["bitlength"] == "1")
                        result.Add((signal.Name));
                }
            }

            return result.ToArray();
        }
    }
}
