﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Extension;
using Newtonsoft.Json;
using Exception = System.Exception;

namespace ConsoleApplication
{
    class Program
    {
        private static readonly StreamWriter _logger = new StreamWriter(Path.Combine(Utility.Helper.AssemblyDirectory, "SimpleExample.csv"));
        private static readonly object _locker = new object();
        private static readonly System.Collections.Generic.Queue<string>
            _logs = new System.Collections.Generic.Queue<string>();

        static void Main(string[] args)
        {
            Task.Run(() =>
            {
                while (true)
                {
                   // lock (_locker)
                    {
                        try
                        {
                            if (!_logs.Any())
                            {
                                System.Threading.Thread.Sleep(25);
                            }
                            else
                            {
                                var info = _logs.Dequeue();
                                _logger.WriteLine(info);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.WriteLine("-");
                        }
                    }
                    System.Threading.Thread.Sleep(1);
                }
            });


            var adapter = new Adapter();
            adapter.OnReceivedMany += Adapter_OnReceived;

            var db = Adapter.Database();
            var signals = db["signals"];

            adapter.Open(blocking: false);  // blocking, wait until can actually connect

            adapter.Send(new Adapter.Packet() { Id="100", Subscribe = "true" });
            adapter.Send(new Adapter.Packet() { Type = "Signal", Name = "SPMP_SysPwrModeAuth", Subscribe = "true" });
            adapter.Send(new Adapter.Packet() { Type = "Signal", Name = "DrvDrAjrStat", Subscribe = "true" });

            adapter.Send(new Adapter.Packet() {Type = "VehicleSim", Name = "subscriptions", value = "echo"});
            //adapter.Send(new Adapter.Packet() { Type = "VehicleSim", Name = "subscriptions", value = "clear" });

            while (true)
            {
                if (adapter.IsConnnected)
                    ; // adapter.Send(test);
                System.Threading.Thread.Sleep(125);
            }
        }

        private static int _count = 0;
        private static void Adapter_OnReceived(object sender, Adapter.Packet[] packets)
        {
            // Console.WriteLine(packets.Length);
            //Parallel.ForEach(packets, (packet) =>
            foreach (var packet in packets)
            {
                try
                {
                    var info = DateTime.Now.ToString("hh:mm:ss.fff") + ',' + packet.Value;
                    if (packet.Id != null && packet.Id.Contains("100") &&
                        packet.Type == Adapter.Packet.Types[3]) // raw data
                    {
                        //Console.WriteLine(_count++);                        
                        info = DateTime.Now.ToString("HH:mm:ss.FFF") + "," + packet.Value;
                        Console.WriteLine(info);
                    }
                    else
                    {                        
                        //Console.WriteLine(JsonConvert.SerializeObject(packet));
                    }

                    if (!string.IsNullOrEmpty(packet.Name) && packet.Name == "SPMP_SysPwrModeAuth")
                    {
                        //Console.WriteLine(info);
                    }

                    if (!string.IsNullOrEmpty(packet.Name) && packet.Name.Contains("DrvDrAjr"))
                    {                        
                        //Console.WriteLine(info);
                    }

                    try
                    {
                        //lock (_locker)
                        {
                            if (!string.IsNullOrEmpty(info))
                            {                                
                                _logs.Enqueue(info);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ;
                    }

                }
                catch (Exception e)
                {
                    ; //Console.WriteLine(e);
                }
            }//);
        }
    }
}
