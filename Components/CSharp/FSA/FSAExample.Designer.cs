﻿namespace FSA
{
    partial class FSAExample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSAExample));
            this.btnInit = new System.Windows.Forms.Button();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.btnFSACalls = new System.Windows.Forms.Button();
            this.lblService = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.txtServiceID = new System.Windows.Forms.TextBox();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.txtFSAValue = new System.Windows.Forms.TextBox();
            this.lblFSAValue = new System.Windows.Forms.Label();
            this.chbEnableInit = new System.Windows.Forms.CheckBox();
            this.chbEnableFsa = new System.Windows.Forms.CheckBox();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.cmbService = new System.Windows.Forms.ComboBox();
            this.cmbName = new System.Windows.Forms.ComboBox();
            this.btnClean = new System.Windows.Forms.Button();
            this.lblResponse = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnInit
            // 
            this.btnInit.Enabled = false;
            this.btnInit.Location = new System.Drawing.Point(776, 32);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(92, 45);
            this.btnInit.TabIndex = 0;
            this.btnInit.Text = "Init";
            this.btnInit.UseVisualStyleBackColor = true;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // txtResponse
            // 
            this.txtResponse.Location = new System.Drawing.Point(44, 340);
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResponse.Size = new System.Drawing.Size(812, 324);
            this.txtResponse.TabIndex = 1;
            // 
            // btnFSACalls
            // 
            this.btnFSACalls.Enabled = false;
            this.btnFSACalls.Location = new System.Drawing.Point(776, 121);
            this.btnFSACalls.Name = "btnFSACalls";
            this.btnFSACalls.Size = new System.Drawing.Size(92, 45);
            this.btnFSACalls.TabIndex = 2;
            this.btnFSACalls.Text = "Send FSA";
            this.btnFSACalls.UseVisualStyleBackColor = true;
            this.btnFSACalls.Click += new System.EventHandler(this.btnFSACalls_Click);
            // 
            // lblService
            // 
            this.lblService.AutoSize = true;
            this.lblService.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblService.Location = new System.Drawing.Point(73, 9);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(72, 20);
            this.lblService.TabIndex = 3;
            this.lblService.Text = "Service";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(242, 9);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(91, 20);
            this.lblID.TabIndex = 4;
            this.lblID.Text = "ServiceID";
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.Location = new System.Drawing.Point(454, 9);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(56, 20);
            this.lblValue.TabIndex = 5;
            this.lblValue.Text = "Value";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(242, 121);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(57, 20);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(73, 121);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(49, 20);
            this.lblType.TabIndex = 7;
            this.lblType.Text = "Type";
            // 
            // txtServiceID
            // 
            this.txtServiceID.Location = new System.Drawing.Point(216, 32);
            this.txtServiceID.Multiline = true;
            this.txtServiceID.Name = "txtServiceID";
            this.txtServiceID.Size = new System.Drawing.Size(138, 24);
            this.txtServiceID.TabIndex = 9;
            this.txtServiceID.Text = "1016";
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(415, 32);
            this.txtValue.Multiline = true;
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(161, 24);
            this.txtValue.TabIndex = 10;
            this.txtValue.Text = "192.168.1.102:9005";
            // 
            // txtFSAValue
            // 
            this.txtFSAValue.Enabled = false;
            this.txtFSAValue.Location = new System.Drawing.Point(475, 142);
            this.txtFSAValue.Multiline = true;
            this.txtFSAValue.Name = "txtFSAValue";
            this.txtFSAValue.Size = new System.Drawing.Size(138, 24);
            this.txtFSAValue.TabIndex = 13;
            // 
            // lblFSAValue
            // 
            this.lblFSAValue.AutoSize = true;
            this.lblFSAValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFSAValue.Location = new System.Drawing.Point(507, 121);
            this.lblFSAValue.Name = "lblFSAValue";
            this.lblFSAValue.Size = new System.Drawing.Size(56, 20);
            this.lblFSAValue.TabIndex = 14;
            this.lblFSAValue.Text = "Value";
            // 
            // chbEnableInit
            // 
            this.chbEnableInit.AutoSize = true;
            this.chbEnableInit.Location = new System.Drawing.Point(687, 45);
            this.chbEnableInit.Name = "chbEnableInit";
            this.chbEnableInit.Size = new System.Drawing.Size(74, 21);
            this.chbEnableInit.TabIndex = 15;
            this.chbEnableInit.Text = "Enable";
            this.chbEnableInit.UseVisualStyleBackColor = true;
            this.chbEnableInit.CheckedChanged += new System.EventHandler(this.chbEnableInit_CheckedChanged);
            // 
            // chbEnableFsa
            // 
            this.chbEnableFsa.AutoSize = true;
            this.chbEnableFsa.Location = new System.Drawing.Point(687, 134);
            this.chbEnableFsa.Name = "chbEnableFsa";
            this.chbEnableFsa.Size = new System.Drawing.Size(74, 21);
            this.chbEnableFsa.TabIndex = 16;
            this.chbEnableFsa.Text = "Enable";
            this.chbEnableFsa.UseVisualStyleBackColor = true;
            this.chbEnableFsa.CheckedChanged += new System.EventHandler(this.chbEnableFsa_CheckedChanged);
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Subscribe",
            "UnSubscribe",
            "Get",
            "Set",
            "Request",
            "RequestResponse"});
            this.cmbType.Location = new System.Drawing.Point(44, 142);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(138, 24);
            this.cmbType.TabIndex = 17;
            this.cmbType.Text = "Subscribe";
            // 
            // cmbService
            // 
            this.cmbService.FormattingEnabled = true;
            this.cmbService.Items.AddRange(new object[] {
            "Init",
            "OnStarFunctions"});
            this.cmbService.Location = new System.Drawing.Point(44, 32);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(138, 24);
            this.cmbService.TabIndex = 18;
            this.cmbService.Text = "Init";
            // 
            // cmbName
            // 
            this.cmbName.FormattingEnabled = true;
            this.cmbName.Items.AddRange(new object[] {
            "OnStarMessage",
            "TTYMode",
            "SendTTYText",
            "NeedLocationSharingEnabledResponse"});
            this.cmbName.Location = new System.Drawing.Point(207, 142);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(225, 24);
            this.cmbName.TabIndex = 19;
            this.cmbName.Text = "OnStarMessage";
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(776, 200);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(92, 45);
            this.btnClean.TabIndex = 20;
            this.btnClean.Text = "Clean";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // lblResponse
            // 
            this.lblResponse.AutoSize = true;
            this.lblResponse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponse.Location = new System.Drawing.Point(50, 317);
            this.lblResponse.Name = "lblResponse";
            this.lblResponse.Size = new System.Drawing.Size(65, 20);
            this.lblResponse.TabIndex = 21;
            this.lblResponse.Text = "Output";
            // 
            // FSAExample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 700);
            this.Controls.Add(this.lblResponse);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.cmbName);
            this.Controls.Add(this.cmbService);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.chbEnableFsa);
            this.Controls.Add(this.chbEnableInit);
            this.Controls.Add(this.lblFSAValue);
            this.Controls.Add(this.txtFSAValue);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.txtServiceID);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblService);
            this.Controls.Add(this.btnFSACalls);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.btnInit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FSAExample";
            this.Text = "FSAExample";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInit;
        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.Button btnFSACalls;
        private System.Windows.Forms.Label lblService;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblValue;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.TextBox txtServiceID;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.TextBox txtFSAValue;
        private System.Windows.Forms.Label lblFSAValue;
        private System.Windows.Forms.CheckBox chbEnableInit;
        private System.Windows.Forms.CheckBox chbEnableFsa;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.ComboBox cmbService;
        private System.Windows.Forms.ComboBox cmbName;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Label lblResponse;
    }
}

