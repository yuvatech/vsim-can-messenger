﻿using Extension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSA
{
    public partial class FSAExample : Form
    {
        static FSAExample __this;
        private Adapter _adapter = null;
        string timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

        static void Log(string text)
        {
            __this.Invoke((MethodInvoker)delegate
            {
                __this.txtResponse.Text += text + "\r\n";
            });
            //txtResponse.Text = text + "\r\n";

        }

        public FSAExample()
        {
            InitializeComponent();
            _adapter = new Adapter();
            _adapter.Open(false);
            _adapter.OnReceived += _adapter_OnReceived;

            __this = this;
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            var packeters = new List<Adapter.Packet>();
            packeters.Add(new Adapter.Packet { Service = cmbService.Text, Id = txtServiceID.Text, Value = txtValue.Text });
            _adapter.Send(packeters.ToArray());

            cmbType.Enabled = true;
            cmbName.Enabled = true;
            txtFSAValue.Enabled = true;           
        }

        private void _adapter_OnReceived(object sender, Adapter.Packet packet)
        {
            if (packet.Type == "Subscribe" || packet.Type=="UnSubscribe" || packet.Type == "Resp" || packet.Type == "Event" || packet.Type == "Status" || packet.Type == "Failure")
            {
                Log(string.Format("JSON data: "+ timestamp + ", Name: " +packet.Name+ ", Type: "+packet.Type +", Output: "+packet.value));
            }

            if(packet.Id == "1016")
            {
                Log(string.Format("Heartbbeat: "+ timestamp +" : "+  packet.value));
            }
        }

        private void chbEnableInit_CheckedChanged(object sender, EventArgs e)
        {
            btnInit.Enabled = true;

        }

        private void btnFSACalls_Click(object sender, EventArgs e)
        {
            var packeters = new List<Adapter.Packet>();
            packeters.Add(new Adapter.Packet { Service = cmbService.Text, Type = cmbType.Text,Name=cmbName.Text, Value = txtValue.Text });
            _adapter.Send(packeters.ToArray());
        }

        private void chbEnableFsa_CheckedChanged(object sender, EventArgs e)
        {
            cmbService.Text = "OnStarFunctions";
            btnFSACalls.Enabled = true;            
        }
        
        private void btnClean_Click(object sender, EventArgs e)
        {
            var packeters = new List<Adapter.Packet>();
            packeters.Add(new Adapter.Packet { Service = "Clean", Id = txtServiceID.Text ,Type = "Clean"});
            _adapter.Send(packeters.ToArray());
        }
    }
}
