﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Extension
{
    public class Adapter
    {
        /***        
            Important: is copy and paste for most part of files in main solution, try to keep in sync.
            Reference: SocketServer.cs, SocketTypes.cs, Client.cs, 
            Note: was done to have "stand-alone" project to build from...
        ***/
        public class Packet
        {
            public static readonly string[] Types = { "Signal", "Message", "VehicleSim", "Raw" };
            public static readonly string[] Modes = { "HS", "LS", "MF", "FD", "FD_MF", "LIN" };
            public static readonly string[] Ports = { "1", "2" };

            //v1
            public string msgType;
            public string signalName;
            public string value;

            //v2
            public string Id;
            public string Type; // Message, Signal, Raw, 
            public string Mode; //HS,LS,FD,MF
            public string Name;
            public object Value;
            public string Port;
            public string Periodic; // true, false
            public string Subscribe; // true / false
            public object Extension; // first time being used

            //v3
            public string Service; // FSA service

            // takes the prevous message type and changes back and forth for legacy support!
            public static List<Packet> Transform(List<Packet> payload)
            {
                var transform = new List<Packet>();
                foreach (var item in payload)
                {
                    // ### V2 -> V1 ###
                    if (!string.IsNullOrEmpty(item.Name))
                    {
                        if (string.IsNullOrEmpty(item.signalName))
                            item.signalName = item.Name;
                    }

                    if (item.Value != null)
                    {
                        if (string.IsNullOrEmpty(item.value))
                            item.value = Convert.ToString(item.Value);
                    }

                    if (!string.IsNullOrEmpty(item.Mode))
                    {
                        if (string.IsNullOrEmpty(item.msgType))
                            item.msgType = item.Mode;
                    }

                    // ### V1 -> V2 ###
                    // Type
                    if (string.IsNullOrEmpty(item.Type))
                    {
                        if (item.msgType == "vehicle_sim") // old way
                            item.Type = Packet.Types[2];
                        else
                        {
                            item.Type = Types[0]; // v1 only sent signal types
                            item.Mode = item.msgType;
                        }
                    }

                    // Name
                    if (string.IsNullOrEmpty(item.Name))
                    {
                        if (!string.IsNullOrEmpty(item.signalName))
                            item.Name = item.signalName;
                    }

                    // Value
                    if (item.Value == null)
                        item.Value = item.value;

                    // Port
                    if (string.IsNullOrEmpty(item.Port))
                        item.Port = Ports[0]; // default

                    transform.Add(item);
                }

                return transform;
            }
        }

        private const int BUFFER_SIZE = 512000 * 2 * 5;
        private Socket _client = null;
        private bool _active = false;
        private bool _reconnect = true;

        public Guid Id { get; private set; }
        internal byte[] Terminator { get; set; } // future

        private readonly string _address = "127.0.0.1";
        private readonly int _port = 55555;

        //  event to pass received data to the server class
        public delegate void ReceivedHandler(object sender, Packet packet);
        public delegate void ReceivedManyHandler(object sender, Packet[] packets);
        public event ReceivedHandler OnReceived;
        public event ReceivedManyHandler OnReceivedMany;

        private static readonly object _lockDB = new object();
        private static dynamic _db = null;

        public static dynamic Database(int channel = 0)
        {
            lock (_lockDB)
            {
                if (_db == null)
                    if (channel == 0)
                        _db = FindDB();
                    else
                        _db = FindDB("arxml2");

                return _db;
            }
        }

        public Adapter()
        {

        }

        public Adapter(string address, int port)
        {
            _address = address;
            _port = port;
        }

        public bool IsConnnected => _client != null && _client.Connected;

        private Thread _thread = null;
        public void Open(bool blocking = true, bool reconnect = true)
        {
            _reconnect = reconnect;

            var ipAddress = IPAddress.Parse(_address); // local host
            var ipEndpoint = new IPEndPoint(ipAddress, _port);
            var client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            while (true)
            {
                try
                {
                    client.Connect(ipEndpoint);
                    break;
                }
                catch (Exception ex)
                {
                    if (!blocking)
                        break;// throw ex;

                    // eat it, is expected condition sometimes
                    Debug.WriteLine(ex);
                    Thread.Sleep(1000);
                }
            }

            var id = Guid.NewGuid();

            _active = true;

            //Assign members
            this._client = client;
            this.Id = id;

            this._client.SendBufferSize = BUFFER_SIZE;
            this._client.ReceiveBufferSize = BUFFER_SIZE;

            this._client.SendTimeout = -1;
            this._client.ReceiveTimeout = -1;

            this._client.Blocking = true;
            this._client.NoDelay = true;

            Terminator = null;

            //Init the StreamWriter
            if (_thread == null)
            {
                var thread = new Thread(() => { Listen(); }) { Priority = ThreadPriority.Highest }; // feels good...
                thread.Start();
                _thread = thread;
            }
            Thread.Sleep(125); // sorry feels good and seems to help with trying to Tx
        }

        //Closes the connection
        public void Close(bool force = false)
        {
            // close streams
            if (_active)
            {
                try
                {
                    _client.Close();
                }
                catch { }

                Trace.WriteLine("Client " + this.Id + " disconnceted.");
            }

            _client = null;
            // stop listening            
            if (!_reconnect || force)
            {
                _active = false;
                _thread = null;
            }
        }

        private object _bufferLock = new object();
        //Reads data from the connection and fires an event wih the recived data
        private void Listen()
        {
            var buffer = new List<byte>();
            var data = new byte[BUFFER_SIZE * 100];

            while (_active)
            {
                if (!IsConnnected)
                {
                    // re-connect
                    Open(blocking: false);
                    Thread.Sleep(250);
                    continue;
                }

                try
                {
                    while (IsConnnected)
                    {
                        var socket = _client;
                        var count = 0;
                        while (true)
                        {
                            try
                            {
                                count = socket.Receive(data);
                            }
                            catch (SocketException e)
                            {
                                if (socket.Connected)
                                {
                                    Thread.Sleep(1);
                                    continue;
                                }
                            }

                            break;
                        }

                        if (count <= 0)
                        {
                            Thread.Sleep(1); // give CPU a breathe
                            continue;
                        }

                        for (var x = 0; x <= count; x++)
                            buffer.Add(data[x]);

                        // added terminator option for future because feels good                                                
                        if (Terminator != null && Terminator.Length > 0)
                        {
                            var end = buffer.Count;
                            // Todo: ...
                        }

                        var leftover = Process(buffer.ToArray());
                        buffer.Clear();
                        //buffer.AddRange(leftover);

                        Thread.Sleep(1);
                    }
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    Debug.WriteLine(ex);
                    if (!_reconnect)
                    {
                        Close(force: true);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        // received from socket server client(s)        
        private List<byte> Process(byte[] buffer)
        {
            var payload = new List<Packet>();
            var parseBuffer = buffer.ToList();

            try
            {
                if (!parseBuffer.Any())
                    return parseBuffer;

                /*
                parseBuffer.RemoveAll(b =>
                    Encoding.ASCII.GetString(new[] { b }).Equals("\n")); // newlines not needed
                */
                var startIndex = parseBuffer.IndexOf(91); // [
                                                          // removing junk if partial message
                if (startIndex > 0)
                {
                    parseBuffer.RemoveRange(0, startIndex);
                    startIndex = parseBuffer.IndexOf(91); // find new index now...
                }

                var endIndex = parseBuffer.IndexOf(93); // ]

                if (startIndex == -1 || endIndex == -1) // no good data ready!!! Lossing data?
                    return parseBuffer; // not enough

                if (endIndex - startIndex < 0)
                {
                    parseBuffer.RemoveRange(0, endIndex);
                    return parseBuffer;
                }

                if (startIndex > endIndex) // partial data received (first message?)
                    parseBuffer.RemoveRange(0, startIndex);

                var temp = parseBuffer.GetRange(startIndex, (endIndex - startIndex) + 1);
                var startRemove = 0;
                // treat list in a list (in list...) as single list and process on next loop around...
                foreach (var t in temp)  // Todo: linq style...
                {
                    if (t == 91)
                        startRemove++;
                    else
                        break;
                }

                if (startRemove > 1 && temp.Count >= (startIndex + startRemove))
                    temp.RemoveRange(startIndex, startRemove - 1);
                parseBuffer.RemoveRange(startIndex, (endIndex - startIndex) + startRemove);

                var data = Encoding.ASCII.GetString(temp.ToArray(), 0, temp.Count);

                try
                {
                    var decode = JsonConvert.DeserializeObject<List<Packet>>(data);
                    payload.AddRange(decode);
                }
                catch (Exception ex)
                {
                    return parseBuffer;
                }

                if (payload.Any())
                {
                    Task.Run(() =>
                    {
                        if (OnReceivedMany != null)
                            OnReceivedMany(this, payload.ToArray());
                        else if (OnReceived != null)
                            Parallel.ForEach(payload, (entry) => { OnReceived(this, entry); });
                    });
                }

            }
            catch (Exception ex)
            {
                // bad data does anyone care?
                Debug.WriteLine(ex);
            }

            return parseBuffer;
        }

        public bool Send(Packet[] packets)
        {
            var payload = new List<Packet>();
            payload.AddRange(packets);
            var dataRevert = JsonConvert.SerializeObject(payload); // magic decoder
            var bufferRevert = Encoding.ASCII.GetBytes(dataRevert);
            return Send(bufferRevert);
        }

        public bool Send(Packet packet)
        {
            var payload = new List<Packet>();
            payload.Add(packet);
            var dataRevert = JsonConvert.SerializeObject(payload); // magic decoder
            var bufferRevert = Encoding.ASCII.GetBytes(dataRevert);
            return Send(bufferRevert);
        }

        //Sends the string "data" to the client
        private bool Send(byte[] buffer)
        {
            try
            {
                // write immeditaly
                if (IsConnnected)
                {
                    if (buffer.Length == _client.Send(buffer))
                        return true;
                }
            }
            catch (Exception ex)
            {
                // cannot write then client is gone
                Close();
            }
            return false;
        }

        private static int IndexOf(byte[] data, byte[] find)
        {
            if (find.Length > data.Length)
                return -1;
            for (var i = 0; i < data.Length - find.Length; i++)
            {
                bool found = true;
                for (var j = 0; j < find.Length; j++)
                {
                    if (data[i + j] != find[j])
                    {
                        found = false;
                        break;
                    }
                }
                if (found)
                    return i;
            }
            return -1;
        }
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private static dynamic FindDB(string contains = "arxml1")
        {
            // Todo: better catching everything is just lazy...
            try
            {
#if UNIX 
                var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                path = Path.Combine(path, ".gmvehiclesimulator");
#else         
                var find = "GMVehicleSimulator";
                var path = "";
                var index = AssemblyDirectory.LastIndexOf(find);
                if (index < 0)
                    find = "gmvehiclesim"; // dev location
                index = AssemblyDirectory.LastIndexOf(find);
                path = AssemblyDirectory.Remove(index);
                path = Path.Combine(path, find);
#endif
                /*
                if (split.Length < 2)
                    split = AssemblyDirectory.Split(new[] {  }, StringSplitOptions.RemoveEmptyEntries);  // dev placement
                var path = Path.Combine(split[0], find);
                */
                var db = Directory.GetFiles(Path.Combine(path), "*.json", SearchOption.AllDirectories).FirstOrDefault(f => f.ToLower().Contains(contains));
                if (!string.IsNullOrEmpty(db))
                {
                    using (var r = new StreamReader(db))
                    {
                        var json = r.ReadToEnd();
                        dynamic data = JsonConvert.DeserializeObject(json);
                        return data;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return null;
        }
    }
}
