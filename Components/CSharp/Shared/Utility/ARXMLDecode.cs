﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class ARXMLDecode
    {
        /// <summary>
        /// Enums for passed signal name.
        /// </summary>
        /// <param name="signals">ARXML signals passed</param>
        /// <param name="name">Signal name to get list of enums</param>
        /// <returns></returns>
        public static Dictionary<string, string> ParseEnums(dynamic signals, string name)
        {
            var result = new Dictionary<string, string>();
            foreach (var signal in signals)
            {
                foreach (var item in signal)
                {
                    if (Convert.ToString(item["name"]).ToLower() == name.ToLower())
                    {
                        var enums = item["enumerations"];                       
                        var enumRaw = Convert.ToString(enums).Trim().Replace(System.Environment.NewLine, "").Replace("\"", "").Replace("{", "").Replace("}", "");
                        var enumVal = enumRaw.Split(new[] { ',', ':' });

                        for (var i = 0; i < enumVal.Length; i += 2)
                        {
                            var key = enumVal[i].Trim();
                            var value = enumVal[i + 1].Trim();

                            result.Add(key, value);
                        }
                        break;                       
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Range for passed signal name.
        /// </summary>
        /// <param name="signals">ARXML signals passed</param>
        /// <param name="name">Signal name to get list of enums</param>
        /// <returns></returns>
        public static Dictionary<int, int> ParseRange(dynamic signals, string name)
        {
            var result = new Dictionary<int, int>();
            foreach (var signal in signals)
            {
                foreach (var item in signal)
                {
                    if (Convert.ToString(item["name"]).ToLower() == name.ToLower())
                    {
                        int min = item["min"];
                        int max = item["max"];
                        
                        result.Add(min, max);
                        break;
                    }
                }
            }
            return result;
        }
    }
}
