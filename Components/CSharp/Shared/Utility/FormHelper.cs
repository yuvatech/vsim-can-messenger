﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Extension;

namespace Utility
{
    public class FormHelper
    {
        public static string[] Setting(string key)
        {
            // Todo:  read app config and parse value
            // ConfigurationManagement()

            var result = new List<string>();
            var value = "ON,Off"; // todo value from the app config matching key name
            
            if (value.Contains(','))
            {
                var split = value.Split(new[] { ',' });
                foreach (var item in split)
                    result.Add(item);
            } else if(!string.IsNullOrEmpty(value))
            {
                result.Add(value);
            }

            return result.ToArray();
        }
        public static void ComboFillEnums(ComboBox cbo, string signalName, dynamic db, string[] setting = null)
        {
            var signals = db["signals"];
            var values = ARXMLDecode.ParseEnums(signals, signalName);
            foreach (KeyValuePair<string, string> entry in values)
            {
                if (setting != null && setting.Length > 0)
                {
                    if (setting.Contains(entry.Value))
                        cbo.Items.Add(entry.Value + "=" + entry.Key);
                }
                else
                    cbo.Items.Add(entry.Value + "=" + entry.Key);
            }
        }

        public static Adapter.Packet SelectToSend(ComboBox cbo, string signalName)
        {
            var parse = cbo.SelectedItem.ToString().Split('=');
            var value = parse[0].Trim();
            var packet = new Adapter.Packet() { Name = signalName, Value = value };
            return packet;
        }

    }
}
