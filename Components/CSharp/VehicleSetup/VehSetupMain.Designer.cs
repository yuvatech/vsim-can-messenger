﻿namespace VehicleSetup
{
    partial class VehSetupMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.rbKeyIDPresentTrue = new System.Windows.Forms.RadioButton();
            this.rbKeyIDPresentFalse = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.sldBatterySOC = new System.Windows.Forms.TrackBar();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.rbTransShiftLeverLockTrue = new System.Windows.Forms.RadioButton();
            this.rbTransShiftLeverLockFalse = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.rbKeyInWarnIndONTrue = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.rbBatSOCCritLowTrue = new System.Windows.Forms.RadioButton();
            this.rbBatSOCCritLowFalse = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.rbHVChargerStatusConnected = new System.Windows.Forms.RadioButton();
            this.rbHVChargerStatusDisconnected = new System.Windows.Forms.RadioButton();
            this.rbHVChargerStatusNoAction = new System.Windows.Forms.RadioButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsUSBProgStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.sldPowerModeOffBattSOC = new System.Windows.Forms.TrackBar();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.rbBattSOCLIOTrue = new System.Windows.Forms.RadioButton();
            this.rbBattSOCLIOFalse = new System.Windows.Forms.RadioButton();
            this.btnPassiveEntryChallenge = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.chkbxShftLvrLckRqdfalse = new System.Windows.Forms.CheckBox();
            this.chkbxCPIDFC = new System.Windows.Forms.CheckBox();
            this.chkbxWakeUp = new System.Windows.Forms.CheckBox();
            this.chkbxShftLvrLckRqdtrue = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.rbKeyInWarnIndONFalse = new System.Windows.Forms.RadioButton();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldBatterySOC)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel11.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldPowerModeOffBattSOC)).BeginInit();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(16, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(135, 107);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "Engine Run \r\nActive";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.rbKeyIDPresentTrue);
            this.panel2.Controls.Add(this.rbKeyIDPresentFalse);
            this.panel2.Location = new System.Drawing.Point(16, 130);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(135, 107);
            this.panel2.TabIndex = 1;
            this.panel2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 34);
            this.label3.TabIndex = 4;
            this.label3.Text = "Key ID \r\nDevice Present";
            // 
            // rbKeyIDPresentTrue
            // 
            this.rbKeyIDPresentTrue.AutoSize = true;
            this.rbKeyIDPresentTrue.Location = new System.Drawing.Point(4, 73);
            this.rbKeyIDPresentTrue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbKeyIDPresentTrue.Name = "rbKeyIDPresentTrue";
            this.rbKeyIDPresentTrue.Size = new System.Drawing.Size(59, 21);
            this.rbKeyIDPresentTrue.TabIndex = 5;
            this.rbKeyIDPresentTrue.TabStop = true;
            this.rbKeyIDPresentTrue.Text = "True";
            this.rbKeyIDPresentTrue.UseVisualStyleBackColor = true;
            // 
            // rbKeyIDPresentFalse
            // 
            this.rbKeyIDPresentFalse.AutoSize = true;
            this.rbKeyIDPresentFalse.Location = new System.Drawing.Point(4, 44);
            this.rbKeyIDPresentFalse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbKeyIDPresentFalse.Name = "rbKeyIDPresentFalse";
            this.rbKeyIDPresentFalse.Size = new System.Drawing.Size(63, 21);
            this.rbKeyIDPresentFalse.TabIndex = 4;
            this.rbKeyIDPresentFalse.TabStop = true;
            this.rbKeyIDPresentFalse.Text = "False";
            this.rbKeyIDPresentFalse.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.comboBox2);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(160, 15);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(135, 107);
            this.panel3.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 34);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rem Veh Start \r\nEng Running";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.sldBatterySOC);
            this.panel4.Location = new System.Drawing.Point(16, 399);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(573, 110);
            this.panel4.TabIndex = 2;
            this.panel4.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 2);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Battery SOC";
            // 
            // sldBatterySOC
            // 
            this.sldBatterySOC.Location = new System.Drawing.Point(4, 31);
            this.sldBatterySOC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sldBatterySOC.Maximum = 100;
            this.sldBatterySOC.Name = "sldBatterySOC";
            this.sldBatterySOC.Size = new System.Drawing.Size(568, 56);
            this.sldBatterySOC.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.rbTransShiftLeverLockTrue);
            this.panel7.Controls.Add(this.rbTransShiftLeverLockFalse);
            this.panel7.Location = new System.Drawing.Point(304, 246);
            this.panel7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(135, 107);
            this.panel7.TabIndex = 2;
            this.panel7.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 2);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 34);
            this.label7.TabIndex = 12;
            this.label7.Text = "Trans Shift\r\nLever Lock";
            // 
            // rbTransShiftLeverLockTrue
            // 
            this.rbTransShiftLeverLockTrue.AutoSize = true;
            this.rbTransShiftLeverLockTrue.Location = new System.Drawing.Point(4, 73);
            this.rbTransShiftLeverLockTrue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbTransShiftLeverLockTrue.Name = "rbTransShiftLeverLockTrue";
            this.rbTransShiftLeverLockTrue.Size = new System.Drawing.Size(88, 21);
            this.rbTransShiftLeverLockTrue.TabIndex = 13;
            this.rbTransShiftLeverLockTrue.TabStop = true;
            this.rbTransShiftLeverLockTrue.Text = "Unlocked";
            this.rbTransShiftLeverLockTrue.UseVisualStyleBackColor = true;
            // 
            // rbTransShiftLeverLockFalse
            // 
            this.rbTransShiftLeverLockFalse.AutoSize = true;
            this.rbTransShiftLeverLockFalse.Location = new System.Drawing.Point(4, 44);
            this.rbTransShiftLeverLockFalse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbTransShiftLeverLockFalse.Name = "rbTransShiftLeverLockFalse";
            this.rbTransShiftLeverLockFalse.Size = new System.Drawing.Size(75, 21);
            this.rbTransShiftLeverLockFalse.TabIndex = 12;
            this.rbTransShiftLeverLockFalse.TabStop = true;
            this.rbTransShiftLeverLockFalse.Text = "Locked";
            this.rbTransShiftLeverLockFalse.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.comboBox3);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Location = new System.Drawing.Point(304, 130);
            this.panel8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(135, 107);
            this.panel8.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 2);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 34);
            this.label6.TabIndex = 10;
            this.label6.Text = "Park Break \r\nSwit Act";
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.label5);
            this.panel9.Controls.Add(this.rbKeyInWarnIndONTrue);
            this.panel9.Controls.Add(this.rbKeyInWarnIndONFalse);
            this.panel9.Location = new System.Drawing.Point(304, 15);
            this.panel9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(135, 107);
            this.panel9.TabIndex = 2;
            this.panel9.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 2);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 34);
            this.label5.TabIndex = 8;
            this.label5.Text = "Key In Warn\r\nIndicator On";
            // 
            // rbKeyInWarnIndONTrue
            // 
            this.rbKeyInWarnIndONTrue.AutoSize = true;
            this.rbKeyInWarnIndONTrue.Location = new System.Drawing.Point(4, 73);
            this.rbKeyInWarnIndONTrue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbKeyInWarnIndONTrue.Name = "rbKeyInWarnIndONTrue";
            this.rbKeyInWarnIndONTrue.Size = new System.Drawing.Size(59, 21);
            this.rbKeyInWarnIndONTrue.TabIndex = 9;
            this.rbKeyInWarnIndONTrue.TabStop = true;
            this.rbKeyInWarnIndONTrue.Text = "True";
            this.rbKeyInWarnIndONTrue.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.rbBatSOCCritLowTrue);
            this.panel10.Controls.Add(this.rbBatSOCCritLowFalse);
            this.panel10.Location = new System.Drawing.Point(160, 130);
            this.panel10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(135, 107);
            this.panel10.TabIndex = 2;
            this.panel10.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 34);
            this.label4.TabIndex = 6;
            this.label4.Text = "Batt SOC \r\nCrit Low";
            // 
            // rbBatSOCCritLowTrue
            // 
            this.rbBatSOCCritLowTrue.AutoSize = true;
            this.rbBatSOCCritLowTrue.Location = new System.Drawing.Point(4, 73);
            this.rbBatSOCCritLowTrue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbBatSOCCritLowTrue.Name = "rbBatSOCCritLowTrue";
            this.rbBatSOCCritLowTrue.Size = new System.Drawing.Size(59, 21);
            this.rbBatSOCCritLowTrue.TabIndex = 7;
            this.rbBatSOCCritLowTrue.TabStop = true;
            this.rbBatSOCCritLowTrue.Text = "True";
            this.rbBatSOCCritLowTrue.UseVisualStyleBackColor = true;
            // 
            // rbBatSOCCritLowFalse
            // 
            this.rbBatSOCCritLowFalse.AutoSize = true;
            this.rbBatSOCCritLowFalse.Location = new System.Drawing.Point(4, 44);
            this.rbBatSOCCritLowFalse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbBatSOCCritLowFalse.Name = "rbBatSOCCritLowFalse";
            this.rbBatSOCCritLowFalse.Size = new System.Drawing.Size(63, 21);
            this.rbBatSOCCritLowFalse.TabIndex = 6;
            this.rbBatSOCCritLowFalse.TabStop = true;
            this.rbBatSOCCritLowFalse.Text = "False";
            this.rbBatSOCCritLowFalse.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.rbHVChargerStatusConnected);
            this.panel6.Controls.Add(this.rbHVChargerStatusDisconnected);
            this.panel6.Controls.Add(this.rbHVChargerStatusNoAction);
            this.panel6.Location = new System.Drawing.Point(16, 246);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(135, 137);
            this.panel6.TabIndex = 3;
            this.panel6.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 2);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 34);
            this.label9.TabIndex = 15;
            this.label9.Text = "HV Charger\r\nCoupler Status";
            // 
            // rbHVChargerStatusConnected
            // 
            this.rbHVChargerStatusConnected.AutoSize = true;
            this.rbHVChargerStatusConnected.Location = new System.Drawing.Point(4, 101);
            this.rbHVChargerStatusConnected.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbHVChargerStatusConnected.Name = "rbHVChargerStatusConnected";
            this.rbHVChargerStatusConnected.Size = new System.Drawing.Size(97, 21);
            this.rbHVChargerStatusConnected.TabIndex = 20;
            this.rbHVChargerStatusConnected.TabStop = true;
            this.rbHVChargerStatusConnected.Text = "Connected";
            this.rbHVChargerStatusConnected.UseVisualStyleBackColor = true;
            // 
            // rbHVChargerStatusDisconnected
            // 
            this.rbHVChargerStatusDisconnected.AutoSize = true;
            this.rbHVChargerStatusDisconnected.Location = new System.Drawing.Point(4, 73);
            this.rbHVChargerStatusDisconnected.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbHVChargerStatusDisconnected.Name = "rbHVChargerStatusDisconnected";
            this.rbHVChargerStatusDisconnected.Size = new System.Drawing.Size(115, 21);
            this.rbHVChargerStatusDisconnected.TabIndex = 19;
            this.rbHVChargerStatusDisconnected.TabStop = true;
            this.rbHVChargerStatusDisconnected.Text = "Disconnected";
            this.rbHVChargerStatusDisconnected.UseVisualStyleBackColor = true;
            // 
            // rbHVChargerStatusNoAction
            // 
            this.rbHVChargerStatusNoAction.AutoSize = true;
            this.rbHVChargerStatusNoAction.Location = new System.Drawing.Point(4, 44);
            this.rbHVChargerStatusNoAction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbHVChargerStatusNoAction.Name = "rbHVChargerStatusNoAction";
            this.rbHVChargerStatusNoAction.Size = new System.Drawing.Size(90, 21);
            this.rbHVChargerStatusNoAction.TabIndex = 18;
            this.rbHVChargerStatusNoAction.TabStop = true;
            this.rbHVChargerStatusNoAction.Text = "No Action";
            this.rbHVChargerStatusNoAction.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.comboBox4);
            this.panel11.Controls.Add(this.label10);
            this.panel11.Location = new System.Drawing.Point(160, 246);
            this.panel11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(135, 96);
            this.panel11.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 2);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 34);
            this.label10.TabIndex = 21;
            this.label10.Text = "Electric Park\r\nBrake Status";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsUSBProgStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 643);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(607, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsUSBProgStatus
            // 
            this.tsUSBProgStatus.AutoSize = false;
            this.tsUSBProgStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tsUSBProgStatus.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.tsUSBProgStatus.Name = "tsUSBProgStatus";
            this.tsUSBProgStatus.Size = new System.Drawing.Size(453, 17);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.sldPowerModeOffBattSOC);
            this.panel5.Location = new System.Drawing.Point(16, 517);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(573, 110);
            this.panel5.TabIndex = 15;
            this.panel5.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 2);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(212, 17);
            this.label11.TabIndex = 14;
            this.label11.Text = "Power Mode OffBattery SOC";
            // 
            // sldPowerModeOffBattSOC
            // 
            this.sldPowerModeOffBattSOC.Location = new System.Drawing.Point(4, 31);
            this.sldPowerModeOffBattSOC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sldPowerModeOffBattSOC.Maximum = 100;
            this.sldPowerModeOffBattSOC.Name = "sldPowerModeOffBattSOC";
            this.sldPowerModeOffBattSOC.Size = new System.Drawing.Size(565, 56);
            this.sldPowerModeOffBattSOC.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel12.Controls.Add(this.label12);
            this.panel12.Controls.Add(this.rbBattSOCLIOTrue);
            this.panel12.Controls.Add(this.rbBattSOCLIOFalse);
            this.panel12.Location = new System.Drawing.Point(448, 15);
            this.panel12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(141, 107);
            this.panel12.TabIndex = 14;
            this.panel12.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 2);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 34);
            this.label12.TabIndex = 12;
            this.label12.Text = "Batt SOC Low\r\nIndication On";
            // 
            // rbBattSOCLIOTrue
            // 
            this.rbBattSOCLIOTrue.AutoSize = true;
            this.rbBattSOCLIOTrue.Location = new System.Drawing.Point(4, 73);
            this.rbBattSOCLIOTrue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbBattSOCLIOTrue.Name = "rbBattSOCLIOTrue";
            this.rbBattSOCLIOTrue.Size = new System.Drawing.Size(59, 21);
            this.rbBattSOCLIOTrue.TabIndex = 13;
            this.rbBattSOCLIOTrue.TabStop = true;
            this.rbBattSOCLIOTrue.Text = "True";
            this.rbBattSOCLIOTrue.UseVisualStyleBackColor = true;
            // 
            // rbBattSOCLIOFalse
            // 
            this.rbBattSOCLIOFalse.AutoSize = true;
            this.rbBattSOCLIOFalse.Location = new System.Drawing.Point(4, 44);
            this.rbBattSOCLIOFalse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbBattSOCLIOFalse.Name = "rbBattSOCLIOFalse";
            this.rbBattSOCLIOFalse.Size = new System.Drawing.Size(63, 21);
            this.rbBattSOCLIOFalse.TabIndex = 12;
            this.rbBattSOCLIOFalse.TabStop = true;
            this.rbBattSOCLIOFalse.Text = "False";
            this.rbBattSOCLIOFalse.UseVisualStyleBackColor = true;
            // 
            // btnPassiveEntryChallenge
            // 
            this.btnPassiveEntryChallenge.Location = new System.Drawing.Point(16, 41);
            this.btnPassiveEntryChallenge.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPassiveEntryChallenge.Name = "btnPassiveEntryChallenge";
            this.btnPassiveEntryChallenge.Size = new System.Drawing.Size(100, 28);
            this.btnPassiveEntryChallenge.TabIndex = 16;
            this.btnPassiveEntryChallenge.Text = "Send";
            this.btnPassiveEntryChallenge.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Controls.Add(this.label13);
            this.panel13.Controls.Add(this.btnPassiveEntryChallenge);
            this.panel13.Location = new System.Drawing.Point(448, 130);
            this.panel13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(141, 78);
            this.panel13.TabIndex = 15;
            this.panel13.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 2);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 34);
            this.label13.TabIndex = 12;
            this.label13.Text = "Send Passive \r\nEntry Challenge";
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel14.Controls.Add(this.chkbxShftLvrLckRqdfalse);
            this.panel14.Controls.Add(this.chkbxCPIDFC);
            this.panel14.Controls.Add(this.chkbxWakeUp);
            this.panel14.Controls.Add(this.chkbxShftLvrLckRqdtrue);
            this.panel14.Controls.Add(this.label14);
            this.panel14.Location = new System.Drawing.Point(448, 217);
            this.panel14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(141, 166);
            this.panel14.TabIndex = 17;
            this.panel14.Visible = false;
            // 
            // chkbxShftLvrLckRqdfalse
            // 
            this.chkbxShftLvrLckRqdfalse.AutoSize = true;
            this.chkbxShftLvrLckRqdfalse.Checked = true;
            this.chkbxShftLvrLckRqdfalse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxShftLvrLckRqdfalse.Location = new System.Drawing.Point(3, 66);
            this.chkbxShftLvrLckRqdfalse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkbxShftLvrLckRqdfalse.Name = "chkbxShftLvrLckRqdfalse";
            this.chkbxShftLvrLckRqdfalse.Size = new System.Drawing.Size(136, 38);
            this.chkbxShftLvrLckRqdfalse.TabIndex = 16;
            this.chkbxShftLvrLckRqdfalse.Text = "Disp Trans Shf\r\nLver Lock (false)";
            this.chkbxShftLvrLckRqdfalse.UseVisualStyleBackColor = true;
            // 
            // chkbxCPIDFC
            // 
            this.chkbxCPIDFC.AutoSize = true;
            this.chkbxCPIDFC.Checked = true;
            this.chkbxCPIDFC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxCPIDFC.Location = new System.Drawing.Point(3, 134);
            this.chkbxCPIDFC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkbxCPIDFC.Name = "chkbxCPIDFC";
            this.chkbxCPIDFC.Size = new System.Drawing.Size(123, 21);
            this.chkbxCPIDFC.TabIndex = 15;
            this.chkbxCPIDFC.Text = "BCM CPID $FC";
            this.chkbxCPIDFC.UseVisualStyleBackColor = true;
            // 
            // chkbxWakeUp
            // 
            this.chkbxWakeUp.AutoSize = true;
            this.chkbxWakeUp.Checked = true;
            this.chkbxWakeUp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxWakeUp.Location = new System.Drawing.Point(3, 106);
            this.chkbxWakeUp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkbxWakeUp.Name = "chkbxWakeUp";
            this.chkbxWakeUp.Size = new System.Drawing.Size(88, 21);
            this.chkbxWakeUp.TabIndex = 14;
            this.chkbxWakeUp.Text = "Wake Up";
            this.chkbxWakeUp.UseVisualStyleBackColor = true;
            // 
            // chkbxShftLvrLckRqdtrue
            // 
            this.chkbxShftLvrLckRqdtrue.AutoSize = true;
            this.chkbxShftLvrLckRqdtrue.Checked = true;
            this.chkbxShftLvrLckRqdtrue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxShftLvrLckRqdtrue.Location = new System.Drawing.Point(3, 32);
            this.chkbxShftLvrLckRqdtrue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkbxShftLvrLckRqdtrue.Name = "chkbxShftLvrLckRqdtrue";
            this.chkbxShftLvrLckRqdtrue.Size = new System.Drawing.Size(131, 38);
            this.chkbxShftLvrLckRqdtrue.TabIndex = 13;
            this.chkbxShftLvrLckRqdtrue.Text = "Disp Trans Shf\r\nLver Lock (true)";
            this.chkbxShftLvrLckRqdtrue.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(0, 1);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 34);
            this.label14.TabIndex = 12;
            this.label14.Text = "BCM Auto\r\nResponse Enable";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 56);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(7, 56);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 24);
            this.comboBox2.TabIndex = 4;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // rbKeyInWarnIndONFalse
            // 
            this.rbKeyInWarnIndONFalse.AutoSize = true;
            this.rbKeyInWarnIndONFalse.Location = new System.Drawing.Point(4, 44);
            this.rbKeyInWarnIndONFalse.Margin = new System.Windows.Forms.Padding(4);
            this.rbKeyInWarnIndONFalse.Name = "rbKeyInWarnIndONFalse";
            this.rbKeyInWarnIndONFalse.Size = new System.Drawing.Size(63, 21);
            this.rbKeyInWarnIndONFalse.TabIndex = 8;
            this.rbKeyInWarnIndONFalse.TabStop = true;
            this.rbKeyInWarnIndONFalse.Text = "False";
            this.rbKeyInWarnIndONFalse.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 50);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 24);
            this.comboBox3.TabIndex = 11;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(6, 51);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 24);
            this.comboBox4.TabIndex = 22;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // VehSetupMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(607, 665);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "VehSetupMain";
            this.Text = "Vehicle Setup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VehSetupMain_FormClosing);
            this.Load += new System.EventHandler(this.VehSetupMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldBatterySOC)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldPowerModeOffBattSOC)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel11;
        public System.Windows.Forms.RadioButton rbKeyIDPresentTrue;
        public System.Windows.Forms.RadioButton rbKeyIDPresentFalse;
        public System.Windows.Forms.RadioButton rbTransShiftLeverLockTrue;
        public System.Windows.Forms.RadioButton rbTransShiftLeverLockFalse;
        public System.Windows.Forms.RadioButton rbKeyInWarnIndONTrue;
        public System.Windows.Forms.RadioButton rbBatSOCCritLowTrue;
        public System.Windows.Forms.RadioButton rbBatSOCCritLowFalse;
        public System.Windows.Forms.RadioButton rbHVChargerStatusConnected;
        public System.Windows.Forms.RadioButton rbHVChargerStatusDisconnected;
        public System.Windows.Forms.RadioButton rbHVChargerStatusNoAction;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TrackBar sldBatterySOC;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel tsUSBProgStatus;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TrackBar sldPowerModeOffBattSOC;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.RadioButton rbBattSOCLIOTrue;
        public System.Windows.Forms.RadioButton rbBattSOCLIOFalse;
        public System.Windows.Forms.Button btnPassiveEntryChallenge;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.CheckBox chkbxShftLvrLckRqdtrue;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.CheckBox chkbxCPIDFC;
        public System.Windows.Forms.CheckBox chkbxWakeUp;
        public System.Windows.Forms.CheckBox chkbxShftLvrLckRqdfalse;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        public System.Windows.Forms.RadioButton rbKeyInWarnIndONFalse;
        private System.Windows.Forms.ComboBox comboBox4;
    }
}

