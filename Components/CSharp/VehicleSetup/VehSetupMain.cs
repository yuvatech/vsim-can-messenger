﻿using Extension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VehicleSetup
{
    public partial class VehSetupMain : Form
    {
        private readonly Adapter _adapter = null;
        private readonly dynamic _db = Adapter.Database();

        public VehSetupMain()
        {
            InitializeComponent();

            _adapter = new Adapter();
            _adapter.Open(false);
            // _adapter.OnReceived += AdapterOnReceived;
        }

        private void VehSetupMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void VehSetupMain_Load(object sender, EventArgs e)
        {
            ComboFillEnums(comboBox1, "PSP_PrplSysActvAuth");
            ComboFillEnums(comboBox2, "PSP_RmtVehStrtEngRnngAuth");
            ComboFillEnums(comboBox3, "PrkBrkSwAtv");
            ComboFillEnums(comboBox4, "EPBSP_ElecPrkBrkApplStsAuth");
        }

        #region Winform Helpers
        private void ComboFillEnums(ComboBox cbo, string signalName)
        {
            Utility.FormHelper.ComboFillEnums(cbo, signalName, _db);
        }

        private void SelectAndSend(ComboBox cbo, string signalName)
        {
            _adapter.Send(Utility.FormHelper.SelectToSend(cbo, signalName));
        }
        #endregion

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {   
            SelectAndSend(comboBox1, "PSP_PrplSysActvAuth");
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectAndSend(comboBox2, "PSP_RmtVehStrtEngRnngAuth");
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectAndSend(comboBox3, "PrkBrkSwAtv");
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectAndSend(comboBox3, "EPBSP_ElecPrkBrkApplStsAuth");
        }
    }
}
