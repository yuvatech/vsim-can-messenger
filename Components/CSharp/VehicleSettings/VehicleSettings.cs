﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Extension;

namespace VehicleSettings
{
    public partial class VehicleSettings : Form
    {
        private Adapter _adapter = null;
        private dynamic _db = Adapter.Database();
        private List<ComboCheck> _comboChecks = new List<ComboCheck>();
        private Task _task = null;

        public VehicleSettings()
        {
            InitializeComponent();
            _adapter = new Adapter();
            _adapter.Open(false);

            _task = new Task(() => { PeriodicHandler(); });            
        }

        private string FindMessageName(string signalName)
        {
            var db = _db;
            var signals = db["signals"];
            var messages = db["messages"];
            var messageName = "";
            foreach(var item in signals)
            {
                if (item.Name == signalName)
                {
                    messageName = item.Value["canframe"];
                    break;
                }
            }

            var period = 0f;
            foreach(var item in messages)
            {
                if(item.Name == messageName)
                {
                    period = Convert.ToSingle(item.Value["timeperiod"]);
                    break;
                }
            }

            return messageName;
        }

        private float FindMessagePeriod(string messageName)
        {
            var db = _db;
            var signals = db["signals"];
            var messages = db["messages"];
            var period = 0f;
            foreach (var item in messages)
            {
                if (item.Name == messageName)
                {
                    period = Convert.ToSingle(item.Value["timeperiod"]);
                    break;
                }
            }

            return period;
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            switch (e.Index)
            {
                case 0:
                    e.Graphics.FillRectangle(new SolidBrush(Color.DarkSeaGreen), e.Bounds);
                    break;
                case 1:
                    e.Graphics.FillRectangle(new SolidBrush(Color.SeaShell), e.Bounds);
                    break;
                case 2:
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightSkyBlue), e.Bounds);
                    break;
                case 3:
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightGreen), e.Bounds);
                    break;
                case 4:
                    e.Graphics.FillRectangle(new SolidBrush(Color.Silver), e.Bounds);
                    break;
                case 5:
                    e.Graphics.FillRectangle(new SolidBrush(Color.Beige), e.Bounds);
                    break;
                case 6:
                    e.Graphics.FillRectangle(new SolidBrush(Color.DarkSeaGreen), e.Bounds);
                    break;
                case 7:
                    e.Graphics.FillRectangle(new SolidBrush(Color.PaleTurquoise), e.Bounds);
                    break;
                case 8:
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightSteelBlue), e.Bounds);
                    break;
                case 9:
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightCyan), e.Bounds);
                    break;
                case 10:
                    e.Graphics.FillRectangle(new SolidBrush(Color.DarkSeaGreen), e.Bounds);
                    break;
                case 11:
                    e.Graphics.FillRectangle(new SolidBrush(Color.MintCream), e.Bounds);
                    break;
                case 12:
                    e.Graphics.FillRectangle(new SolidBrush(Color.AntiqueWhite), e.Bounds);
                    break;
                case 13:
                    e.Graphics.FillRectangle(new SolidBrush(Color.LemonChiffon), e.Bounds);
                    break;
                default:
                    break;
            }

            // Then draw the current tab button text 
            Rectangle paddedBounds = e.Bounds;
            paddedBounds.Inflate(-3, -3);
            e.Graphics.DrawString(tabControl1.TabPages[e.Index].Text, this.Font,SystemBrushes.WindowText, paddedBounds);

        }


        private void VehicleSettings_Load(object sender, EventArgs e)
        {
            tabRearSeatReminder_Enter(sender, e);

            #region Climate and Air Quality
            _comboChecks.Add(new ComboCheck("AirQltySnsCstCrStVal", cbAirQltySnsCstCrStVal, chbAirQltySnsCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("AirQltySnsCstStAvl", cbAirQltySnsCstStAvl, chbAirQltySnsCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AirQltySnsCstAvlFlg1", cbAirQltySnsCstAvlFlg1, chbAirQltySnsCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AirQltySnsCstAvlFlg2", cbAirQltySnsCstAvlFlg2, chbAirQltySnsCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("AirQltySnsCstAvlFlg3", cbAirQltySnsCstAvlFlg3, chbAirQltySnsCstAvlFlg3, _db));

            _comboChecks.Add(new ComboCheck("AutoAirDstCstCrStVal", cbAutoAirDstCstCrStVal, chbAutoAirDstCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("AutoAirDstCstStAvl", cbAutoAirDstCstStAvl, chbAutoAirDstCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutoAirDstCstAvlFlg1", cbAutoAirDstCstAvlFlg1, chbAutoAirDstCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutoAirDstCstAvlFlg2", cbAutoAirDstCstAvlFlg2, chbAutoAirDstCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("AutoAirDstCstAvlFlg3", cbAutoAirDstCstAvlFlg3, chbAutoAirDstCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("AutoAirDstCstAvlFlg4", cbAutoAirDstCstAvlFlg4, chbAutoAirDstCstAvlFlg4, _db));

            _comboChecks.Add(new ComboCheck("AutoCoolStCstCrStVal", cbAutoCoolStCstCrStVal, chbAutoCoolStCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("AutoCoolStCstStAvl", cbAutoCoolStCstStAvl, chbAutoCoolStCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutoCoolStCstAvlFlg1", cbAutoCoolStCstAvlFlg1, chbAutoCoolStCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutoCoolStCstAvlFlg2", cbAutoCoolStCstAvlFlg2, chbAutoCoolStCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("AutoDfgCstCrStVal", cbAutoDfgCstCrStVal, chbAutoDfgCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("AutoDfgCstStAvl", cbAutoDfgCstStAvl, chbAutoDfgCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutoDfgCstAvlFlg1", cbAutoDfgCstAvlFlg1, chbAutoDfgCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutoDfgCstAvlFlg2", cbAutoDfgCstAvlFlg2, chbAutoDfgCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("AutoFanCstCrStVal", cbAutoFanCstCrStVal, chbAutoFanCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("AutoFanCstStAvl", cbAutoFanCstStAvl, chbAutoFanCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutoFanCstAvlFlg1", cbAutoFanCstAvlFlg1, chbAutoFanCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutoFanCstAvlFlg2", cbAutoFanCstAvlFlg2, chbAutoFanCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("AutoFanCstAvlFlg3", cbAutoFanCstAvlFlg3, chbAutoFanCstAvlFlg3, _db));

            _comboChecks.Add(new ComboCheck("AutoHtdStCstCrStVal", cbAutoHtdStCstCrStVal, chbAutoHtdStCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("AutoHtdStCstStAvl", cbAutoHtdStCstStAvl, chbAutoHtdStCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutoHtdStCstAvlFlg1", cbAutoHtdStCstAvlFlg1, chbAutoHtdStCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutoHtdStCstAvlFlg2", cbAutoHtdStCstAvlFlg2, chbAutoHtdStCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("RrDfgStUpCstCrStVal", cbRrDfgStUpCstCrStVal, chbRrDfgStUpCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RrDfgStUpCstStAvl", cbRrDfgStUpCstStAvl, chbRrDfgStUpCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RrDfgStUpCstAvlFlg1", cbRrDfgStUpCstAvlFlg1, chbRrDfgStUpCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RrDfgStUpCstAvlFlg2", cbRrDfgStUpCstAvlFlg2, chbRrDfgStUpCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("IonzrCtrlCstCrStVal", cbIonzrCtrlCstCrStVal, chbIonzrCtrlCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("IonzrCtrlCstStAvl", cbIonzrCtrlCstStAvl, chbIonzrCtrlCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("IonzrCtrlCstAvlFlg1", cbIonzrCtrlCstAvlFlg1, chbIonzrCtrlCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("IonzrCtrlCstAvlFlg2", cbIonzrCtrlCstAvlFlg2, chbIonzrCtrlCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("PlltnCtrlCstCrStVal", cbPlltnCtrlCstCrStVal, chbPlltnCtrlCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("PlltnCtrlCstStAvl", cbPlltnCtrlCstStAvl, chbPlltnCtrlCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("PlltnCtrlCstAvlFlg1", cbPlltnCtrlCstAvlFlg1, chbPlltnCtrlCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("PlltnCtrlCstAvlFlg2", cbPlltnCtrlCstAvlFlg2, chbPlltnCtrlCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("RrZnStUpCstCrStVal", cbRrZnStUpCstCrStVal, chbRrZnStUpCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RrZnStUpCstStAvl", cbRrZnStUpCstStAvl, chbRrZnStUpCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RrZnStUpCstAvlFlg1", cbRrZnStUpCstAvlFlg1, chbRrZnStUpCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RrZnStUpCstAvlFlg2", cbRrZnStUpCstAvlFlg2, chbRrZnStUpCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("RrZnStUpCstAvlFlg3", cbRrZnStUpCstAvlFlg3, chbRrZnStUpCstAvlFlg3, _db));




            #endregion

            #region Comfort and Convenience

            _comboChecks.Add(new ComboCheck("AutoRrWpCstCurSetVal", cbAutoRrWpCstCurSetVal, chbAutoRrWpCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("AutoRrWpCstStAvl", cbAutoRrWpCstStAvl, chbAutoRrWpCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutoRrWpCstAvlFlg1", cbAutoRrWpCstAvlFlg1, chbAutoRrWpCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutoRrWpCstAvlFlg2", cbAutoRrWpCstAvlFlg2, chbAutoRrWpCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstCurrSetVal", cbcLvlgSysEntEgrsMdCstCurrSetVal, chbLvlgSysEntEgrsMdCstCurrSetVal, _db));
            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstSetAval", cbcLvlgSysEntEgrsMdCstSetAval, chbLvlgSysEntEgrsMdCstSetAval, _db));
            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstAvlFlg1", cbcLvlgSysEntEgrsMdCstAvlFlg1, chbLvlgSysEntEgrsMdCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstAvlFlg2", cbcLvlgSysEntEgrsMdCstAvlFlg2, chbLvlgSysEntEgrsMdCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("ExtdHillStrtAstCustCurSetVal", cbExtdHillStrtAstCustCurSetVal, chbExtdHillStrtAstCustCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("ExtdHillStrtAstCustSetAvl", cbExtdHillStrtAstCustSetAvl, chbExtdHillStrtAstCustSetAvl, _db));
            _comboChecks.Add(new ComboCheck("ExtdHillStrtAstCustAvlFlg1", cbExtdHillStrtAstCustAvlFlg1, chbExtdHillStrtAstCustAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("ExtdHillStrtAstCustAvlFlg2", cbExtdHillStrtAstCustAvlFlg2, chbExtdHillStrtAstCustAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("HFRCCstCurSetVal", cbHFRCCstCurSetVal, chbHFRCCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("HFRCCstStAvl", cbHFRCCstStAvl, chbHFRCCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("HFRCCstAvlFlg1", cbHFRCCstAvlFlg1, chbHFRCCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("HFRCCstAvlFlg2", cbHFRCCstAvlFlg2, chbHFRCCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("HFRCCstAvlFlg3", cbHFRCCstAvlFlg3, chbHFRCCstAvlFlg3, _db));

            _comboChecks.Add(new ComboCheck("PwrRrClsrCstCurrSetVal", cbPwrRrClsrCstCurrSetVal, chbPwrRrClsrCstCurrSetVal, _db));
            _comboChecks.Add(new ComboCheck("PwrRrClsrCstSetAval", cbPwrRrClsrCstSetAval, chbPwrRrClsrCstSetAval, _db));
            _comboChecks.Add(new ComboCheck("PwrRrClsrCstAvlFlg1", cbPwrRrClsrCstAvlFlg1, chbPwrRrClsrCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("PwrRrClsrCstAvlFlg2", cbPwrRrClsrCstAvlFlg2, chbPwrRrClsrCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("PwrRrClsrCstAvlFlg3", cbPwrRrClsrCstAvlFlg3, chbPwrRrClsrCstAvlFlg3, _db));

            _comboChecks.Add(new ComboCheck("RnSnsCstCurSetVal", cbRnSnsCstCurSetVal, chbRnSnsCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RnSnsCstStAvl", cbRnSnsCstStAvl, chbRnSnsCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RnSnsCstAvlFlg1", cbRnSnsCstAvlFlg1, chbRnSnsCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RnSnsCstAvlFlg2", cbRnSnsCstAvlFlg2, chbRnSnsCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("AutMirFldCstCurSetVal", cbAutMirFldCstCurSetVal, chbAutMirFldCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("AutMirFldCstStAvl", cbAutMirFldCstStAvl, chbAutMirFldCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutMirFldCstAvlFlg1", cbAutMirFldCstAvlFlg1, chbAutMirFldCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutMirFldCstAvlFlg2", cbAutMirFldCstAvlFlg2, chbAutMirFldCstAvlFlg2, _db));

            _comboChecks.Add(new ComboCheck("RevTltMirCstCurSetVal", cbRevTltMirCstCurSetVal, chbRevTltMirCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RevTltMirCstStAvl", cbRevTltMirCstStAvl, chbRevTltMirCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RevTltMirCstAvlFlg1", cbRevTltMirCstAvlFlg1, chbRevTltMirCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RevTltMirCstAvlFlg2", cbRevTltMirCstAvlFlg2, chbRevTltMirCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("RevTltMirCstAvlFlg3", cbRevTltMirCstAvlFlg3, chbRevTltMirCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("RevTltMirCstAvlFlg4", cbRevTltMirCstAvlFlg4, chbRevTltMirCstAvlFlg4, _db));

            #endregion

            #region Collision Detection System
            //Adaptive Cruise
            _comboChecks.Add(new ComboCheck("GoNotfrCstCrStVal", cbGoNotfrCstCrStVal, chbGoNotfrCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("GoNotfrCstStAvl", cbGoNotfrCstStAvl, chbGoNotfrCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("GoNotfrCstAvlFlg1", cbGoNotfrCstAvlFlg1, chbGoNotfrCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("GoNotfrCstAvlFlg2", cbGoNotfrCstAvlFlg2, chbGoNotfrCstAvlFlg2, _db));
            //Alert Type
            _comboChecks.Add(new ComboCheck("HSP_DrvAlrtTypCstCurrSetValAuth", cbHSP_DrvAlrtTypCstCurrSetValAuth, chbHSP_DrvAlrtTypCstCurrSetValAuth, _db));
            _comboChecks.Add(new ComboCheck("DrvrAlrtTypCstStAvl", cbDrvrAlrtTypCstStAvl, chbDrvrAlrtTypCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("DrvrAlrtTypCstAvlFlg1", cbDrvrAlrtTypCstAvlFlg1, chbDrvrAlrtTypCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("DrvrAlrtTypCstAvlFlg2", cbDrvrAlrtTypCstAvlFlg2, chbDrvrAlrtTypCstAvlFlg2, _db));
            //Forward Collision Systems
            _comboChecks.Add(new ComboCheck("FrwdImpMtgnOpSlctCstCrStVal", cbFrwdImpMtgnOpSlctCstCrStVal, chbFrwdImpMtgnOpSlctCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("FrwdImpMtgnOpSlctCstStAvl", cbFrwdImpMtgnOpSlctCstStAvl, chbFrwdImpMtgnOpSlctCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("FrwdImpMtgnOpSlctCstAvlFlg1", cbFrwdImpMtgnOpSlctCstAvlFlg1, chbFrwdImpMtgnOpSlctCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("FrwdImpMtgnOpSlctCstAvlFlg2", cbFrwdImpMtgnOpSlctCstAvlFlg2, chbFrwdImpMtgnOpSlctCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("FrwdImpMtgnOpSlctCstAvlFlg3", cbFrwdImpMtgnOpSlctCstAvlFlg3, chbFrwdImpMtgnOpSlctCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("FrwdImpMtgnOpSlctCstAvlFlg4", cbFrwdImpMtgnOpSlctCstAvlFlg4, chbFrwdImpMtgnOpSlctCstAvlFlg4, _db));
            //Front Pedestrian Detection
            _comboChecks.Add(new ComboCheck("FrntPCMRspTypCstCrStVal", cbFrntPCMRspTypCstCrStVal, chbFrntPCMRspTypCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("FrntPCMRspTypCstStAvl", cbFrntPCMRspTypCstStAvl, chbFrntPCMRspTypCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("FrntPCMRspTypCstAvlFlg1", cbFrntPCMRspTypCstAvlFlg1, chbFrntPCMRspTypCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("FrntPCMRspTypCstAvlFlg2", cbFrntPCMRspTypCstAvlFlg2, chbFrntPCMRspTypCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("FrntPCMRspTypCstAvlFlg3", cbFrntPCMRspTypCstAvlFlg3, chbFrntPCMRspTypCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("FrntPCMRspTypCstAvlFlg4", cbFrntPCMRspTypCstAvlFlg4, chbFrntPCMRspTypCstAvlFlg4, _db));
            //Intersection Stop Alert
            _comboChecks.Add(new ComboCheck("TrffcLgtAsstCstCurrSetVal", cbTrffcLgtAsstCstCurrSetVal, chbTrffcLgtAsstCstCurrSetVal, _db));
            _comboChecks.Add(new ComboCheck("TrffcLgtAsstCstSetAval", cbTrffcLgtAsstCstSetAval, chbTrffcLgtAsstCstSetAval, _db));
            _comboChecks.Add(new ComboCheck("TrffcLgtAsstCstAvlFlg1", cbTrffcLgtAsstCstAvlFlg1, chbTrffcLgtAsstCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("TrffcLgtAsstCstAvlFlg2", cbTrffcLgtAsstCstAvlFlg2, chbTrffcLgtAsstCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("TrffcLgtAsstCstAvlFlg3", cbTrffcLgtAsstCstAvlFlg3, chbTrffcLgtAsstCstAvlFlg3, _db));
            //Lane Change Alert
            _comboChecks.Add(new ComboCheck("LnChgAlrtCstCrStVal", cbLnChgAlrtCstCrStVal, chbLnChgAlrtCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("LnChgAlrtCstStAvl", cbLnChgAlrtCstStAvl, chbLnChgAlrtCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("LnChgAlrtCstAvlFlg1", cbLnChgAlrtCstAvlFlg1, chbLnChgAlrtCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("LnChgAlrtCstAvlFlg2", cbLnChgAlrtCstAvlFlg2, chbLnChgAlrtCstAvlFlg2, _db));
            //Park Assist
            _comboChecks.Add(new ComboCheck("PrkAsstONOFFCstCrStVal", cbPrkAsstONOFFCstCrStVal, chbPrkAsstONOFFCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstONOFFCstStAvl", cbPrkAsstONOFFCstStAvl, chbPrkAsstONOFFCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstONOFFCstAvlFlg1", cbPrkAsstONOFFCstAvlFlg1, chbPrkAsstONOFFCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstONOFFCstAvlFlg2", cbPrkAsstONOFFCstAvlFlg2, chbPrkAsstONOFFCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstONOFFCstAvlFlg3", cbPrkAsstONOFFCstAvlFlg3, chbPrkAsstONOFFCstAvlFlg3, _db));
            //Park Assist Towbar
            _comboChecks.Add(new ComboCheck("PrkAsstTrHtchPsntCstCrStVal", cbPrkAsstTrHtchPsntCstCrStVal, chbPrkAsstTrHtchPsntCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstTrHtchPsntCstStAvl", cbPrkAsstTrHtchPsntCstStAvl, chbPrkAsstTrHtchPsntCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstTrHtchPsntCstAvlFlg1", cbPrkAsstTrHtchPsntCstAvlFlg1, chbPrkAsstTrHtchPsntCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("PrkAsstTrHtchPsntCstAvlFlg2", cbPrkAsstTrHtchPsntCstAvlFlg2, chbPrkAsstTrHtchPsntCstAvlFlg2, _db));
            //Rear Camera Park Assist
            _comboChecks.Add(new ComboCheck("RrVwPrkAstSymCsCrStVal", cbRrVwPrkAstSymCsCrStVal, chbRrVwPrkAstSymCsCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RrVwPrkAstSymCsStAvl", cbRrVwPrkAstSymCsStAvl, chbRrVwPrkAstSymCsStAvl, _db));
            _comboChecks.Add(new ComboCheck("RrVwPrkAstSymCsAvlFlg1", cbRrVwPrkAstSymCsAvlFlg1, chbRrVwPrkAstSymCsAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RrVwPrkAstSymCsAvlFlg2", cbRrVwPrkAstSymCsAvlFlg2, chbRrVwPrkAstSymCsAvlFlg2, _db));
            //Rear Cross Traffic
            _comboChecks.Add(new ComboCheck("RrCrsTrfcAlrtCstCrStVal", cbRrCrsTrfcAlrtCstCrStVal, chbRrCrsTrfcAlrtCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RrCrsTrfcAlrtCstStAvl", cbRrCrsTrfcAlrtCstStAvl, chbRrCrsTrfcAlrtCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RrCrsTrfcAlrtCstAvlFlg1", cbRrCrsTrfcAlrtCstAvlFlg1, chbRrCrsTrfcAlrtCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RrCrsTrfcAlrtCstAvlFlg2", cbRrCrsTrfcAlrtCstAvlFlg2, chbRrCrsTrfcAlrtCstAvlFlg2, _db));
            //Rear Pedestrian Detection
            _comboChecks.Add(new ComboCheck("RrPCMRspTypCstCrStVal", cbRrPCMRspTypCstCrStVal, chbRrPCMRspTypCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RrPCMRspTypCstStAvl", cbRrPCMRspTypCstStAvl, chbRrPCMRspTypCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RrPCMRspTypCstAvlFlg1", cbRrPCMRspTypCstAvlFlg1, chbRrPCMRspTypCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RrPCMRspTypCstAvlFlg2", cbRrPCMRspTypCstAvlFlg2, chbRrPCMRspTypCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("RrPCMRspTypCstAvlFlg3", cbRrPCMRspTypCstAvlFlg3, chbRrPCMRspTypCstAvlFlg3, _db));
            //Seat Belt Tightening
            _comboChecks.Add(new ComboCheck("StBltTghtngCstCrStVal", cbStBltTghtngCstCrStVal, chbStBltTghtngCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("StBltTghtngCstStAv", cbStBltTghtngCstStAv, chbStBltTghtngCstStAv, _db));
            _comboChecks.Add(new ComboCheck("StBltTghtngCstAvlFlg1", cbStBltTghtngCstAvlFlg1, chbStBltTghtngCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("StBltTghtngCstAvlFlg2", cbStBltTghtngCstAvlFlg2, chbStBltTghtngCstAvlFlg2, _db));
            //Side Blind Zone
            _comboChecks.Add(new ComboCheck("SBZAlrtCstCrStVal", cbSBZAlrtCstCrStVal, chbSBZAlrtCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("SBZAlrtCstStAvl", cbSBZAlrtCstStAvl, chbSBZAlrtCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("SBZAlrtCstAvlFlg1", cbSBZAlrtCstAvlFlg1, chbSBZAlrtCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("SBZAlrtCstAvlFlg2", cbSBZAlrtCstAvlFlg2, chbSBZAlrtCstAvlFlg2, _db));
            #endregion

            #region Remote Lock Unlock and Start
            //Passive Door Lock 
            _comboChecks.Add(new ComboCheck("WlkAwyLckCstCurSetVal", cbWlkAwyLckCstCurSetVal, chbWlkAwyLckCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("WlkAwyLckCstStAvl", cbWlkAwyLckCstStAvl, chbWlkAwyLckCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("WlkAwyLckCstAvlFlg1", cbWlkAwyLckCstAvlFlg1, chbWlkAwyLckCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("WlkAwyLckCstAvlFlg2", cbWlkAwyLckCstAvlFlg2, chbWlkAwyLckCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("WlkAwyLckCstAvlFlg3", cbWlkAwyLckCstAvlFlg3, chbWlkAwyLckCstAvlFlg3, _db));
            //Passive Door Unlock 
            _comboChecks.Add(new ComboCheck("PsvUnlckCstCurSetVal", cbPsvUnlckCstCurSetVal, chbPsvUnlckCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("PsvUnlckCstStAvl", cbPsvUnlckCstStAvl, chbPsvUnlckCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("PsvUnlckCstAvlFlg1", cbPsvUnlckCstAvlFlg1, chbPsvUnlckCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("PsvUnlckCstAvlFlg2", cbPsvUnlckCstAvlFlg2, chbPsvUnlckCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("PsvUnlckCstAvlFlg3", cbPsvUnlckCstAvlFlg3, chbPsvUnlckCstAvlFlg3, _db));
            //Relock Remotely Unlocked Doors
            _comboChecks.Add(new ComboCheck("RlkRtUlkDrCstCrSetVal", cbRlkRtUlkDrCstCrSetVal, chbRlkRtUlkDrCstCrSetVal, _db));
            _comboChecks.Add(new ComboCheck("RlkRtUlkDrCstStAvl", cbRlkRtUlkDrCstStAvl, chbRlkRtUlkDrCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RlkRtUlkDrCstAvlFlg1", cbRlkRtUlkDrCstAvlFlg1, chbRlkRtUlkDrCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RlkRtUlkDrCstAvlFlg2", cbRlkRtUlkDrCstAvlFlg2, chbRlkRtUlkDrCstAvlFlg2, _db));
            //Remote Door Unlock
            _comboChecks.Add(new ComboCheck("SelUlkCstCurSetVal", cbSelUlkCstCurSetVal, chbSelUlkCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("SelUlkCstStAvl", cbSelUlkCstStAvl, chbSelUlkCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("SelUlkCstAvlFlg1", cbSelUlkCstAvlFlg1, chbSelUlkCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("SelUlkCstAvlFlg2", cbSelUlkCstAvlFlg2, chbSelUlkCstAvlFlg2, _db));
            //Remote Left in Vehicle
            _comboChecks.Add(new ComboCheck("RemInVehRmndrCstCurSetVal", cbRemInVehRmndrCstCurSetVal, chbRemInVehRmndrCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RemInVehRmndrCstStAvl", cbRemInVehRmndrCstStAvl, chbRemInVehRmndrCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RemInVehRmndrCstAvlFlg1", cbRemInVehRmndrCstAvlFlg1, chbRemInVehRmndrCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RemInVehRmndrCstAvlFlg2", cbRemInVehRmndrCstAvlFlg2, chbRemInVehRmndrCstAvlFlg2, _db));
            //Remote Lock Feedback
            _comboChecks.Add(new ComboCheck("RtLkFdbkCstCurSetVal", cbRtLkFdbkCstCurSetVal, chbRtLkFdbkCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RtLkFdbkCstStAvl", cbRtLkFdbkCstStAvl, chbRtLkFdbkCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RtLkFdbkCstAvlFlg1", cbRtLkFdbkCstAvlFlg1, chbRtLkFdbkCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RtLkFdbkCstAvlFlg2", cbRtLkFdbkCstAvlFlg2, chbRtLkFdbkCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("RtLkFdbkCstAvlFlg3", cbRtLkFdbkCstAvlFlg3, chbRtLkFdbkCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("RtLkFdbkCstAvlFlg4", cbRtLkFdbkCstAvlFlg4, chbRtLkFdbkCstAvlFlg4, _db));
            //Remote Removed from Vehicle
            _comboChecks.Add(new ComboCheck("RemLftVehRmndrCstCurSetVal", cbRemLftVehRmndrCstCurSetVal, chbRemLftVehRmndrCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RemLftVehRmndrCstStAvl", cbRemLftVehRmndrCstStAvl, chbRemLftVehRmndrCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RemLftVehRmndrCstAvlFlg1", cbRemLftVehRmndrCstAvlFlg1, chbRemLftVehRmndrCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RemLftVehRmndrCstAvlFlg2", cbRemLftVehRmndrCstAvlFlg2, chbRemLftVehRmndrCstAvlFlg2, _db));
            //Remote Start Auto Cool Seats
            _comboChecks.Add(new ComboCheck("RmtStrtCoolStCstCrStVal", cbRmtStrtCoolStCstCrStVal, chbRmtStrtCoolStCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RmtStrtCoolStCstStAvl", cbRmtStrtCoolStCstStAvl, chbRmtStrtCoolStCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RmtStrtCoolStCstAvlFlg1", cbRmtStrtCoolStCstAvlFlg1, chbRmtStrtCoolStCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RmtStrtCoolStCstAvlFlg2", cbRmtStrtCoolStCstAvlFlg2, chbRmtStrtCoolStCstAvlFlg2, _db));
            //Remote Start Auto Heat Seats
            _comboChecks.Add(new ComboCheck("RmtStrtHtdStCstCrStVal", cbRmtStrtHtdStCstCrStVal, chbRmtStrtHtdStCstCrStVal, _db));
            _comboChecks.Add(new ComboCheck("RmtStrtHtdStCstStAvl", cbRmtStrtHtdStCstStAvl, chbRmtStrtHtdStCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RmtStrtHtdStCstAvlFlg1", cbRmtStrtHtdStCstAvlFlg1, chbRmtStrtHtdStCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RmtStrtHtdStCstAvlFlg2", cbRmtStrtHtdStCstAvlFlg2, chbRmtStrtHtdStCstAvlFlg2, _db));
            //Remote Sunroof Operation
            _comboChecks.Add(new ComboCheck("RmtSnrfOpCstCurrSetVal", cbRmtSnrfOpCstCurrSetVal, chbRmtSnrfOpCstCurrSetVal, _db));
            _comboChecks.Add(new ComboCheck("RmtSnrfOpCstSetAval", cbRmtSnrfOpCstSetAval, chbRmtSnrfOpCstSetAval, _db));
            _comboChecks.Add(new ComboCheck("RmtSnrfOpCstAvlFlg1", cbRmtSnrfOpCstAvlFlg1, chbRmtSnrfOpCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RmtSnrfOpCstAvlFlg2", cbRmtSnrfOpCstAvlFlg2, chbRmtSnrfOpCstAvlFlg2, _db));
            //Remote Unlock Light Feedback
            _comboChecks.Add(new ComboCheck("RtUlkLtFbCstCurSetVal", cbRtUlkLtFbCstCurSetVal, chbRtUlkLtFbCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RtUlkLtFbCstStAvl", cbRtUlkLtFbCstStAvl, chbRtUlkLtFbCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RtUlkLtFbCstAvlFlg1", cbRtUlkLtFbCstAvlFlg1, chbRtUlkLtFbCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RtUlkLtFbCstAvlFlg2", cbRtUlkLtFbCstAvlFlg2, chbRtUlkLtFbCstAvlFlg2, _db));
            //Remote Window Operation
            _comboChecks.Add(new ComboCheck("RmtWndOpCstCurSetVal", cbRmtWndOpCstCurSetVal, chbRmtWndOpCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("RmtWndOpCstStAvl", cbRmtWndOpCstStAvl, chbRmtWndOpCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("RmtWndOpCstAvlFlg1", cbRmtWndOpCstAvlFlg1, chbRmtWndOpCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("RmtWndOpCstAvlFlg2", cbRmtWndOpCstAvlFlg2, chbRmtWndOpCstAvlFlg2, _db));
            #endregion

            #region Power Door Locks

            _comboChecks.Add(new ComboCheck("AutDrvLckCstCurSetVal", cmbAutDrvLckCstCurSetVal, cbAutDrvLckCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("AutDrvLckCstStAvl", cmbAutDrvLckCstStAvl, cbAutDrvLckCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutDrvLckCstAvlFlg1", cmbAutDrvLckCstAvlFlg1, cbAutDrvLckCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutDrvLckCstAvlFlg2", cmbAutDrvLckCstAvlFlg2, cbAutDrvLckCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("AutUnlckCstCurSetVal", cmbAutUnlckCstCurSetVal, cbAutUnlckCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("AutUnlckCstStAvl", cmbAutUnlckCstStAvl, cbAutUnlckCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AutUnlckCstAvlFlg1", cmbAutUnlckCstAvlFlg1, cbAutUnlckCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AutUnlckCstAvlFlg2", cmbAutUnlckCstAvlFlg2, cbAutUnlckCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("AutUnlckCstAvlFlg3", cmbAutUnlckCstAvlFlg3, cbAutUnlckCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("LDCLCstCurSetVal", cmbLDCLCstCurSetVal, cbLDCLCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("LDCLCstStAvl", cmbLDCLCstStAvl, cbLDCLCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("LDCLCstAvlFlg1", cmbLDCLCstAvlFlg1, cbLDCLCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("LDCLCstAvlFlg2", cmbLDCLCstAvlFlg2, cbLDCLCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("OpDrAtLOCstCurSetVal", cmbOpDrAtLOCstCurSetVal, cbOpDrAtLOCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("OpDrAtLOCstStAvl", cmbOpDrAtLOCstStAvl, cbOpDrAtLOCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("OpDrAtLOCstAvlFlg1", cmbOpDrAtLOCstAvlFlg1, cbOpDrAtLOCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("OpDrAtLOCstAvlFlg2", cmbOpDrAtLOCstAvlFlg2, cbOpDrAtLOCstAvlFlg2, _db));

            #endregion

            #region Rear Seat Reminder Form Initialize

            _comboChecks.Add(new ComboCheck("RrStRmndrEnblDsblCstCurrSetVal", cmbRearSeatReminderCurrVal, cbRrStRmndrEnblDsblCstCurrSetVal, _db));//4D5
            _comboChecks.Add(new ComboCheck("RrStRmndrEnblDsblCstSetAval", cmbRearSeatReminderSettingAval, cbRrStRmndrEnblDsblCstSetAval, _db));//4D5
            _comboChecks.Add(new ComboCheck("RrStRmndrEnblDsblCstAvlFlg1", cmbRearSeatReminderOptions1, cbRrStRmndrEnblDsblCstAvlFlg1, _db));//49A
            _comboChecks.Add(new ComboCheck("RrStRmndrEnblDsblCstAvlFlg2", cmbRearSeatReminderOptions2, cbRrStRmndrEnblDsblCstAvlFlg2, _db));//49A

            //ToDo Send all Zeros for Can ID 4D5

            #endregion

            #region Lighting 
            //Advance Forward Lighting
            _comboChecks.Add(new ComboCheck("AdpLghtRspTypCstCurSelVal", cmbAdpLghtRspTypCstCurSelVal, cbAdpLghtRspTypCstCurSelVal, _db));
            _comboChecks.Add(new ComboCheck("AdpLghtRspTypCstStAvl", cmbAdpLghtRspTypCstStAvl, cbAdpLghtRspTypCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("AdpLghtRspTypCstAvlFlg1", cmbAdpLghtRspTypCstAvlFlg1, cbAdpLghtRspTypCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("AdpLghtRspTypCstAvlFlg2", cmbAdpLghtRspTypCstAvlFlg2, cbAdpLghtRspTypCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("AdpLghtRspTypCstAvlFlg3", cmbAdpLghtRspTypCstAvlFlg3, cbAdpLghtRspTypCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("AdpLghtRspTypCstAvlFlg4", cmbAdpLghtRspTypCstAvlFlg4, cbAdpLghtRspTypCstAvlFlg4, _db));
            //Exit Lighting
            _comboChecks.Add(new ComboCheck("ExtLghtCstCurSetVal", cmbExtLghtCstCurSetVal, cbExtLghtCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("ExtLghtCstStAvl", cmbExtLghtCstStAvl, cbExtLghtCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("ExtLghtCstAvlFlg1", cmbExtLghtCstAvlFlg1, cbExtLghtCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("ExtLghtCstAvlFlg2", cmbExtLghtCstAvlFlg2, cbExtLghtCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("ExtLghtCstAvlFlg3", cmbExtLghtCstAvlFlg3, cbExtLghtCstAvlFlg3, _db));
            _comboChecks.Add(new ComboCheck("ExtLghtCstAvlFlg4", cmbExtLghtCstAvlFlg4, cbExtLghtCstAvlFlg4, _db));
            //LeftorRight Hand Traffic
            _comboChecks.Add(new ComboCheck("LtRtHnTrCstCurSetVal", cmbLtRtHnTrCstCurSetVal, cbLtRtHnTrCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("LtRtHnTrCstStAvl", cmbLtRtHnTrCstStAvl, cbLtRtHnTrCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("LtRtHnTrCstAvlFlg1", cmbLtRtHnTrCstAvlFlg1, cbLtRtHnTrCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("LtRtHnTrCstAvlFlg2", cmbLtRtHnTrCstAvlFlg2, cbLtRtHnTrCstAvlFlg2, _db));
            _comboChecks.Add(new ComboCheck("LtRtHnTrCstAvlFlg3", cmbLtRtHnTrCstAvlFlg3, cbLtRtHnTrCstAvlFlg3, _db));
            //Automatic High Beam Assist
            _comboChecks.Add(new ComboCheck("GlrFrHgBmRspTypCstCurSetVal", cmbGlrFrHgBmRspTypCstCurSetVal, cbGlrFrHgBmRspTypCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("GlrFrHgBmRspTypCstStAvl", cmbGlrFrHgBmRspTypCstStAvl, cbGlrFrHgBmRspTypCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("GlrFrHgBmRspTypCstAvlFlg1", cmbGlrFrHgBmRspTypCstAvlFlg1, cbGlrFrHgBmRspTypCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("GlrFrHgBmRspTypCstAvlFlg2", cmbGlrFrHgBmRspTypCstAvlFlg2, cbGlrFrHgBmRspTypCstAvlFlg2, _db));
            //Vehicle Locator Lights
            _comboChecks.Add(new ComboCheck("VehLctrLghtsCstCurSetVal", cbVehLctrLghtsCstCurSetVal, chbVehLctrLghtsCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("VehLctrLghtsCstStAvl", cmbVehLctrLghtsCstStAvl, cbVehLctrLghtsCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("VehLctrLghtsCstAvlFlg1", cbVehLctrLghtsCstAvlFlg1, chbVehLctrLghtsCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("VehLctrLghtsCstAvlFlg2", cbVehLctrLghtsCstAvlFlg2, chbVehLctrLghtsCstAvlFlg2, _db));
            //Daytime Tail Light
            _comboChecks.Add(new ComboCheck("DaytimeTlLghtsCstCurSetVal", cmbDaytimeTlLghtsCstCurSetVal, cbDaytimeTlLghtsCstCurSetVal, _db));
            _comboChecks.Add(new ComboCheck("DaytimeTlLghtsCstStAvl", cmbDaytimeTlLghtsCstStAvl, cbDaytimeTlLghtsCstStAvl, _db));
            _comboChecks.Add(new ComboCheck("DaytimeTlLghtsCstAvlFlg1", cmbDaytimeTlLghtsCstAvlFlg1, cbDaytimeTlLghtsCstAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("DaytimeTlLghtsCstAvlFlg2", cmbDaytimeTlLghtsCstAvlFlg2, cbDaytimeTlLghtsCstAvlFlg2, _db));
            //Surround View Lighting
            _comboChecks.Add(new ComboCheck("SrrndVwLghtCsCrStVal", cbSrrndVwLghtCsCrStVal, chbSrrndVwLghtCsCrStVal, _db));
            _comboChecks.Add(new ComboCheck("SrrndVwLghtCsStAvl", cbSrrndVwLghtCsStAvl, chbSrrndVwLghtCsStAvl, _db));
            _comboChecks.Add(new ComboCheck("SrrndVwLghtCsAvlFlg1", cbSrrndVwLghtCsAvlFlg1, chbSrrndVwLghtCsAvlFlg1, _db));
            _comboChecks.Add(new ComboCheck("SrrndVwLghtCsAvlFlg2", cbSrrndVwLghtCsAvlFlg2, chbSrrndVwLghtCsAvlFlg2, _db));
                       
            #endregion

            #region Ride Height

            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstCurrSetVal", cmbLvlgSysEntEgrsMdCstCurrSetVal, cbLvlgSysEntEgrsMdCstCurrSetVal, _db));//515
            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstAvlFlg1", cmbLvlgSysEntEgrsMdCstAvlFlg1, cbLvlgSysEntEgrsMdCstAvlFlg1, _db));//579
            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstAvlFlg2", cmbLvlgSysEntEgrsMdCstAvlFlg2, cbLvlgSysEntEgrsMdCstAvlFlg2, _db));//579
            _comboChecks.Add(new ComboCheck("LvlgSysEntEgrsMdCstSetAval", cmbLvlgSysEntEgrsMdCstSetAval, cbLvlgSysEntEgrsMdCstSetAval, _db));//56B
            _comboChecks.Add(new ComboCheck("FrtRdHgtGPSCstCurrSetVal", cmbFrtRdHgtGPSCstCurrSetVal, cbFrtRdHgtGPSCstCurrSetVal, _db));//4D1
            _comboChecks.Add(new ComboCheck("FrtRdHgtGPSCstSetAval", cmbFrtRdHgtGPSCstSetAval, cbFrtRdHgtGPSCstSetAval, _db));//4D1
            _comboChecks.Add(new ComboCheck("FrtRdHgtGPSCstAvlbltyFlg1", cmbFrtRdHgtGPSCstAvlbltyFlg1, cbFrtRdHgtGPSCstAvlbltyFlg1, _db));//490
            _comboChecks.Add(new ComboCheck("FrtRdHgtGPSCstAvlbltyFlg2", cmbFrtRdHgtGPSCstAvlbltyFlg2, cbFrtRdHgtGPSCstAvlbltyFlg2, _db));//490

            #endregion

            #region Running Boards

            _comboChecks.Add(new ComboCheck("RnngBrdAutoDplyCstCurrSetVal", cmbRnngBrdAutoDplyCstCurrSetVal, cbRnngBrdAutoDplyCstCurrSetVal, _db));//4D9
            _comboChecks.Add(new ComboCheck("RnngBrdAutoDplyCstSetAval", cmbRnngBrdAutoDplyCstSetAval, cbRnngBrdAutoDplyCstSetAval, _db));//4D8
            _comboChecks.Add(new ComboCheck("RnngBrdAutoDplyCstAvlFlg1", cmbRnngBrdAutoDplyCstAvlFlg1, cbRnngBrdAutoDplyCstAvlFlg1, _db));//4A5
            _comboChecks.Add(new ComboCheck("RnngBrdAutoDplyCstAvlFlg2", cmbRnngBrdAutoDplyCstAvlFlg2, cbRnngBrdAutoDplyCstAvlFlg2, _db));//4A5
            _comboChecks.Add(new ComboCheck("RnngBrdManDplyCstCurrSetVal", cmbRnngBrdManDplyCstCurrSetVal, cbRnngBrdManDplyCstCurrSetVal, _db));//4D9
            _comboChecks.Add(new ComboCheck("RnngBrdManDplyCstSetAval", cmbRnngBrdManDplyCstSetAval, cbRnngBrdManDplyCstSetAval, _db));//4D8
            _comboChecks.Add(new ComboCheck("RnngBrdManDplyCstSetSelbl", cmbRnngBrdManDplyCstSetSelbl, cbRnngBrdManDplyCstSetSelbl, _db));//4D8

            //ToDo Send all 0's 4D8

            _comboChecks.Add(new ComboCheck("RnngBrdManDplyCstAvlFlg1", cmbRnngBrdManDplyCstAvlFlg1, cbRnngBrdManDplyCstAvlFlg1, _db));//4A5
            _comboChecks.Add(new ComboCheck("RnngBrdManDplyCstAvlFlg2", cmbRnngBrdManDplyCstAvlFlg2, cbRnngBrdManDplyCstAvlFlg2, _db));//4A5
            _comboChecks.Add(new ComboCheck("RnngBrdManDplyCstAvlFlg3", cmbRnngBrdManDplyCstAvlFlg3, cbRnngBrdManDplyCstAvlFlg3, _db));//4A5

            #endregion           

            #region Trailering

            _comboChecks.Add(new ComboCheck("CrgBedVwAutoLgtCstCurrSetVal", cmbCrgBedVwAutoLgtCstCurrSetVal, cbCrgBedVwAutoLgtCstCurrSetVal, _db));//4C6
            _comboChecks.Add(new ComboCheck("CrgBedVwAutoLgtCstAvlFlg1", cmbCrgBedVwAutoLgtCstAvlFlg1, cbCrgBedVwAutoLgtCstAvlFlg1, _db));//492
            _comboChecks.Add(new ComboCheck("CrgBedVwAutoLgtCstAvlFlg2", cmbCrgBedVwAutoLgtCstAvlFlg2, cbCrgBedVwAutoLgtCstAvlFlg2, _db));//492
            _comboChecks.Add(new ComboCheck("CrgBedVwAutoLgtCstSetAval", cmbCrgBedVwAutoLgtCstSetAval, cbCrgBedVwAutoLgtCstSetAval, _db));//4D8
            _comboChecks.Add(new ComboCheck("JckKnfDtnCstCurrSetVal", cmbJckKnfDtnCstCurrSetVal, cbJckKnfDtnCstCurrSetVal, _db));//4D9
            _comboChecks.Add(new ComboCheck("JckKnfDtnCstAvlFlg1", cmbJckKnfDtnCstAvlFlg1, cbJckKnfDtnCstAvlFlg1, _db));//492
            _comboChecks.Add(new ComboCheck("JckKnfDtnCstAvlFlg2", cmbJckKnfDtnCstAvlFlg2, cbJckKnfDtnCstAvlFlg2, _db));//492
            _comboChecks.Add(new ComboCheck("JckKnfDtnCstSetAval", cmbJckKnfDtnCstSetAval, cbJckKnfDtnCstSetAval, _db));//4D8
            _comboChecks.Add(new ComboCheck("TrlrMirVidEnblCstCurrSetVal", cmbTrlrMirVidEnblCstCurrSetVal, cbTrlrMirVidEnblCstCurrSetVal, _db));//4C5
            _comboChecks.Add(new ComboCheck("TrlrMirVidEnblCstAvlFlg1", cmbTrlrMirVidEnblCstAvlFlg1, cbTrlrMirVidEnblCstAvlFlg1, _db));//492
            _comboChecks.Add(new ComboCheck("TrlrMirVidEnblCstAvlFlg2", cmbTrlrMirVidEnblCstAvlFlg2, cbTrlrMirVidEnblCstAvlFlg2, _db));//492
            _comboChecks.Add(new ComboCheck("TrlrMirVidEnblCstSetAval", cmbTrlrMirVidEnblCstSetAval, cbTrlrMirVidEnblCstSetAval, _db));//4D8
            _comboChecks.Add(new ComboCheck("TrlrMirVidGdlnsCstCurrSetVal", cmbTrlrMirVidGdlnsCstCurrSetVal, cbTrlrMirVidGdlnsCstCurrSetVal, _db));//4D9
            _comboChecks.Add(new ComboCheck("TrlrMirVidGdlnsCstAvlFlg1", cmbTrlrMirVidGdlnsCstAvlFlg1, cbTrlrMirVidGdlnsCstAvlFlg1, _db));//492
            _comboChecks.Add(new ComboCheck("TrlrMirVidGdlnsCstAvlFlg2", cmbTrlrMirVidGdlnsCstAvlFlg2, cbTrlrMirVidGdlnsCstAvlFlg2, _db));//492
            _comboChecks.Add(new ComboCheck("TrlrMirVidGdlnsCstSetAval", cmbTrlrMirVidGdlnsCstSetAval, cbTrlrMirVidGdlnsCstSetAval, _db));//4D8

            #endregion

            #region Ambient Lighting

            _comboChecks.Add(new ComboCheck("IntDimSeldAnmTypStVal", cmbIntDimSeldAnmTypStVal, cbIntDimSeldAnmTypStVal, _db));
            _comboChecks.Add(new ComboCheck("IDATA_AnmTyp3Avl", cmbIDATA_AnmTyp3Avl, cbIDATA_AnmTyp3Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp21Avl", cmbIDCTA_ClrTyp21Avl, cbIDCTA_ClrTyp21Avl, _db));
            _comboChecks.Add(new ComboCheck("IDATA_AnmTyp1Avl", cmbIDATA_AnmTyp1Avl, cbIDATA_AnmTyp1Avl, _db));
            //_comboChecks.Add(new ComboCheck("IDATA_AnmTyp2Avl", cmbIDATA_AnmTyp2Avl, cbIDATA_AnmTyp2Avl, _db));
            _comboChecks.Add(new ComboCheck("IDATA_AnmTyp2Avl", cmbIDATA_AnmTyp2Avl, cbIDATA_AnmTyp2Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp12Avl", cmbIDCTA_ClrTyp12Avl, cbIDCTA_ClrTyp12Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp15Avl", cmbIDCTA_ClrTyp15Avl, cbIDCTA_ClrTyp15Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp19Avl", cmbIDCTA_ClrTyp19Avl, cbIDCTA_ClrTyp19Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp1Avl", cmbIDCTA_ClrTyp1Avl, cbIDCTA_ClrTyp1Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp24Avl", cmbIDCTA_ClrTyp24Avl, cbIDCTA_ClrTyp24Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp2Avl", cmbIDCTA_ClrTyp2Avl, cbIDCTA_ClrTyp2Avl, _db));
            _comboChecks.Add(new ComboCheck("IDCTA_ClrTyp7Avl", cmbIDCTA_ClrTyp7Avl, cbIDCTA_ClrTyp7Avl, _db));
            _comboChecks.Add(new ComboCheck("IDATA_AnmTyp5Avl", cmbIDATA_AnmTyp5Avl, cbIDATA_AnmTyp5Avl, _db));
            _comboChecks.Add(new ComboCheck("IntAmbtBrghtnsCtrlStAvl", cmbIntAmbtBrghtnsCtrlStAvl, cbIntAmbtBrghtnsCtrlStAvl, _db));
            _comboChecks.Add(new ComboCheck("IntAmbtBrghtnsCtrlStVal", cmbIntAmbtBrghtnsCtrlStVal, cbIntAmbtBrghtnsCtrlStVal, _db));
            //_comboChecks.Add(new ComboCheck("IntDimDspLvl", nudIntDimDspLvl, cbIntDimDspLvl, _db));
            //_comboChecks.Add(new ComboCheck("IntDimLvl", nudIntDimLvl, cbIntDimLvl, _db));
            _comboChecks.Add(new ComboCheck("IntDimSeldClrTypStVal", cmbIntDimSeldClrTypStVal, cbIntDimSeldClrTypStVal, _db));
            _comboChecks.Add(new ComboCheck("IDATA_OffAvl", cmbIDATA_OffAvl, cbIDATA_OffAvl, _db));

            #endregion

            #region Buckle to Drive

            _comboChecks.Add(new ComboCheck("StBltShfIntlkCstSetAval", cmbStBltShfIntlkCstSetAval, cbStBltShfIntlkCstSetAval, _db));//515
            _comboChecks.Add(new ComboCheck("TnDrvStBltShfIntlkCstSetAval", cmbTnDrvStBltShfIntlkCstSetAval, cbTnDrvStBltShfIntlkCstSetAval, _db));//515
            _comboChecks.Add(new ComboCheck("StBltShfIntlkCstCurrSetVal", cmbStBltShfIntlkCstCurrSetVal, cbStBltShfIntlkCstCurrSetVal, _db));//515
            _comboChecks.Add(new ComboCheck("TnDrvStBltShfIntlkCstCurrSetVal", cmbTnDrvStBltShfIntlkCstCurrSetVal, cbTnDrvStBltShfIntlkCstCurrSetVal, _db));//515
            _comboChecks.Add(new ComboCheck("StBltShfIntlkCstAvlFlg1", cmbStBltShfIntlkCstAvlFlg1, cbStBltShfIntlkCstAvlFlg1, _db));//509
            _comboChecks.Add(new ComboCheck("StBltShfIntlkCstAvlFlg2", cmbStBltShfIntlkCstAvlFlg2, cbStBltShfIntlkCstAvlFlg2, _db));//509
            _comboChecks.Add(new ComboCheck("TDAP_TeenDrvrActvAuth", cmbTDAP_TeenDrvrActvAuth, cbTDAP_TeenDrvrActvAuth, _db));//4DE

            //ToDo Send all 0's 4DE

            _comboChecks.Add(new ComboCheck("TnDrvStBltShfIntlkCstAvlFlg1", cmbTnDrvStBltShfIntlkCstAvlFlg1, cbTnDrvStBltShfIntlkCstAvlFlg1, _db));//42A
            _comboChecks.Add(new ComboCheck("TnDrvStBltShfIntlkCstAvlFlg2", cmbTnDrvStBltShfIntlkCstAvlFlg2, cbTnDrvStBltShfIntlkCstAvlFlg2, _db));//42A
            _comboChecks.Add(new ComboCheck("TeenDrvFtrAvl", cmbTeenDrvFtrAvl, cbTeenDrvFtrAvl, _db));//4C1
            _comboChecks.Add(new ComboCheck("TnDrvPINStrd", cmbTnDrvPINStrd, cbTnDrvPINStrd, _db));//4BF

            #endregion

            #region Seating Position

            _comboChecks.Add(new ComboCheck("TDAP_DrvrStMemIdfrAuth", cmbTDAP_DrvrStMemIdfrAuth, cbTDAP_DrvrStMemIdfrAuth, _db));//4DE
            _comboChecks.Add(new ComboCheck("AutMemRclCstCurSetVal", cmbAutMemRclCstCurSetVal, cbAutMemRclCstCurSetVal, _db));//4DB
            _comboChecks.Add(new ComboCheck("AutMemRclCstAvlFlg1", cmbAutMemRclCstAvlFlg1, cbAutMemRclCstAvlFlg1, _db));//4A6
            _comboChecks.Add(new ComboCheck("AutMemRclCstAvlFlg2", cmbAutMemRclCstAvlFlg2, cbAutMemRclCstAvlFlg2, _db));//4A6
            _comboChecks.Add(new ComboCheck("AutMemRclCstStAvl", cmbAutMemRclCstStAvl, cbAutMemRclCstStAvl, _db));//4E0
            _comboChecks.Add(new ComboCheck("EsyExtCstCurSetVal", cmbEsyExtCstCurSetVal, cbEsyExtCstCurSetVal, _db));//4DA
            _comboChecks.Add(new ComboCheck("EsyExtCstAvlFlg1", cmbEsyExtCstAvlFlg1, cbEsyExtCstAvlFlg1, _db));//4A6
            _comboChecks.Add(new ComboCheck("EsyExtCstAvlFlg2", cmbEsyExtCstAvlFlg2, cbEsyExtCstAvlFlg2, _db));//4A6
            _comboChecks.Add(new ComboCheck("EsyExtCstStAvl", cmbEsyExtCstStAvl, cbEsyExtCstStAvl, _db));//4D8

            #endregion

            #region Suspension

            _comboChecks.Add(new ComboCheck("LvlgSysAlgnMdCstCurrSetVal", cmbLvlgSysAlgnMdCstCurrSetVal, cbLvlgSysAlgnMdCstCurrSetVal, _db));//515
            _comboChecks.Add(new ComboCheck("LvlgSysAlgnMdCstSetAval", cmbLvlgSysAlgnMdCstSetAval, cbLvlgSysAlgnMdCstSetAval, _db));//515
            _comboChecks.Add(new ComboCheck("LvlgSysAlgnMdCstAvlFlg1", cmbLvlgSysAlgnMdCstAvlFlg1, cbLvlgSysAlgnMdCstAvlFlg1, _db));//452
            _comboChecks.Add(new ComboCheck("LvlgSysAlgnMdCstAvlFlg2", cmbLvlgSysAlgnMdCstAvlFlg2, cbLvlgSysAlgnMdCstAvlFlg2, _db));//452

            //ToDo Send all 0's 515
            //ToDo Send all 0's 56B

            _comboChecks.Add(new ComboCheck("LvlgSysAlgnMdCstSetSelable", cmbLvlgSysAlgnMdCstSetSelable, cbLvlgSysAlgnMdCstSetSelable, _db));//56B
            _comboChecks.Add(new ComboCheck("LvlgSysServMdCstSetAval", cmbLvlgSysServMdCstSetAval, cbLvlgSysServMdCstSetAval, _db));//56B
            _comboChecks.Add(new ComboCheck("LvlgSysServMdCstAvlFlg1", cmbLvlgSysServMdCstAvlFlg1, cbLvlgSysServMdCstAvlFlg1, _db));//579
            _comboChecks.Add(new ComboCheck("LvlgSysServMdCstAvlFlg2", cmbLvlgSysServMdCstAvlFlg2, cbLvlgSysServMdCstAvlFlg2, _db));//579
            _comboChecks.Add(new ComboCheck("LvlgSysServMdCstSetSelable", cmbLvlgSysServMdCstSetSelable, cbLvlgSysServMdCstSetSelable, _db));//56B
            _comboChecks.Add(new ComboCheck("LvlgSysServMdCstCurrSetVal", cmbLvlgSysServMdCstCurrSetVal, cbLvlgSysServMdCstCurrSetVal, _db));//515

            #endregion
        }

        private class ComboCheck
        {
            public string Signal;
            public ComboBox Box;
            public CheckBox Check;

            public ComboCheck(string signal,ComboBox box,CheckBox check, dynamic db)
            {
                // Todo: if property is not set yet pass as argument
                Signal = signal;
                Box = box;
                Check = check;
                Utility.FormHelper.ComboFillEnums(Box, Signal, db);

                box.Enabled = false;
                check.CheckedChanged += Check_CheckedChanged;
            }

            private void Check_CheckedChanged(object sender, EventArgs e)
            {
                Box.Enabled = Check.Checked;
            }
        }

        #region Helper

        private void ComboFillEnums(ComboBox cbo, string signalName)
        {
            Utility.FormHelper.ComboFillEnums(cbo, signalName, _db);
        }

        private void SelectAndSend(ComboBox cbo, string signalName)
        {
            _adapter.Send(Utility.FormHelper.SelectToSend(cbo, signalName));
        }

        public void SignalSend(string name, object value)
        {
            var packet = new Adapter.Packet() { Name = name, Value = Convert.ToString(value) };
            _adapter.Send(packet);
        }

        #endregion

        private bool _peridoic = false;

        private void btnPerioic_Click(object sender, EventArgs e)
        {
            _peridoic = !_peridoic;
            if(_peridoic)
            {
                btnPeriodic.Text = "Sending";
                btnPeriodic.BackColor = System.Drawing.Color.PaleGreen;
            }
            else
            {
                btnPeriodic.Text = "Stopped";
                btnPeriodic.BackColor = System.Drawing.Color.Red;
            }
        }

        private bool _active = false;
        private Dictionary<string, Task> _threadDict = new Dictionary<string, Task>();
        private void PeriodicHandler()
        {
            while (_active) // todo: smarter so that it does not run unless form is visible and start again when come back!!!!
            {
                if (!_peridoic)
                {
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }

                // if checked then want to send
                var packets = new List<Adapter.Packet>();
                foreach (var comboCheck in _comboChecks)
                {
                    if (comboCheck.Check.Checked)
                    {                        
                        if(!string.IsNullOrEmpty(comboCheck.Signal))
                            this.Invoke(new Action(() => { packets.Add(new Adapter.Packet() { Name = comboCheck.Signal, Value = Convert.ToString(comboCheck.Box.SelectedItem).Split('=')[0].Trim() }); }));
                    }
                }

                // find message to group the signals together
                var packetMessages = new Dictionary<string, List<Adapter.Packet>>();
                foreach(var packet in packets)
                {
                    var messageName = FindMessageName(packet.Name);
                    if (!packetMessages.Keys.Any(n => n == messageName))
                        packetMessages.Add(messageName, new List<Adapter.Packet>());

                    packetMessages[messageName].Add(packet);
                }

                // for each message send at specific rate in its own thread
                foreach (var message in packetMessages)
                {
                    var messageName = message.Key;
                    if (!_threadDict.Keys.Any(n => n == messageName))
                    {
                        var task = new Task(() =>
                        {
                            var period = FindMessagePeriod(messageName);
                            Thread.Sleep((int)Convert.ToInt64(1000 * period));
                            _adapter.Send(message.Value.ToArray());
                        });
                        task.Start();
                        _threadDict.Add(messageName, task);

                    }
                    else
                    {
                        var thread = _threadDict[messageName];
                        if (thread.Status != TaskStatus.Running)
                            _threadDict.Remove(messageName);
                    }                        
                }

                if (!_threadDict.Values.Any(n => n.Status != TaskStatus.Running))
                    continue;

                System.Threading.Thread.Sleep(25);  // Todo: send in text box value
            }
        }
        
        private void tabRearSeatReminder_Enter(object sender, EventArgs e)
        {
            _active = true;
            _task.Start();
        }

        private void tabRearSeatReminder_Leave(object sender, EventArgs e)
        {
            _active = false;
            if (_task != null)
                _task.Dispose();
        }

        private void VehicleSettings_Leave(object sender, EventArgs e)
        {
            tabRearSeatReminder_Leave(sender, e);
        }

        private void VehicleSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            e.Cancel = true;
            this.Hide();
        }
    }
}
