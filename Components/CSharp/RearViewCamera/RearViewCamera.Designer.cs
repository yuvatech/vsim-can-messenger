﻿namespace RearViewCamera
{
    partial class RearViewCamera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RearViewCamera));
            this.panel4 = new System.Windows.Forms.Panel();
            this.cbRPDCustomValue = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cbDisplayVideoTrig = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.cbVidFeaturePresent = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.cbCameraOn = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.cb360DegreePresent = new System.Windows.Forms.ComboBox();
            this.cbRPDCustomSetting = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.sldVehSpeedDrvn = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTransLever = new System.Windows.Forms.ComboBox();
            this.cbVehMovState = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVehSpeed = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.cbDisSmrtTowVidOn = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.cb_SWACSA = new System.Windows.Forms.ComboBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.cb_RCTARIR = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.cb_RCTALIR = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cbRearRegion4Object = new System.Windows.Forms.ComboBox();
            this.cbRearRegion3Object = new System.Windows.Forms.ComboBox();
            this.cbRearRegion2Object = new System.Windows.Forms.ComboBox();
            this.cbRearRegion1Object = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbFrontSystemStat = new System.Windows.Forms.ComboBox();
            this.cbFrontRegion4Object = new System.Windows.Forms.ComboBox();
            this.cbFrontRegion3Object = new System.Windows.Forms.ComboBox();
            this.cbFrontRegion2Object = new System.Windows.Forms.ComboBox();
            this.cbFrontRegion1Object = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.kbSteeringWheelAngle = new KnobControl.KnobControl();
            this.label24 = new System.Windows.Forms.Label();
            this.txtSteeringWheelAngle = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldVehSpeedDrvn)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.cbRPDCustomValue);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.cbDisplayVideoTrig);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.cbVidFeaturePresent);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.cbCameraOn);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.cb360DegreePresent);
            this.panel4.Controls.Add(this.cbRPDCustomSetting);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Location = new System.Drawing.Point(455, 18);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(317, 312);
            this.panel4.TabIndex = 38;
            // 
            // cbRPDCustomValue
            // 
            this.cbRPDCustomValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRPDCustomValue.FormattingEnabled = true;
            this.cbRPDCustomValue.Location = new System.Drawing.Point(161, 171);
            this.cbRPDCustomValue.Margin = new System.Windows.Forms.Padding(4);
            this.cbRPDCustomValue.Name = "cbRPDCustomValue";
            this.cbRPDCustomValue.Size = new System.Drawing.Size(135, 24);
            this.cbRPDCustomValue.Sorted = true;
            this.cbRPDCustomValue.TabIndex = 32;
            this.cbRPDCustomValue.SelectedIndexChanged += new System.EventHandler(this.cbRPDCustomValue_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(152, 154);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(128, 17);
            this.label29.TabIndex = 31;
            this.label29.Text = "RPD Custom Value";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(53, 4);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(160, 20);
            this.label28.TabIndex = 17;
            this.label28.Text = "Front 360 Camera";
            // 
            // cbDisplayVideoTrig
            // 
            this.cbDisplayVideoTrig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDisplayVideoTrig.FormattingEnabled = true;
            this.cbDisplayVideoTrig.Location = new System.Drawing.Point(161, 116);
            this.cbDisplayVideoTrig.Margin = new System.Windows.Forms.Padding(4);
            this.cbDisplayVideoTrig.Name = "cbDisplayVideoTrig";
            this.cbDisplayVideoTrig.Size = new System.Drawing.Size(135, 24);
            this.cbDisplayVideoTrig.Sorted = true;
            this.cbDisplayVideoTrig.TabIndex = 22;
            this.cbDisplayVideoTrig.SelectedIndexChanged += new System.EventHandler(this.cbDisplayVideoTrig_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(157, 97);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(123, 17);
            this.label22.TabIndex = 30;
            this.label22.Text = "Display Video Trig";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(149, 39);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(136, 17);
            this.label23.TabIndex = 29;
            this.label23.Text = "360 Degree Present";
            // 
            // cbVidFeaturePresent
            // 
            this.cbVidFeaturePresent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVidFeaturePresent.FormattingEnabled = true;
            this.cbVidFeaturePresent.Location = new System.Drawing.Point(19, 116);
            this.cbVidFeaturePresent.Margin = new System.Windows.Forms.Padding(4);
            this.cbVidFeaturePresent.Name = "cbVidFeaturePresent";
            this.cbVidFeaturePresent.Size = new System.Drawing.Size(111, 24);
            this.cbVidFeaturePresent.Sorted = true;
            this.cbVidFeaturePresent.TabIndex = 20;
            this.cbVidFeaturePresent.SelectedIndexChanged += new System.EventHandler(this.cbVidFeaturePresent_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(32, 39);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(80, 17);
            this.label27.TabIndex = 26;
            this.label27.Text = "Camera On";
            // 
            // cbCameraOn
            // 
            this.cbCameraOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCameraOn.FormattingEnabled = true;
            this.cbCameraOn.Location = new System.Drawing.Point(19, 58);
            this.cbCameraOn.Margin = new System.Windows.Forms.Padding(4);
            this.cbCameraOn.Name = "cbCameraOn";
            this.cbCameraOn.Size = new System.Drawing.Size(111, 24);
            this.cbCameraOn.Sorted = true;
            this.cbCameraOn.TabIndex = 18;
            this.cbCameraOn.SelectedIndexChanged += new System.EventHandler(this.cbCameraOn_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 96);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(134, 17);
            this.label25.TabIndex = 28;
            this.label25.Text = "Vid Feature Present";
            // 
            // cb360DegreePresent
            // 
            this.cb360DegreePresent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb360DegreePresent.FormattingEnabled = true;
            this.cb360DegreePresent.Location = new System.Drawing.Point(161, 58);
            this.cb360DegreePresent.Margin = new System.Windows.Forms.Padding(4);
            this.cb360DegreePresent.Name = "cb360DegreePresent";
            this.cb360DegreePresent.Size = new System.Drawing.Size(135, 24);
            this.cb360DegreePresent.Sorted = true;
            this.cb360DegreePresent.TabIndex = 21;
            this.cb360DegreePresent.SelectedIndexChanged += new System.EventHandler(this.cb360DegreePresent_SelectedIndexChanged);
            // 
            // cbRPDCustomSetting
            // 
            this.cbRPDCustomSetting.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRPDCustomSetting.FormattingEnabled = true;
            this.cbRPDCustomSetting.Location = new System.Drawing.Point(19, 171);
            this.cbRPDCustomSetting.Margin = new System.Windows.Forms.Padding(4);
            this.cbRPDCustomSetting.Name = "cbRPDCustomSetting";
            this.cbRPDCustomSetting.Size = new System.Drawing.Size(111, 24);
            this.cbRPDCustomSetting.Sorted = true;
            this.cbRPDCustomSetting.TabIndex = 19;
            this.cbRPDCustomSetting.SelectedIndexChanged += new System.EventHandler(this.cbRPDCustomSetting_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 153);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(136, 17);
            this.label26.TabIndex = 27;
            this.label26.Text = "RPD Custom Setting";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.sldVehSpeedDrvn);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbTransLever);
            this.panel1.Controls.Add(this.cbVehMovState);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtVehSpeed);
            this.panel1.Location = new System.Drawing.Point(43, 18);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(363, 312);
            this.panel1.TabIndex = 35;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(29, 10);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(212, 20);
            this.label30.TabIndex = 7;
            this.label30.Text = "Vehicle Movement State";
            // 
            // sldVehSpeedDrvn
            // 
            this.sldVehSpeedDrvn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sldVehSpeedDrvn.Location = new System.Drawing.Point(25, 146);
            this.sldVehSpeedDrvn.Margin = new System.Windows.Forms.Padding(4);
            this.sldVehSpeedDrvn.Maximum = 0;
            this.sldVehSpeedDrvn.Name = "sldVehSpeedDrvn";
            this.sldVehSpeedDrvn.Size = new System.Drawing.Size(299, 56);
            this.sldVehSpeedDrvn.TabIndex = 2;
            this.sldVehSpeedDrvn.Scroll += new System.EventHandler(this.sldVehSpeedDrvn_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(153, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Trans Shift Lever Position";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Veh Mov State";
            // 
            // cbTransLever
            // 
            this.cbTransLever.BackColor = System.Drawing.SystemColors.Control;
            this.cbTransLever.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTransLever.FormattingEnabled = true;
            this.cbTransLever.Location = new System.Drawing.Point(157, 68);
            this.cbTransLever.Margin = new System.Windows.Forms.Padding(4);
            this.cbTransLever.Name = "cbTransLever";
            this.cbTransLever.Size = new System.Drawing.Size(167, 24);
            this.cbTransLever.Sorted = true;
            this.cbTransLever.TabIndex = 5;
            this.cbTransLever.SelectedIndexChanged += new System.EventHandler(this.cbTransLever_SelectedIndexChanged);
            // 
            // cbVehMovState
            // 
            this.cbVehMovState.BackColor = System.Drawing.SystemColors.Control;
            this.cbVehMovState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVehMovState.FormattingEnabled = true;
            this.cbVehMovState.Location = new System.Drawing.Point(25, 68);
            this.cbVehMovState.Margin = new System.Windows.Forms.Padding(4);
            this.cbVehMovState.Name = "cbVehMovState";
            this.cbVehMovState.Size = new System.Drawing.Size(99, 24);
            this.cbVehMovState.Sorted = true;
            this.cbVehMovState.TabIndex = 1;
            this.cbVehMovState.SelectedIndexChanged += new System.EventHandler(this.cbVehMovState_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 121);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "km/h";
            // 
            // txtVehSpeed
            // 
            this.txtVehSpeed.BackColor = System.Drawing.Color.LightGray;
            this.txtVehSpeed.Enabled = false;
            this.txtVehSpeed.Location = new System.Drawing.Point(25, 117);
            this.txtVehSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.txtVehSpeed.Name = "txtVehSpeed";
            this.txtVehSpeed.Size = new System.Drawing.Size(55, 22);
            this.txtVehSpeed.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.label34);
            this.panel7.Location = new System.Drawing.Point(820, 18);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(363, 312);
            this.panel7.TabIndex = 40;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.cbDisSmrtTowVidOn);
            this.panel11.Controls.Add(this.label37);
            this.panel11.Location = new System.Drawing.Point(13, 171);
            this.panel11.Margin = new System.Windows.Forms.Padding(4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(165, 98);
            this.panel11.TabIndex = 19;
            // 
            // cbDisSmrtTowVidOn
            // 
            this.cbDisSmrtTowVidOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDisSmrtTowVidOn.FormattingEnabled = true;
            this.cbDisSmrtTowVidOn.Location = new System.Drawing.Point(13, 50);
            this.cbDisSmrtTowVidOn.Margin = new System.Windows.Forms.Padding(4);
            this.cbDisSmrtTowVidOn.Name = "cbDisSmrtTowVidOn";
            this.cbDisSmrtTowVidOn.Size = new System.Drawing.Size(111, 24);
            this.cbDisSmrtTowVidOn.Sorted = true;
            this.cbDisSmrtTowVidOn.TabIndex = 27;
            this.cbDisSmrtTowVidOn.SelectedIndexChanged += new System.EventHandler(this.cbDisSmrtTowVidOn_SelectedIndexChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(-3, 1);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(125, 34);
            this.label37.TabIndex = 0;
            this.label37.Text = "Display Smart Tow\r\nVideo On";
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.label40);
            this.panel10.Controls.Add(this.cb_SWACSA);
            this.panel10.Location = new System.Drawing.Point(189, 171);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(167, 98);
            this.panel10.TabIndex = 19;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(8, 1);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(149, 34);
            this.label40.TabIndex = 26;
            this.label40.Text = "Steering Wheel Angle \r\nSensor Cal Status\r\n";
            // 
            // cb_SWACSA
            // 
            this.cb_SWACSA.BackColor = System.Drawing.Color.LightGray;
            this.cb_SWACSA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_SWACSA.FormattingEnabled = true;
            this.cb_SWACSA.Location = new System.Drawing.Point(11, 50);
            this.cb_SWACSA.Margin = new System.Windows.Forms.Padding(4);
            this.cb_SWACSA.Name = "cb_SWACSA";
            this.cb_SWACSA.Size = new System.Drawing.Size(121, 24);
            this.cb_SWACSA.Sorted = true;
            this.cb_SWACSA.TabIndex = 23;
            this.cb_SWACSA.SelectedIndexChanged += new System.EventHandler(this.cb_SWACSA_SelectedIndexChanged);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.cb_RCTARIR);
            this.panel9.Controls.Add(this.label39);
            this.panel9.Location = new System.Drawing.Point(187, 47);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(169, 104);
            this.panel9.TabIndex = 19;
            // 
            // cb_RCTARIR
            // 
            this.cb_RCTARIR.BackColor = System.Drawing.Color.LightGray;
            this.cb_RCTARIR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_RCTARIR.FormattingEnabled = true;
            this.cb_RCTARIR.Location = new System.Drawing.Point(13, 48);
            this.cb_RCTARIR.Margin = new System.Windows.Forms.Padding(4);
            this.cb_RCTARIR.Name = "cb_RCTARIR";
            this.cb_RCTARIR.Size = new System.Drawing.Size(121, 24);
            this.cb_RCTARIR.Sorted = true;
            this.cb_RCTARIR.TabIndex = 20;
            this.cb_RCTARIR.SelectedIndexChanged += new System.EventHandler(this.cb_RCTARIR_SelectedIndexChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 0);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(156, 34);
            this.label39.TabIndex = 25;
            this.label39.Text = "Rear Cross Traffic Alert\r\nRight Indicator Request\r\n";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.label38);
            this.panel8.Controls.Add(this.cb_RCTALIR);
            this.panel8.Location = new System.Drawing.Point(4, 47);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(175, 104);
            this.panel8.TabIndex = 18;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 0);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(156, 34);
            this.label38.TabIndex = 24;
            this.label38.Text = "Rear Cross Traffic Alert\r\nLeft Indicator Request\r\n";
            // 
            // cb_RCTALIR
            // 
            this.cb_RCTALIR.BackColor = System.Drawing.Color.LightGray;
            this.cb_RCTALIR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_RCTALIR.FormattingEnabled = true;
            this.cb_RCTALIR.Location = new System.Drawing.Point(21, 48);
            this.cb_RCTALIR.Margin = new System.Windows.Forms.Padding(4);
            this.cb_RCTALIR.Name = "cb_RCTALIR";
            this.cb_RCTALIR.Size = new System.Drawing.Size(111, 24);
            this.cb_RCTALIR.Sorted = true;
            this.cb_RCTALIR.TabIndex = 21;
            this.cb_RCTALIR.SelectedIndexChanged += new System.EventHandler(this.cb_RCTALIR_SelectedIndexChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(37, 10);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(222, 20);
            this.label34.TabIndex = 17;
            this.label34.Text = "Video Processing Module";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.cbRearRegion4Object);
            this.panel3.Controls.Add(this.cbRearRegion3Object);
            this.panel3.Controls.Add(this.cbRearRegion2Object);
            this.panel3.Controls.Add(this.cbRearRegion1Object);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Location = new System.Drawing.Point(455, 344);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(317, 312);
            this.panel3.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(171, 106);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 17);
            this.label16.TabIndex = 13;
            this.label16.Text = "Region 4 Status";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(32, 105);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 17);
            this.label18.TabIndex = 12;
            this.label18.Text = "Region 3 Status";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(171, 47);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 17);
            this.label19.TabIndex = 11;
            this.label19.Text = "Region 2 Status";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(32, 49);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(109, 17);
            this.label20.TabIndex = 10;
            this.label20.Text = "Region 1 Status";
            // 
            // cbRearRegion4Object
            // 
            this.cbRearRegion4Object.BackColor = System.Drawing.Color.LightGray;
            this.cbRearRegion4Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRearRegion4Object.FormattingEnabled = true;
            this.cbRearRegion4Object.Location = new System.Drawing.Point(169, 126);
            this.cbRearRegion4Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbRearRegion4Object.Name = "cbRearRegion4Object";
            this.cbRearRegion4Object.Size = new System.Drawing.Size(133, 24);
            this.cbRearRegion4Object.Sorted = true;
            this.cbRearRegion4Object.TabIndex = 4;
            this.cbRearRegion4Object.SelectedIndexChanged += new System.EventHandler(this.cbRearRegion4Object_SelectedIndexChanged);
            // 
            // cbRearRegion3Object
            // 
            this.cbRearRegion3Object.BackColor = System.Drawing.Color.LightGray;
            this.cbRearRegion3Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRearRegion3Object.FormattingEnabled = true;
            this.cbRearRegion3Object.Location = new System.Drawing.Point(24, 126);
            this.cbRearRegion3Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbRearRegion3Object.Name = "cbRearRegion3Object";
            this.cbRearRegion3Object.Size = new System.Drawing.Size(137, 24);
            this.cbRearRegion3Object.Sorted = true;
            this.cbRearRegion3Object.TabIndex = 3;
            this.cbRearRegion3Object.SelectedIndexChanged += new System.EventHandler(this.cbRearRegion3Object_SelectedIndexChanged);
            // 
            // cbRearRegion2Object
            // 
            this.cbRearRegion2Object.BackColor = System.Drawing.Color.LightGray;
            this.cbRearRegion2Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRearRegion2Object.FormattingEnabled = true;
            this.cbRearRegion2Object.Location = new System.Drawing.Point(169, 68);
            this.cbRearRegion2Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbRearRegion2Object.Name = "cbRearRegion2Object";
            this.cbRearRegion2Object.Size = new System.Drawing.Size(133, 24);
            this.cbRearRegion2Object.Sorted = true;
            this.cbRearRegion2Object.TabIndex = 2;
            this.cbRearRegion2Object.SelectedIndexChanged += new System.EventHandler(this.cbRearRegion2Object_SelectedIndexChanged);
            // 
            // cbRearRegion1Object
            // 
            this.cbRearRegion1Object.BackColor = System.Drawing.Color.LightGray;
            this.cbRearRegion1Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRearRegion1Object.FormattingEnabled = true;
            this.cbRearRegion1Object.Location = new System.Drawing.Point(24, 68);
            this.cbRearRegion1Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbRearRegion1Object.Name = "cbRearRegion1Object";
            this.cbRearRegion1Object.Size = new System.Drawing.Size(137, 24);
            this.cbRearRegion1Object.Sorted = true;
            this.cbRearRegion1Object.TabIndex = 1;
            this.cbRearRegion1Object.SelectedIndexChanged += new System.EventHandler(this.cbRearRegion1Object_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(20, 10);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(228, 20);
            this.label21.TabIndex = 0;
            this.label21.Text = "Park Assitant Rear Status";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cbFrontSystemStat);
            this.panel2.Controls.Add(this.cbFrontRegion4Object);
            this.panel2.Controls.Add(this.cbFrontRegion3Object);
            this.panel2.Controls.Add(this.cbFrontRegion2Object);
            this.panel2.Controls.Add(this.cbFrontRegion1Object);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(43, 344);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(363, 312);
            this.panel2.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 162);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "System Status";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(184, 105);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Region 4 Status";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 106);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Region 3 Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(184, 47);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Region 2 Status";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 49);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Region 1 Status";
            // 
            // cbFrontSystemStat
            // 
            this.cbFrontSystemStat.BackColor = System.Drawing.Color.LightGray;
            this.cbFrontSystemStat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrontSystemStat.FormattingEnabled = true;
            this.cbFrontSystemStat.Location = new System.Drawing.Point(41, 181);
            this.cbFrontSystemStat.Margin = new System.Windows.Forms.Padding(4);
            this.cbFrontSystemStat.Name = "cbFrontSystemStat";
            this.cbFrontSystemStat.Size = new System.Drawing.Size(137, 24);
            this.cbFrontSystemStat.Sorted = true;
            this.cbFrontSystemStat.TabIndex = 5;
            this.cbFrontSystemStat.SelectedIndexChanged += new System.EventHandler(this.cbFrontSystemStat_SelectedIndexChanged);
            // 
            // cbFrontRegion4Object
            // 
            this.cbFrontRegion4Object.BackColor = System.Drawing.Color.LightGray;
            this.cbFrontRegion4Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrontRegion4Object.FormattingEnabled = true;
            this.cbFrontRegion4Object.Location = new System.Drawing.Point(187, 126);
            this.cbFrontRegion4Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbFrontRegion4Object.Name = "cbFrontRegion4Object";
            this.cbFrontRegion4Object.Size = new System.Drawing.Size(137, 24);
            this.cbFrontRegion4Object.Sorted = true;
            this.cbFrontRegion4Object.TabIndex = 4;
            this.cbFrontRegion4Object.SelectedIndexChanged += new System.EventHandler(this.cbFrontRegion4Object_SelectedIndexChanged);
            // 
            // cbFrontRegion3Object
            // 
            this.cbFrontRegion3Object.BackColor = System.Drawing.Color.LightGray;
            this.cbFrontRegion3Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrontRegion3Object.FormattingEnabled = true;
            this.cbFrontRegion3Object.Location = new System.Drawing.Point(41, 126);
            this.cbFrontRegion3Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbFrontRegion3Object.Name = "cbFrontRegion3Object";
            this.cbFrontRegion3Object.Size = new System.Drawing.Size(137, 24);
            this.cbFrontRegion3Object.Sorted = true;
            this.cbFrontRegion3Object.TabIndex = 3;
            this.cbFrontRegion3Object.SelectedIndexChanged += new System.EventHandler(this.cbFrontRegion3Object_SelectedIndexChanged);
            // 
            // cbFrontRegion2Object
            // 
            this.cbFrontRegion2Object.BackColor = System.Drawing.Color.LightGray;
            this.cbFrontRegion2Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrontRegion2Object.FormattingEnabled = true;
            this.cbFrontRegion2Object.Location = new System.Drawing.Point(187, 68);
            this.cbFrontRegion2Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbFrontRegion2Object.Name = "cbFrontRegion2Object";
            this.cbFrontRegion2Object.Size = new System.Drawing.Size(137, 24);
            this.cbFrontRegion2Object.Sorted = true;
            this.cbFrontRegion2Object.TabIndex = 2;
            this.cbFrontRegion2Object.SelectedIndexChanged += new System.EventHandler(this.cbFrontRegion2Object_SelectedIndexChanged);
            // 
            // cbFrontRegion1Object
            // 
            this.cbFrontRegion1Object.BackColor = System.Drawing.Color.LightGray;
            this.cbFrontRegion1Object.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrontRegion1Object.FormattingEnabled = true;
            this.cbFrontRegion1Object.Location = new System.Drawing.Point(41, 68);
            this.cbFrontRegion1Object.Margin = new System.Windows.Forms.Padding(4);
            this.cbFrontRegion1Object.Name = "cbFrontRegion1Object";
            this.cbFrontRegion1Object.Size = new System.Drawing.Size(137, 24);
            this.cbFrontRegion1Object.Sorted = true;
            this.cbFrontRegion1Object.TabIndex = 1;
            this.cbFrontRegion1Object.SelectedIndexChanged += new System.EventHandler(this.cbFrontRegion1Object_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(232, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Park Assitant Front Status";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.kbSteeringWheelAngle);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.txtSteeringWheelAngle);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Location = new System.Drawing.Point(820, 344);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(363, 312);
            this.panel5.TabIndex = 42;
            // 
            // kbSteeringWheelAngle
            // 
            this.kbSteeringWheelAngle.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.kbSteeringWheelAngle.ForeColor = System.Drawing.Color.Blue;
            this.kbSteeringWheelAngle.ImeMode = System.Windows.Forms.ImeMode.On;
            this.kbSteeringWheelAngle.LargeChange = 50;
            this.kbSteeringWheelAngle.Location = new System.Drawing.Point(26, 32);
            this.kbSteeringWheelAngle.Margin = new System.Windows.Forms.Padding(4);
            this.kbSteeringWheelAngle.Maximum = 25;
            this.kbSteeringWheelAngle.Minimum = 0;
            this.kbSteeringWheelAngle.Name = "kbSteeringWheelAngle";
            this.kbSteeringWheelAngle.ShowLargeScale = true;
            this.kbSteeringWheelAngle.ShowSmallScale = false;
            this.kbSteeringWheelAngle.Size = new System.Drawing.Size(254, 237);
            this.kbSteeringWheelAngle.SmallChange = 1;
            this.kbSteeringWheelAngle.TabIndex = 44;
            this.kbSteeringWheelAngle.Value = 0;
            this.kbSteeringWheelAngle.ValueChanged += new KnobControl.ValueChangedEventHandler(this.kbSteeringWheelAngle_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(159, 283);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 17);
            this.label24.TabIndex = 42;
            this.label24.Text = "degrees";
            // 
            // txtSteeringWheelAngle
            // 
            this.txtSteeringWheelAngle.Enabled = false;
            this.txtSteeringWheelAngle.Location = new System.Drawing.Point(85, 278);
            this.txtSteeringWheelAngle.Margin = new System.Windows.Forms.Padding(5);
            this.txtSteeringWheelAngle.Name = "txtSteeringWheelAngle";
            this.txtSteeringWheelAngle.Size = new System.Drawing.Size(68, 22);
            this.txtSteeringWheelAngle.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(35, 10);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(190, 20);
            this.label14.TabIndex = 1;
            this.label14.Text = "Steering Wheel Angle";
            // 
            // RearViewCamera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 695);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "RearViewCamera";
            this.Text = "RearViewCamera";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RearViewCamera_FormClosing);
            this.Load += new System.EventHandler(this.RearViewCamera_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldVehSpeedDrvn)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.ComboBox cbRPDCustomValue;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.ComboBox cbDisplayVideoTrig;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.ComboBox cbVidFeaturePresent;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.ComboBox cbCameraOn;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.ComboBox cb360DegreePresent;
        public System.Windows.Forms.ComboBox cbRPDCustomSetting;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TrackBar sldVehSpeedDrvn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbTransLever;
        public System.Windows.Forms.ComboBox cbVehMovState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVehSpeed;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        public System.Windows.Forms.ComboBox cb_SWACSA;
        public System.Windows.Forms.ComboBox cb_RCTALIR;
        public System.Windows.Forms.ComboBox cb_RCTARIR;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox cbRearRegion4Object;
        public System.Windows.Forms.ComboBox cbRearRegion3Object;
        public System.Windows.Forms.ComboBox cbRearRegion2Object;
        public System.Windows.Forms.ComboBox cbRearRegion1Object;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cbFrontSystemStat;
        public System.Windows.Forms.ComboBox cbFrontRegion4Object;
        public System.Windows.Forms.ComboBox cbFrontRegion3Object;
        public System.Windows.Forms.ComboBox cbFrontRegion2Object;
        public System.Windows.Forms.ComboBox cbFrontRegion1Object;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label30;
        public System.Windows.Forms.ComboBox cbDisSmrtTowVidOn;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtSteeringWheelAngle;
        private System.Windows.Forms.Label label14;
        private KnobControl.KnobControl kbSteeringWheelAngle;
    }
}

