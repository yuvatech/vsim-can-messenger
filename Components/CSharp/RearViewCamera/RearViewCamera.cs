﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Extension;
using Utility;

namespace RearViewCamera
{
    public partial class RearViewCamera : Form
    {
        private Adapter _adapter = null;
        private dynamic _db = Adapter.Database();
        public RearViewCamera()
        {
            InitializeComponent();
            _adapter = new Adapter();
            _adapter.Open(false);
            //_adapter.OnReceived += AdapterOnReceived;

        }

        private void RearViewCamera_Load(object sender, EventArgs e)
        {
            //Vehicle Movement State
            ComboFillEnums(cbVehMovState, "VMMP_VehMtnMvmtStatAuth");
            ComboFillEnums(cbTransLever, "TEGP_TrnsShftLvrPstnAuth");
            SlideFill(sldVehSpeedDrvn,"VSADP_VehSpdAvgDrvnAuth");

            //Front 360 Degree Camera
            ComboFillEnums(cbCameraOn, "DspFrnt360CmrON");
            ComboFillEnums(cb360DegreePresent, "Deg360VidFtrPrsnt");
            ComboFillEnums(cbVidFeaturePresent, "TrgrdVidRcrdrFtrPrsnt");
            ComboFillEnums(cbDisplayVideoTrig, "DsplyTrgVdOn");
            ComboFillEnums(cbRPDCustomSetting, "RrPCMRspTypCstStAvl");
            ComboFillEnums(cbRPDCustomValue, "RrPCMRspTypCstCrStVal");

            //Video Processing On
            ComboFillEnums(cb_RCTALIR, "RrCrsTrfcAlrtLftIndCtrl");
            ComboFillEnums(cb_RCTARIR, "RrCrsTrfcAlrtRgIndCtrl");
            ComboFillEnums(cbDisSmrtTowVidOn, "DispSmrtTowVidOn");
            ComboFillEnums(cb_SWACSA, "SWIP_StrgWhlAngCalStsAuth");

            //Park Assitant Front Status
            ComboFillEnums(cbFrontRegion1Object, "FPARS_LtCntr");
            ComboFillEnums(cbFrontRegion2Object, "FPARS_Lt");
            ComboFillEnums(cbFrontRegion3Object, "FPARS_Rt");
            ComboFillEnums(cbFrontRegion4Object, "FPARS_RtCntr");
            ComboFillEnums(cbFrontSystemStat, "PrkAsstStat");

            //Park Assitant Rear Status
            ComboFillEnums(cbRearRegion1Object, "RPARS_LtCntr");
            ComboFillEnums(cbRearRegion2Object, "RPARS_Lt");
            ComboFillEnums(cbRearRegion3Object, "RPARS_Rt");
            ComboFillEnums(cbRearRegion4Object, "RPARS_RtCntr");

            //Steering Wheel Angle
            KnobFill(kbSteeringWheelAngle,"SWIP_StrgWhlAngAuth");
        }

        #region Winform Helpers
        public void ComboFillEnums(ComboBox cbo, string signalName)
        {
            Utility.FormHelper.ComboFillEnums(cbo, signalName, _db);
        }
        
        public void SlideFill(TrackBar tb, string signalName)
        {
            var signals = _db["signals"];
            var values = ARXMLDecode.ParseRange(signals, signalName);
            foreach (KeyValuePair<int, int> entry in values)
            {
                tb.Minimum = entry.Key; 
                tb.Maximum = entry.Value;
            }
        }

        public void KnobFill(KnobControl.KnobControl kb, string signalName) 
        {
            var signals = _db["signals"];
            var values = ARXMLDecode.ParseRange(signals, "SWIP_StrgWhlAngAuth");

            foreach (KeyValuePair<int, int> entry in values)
            {
                kb.Minimum = entry.Key + entry.Value;
                kb.Maximum = entry.Value - entry.Key;
                kb.Value = entry.Value;
            }
        }

        #endregion

        #region Vehicle Movement State
        private void cbVehMovState_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbVehMovState.SelectedItem.ToString().Split('=');        
            var packet1 = new Adapter.Packet() { Name = "VMMP_VehMtnMvmtStatAuth", Value = val[0].Trim() };
            _adapter.Send(packet1);
            var packet2 = new Adapter.Packet() { Name = "TEGP_TransCmndGrAuth", Value = val[0].Trim() };
            _adapter.Send(packet2);
        }

        private void cbTransLever_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TEGP_TrnsShftLvrPstnAuth

            string[] val = cbTransLever.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "TEGP_TrnsShftLvrPstnAuth", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void sldVehSpeedDrvn_Scroll(object sender, EventArgs e)
        {
            this.txtVehSpeed.Text = sldVehSpeedDrvn.Value.ToString();
            var packet = new Adapter.Packet() { Name = "VSADP_VehSpdAvgDrvnAuth", Value = sldVehSpeedDrvn.Value };
            _adapter.Send(packet);
        }

        #endregion

        #region Front 360 Degree Camera
        private void cbCameraOn_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbCameraOn.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "DspFrnt360CmrON", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cb360DegreePresent_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cb360DegreePresent.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "Deg360VidFtrPrsnt", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbVidFeaturePresent_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbVidFeaturePresent.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "TrgrdVidRcrdrFtrPrsnt", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbDisplayVideoTrig_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbDisplayVideoTrig.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "DsplyTrgVdOn", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbRPDCustomSetting_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbRPDCustomSetting.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "RrPCMRspTypCstStAvl", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbRPDCustomValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbRPDCustomValue.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "RrPCMRspTypCstCrStVal", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        #endregion

        #region Video Processing On
        private void cb_RCTALIR_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cb_RCTALIR.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "RrCrsTrfcAlrtLftIndCtrl", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cb_RCTARIR_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cb_RCTARIR.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "RrCrsTrfcAlrtRgIndCtrl", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbDisSmrtTowVidOn_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbDisSmrtTowVidOn.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "DispSmrtTowVidOn", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cb_SWACSA_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cb_SWACSA.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "SWIP_StrgWhlAngCalStsAuth", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        #endregion

        #region Park Assitant Front Status
        private void cbFrontRegion1Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbFrontRegion1Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "FPARS_LtCntr", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbFrontRegion2Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbFrontRegion2Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "FPARS_Lt", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbFrontRegion3Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbFrontRegion3Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "FPARS_Rt", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbFrontRegion4Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbFrontRegion4Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "FPARS_RtCntr", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbFrontSystemStat_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbFrontSystemStat.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "PrkAsstStat", Value = val[0].Trim() };
            _adapter.Send(packet);
        }
        
        #endregion

        #region  Park Assitant Rear Status

        private void cbRearRegion1Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbRearRegion1Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "TEGP_TrnsShftLvrPstnAuth", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbRearRegion2Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbRearRegion2Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "TEGP_TrnsShftLvrPstnAuth", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbRearRegion3Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbRearRegion3Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "TEGP_TrnsShftLvrPstnAuth", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        private void cbRearRegion4Object_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cbRearRegion4Object.SelectedItem.ToString().Split('=');
            var packet = new Adapter.Packet() { Name = "TEGP_TrnsShftLvrPstnAuth", Value = val[0].Trim() };
            _adapter.Send(packet);
        }

        #endregion

        #region Steering Wheel Angle
        private void kbSteeringWheelAngle_ValueChanged(object Sender)
        {
            var val = (kbSteeringWheelAngle.Maximum) / 2;
            this.txtSteeringWheelAngle.Text = (kbSteeringWheelAngle.Value - val).ToString();
            var packet = new Adapter.Packet() { Name = "SWIP_StrgWhlAngAuth", Value = Convert.ToInt16(kbSteeringWheelAngle.Value - val) };
            _adapter.Send(packet);
        }

        #endregion

        //private void AdapterOnReceived(object sender, Adapter.Packet packet)
        //{

        //}

        private void RearViewCamera_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            e.Cancel = true;
            this.Hide();
        }
    }
}
