﻿namespace HVAC
{
    partial class HVAC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HVAC));
            this.tabHVACControl1 = new System.Windows.Forms.TabControl();
            this.tabPageFrontHvac = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.ImperialRadBtn = new System.Windows.Forms.RadioButton();
            this.USRadBtn = new System.Windows.Forms.RadioButton();
            this.MetricRadBtn = new System.Windows.Forms.RadioButton();
            this.tabControlAirDist = new System.Windows.Forms.TabControl();
            this.tabPageCombiMode = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCombiMode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAirDistCombiAuto = new System.Windows.Forms.Button();
            this.btnAirDistCombiMaxDefrost = new System.Windows.Forms.Button();
            this.btnAirDistCombitWindshield = new System.Windows.Forms.Button();
            this.btnAirDistCombiFloor = new System.Windows.Forms.Button();
            this.btnAirDistCombiFace = new System.Windows.Forms.Button();
            this.picBoxAutoCombi = new System.Windows.Forms.PictureBox();
            this.picBoxMaxDefrost = new System.Windows.Forms.PictureBox();
            this.tabPageDiscreteMode = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAirDistLeftFloorWindShield = new System.Windows.Forms.Button();
            this.btnAirDistLeftBiLevel = new System.Windows.Forms.Button();
            this.btnAirDistRightBiLevel = new System.Windows.Forms.Button();
            this.btnAirDistLeftStatus = new System.Windows.Forms.Button();
            this.btnAirDistRightStatus = new System.Windows.Forms.Button();
            this.btnHeat = new System.Windows.Forms.Button();
            this.lblRightMode = new System.Windows.Forms.Label();
            this.lblLeftMode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAirDistRightAuto = new System.Windows.Forms.Button();
            this.btnAirDistLeftAuto = new System.Windows.Forms.Button();
            this.btnAirDistMaxDefrost = new System.Windows.Forms.Button();
            this.btnAirDistRightFloor = new System.Windows.Forms.Button();
            this.btnAirDistRightFace = new System.Windows.Forms.Button();
            this.btnAirDistRightWindshield = new System.Windows.Forms.Button();
            this.btnAirDistLeftWindshield = new System.Windows.Forms.Button();
            this.btnAirDistLeftFace = new System.Windows.Forms.Button();
            this.btnAirDistLeftFloor = new System.Windows.Forms.Button();
            this.picBoxAutoLeft = new System.Windows.Forms.PictureBox();
            this.picBoxAutoRight = new System.Windows.Forms.PictureBox();
            this.picBoxAuto = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPageFourMode = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnAirDistFourModeBiLevel = new System.Windows.Forms.Button();
            this.btnAirDistFourModeFloorWindShield = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAirDistFourModeStatus = new System.Windows.Forms.Button();
            this.btnAirDistFourModeAuto = new System.Windows.Forms.Button();
            this.btnAirDistFourModeMaxDefrost = new System.Windows.Forms.Button();
            this.btnAirDistFourModeFloor = new System.Windows.Forms.Button();
            this.btnAirDistFourModeFace = new System.Windows.Forms.Button();
            this.picBoxAutoFourMode = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFrontFanSpeedSettings = new System.Windows.Forms.Label();
            this.lblFrontLeftTempSettings = new System.Windows.Forms.Label();
            this.lblFrontRightTempSettings = new System.Windows.Forms.Label();
            this.cmbIndBlwStngStFrt = new System.Windows.Forms.ComboBox();
            this.cmbIndTmpStngStFL = new System.Windows.Forms.ComboBox();
            this.cmbIndTmpStngStFR = new System.Windows.Forms.ComboBox();
            this.lblFrontFanSpeed = new System.Windows.Forms.Label();
            this.lblFrontLeftTemp = new System.Windows.Forms.Label();
            this.lblFrontRightTemp = new System.Windows.Forms.Label();
            this.nudFrontFanSpeed = new System.Windows.Forms.NumericUpDown();
            this.nudFrontLeftTemp = new System.Windows.Forms.NumericUpDown();
            this.nudFrontRightTemp = new System.Windows.Forms.NumericUpDown();
            this.btnIonizerStatusOn = new System.Windows.Forms.Button();
            this.btnSync = new System.Windows.Forms.Button();
            this.btnAuto = new System.Windows.Forms.Button();
            this.btnRecir = new System.Windows.Forms.Button();
            this.btnAC = new System.Windows.Forms.Button();
            this.btnPower = new System.Windows.Forms.Button();
            this.tabPageRearHvac = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblFanSpeedSettings = new System.Windows.Forms.Label();
            this.lblRearLeftTempSettings = new System.Windows.Forms.Label();
            this.lblRightTempSettings = new System.Windows.Forms.Label();
            this.cmbIndBlwStngStRr = new System.Windows.Forms.ComboBox();
            this.cmbIndTmpStngStRL = new System.Windows.Forms.ComboBox();
            this.cmbIndTmpStngStRR = new System.Windows.Forms.ComboBox();
            this.lblRearFanSpeed = new System.Windows.Forms.Label();
            this.lblRearLeftTemp = new System.Windows.Forms.Label();
            this.lblRearRightTemp = new System.Windows.Forms.Label();
            this.nudFanSpeed = new System.Windows.Forms.NumericUpDown();
            this.nudRearLeftTemp = new System.Windows.Forms.NumericUpDown();
            this.nudRearRightTemp = new System.Windows.Forms.NumericUpDown();
            this.btnRearPanelLock = new System.Windows.Forms.Button();
            this.btnRearAuto = new System.Windows.Forms.Button();
            this.btnRearSync = new System.Windows.Forms.Button();
            this.btnRearPower = new System.Windows.Forms.Button();
            this.tabControlRearAirDistribution = new System.Windows.Forms.TabControl();
            this.tabPageRearCombiMode = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRearAirDistCombiAuto = new System.Windows.Forms.Button();
            this.btnRearAirDistCombiFloor = new System.Windows.Forms.Button();
            this.btnRearAirDistCombiFace = new System.Windows.Forms.Button();
            this.picBoxRearAutoCombi = new System.Windows.Forms.PictureBox();
            this.tabPageRearFourMode = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnRearAirDistFourModeBiDir = new System.Windows.Forms.Button();
            this.lblFourMode = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnRearAirDistFourModeStatus = new System.Windows.Forms.Button();
            this.btnRearAirDistFourModeAuto = new System.Windows.Forms.Button();
            this.btnRearAirDistFourModeFloor = new System.Windows.Forms.Button();
            this.btnRearAirDistFourModeFace = new System.Windows.Forms.Button();
            this.picBoxRearAutoFourMode = new System.Windows.Forms.PictureBox();
            this.tabPageRearCombiRoofMode = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnAirCombiRoofDistModeRoof = new System.Windows.Forms.Button();
            this.lblCombiRoofMode = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnRearCombiRoofAirDistModeStatus = new System.Windows.Forms.Button();
            this.btnRearCombiRoofAirDistModeAuto = new System.Windows.Forms.Button();
            this.btnRearCombiRoofAirDistModeFloor = new System.Windows.Forms.Button();
            this.btnCombiRoofRearAirDistModeFace = new System.Windows.Forms.Button();
            this.picBoxRearCombiRoofAutoMode = new System.Windows.Forms.PictureBox();
            this.tabHVACControl1.SuspendLayout();
            this.tabPageFrontHvac.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tabControlAirDist.SuspendLayout();
            this.tabPageCombiMode.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoCombi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMaxDefrost)).BeginInit();
            this.tabPageDiscreteMode.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPageFourMode.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoFourMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFrontFanSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFrontLeftTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFrontRightTemp)).BeginInit();
            this.tabPageRearHvac.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRearLeftTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRearRightTemp)).BeginInit();
            this.tabControlRearAirDistribution.SuspendLayout();
            this.tabPageRearCombiMode.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRearAutoCombi)).BeginInit();
            this.tabPageRearFourMode.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRearAutoFourMode)).BeginInit();
            this.tabPageRearCombiRoofMode.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRearCombiRoofAutoMode)).BeginInit();
            this.SuspendLayout();
            // 
            // tabHVACControl1
            // 
            this.tabHVACControl1.Controls.Add(this.tabPageFrontHvac);
            this.tabHVACControl1.Controls.Add(this.tabPageRearHvac);
            this.tabHVACControl1.Location = new System.Drawing.Point(12, 12);
            this.tabHVACControl1.Name = "tabHVACControl1";
            this.tabHVACControl1.SelectedIndex = 0;
            this.tabHVACControl1.Size = new System.Drawing.Size(727, 698);
            this.tabHVACControl1.TabIndex = 0;
            // 
            // tabPageFrontHvac
            // 
            this.tabPageFrontHvac.Controls.Add(this.panel13);
            this.tabPageFrontHvac.Controls.Add(this.tabControlAirDist);
            this.tabPageFrontHvac.Controls.Add(this.panel1);
            this.tabPageFrontHvac.Location = new System.Drawing.Point(4, 25);
            this.tabPageFrontHvac.Name = "tabPageFrontHvac";
            this.tabPageFrontHvac.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFrontHvac.Size = new System.Drawing.Size(719, 669);
            this.tabPageFrontHvac.TabIndex = 0;
            this.tabPageFrontHvac.Text = "Front Hvac";
            this.tabPageFrontHvac.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Controls.Add(this.ImperialRadBtn);
            this.panel13.Controls.Add(this.USRadBtn);
            this.panel13.Controls.Add(this.MetricRadBtn);
            this.panel13.Location = new System.Drawing.Point(17, 599);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(306, 50);
            this.panel13.TabIndex = 51;
            // 
            // ImperialRadBtn
            // 
            this.ImperialRadBtn.AutoSize = true;
            this.ImperialRadBtn.Location = new System.Drawing.Point(206, 13);
            this.ImperialRadBtn.Name = "ImperialRadBtn";
            this.ImperialRadBtn.Size = new System.Drawing.Size(78, 21);
            this.ImperialRadBtn.TabIndex = 2;
            this.ImperialRadBtn.Text = "Imperial";
            this.ImperialRadBtn.UseVisualStyleBackColor = true;
            this.ImperialRadBtn.CheckedChanged += new System.EventHandler(this.ImperialRadBtn_CheckedChanged);
            // 
            // USRadBtn
            // 
            this.USRadBtn.AutoSize = true;
            this.USRadBtn.Checked = true;
            this.USRadBtn.Location = new System.Drawing.Point(117, 13);
            this.USRadBtn.Name = "USRadBtn";
            this.USRadBtn.Size = new System.Drawing.Size(48, 21);
            this.USRadBtn.TabIndex = 1;
            this.USRadBtn.TabStop = true;
            this.USRadBtn.Text = "US";
            this.USRadBtn.UseVisualStyleBackColor = true;
            this.USRadBtn.CheckedChanged += new System.EventHandler(this.USRadBtn_CheckedChanged);
            // 
            // MetricRadBtn
            // 
            this.MetricRadBtn.AutoSize = true;
            this.MetricRadBtn.Location = new System.Drawing.Point(5, 13);
            this.MetricRadBtn.Name = "MetricRadBtn";
            this.MetricRadBtn.Size = new System.Drawing.Size(67, 21);
            this.MetricRadBtn.TabIndex = 0;
            this.MetricRadBtn.Text = "Metric";
            this.MetricRadBtn.UseVisualStyleBackColor = true;
            this.MetricRadBtn.CheckedChanged += new System.EventHandler(this.MetricRadBtn_CheckedChanged);
            // 
            // tabControlAirDist
            // 
            this.tabControlAirDist.Controls.Add(this.tabPageCombiMode);
            this.tabControlAirDist.Controls.Add(this.tabPageDiscreteMode);
            this.tabControlAirDist.Controls.Add(this.tabPageFourMode);
            this.tabControlAirDist.Location = new System.Drawing.Point(4, 288);
            this.tabControlAirDist.Margin = new System.Windows.Forms.Padding(4);
            this.tabControlAirDist.Name = "tabControlAirDist";
            this.tabControlAirDist.SelectedIndex = 0;
            this.tabControlAirDist.Size = new System.Drawing.Size(697, 311);
            this.tabControlAirDist.TabIndex = 33;
            // 
            // tabPageCombiMode
            // 
            this.tabPageCombiMode.Controls.Add(this.panel3);
            this.tabPageCombiMode.Location = new System.Drawing.Point(4, 25);
            this.tabPageCombiMode.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageCombiMode.Name = "tabPageCombiMode";
            this.tabPageCombiMode.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageCombiMode.Size = new System.Drawing.Size(689, 282);
            this.tabPageCombiMode.TabIndex = 1;
            this.tabPageCombiMode.Text = "Combi Mode";
            this.tabPageCombiMode.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel3.Controls.Add(this.lblCombiMode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btnAirDistCombiAuto);
            this.panel3.Controls.Add(this.btnAirDistCombiMaxDefrost);
            this.panel3.Controls.Add(this.btnAirDistCombitWindshield);
            this.panel3.Controls.Add(this.btnAirDistCombiFloor);
            this.panel3.Controls.Add(this.btnAirDistCombiFace);
            this.panel3.Controls.Add(this.picBoxAutoCombi);
            this.panel3.Controls.Add(this.picBoxMaxDefrost);
            this.panel3.Location = new System.Drawing.Point(6, -5);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(679, 293);
            this.panel3.TabIndex = 2;
            // 
            // lblCombiMode
            // 
            this.lblCombiMode.AutoSize = true;
            this.lblCombiMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCombiMode.ForeColor = System.Drawing.Color.White;
            this.lblCombiMode.Location = new System.Drawing.Point(197, 145);
            this.lblCombiMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCombiMode.Name = "lblCombiMode";
            this.lblCombiMode.Size = new System.Drawing.Size(106, 17);
            this.lblCombiMode.TabIndex = 51;
            this.lblCombiMode.Text = "Combi Mode: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(179, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 20);
            this.label1.TabIndex = 50;
            this.label1.Text = "Air Distribution Combi Mode";
            // 
            // btnAirDistCombiAuto
            // 
            this.btnAirDistCombiAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistCombiAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAirDistCombiAuto.Location = new System.Drawing.Point(284, 174);
            this.btnAirDistCombiAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistCombiAuto.Name = "btnAirDistCombiAuto";
            this.btnAirDistCombiAuto.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistCombiAuto.TabIndex = 46;
            this.btnAirDistCombiAuto.Text = "AUTO OFF";
            this.btnAirDistCombiAuto.UseVisualStyleBackColor = true;
            this.btnAirDistCombiAuto.Click += new System.EventHandler(this.btnAirDistCombiAuto_Click);
            // 
            // btnAirDistCombiMaxDefrost
            // 
            this.btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
            this.btnAirDistCombiMaxDefrost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistCombiMaxDefrost.Location = new System.Drawing.Point(369, 174);
            this.btnAirDistCombiMaxDefrost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistCombiMaxDefrost.Name = "btnAirDistCombiMaxDefrost";
            this.btnAirDistCombiMaxDefrost.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistCombiMaxDefrost.TabIndex = 45;
            this.btnAirDistCombiMaxDefrost.UseVisualStyleBackColor = true;
            this.btnAirDistCombiMaxDefrost.Click += new System.EventHandler(this.btnAirDistCombiMaxDefrost_Click);
            // 
            // btnAirDistCombitWindshield
            // 
            this.btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
            this.btnAirDistCombitWindshield.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistCombitWindshield.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnAirDistCombitWindshield.Location = new System.Drawing.Point(199, 55);
            this.btnAirDistCombitWindshield.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistCombitWindshield.Name = "btnAirDistCombitWindshield";
            this.btnAirDistCombitWindshield.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistCombitWindshield.TabIndex = 34;
            this.btnAirDistCombitWindshield.UseVisualStyleBackColor = true;
            this.btnAirDistCombitWindshield.Click += new System.EventHandler(this.btnAirDistCombitWindshield_Click);
            // 
            // btnAirDistCombiFloor
            // 
            this.btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press;
            this.btnAirDistCombiFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistCombiFloor.Location = new System.Drawing.Point(369, 55);
            this.btnAirDistCombiFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistCombiFloor.Name = "btnAirDistCombiFloor";
            this.btnAirDistCombiFloor.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistCombiFloor.TabIndex = 33;
            this.btnAirDistCombiFloor.UseVisualStyleBackColor = true;
            this.btnAirDistCombiFloor.Click += new System.EventHandler(this.btnAirDistCombiFloor_Click);
            // 
            // btnAirDistCombiFace
            // 
            this.btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
            this.btnAirDistCombiFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistCombiFace.Location = new System.Drawing.Point(284, 55);
            this.btnAirDistCombiFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistCombiFace.Name = "btnAirDistCombiFace";
            this.btnAirDistCombiFace.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistCombiFace.TabIndex = 32;
            this.btnAirDistCombiFace.UseVisualStyleBackColor = true;
            this.btnAirDistCombiFace.Click += new System.EventHandler(this.btnAirDistCombiFace_Click);
            // 
            // picBoxAutoCombi
            // 
            this.picBoxAutoCombi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxAutoCombi.Image = global::HVAC.Properties.Resources.climate_air_auto_right;
            this.picBoxAutoCombi.Location = new System.Drawing.Point(191, 33);
            this.picBoxAutoCombi.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxAutoCombi.Name = "picBoxAutoCombi";
            this.picBoxAutoCombi.Size = new System.Drawing.Size(267, 42);
            this.picBoxAutoCombi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxAutoCombi.TabIndex = 48;
            this.picBoxAutoCombi.TabStop = false;
            // 
            // picBoxMaxDefrost
            // 
            this.picBoxMaxDefrost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxMaxDefrost.Location = new System.Drawing.Point(191, 32);
            this.picBoxMaxDefrost.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxMaxDefrost.Name = "picBoxMaxDefrost";
            this.picBoxMaxDefrost.Size = new System.Drawing.Size(267, 42);
            this.picBoxMaxDefrost.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxMaxDefrost.TabIndex = 49;
            this.picBoxMaxDefrost.TabStop = false;
            // 
            // tabPageDiscreteMode
            // 
            this.tabPageDiscreteMode.Controls.Add(this.panel5);
            this.tabPageDiscreteMode.Location = new System.Drawing.Point(4, 25);
            this.tabPageDiscreteMode.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageDiscreteMode.Name = "tabPageDiscreteMode";
            this.tabPageDiscreteMode.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageDiscreteMode.Size = new System.Drawing.Size(689, 282);
            this.tabPageDiscreteMode.TabIndex = 0;
            this.tabPageDiscreteMode.Text = "Discrete Mode";
            this.tabPageDiscreteMode.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel5.Controls.Add(this.btnAirDistLeftFloorWindShield);
            this.panel5.Controls.Add(this.btnAirDistLeftBiLevel);
            this.panel5.Controls.Add(this.btnAirDistRightBiLevel);
            this.panel5.Controls.Add(this.btnAirDistLeftStatus);
            this.panel5.Controls.Add(this.btnAirDistRightStatus);
            this.panel5.Controls.Add(this.btnHeat);
            this.panel5.Controls.Add(this.lblRightMode);
            this.panel5.Controls.Add(this.lblLeftMode);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.btnAirDistRightAuto);
            this.panel5.Controls.Add(this.btnAirDistLeftAuto);
            this.panel5.Controls.Add(this.btnAirDistMaxDefrost);
            this.panel5.Controls.Add(this.btnAirDistRightFloor);
            this.panel5.Controls.Add(this.btnAirDistRightFace);
            this.panel5.Controls.Add(this.btnAirDistRightWindshield);
            this.panel5.Controls.Add(this.btnAirDistLeftWindshield);
            this.panel5.Controls.Add(this.btnAirDistLeftFace);
            this.panel5.Controls.Add(this.btnAirDistLeftFloor);
            this.panel5.Controls.Add(this.picBoxAutoLeft);
            this.panel5.Controls.Add(this.picBoxAutoRight);
            this.panel5.Controls.Add(this.picBoxAuto);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Location = new System.Drawing.Point(6, -5);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(676, 293);
            this.panel5.TabIndex = 27;
            this.panel5.Visible = false;
            // 
            // btnAirDistLeftFloorWindShield
            // 
            this.btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
            this.btnAirDistLeftFloorWindShield.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftFloorWindShield.Location = new System.Drawing.Point(284, 54);
            this.btnAirDistLeftFloorWindShield.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftFloorWindShield.Name = "btnAirDistLeftFloorWindShield";
            this.btnAirDistLeftFloorWindShield.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftFloorWindShield.TabIndex = 47;
            this.btnAirDistLeftFloorWindShield.UseVisualStyleBackColor = true;
            this.btnAirDistLeftFloorWindShield.Click += new System.EventHandler(this.btnAirDistLeftFloorWindShield_Click);
            // 
            // btnAirDistLeftBiLevel
            // 
            this.btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
            this.btnAirDistLeftBiLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftBiLevel.Location = new System.Drawing.Point(195, 54);
            this.btnAirDistLeftBiLevel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftBiLevel.Name = "btnAirDistLeftBiLevel";
            this.btnAirDistLeftBiLevel.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftBiLevel.TabIndex = 46;
            this.btnAirDistLeftBiLevel.UseVisualStyleBackColor = true;
            this.btnAirDistLeftBiLevel.Click += new System.EventHandler(this.btnAirDistLeftBiLevel_Click);
            // 
            // btnAirDistRightBiLevel
            // 
            this.btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
            this.btnAirDistRightBiLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistRightBiLevel.Location = new System.Drawing.Point(396, 54);
            this.btnAirDistRightBiLevel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistRightBiLevel.Name = "btnAirDistRightBiLevel";
            this.btnAirDistRightBiLevel.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistRightBiLevel.TabIndex = 45;
            this.btnAirDistRightBiLevel.UseVisualStyleBackColor = true;
            this.btnAirDistRightBiLevel.Click += new System.EventHandler(this.btnAirDistRightBiLevel_Click);
            // 
            // btnAirDistLeftStatus
            // 
            this.btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
            this.btnAirDistLeftStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftStatus.Location = new System.Drawing.Point(32, 176);
            this.btnAirDistLeftStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftStatus.Name = "btnAirDistLeftStatus";
            this.btnAirDistLeftStatus.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftStatus.TabIndex = 44;
            this.btnAirDistLeftStatus.UseVisualStyleBackColor = true;
            this.btnAirDistLeftStatus.Click += new System.EventHandler(this.btnAirDistLeftStatus_Click);
            // 
            // btnAirDistRightStatus
            // 
            this.btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
            this.btnAirDistRightStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistRightStatus.Location = new System.Drawing.Point(563, 176);
            this.btnAirDistRightStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistRightStatus.Name = "btnAirDistRightStatus";
            this.btnAirDistRightStatus.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistRightStatus.TabIndex = 43;
            this.btnAirDistRightStatus.UseVisualStyleBackColor = true;
            this.btnAirDistRightStatus.Click += new System.EventHandler(this.btnAirDistRightStatus_Click);
            // 
            // btnHeat
            // 
            this.btnHeat.BackgroundImage = global::HVAC.Properties.Resources.climate_heat_inactive_press;
            this.btnHeat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHeat.Location = new System.Drawing.Point(341, 176);
            this.btnHeat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHeat.Name = "btnHeat";
            this.btnHeat.Size = new System.Drawing.Size(80, 74);
            this.btnHeat.TabIndex = 42;
            this.btnHeat.UseVisualStyleBackColor = true;
            this.btnHeat.Click += new System.EventHandler(this.btnHeat_Click);
            // 
            // lblRightMode
            // 
            this.lblRightMode.AutoSize = true;
            this.lblRightMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRightMode.ForeColor = System.Drawing.Color.White;
            this.lblRightMode.Location = new System.Drawing.Point(393, 143);
            this.lblRightMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRightMode.Name = "lblRightMode";
            this.lblRightMode.Size = new System.Drawing.Size(100, 17);
            this.lblRightMode.TabIndex = 41;
            this.lblRightMode.Text = "Right Mode: ";
            // 
            // lblLeftMode
            // 
            this.lblLeftMode.AutoSize = true;
            this.lblLeftMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeftMode.ForeColor = System.Drawing.Color.White;
            this.lblLeftMode.Location = new System.Drawing.Point(25, 143);
            this.lblLeftMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLeftMode.Name = "lblLeftMode";
            this.lblLeftMode.Size = new System.Drawing.Size(90, 17);
            this.lblLeftMode.TabIndex = 40;
            this.lblLeftMode.Text = "Left Mode: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(168, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(325, 20);
            this.label2.TabIndex = 35;
            this.label2.Text = "Dual Air Distribution Mode (Discrete)";
            // 
            // btnAirDistRightAuto
            // 
            this.btnAirDistRightAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistRightAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAirDistRightAuto.Location = new System.Drawing.Point(468, 176);
            this.btnAirDistRightAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistRightAuto.Name = "btnAirDistRightAuto";
            this.btnAirDistRightAuto.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistRightAuto.TabIndex = 34;
            this.btnAirDistRightAuto.Text = "AUTO OFF";
            this.btnAirDistRightAuto.UseVisualStyleBackColor = true;
            this.btnAirDistRightAuto.Click += new System.EventHandler(this.btnAirDistRightAuto_Click);
            // 
            // btnAirDistLeftAuto
            // 
            this.btnAirDistLeftAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAirDistLeftAuto.Location = new System.Drawing.Point(121, 176);
            this.btnAirDistLeftAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftAuto.Name = "btnAirDistLeftAuto";
            this.btnAirDistLeftAuto.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftAuto.TabIndex = 33;
            this.btnAirDistLeftAuto.Text = "AUTO OFF";
            this.btnAirDistLeftAuto.UseVisualStyleBackColor = true;
            this.btnAirDistLeftAuto.Click += new System.EventHandler(this.btnAirDistLeftAuto_Click);
            // 
            // btnAirDistMaxDefrost
            // 
            this.btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
            this.btnAirDistMaxDefrost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistMaxDefrost.Location = new System.Drawing.Point(247, 176);
            this.btnAirDistMaxDefrost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistMaxDefrost.Name = "btnAirDistMaxDefrost";
            this.btnAirDistMaxDefrost.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistMaxDefrost.TabIndex = 32;
            this.btnAirDistMaxDefrost.UseVisualStyleBackColor = true;
            this.btnAirDistMaxDefrost.Click += new System.EventHandler(this.btnAirDistMaxDefrost_Click);
            // 
            // btnAirDistRightFloor
            // 
            this.btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press;
            this.btnAirDistRightFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistRightFloor.Location = new System.Drawing.Point(567, 54);
            this.btnAirDistRightFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistRightFloor.Name = "btnAirDistRightFloor";
            this.btnAirDistRightFloor.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistRightFloor.TabIndex = 31;
            this.btnAirDistRightFloor.UseVisualStyleBackColor = true;
            this.btnAirDistRightFloor.Click += new System.EventHandler(this.btnAirDistRightFloor_Click);
            // 
            // btnAirDistRightFace
            // 
            this.btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
            this.btnAirDistRightFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistRightFace.Location = new System.Drawing.Point(481, 54);
            this.btnAirDistRightFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistRightFace.Name = "btnAirDistRightFace";
            this.btnAirDistRightFace.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistRightFace.TabIndex = 30;
            this.btnAirDistRightFace.UseVisualStyleBackColor = true;
            this.btnAirDistRightFace.Click += new System.EventHandler(this.btnAirDistRightFace_Click);
            // 
            // btnAirDistRightWindshield
            // 
            this.btnAirDistRightWindshield.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistRightWindshield.Location = new System.Drawing.Point(397, 54);
            this.btnAirDistRightWindshield.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistRightWindshield.Name = "btnAirDistRightWindshield";
            this.btnAirDistRightWindshield.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistRightWindshield.TabIndex = 29;
            this.btnAirDistRightWindshield.UseVisualStyleBackColor = true;
            this.btnAirDistRightWindshield.Visible = false;
            // 
            // btnAirDistLeftWindshield
            // 
            this.btnAirDistLeftWindshield.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftWindshield.Location = new System.Drawing.Point(197, 54);
            this.btnAirDistLeftWindshield.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftWindshield.Name = "btnAirDistLeftWindshield";
            this.btnAirDistLeftWindshield.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftWindshield.TabIndex = 28;
            this.btnAirDistLeftWindshield.UseVisualStyleBackColor = true;
            // 
            // btnAirDistLeftFace
            // 
            this.btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
            this.btnAirDistLeftFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftFace.Location = new System.Drawing.Point(109, 54);
            this.btnAirDistLeftFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftFace.Name = "btnAirDistLeftFace";
            this.btnAirDistLeftFace.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftFace.TabIndex = 27;
            this.btnAirDistLeftFace.UseVisualStyleBackColor = true;
            this.btnAirDistLeftFace.Click += new System.EventHandler(this.btnAirDistLeftFace_Click);
            // 
            // btnAirDistLeftFloor
            // 
            this.btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
            this.btnAirDistLeftFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistLeftFloor.Location = new System.Drawing.Point(24, 54);
            this.btnAirDistLeftFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistLeftFloor.Name = "btnAirDistLeftFloor";
            this.btnAirDistLeftFloor.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistLeftFloor.TabIndex = 26;
            this.btnAirDistLeftFloor.UseVisualStyleBackColor = true;
            this.btnAirDistLeftFloor.Click += new System.EventHandler(this.btnAirDistLeftFloor_Click);
            // 
            // picBoxAutoLeft
            // 
            this.picBoxAutoLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxAutoLeft.Image = global::HVAC.Properties.Resources.climate_air_auto_left;
            this.picBoxAutoLeft.Location = new System.Drawing.Point(16, 32);
            this.picBoxAutoLeft.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxAutoLeft.Name = "picBoxAutoLeft";
            this.picBoxAutoLeft.Size = new System.Drawing.Size(357, 42);
            this.picBoxAutoLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxAutoLeft.TabIndex = 36;
            this.picBoxAutoLeft.TabStop = false;
            // 
            // picBoxAutoRight
            // 
            this.picBoxAutoRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxAutoRight.Image = global::HVAC.Properties.Resources.climate_air_auto_right;
            this.picBoxAutoRight.Location = new System.Drawing.Point(387, 32);
            this.picBoxAutoRight.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxAutoRight.Name = "picBoxAutoRight";
            this.picBoxAutoRight.Size = new System.Drawing.Size(267, 42);
            this.picBoxAutoRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxAutoRight.TabIndex = 37;
            this.picBoxAutoRight.TabStop = false;
            // 
            // picBoxAuto
            // 
            this.picBoxAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxAuto.Image = global::HVAC.Properties.Resources.climate_air_auto;
            this.picBoxAuto.Location = new System.Drawing.Point(16, 33);
            this.picBoxAuto.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxAuto.Name = "picBoxAuto";
            this.picBoxAuto.Size = new System.Drawing.Size(635, 42);
            this.picBoxAuto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxAuto.TabIndex = 38;
            this.picBoxAuto.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(16, 32);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(637, 42);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            // 
            // tabPageFourMode
            // 
            this.tabPageFourMode.Controls.Add(this.panel6);
            this.tabPageFourMode.Location = new System.Drawing.Point(4, 25);
            this.tabPageFourMode.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageFourMode.Name = "tabPageFourMode";
            this.tabPageFourMode.Size = new System.Drawing.Size(689, 282);
            this.tabPageFourMode.TabIndex = 2;
            this.tabPageFourMode.Text = "4-Mode";
            this.tabPageFourMode.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel6.Controls.Add(this.btnAirDistFourModeBiLevel);
            this.panel6.Controls.Add(this.btnAirDistFourModeFloorWindShield);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.btnAirDistFourModeStatus);
            this.panel6.Controls.Add(this.btnAirDistFourModeAuto);
            this.panel6.Controls.Add(this.btnAirDistFourModeMaxDefrost);
            this.panel6.Controls.Add(this.btnAirDistFourModeFloor);
            this.panel6.Controls.Add(this.btnAirDistFourModeFace);
            this.panel6.Controls.Add(this.picBoxAutoFourMode);
            this.panel6.Controls.Add(this.pictureBox2);
            this.panel6.Location = new System.Drawing.Point(6, -5);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(676, 293);
            this.panel6.TabIndex = 3;
            this.panel6.Visible = false;
            // 
            // btnAirDistFourModeBiLevel
            // 
            this.btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
            this.btnAirDistFourModeBiLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeBiLevel.Location = new System.Drawing.Point(321, 55);
            this.btnAirDistFourModeBiLevel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeBiLevel.Name = "btnAirDistFourModeBiLevel";
            this.btnAirDistFourModeBiLevel.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeBiLevel.TabIndex = 53;
            this.btnAirDistFourModeBiLevel.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeBiLevel.Click += new System.EventHandler(this.btnAirDistFourModeBiLevel_Click);
            // 
            // btnAirDistFourModeFloorWindShield
            // 
            this.btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
            this.btnAirDistFourModeFloorWindShield.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeFloorWindShield.Location = new System.Drawing.Point(407, 55);
            this.btnAirDistFourModeFloorWindShield.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeFloorWindShield.Name = "btnAirDistFourModeFloorWindShield";
            this.btnAirDistFourModeFloorWindShield.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeFloorWindShield.TabIndex = 52;
            this.btnAirDistFourModeFloorWindShield.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeFloorWindShield.Click += new System.EventHandler(this.btnAirDistFourModeFloorWindShield_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(197, 145);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 51;
            this.label3.Text = "Four Mode: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(197, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(232, 20);
            this.label4.TabIndex = 50;
            this.label4.Text = "Air Distribution Four Mode";
            // 
            // btnAirDistFourModeStatus
            // 
            this.btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press1;
            this.btnAirDistFourModeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeStatus.Location = new System.Drawing.Point(199, 174);
            this.btnAirDistFourModeStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeStatus.Name = "btnAirDistFourModeStatus";
            this.btnAirDistFourModeStatus.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeStatus.TabIndex = 47;
            this.btnAirDistFourModeStatus.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeStatus.Click += new System.EventHandler(this.btnAirDistFourModeStatus_Click);
            // 
            // btnAirDistFourModeAuto
            // 
            this.btnAirDistFourModeAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAirDistFourModeAuto.Location = new System.Drawing.Point(284, 174);
            this.btnAirDistFourModeAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeAuto.Name = "btnAirDistFourModeAuto";
            this.btnAirDistFourModeAuto.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeAuto.TabIndex = 46;
            this.btnAirDistFourModeAuto.Text = "AUTO OFF";
            this.btnAirDistFourModeAuto.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeAuto.Click += new System.EventHandler(this.btnAirDistFourModeAuto_Click);
            // 
            // btnAirDistFourModeMaxDefrost
            // 
            this.btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
            this.btnAirDistFourModeMaxDefrost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeMaxDefrost.Location = new System.Drawing.Point(369, 174);
            this.btnAirDistFourModeMaxDefrost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeMaxDefrost.Name = "btnAirDistFourModeMaxDefrost";
            this.btnAirDistFourModeMaxDefrost.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeMaxDefrost.TabIndex = 45;
            this.btnAirDistFourModeMaxDefrost.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeMaxDefrost.Click += new System.EventHandler(this.btnAirDistFourModeMaxDefrost_Click);
            // 
            // btnAirDistFourModeFloor
            // 
            this.btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press;
            this.btnAirDistFourModeFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeFloor.Location = new System.Drawing.Point(236, 55);
            this.btnAirDistFourModeFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeFloor.Name = "btnAirDistFourModeFloor";
            this.btnAirDistFourModeFloor.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeFloor.TabIndex = 33;
            this.btnAirDistFourModeFloor.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeFloor.Click += new System.EventHandler(this.btnAirDistFourModeFloor_Click);
            // 
            // btnAirDistFourModeFace
            // 
            this.btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
            this.btnAirDistFourModeFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirDistFourModeFace.Location = new System.Drawing.Point(151, 55);
            this.btnAirDistFourModeFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirDistFourModeFace.Name = "btnAirDistFourModeFace";
            this.btnAirDistFourModeFace.Size = new System.Drawing.Size(80, 74);
            this.btnAirDistFourModeFace.TabIndex = 32;
            this.btnAirDistFourModeFace.UseVisualStyleBackColor = true;
            this.btnAirDistFourModeFace.Click += new System.EventHandler(this.btnAirDistFourModeFace_Click);
            // 
            // picBoxAutoFourMode
            // 
            this.picBoxAutoFourMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxAutoFourMode.Image = global::HVAC.Properties.Resources.climate_air_auto_right;
            this.picBoxAutoFourMode.Location = new System.Drawing.Point(137, 31);
            this.picBoxAutoFourMode.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxAutoFourMode.Name = "picBoxAutoFourMode";
            this.picBoxAutoFourMode.Size = new System.Drawing.Size(363, 42);
            this.picBoxAutoFourMode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxAutoFourMode.TabIndex = 48;
            this.picBoxAutoFourMode.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Location = new System.Drawing.Point(137, 32);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(363, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 49;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel1.Controls.Add(this.lblFrontFanSpeedSettings);
            this.panel1.Controls.Add(this.lblFrontLeftTempSettings);
            this.panel1.Controls.Add(this.lblFrontRightTempSettings);
            this.panel1.Controls.Add(this.cmbIndBlwStngStFrt);
            this.panel1.Controls.Add(this.cmbIndTmpStngStFL);
            this.panel1.Controls.Add(this.cmbIndTmpStngStFR);
            this.panel1.Controls.Add(this.lblFrontFanSpeed);
            this.panel1.Controls.Add(this.lblFrontLeftTemp);
            this.panel1.Controls.Add(this.lblFrontRightTemp);
            this.panel1.Controls.Add(this.nudFrontFanSpeed);
            this.panel1.Controls.Add(this.nudFrontLeftTemp);
            this.panel1.Controls.Add(this.nudFrontRightTemp);
            this.panel1.Controls.Add(this.btnIonizerStatusOn);
            this.panel1.Controls.Add(this.btnSync);
            this.panel1.Controls.Add(this.btnAuto);
            this.panel1.Controls.Add(this.btnRecir);
            this.panel1.Controls.Add(this.btnAC);
            this.panel1.Controls.Add(this.btnPower);
            this.panel1.Location = new System.Drawing.Point(7, 7);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(693, 273);
            this.panel1.TabIndex = 31;
            // 
            // lblFrontFanSpeedSettings
            // 
            this.lblFrontFanSpeedSettings.AutoSize = true;
            this.lblFrontFanSpeedSettings.Location = new System.Drawing.Point(353, 132);
            this.lblFrontFanSpeedSettings.Name = "lblFrontFanSpeedSettings";
            this.lblFrontFanSpeedSettings.Size = new System.Drawing.Size(132, 17);
            this.lblFrontFanSpeedSettings.TabIndex = 50;
            this.lblFrontFanSpeedSettings.Text = "Fan Speed Settings";
            // 
            // lblFrontLeftTempSettings
            // 
            this.lblFrontLeftTempSettings.AutoSize = true;
            this.lblFrontLeftTempSettings.Location = new System.Drawing.Point(354, 77);
            this.lblFrontLeftTempSettings.Name = "lblFrontLeftTempSettings";
            this.lblFrontLeftTempSettings.Size = new System.Drawing.Size(164, 17);
            this.lblFrontLeftTempSettings.TabIndex = 49;
            this.lblFrontLeftTempSettings.Text = "Front Left Temp Settings";
            // 
            // lblFrontRightTempSettings
            // 
            this.lblFrontRightTempSettings.AutoSize = true;
            this.lblFrontRightTempSettings.Location = new System.Drawing.Point(354, 29);
            this.lblFrontRightTempSettings.Name = "lblFrontRightTempSettings";
            this.lblFrontRightTempSettings.Size = new System.Drawing.Size(173, 17);
            this.lblFrontRightTempSettings.TabIndex = 48;
            this.lblFrontRightTempSettings.Text = "Front Right Temp Settings";
            // 
            // cmbIndBlwStngStFrt
            // 
            this.cmbIndBlwStngStFrt.FormattingEnabled = true;
            this.cmbIndBlwStngStFrt.Location = new System.Drawing.Point(540, 133);
            this.cmbIndBlwStngStFrt.Name = "cmbIndBlwStngStFrt";
            this.cmbIndBlwStngStFrt.Size = new System.Drawing.Size(121, 24);
            this.cmbIndBlwStngStFrt.TabIndex = 41;
            this.cmbIndBlwStngStFrt.SelectedIndexChanged += new System.EventHandler(this.cmbIndBlwStngStFrt_SelectedIndexChanged);
            // 
            // cmbIndTmpStngStFL
            // 
            this.cmbIndTmpStngStFL.FormattingEnabled = true;
            this.cmbIndTmpStngStFL.Location = new System.Drawing.Point(540, 77);
            this.cmbIndTmpStngStFL.Name = "cmbIndTmpStngStFL";
            this.cmbIndTmpStngStFL.Size = new System.Drawing.Size(121, 24);
            this.cmbIndTmpStngStFL.TabIndex = 40;
            this.cmbIndTmpStngStFL.SelectedIndexChanged += new System.EventHandler(this.cmbIndTmpStngStFL_SelectedIndexChanged);
            // 
            // cmbIndTmpStngStFR
            // 
            this.cmbIndTmpStngStFR.FormattingEnabled = true;
            this.cmbIndTmpStngStFR.Location = new System.Drawing.Point(540, 22);
            this.cmbIndTmpStngStFR.Name = "cmbIndTmpStngStFR";
            this.cmbIndTmpStngStFR.Size = new System.Drawing.Size(121, 24);
            this.cmbIndTmpStngStFR.TabIndex = 39;
            this.cmbIndTmpStngStFR.SelectedIndexChanged += new System.EventHandler(this.cmbIndTmpStngStFR_SelectedIndexChanged);
            // 
            // lblFrontFanSpeed
            // 
            this.lblFrontFanSpeed.AutoSize = true;
            this.lblFrontFanSpeed.Location = new System.Drawing.Point(42, 136);
            this.lblFrontFanSpeed.Name = "lblFrontFanSpeed";
            this.lblFrontFanSpeed.Size = new System.Drawing.Size(77, 17);
            this.lblFrontFanSpeed.TabIndex = 38;
            this.lblFrontFanSpeed.Text = "Fan Speed";
            // 
            // lblFrontLeftTemp
            // 
            this.lblFrontLeftTemp.AutoSize = true;
            this.lblFrontLeftTemp.Location = new System.Drawing.Point(39, 80);
            this.lblFrontLeftTemp.Name = "lblFrontLeftTemp";
            this.lblFrontLeftTemp.Size = new System.Drawing.Size(109, 17);
            this.lblFrontLeftTemp.TabIndex = 37;
            this.lblFrontLeftTemp.Text = "Front Left Temp";
            // 
            // lblFrontRightTemp
            // 
            this.lblFrontRightTemp.AutoSize = true;
            this.lblFrontRightTemp.Location = new System.Drawing.Point(42, 24);
            this.lblFrontRightTemp.Name = "lblFrontRightTemp";
            this.lblFrontRightTemp.Size = new System.Drawing.Size(118, 17);
            this.lblFrontRightTemp.TabIndex = 36;
            this.lblFrontRightTemp.Text = "Front Right Temp";
            // 
            // nudFrontFanSpeed
            // 
            this.nudFrontFanSpeed.Location = new System.Drawing.Point(196, 134);
            this.nudFrontFanSpeed.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudFrontFanSpeed.Name = "nudFrontFanSpeed";
            this.nudFrontFanSpeed.Size = new System.Drawing.Size(120, 22);
            this.nudFrontFanSpeed.TabIndex = 27;
            this.nudFrontFanSpeed.ValueChanged += new System.EventHandler(this.nudFrontFanSpeed_ValueChanged);
            // 
            // nudFrontLeftTemp
            // 
            this.nudFrontLeftTemp.Location = new System.Drawing.Point(196, 78);
            this.nudFrontLeftTemp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudFrontLeftTemp.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudFrontLeftTemp.Name = "nudFrontLeftTemp";
            this.nudFrontLeftTemp.Size = new System.Drawing.Size(120, 22);
            this.nudFrontLeftTemp.TabIndex = 26;
            this.nudFrontLeftTemp.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudFrontLeftTemp.ValueChanged += new System.EventHandler(this.nudFrontLeftTemp_ValueChanged);
            // 
            // nudFrontRightTemp
            // 
            this.nudFrontRightTemp.Location = new System.Drawing.Point(196, 22);
            this.nudFrontRightTemp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudFrontRightTemp.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudFrontRightTemp.Name = "nudFrontRightTemp";
            this.nudFrontRightTemp.Size = new System.Drawing.Size(120, 22);
            this.nudFrontRightTemp.TabIndex = 25;
            this.nudFrontRightTemp.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudFrontRightTemp.ValueChanged += new System.EventHandler(this.nudFrontRightTemp_ValueChanged);
            // 
            // btnIonizerStatusOn
            // 
            this.btnIonizerStatusOn.Location = new System.Drawing.Point(533, 191);
            this.btnIonizerStatusOn.Margin = new System.Windows.Forms.Padding(4);
            this.btnIonizerStatusOn.Name = "btnIonizerStatusOn";
            this.btnIonizerStatusOn.Size = new System.Drawing.Size(66, 61);
            this.btnIonizerStatusOn.TabIndex = 24;
            this.btnIonizerStatusOn.Text = "Ionizer Status OFF";
            this.btnIonizerStatusOn.UseVisualStyleBackColor = true;
            this.btnIonizerStatusOn.Click += new System.EventHandler(this.btnIonizerStatusOn_Click);
            // 
            // btnSync
            // 
            this.btnSync.Location = new System.Drawing.Point(403, 191);
            this.btnSync.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSync.Name = "btnSync";
            this.btnSync.Size = new System.Drawing.Size(97, 53);
            this.btnSync.TabIndex = 22;
            this.btnSync.Text = "Sync Off";
            this.btnSync.UseVisualStyleBackColor = true;
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            // 
            // btnAuto
            // 
            this.btnAuto.Location = new System.Drawing.Point(204, 191);
            this.btnAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(66, 53);
            this.btnAuto.TabIndex = 23;
            this.btnAuto.Text = "Auto Off";
            this.btnAuto.UseVisualStyleBackColor = true;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // btnRecir
            // 
            this.btnRecir.Location = new System.Drawing.Point(291, 191);
            this.btnRecir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRecir.Name = "btnRecir";
            this.btnRecir.Size = new System.Drawing.Size(80, 53);
            this.btnRecir.TabIndex = 21;
            this.btnRecir.Text = "Recir Off";
            this.btnRecir.UseVisualStyleBackColor = true;
            this.btnRecir.Click += new System.EventHandler(this.btnRecir_Click);
            // 
            // btnAC
            // 
            this.btnAC.Location = new System.Drawing.Point(116, 191);
            this.btnAC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAC.Name = "btnAC";
            this.btnAC.Size = new System.Drawing.Size(66, 53);
            this.btnAC.TabIndex = 20;
            this.btnAC.Text = "A/C Off";
            this.btnAC.UseVisualStyleBackColor = true;
            this.btnAC.Click += new System.EventHandler(this.btnAC_Click);
            // 
            // btnPower
            // 
            this.btnPower.Location = new System.Drawing.Point(23, 191);
            this.btnPower.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPower.Name = "btnPower";
            this.btnPower.Size = new System.Drawing.Size(66, 53);
            this.btnPower.TabIndex = 15;
            this.btnPower.Text = "Power Off";
            this.btnPower.UseVisualStyleBackColor = true;
            this.btnPower.Click += new System.EventHandler(this.btnPower_Click);
            // 
            // tabPageRearHvac
            // 
            this.tabPageRearHvac.Controls.Add(this.panel2);
            this.tabPageRearHvac.Controls.Add(this.tabControlRearAirDistribution);
            this.tabPageRearHvac.Location = new System.Drawing.Point(4, 25);
            this.tabPageRearHvac.Name = "tabPageRearHvac";
            this.tabPageRearHvac.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRearHvac.Size = new System.Drawing.Size(719, 669);
            this.tabPageRearHvac.TabIndex = 1;
            this.tabPageRearHvac.Text = "Rear Hvac";
            this.tabPageRearHvac.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel2.Controls.Add(this.lblFanSpeedSettings);
            this.panel2.Controls.Add(this.lblRearLeftTempSettings);
            this.panel2.Controls.Add(this.lblRightTempSettings);
            this.panel2.Controls.Add(this.cmbIndBlwStngStRr);
            this.panel2.Controls.Add(this.cmbIndTmpStngStRL);
            this.panel2.Controls.Add(this.cmbIndTmpStngStRR);
            this.panel2.Controls.Add(this.lblRearFanSpeed);
            this.panel2.Controls.Add(this.lblRearLeftTemp);
            this.panel2.Controls.Add(this.lblRearRightTemp);
            this.panel2.Controls.Add(this.nudFanSpeed);
            this.panel2.Controls.Add(this.nudRearLeftTemp);
            this.panel2.Controls.Add(this.nudRearRightTemp);
            this.panel2.Controls.Add(this.btnRearPanelLock);
            this.panel2.Controls.Add(this.btnRearAuto);
            this.panel2.Controls.Add(this.btnRearSync);
            this.panel2.Controls.Add(this.btnRearPower);
            this.panel2.Location = new System.Drawing.Point(7, 7);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(692, 293);
            this.panel2.TabIndex = 33;
            // 
            // lblFanSpeedSettings
            // 
            this.lblFanSpeedSettings.AutoSize = true;
            this.lblFanSpeedSettings.Location = new System.Drawing.Point(337, 125);
            this.lblFanSpeedSettings.Name = "lblFanSpeedSettings";
            this.lblFanSpeedSettings.Size = new System.Drawing.Size(132, 17);
            this.lblFanSpeedSettings.TabIndex = 47;
            this.lblFanSpeedSettings.Text = "Fan Speed Settings";
            // 
            // lblRearLeftTempSettings
            // 
            this.lblRearLeftTempSettings.AutoSize = true;
            this.lblRearLeftTempSettings.Location = new System.Drawing.Point(338, 70);
            this.lblRearLeftTempSettings.Name = "lblRearLeftTempSettings";
            this.lblRearLeftTempSettings.Size = new System.Drawing.Size(162, 17);
            this.lblRearLeftTempSettings.TabIndex = 46;
            this.lblRearLeftTempSettings.Text = "Rear Left Temp Settings";
            // 
            // lblRightTempSettings
            // 
            this.lblRightTempSettings.AutoSize = true;
            this.lblRightTempSettings.Location = new System.Drawing.Point(338, 22);
            this.lblRightTempSettings.Name = "lblRightTempSettings";
            this.lblRightTempSettings.Size = new System.Drawing.Size(171, 17);
            this.lblRightTempSettings.TabIndex = 45;
            this.lblRightTempSettings.Text = "Rear Right Temp Settings";
            // 
            // cmbIndBlwStngStRr
            // 
            this.cmbIndBlwStngStRr.FormattingEnabled = true;
            this.cmbIndBlwStngStRr.Location = new System.Drawing.Point(524, 122);
            this.cmbIndBlwStngStRr.Name = "cmbIndBlwStngStRr";
            this.cmbIndBlwStngStRr.Size = new System.Drawing.Size(121, 24);
            this.cmbIndBlwStngStRr.TabIndex = 44;
            this.cmbIndBlwStngStRr.SelectedIndexChanged += new System.EventHandler(this.cmbIndBlwStngStRr_SelectedIndexChanged);
            // 
            // cmbIndTmpStngStRL
            // 
            this.cmbIndTmpStngStRL.FormattingEnabled = true;
            this.cmbIndTmpStngStRL.Location = new System.Drawing.Point(524, 70);
            this.cmbIndTmpStngStRL.Name = "cmbIndTmpStngStRL";
            this.cmbIndTmpStngStRL.Size = new System.Drawing.Size(121, 24);
            this.cmbIndTmpStngStRL.TabIndex = 43;
            this.cmbIndTmpStngStRL.SelectedIndexChanged += new System.EventHandler(this.cmbIndTmpStngStRL_SelectedIndexChanged);
            // 
            // cmbIndTmpStngStRR
            // 
            this.cmbIndTmpStngStRR.FormattingEnabled = true;
            this.cmbIndTmpStngStRR.Location = new System.Drawing.Point(524, 22);
            this.cmbIndTmpStngStRR.Name = "cmbIndTmpStngStRR";
            this.cmbIndTmpStngStRR.Size = new System.Drawing.Size(121, 24);
            this.cmbIndTmpStngStRR.TabIndex = 42;
            this.cmbIndTmpStngStRR.SelectedIndexChanged += new System.EventHandler(this.cmbIndTmpStngStRR_SelectedIndexChanged);
            // 
            // lblRearFanSpeed
            // 
            this.lblRearFanSpeed.AutoSize = true;
            this.lblRearFanSpeed.Location = new System.Drawing.Point(43, 122);
            this.lblRearFanSpeed.Name = "lblRearFanSpeed";
            this.lblRearFanSpeed.Size = new System.Drawing.Size(77, 17);
            this.lblRearFanSpeed.TabIndex = 35;
            this.lblRearFanSpeed.Text = "Fan Speed";
            // 
            // lblRearLeftTemp
            // 
            this.lblRearLeftTemp.AutoSize = true;
            this.lblRearLeftTemp.Location = new System.Drawing.Point(43, 67);
            this.lblRearLeftTemp.Name = "lblRearLeftTemp";
            this.lblRearLeftTemp.Size = new System.Drawing.Size(107, 17);
            this.lblRearLeftTemp.TabIndex = 34;
            this.lblRearLeftTemp.Text = "Rear Left Temp";
            // 
            // lblRearRightTemp
            // 
            this.lblRearRightTemp.AutoSize = true;
            this.lblRearRightTemp.Location = new System.Drawing.Point(43, 21);
            this.lblRearRightTemp.Name = "lblRearRightTemp";
            this.lblRearRightTemp.Size = new System.Drawing.Size(116, 17);
            this.lblRearRightTemp.TabIndex = 33;
            this.lblRearRightTemp.Text = "Rear Right Temp";
            // 
            // nudFanSpeed
            // 
            this.nudFanSpeed.Location = new System.Drawing.Point(181, 120);
            this.nudFanSpeed.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudFanSpeed.Name = "nudFanSpeed";
            this.nudFanSpeed.Size = new System.Drawing.Size(120, 22);
            this.nudFanSpeed.TabIndex = 32;
            this.nudFanSpeed.ValueChanged += new System.EventHandler(this.nudFanSpeed_ValueChanged);
            // 
            // nudRearLeftTemp
            // 
            this.nudRearLeftTemp.Location = new System.Drawing.Point(181, 68);
            this.nudRearLeftTemp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudRearLeftTemp.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudRearLeftTemp.Name = "nudRearLeftTemp";
            this.nudRearLeftTemp.Size = new System.Drawing.Size(120, 22);
            this.nudRearLeftTemp.TabIndex = 31;
            this.nudRearLeftTemp.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudRearLeftTemp.ValueChanged += new System.EventHandler(this.nudRearLeftTemp_ValueChanged);
            // 
            // nudRearRightTemp
            // 
            this.nudRearRightTemp.Location = new System.Drawing.Point(181, 21);
            this.nudRearRightTemp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudRearRightTemp.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudRearRightTemp.Name = "nudRearRightTemp";
            this.nudRearRightTemp.Size = new System.Drawing.Size(120, 22);
            this.nudRearRightTemp.TabIndex = 30;
            this.nudRearRightTemp.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudRearRightTemp.ValueChanged += new System.EventHandler(this.nudRearRightTemp_ValueChanged);
            // 
            // btnRearPanelLock
            // 
            this.btnRearPanelLock.Location = new System.Drawing.Point(276, 199);
            this.btnRearPanelLock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearPanelLock.Name = "btnRearPanelLock";
            this.btnRearPanelLock.Size = new System.Drawing.Size(99, 53);
            this.btnRearPanelLock.TabIndex = 29;
            this.btnRearPanelLock.Text = "Panel Lock OFF";
            this.btnRearPanelLock.UseVisualStyleBackColor = true;
            this.btnRearPanelLock.Click += new System.EventHandler(this.btnRearPanelLock_Click);
            // 
            // btnRearAuto
            // 
            this.btnRearAuto.Location = new System.Drawing.Point(144, 199);
            this.btnRearAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAuto.Name = "btnRearAuto";
            this.btnRearAuto.Size = new System.Drawing.Size(99, 53);
            this.btnRearAuto.TabIndex = 28;
            this.btnRearAuto.Text = "Auto OFF";
            this.btnRearAuto.UseVisualStyleBackColor = true;
            this.btnRearAuto.Click += new System.EventHandler(this.btnRearAuto_Click);
            // 
            // btnRearSync
            // 
            this.btnRearSync.Location = new System.Drawing.Point(409, 199);
            this.btnRearSync.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearSync.Name = "btnRearSync";
            this.btnRearSync.Size = new System.Drawing.Size(100, 53);
            this.btnRearSync.TabIndex = 27;
            this.btnRearSync.Text = "Sync OFF";
            this.btnRearSync.UseVisualStyleBackColor = true;
            this.btnRearSync.Click += new System.EventHandler(this.btnRearSync_Click);
            // 
            // btnRearPower
            // 
            this.btnRearPower.Location = new System.Drawing.Point(24, 199);
            this.btnRearPower.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearPower.Name = "btnRearPower";
            this.btnRearPower.Size = new System.Drawing.Size(95, 53);
            this.btnRearPower.TabIndex = 16;
            this.btnRearPower.Text = "Power Off";
            this.btnRearPower.UseVisualStyleBackColor = true;
            this.btnRearPower.Click += new System.EventHandler(this.btnRearPower_Click);
            // 
            // tabControlRearAirDistribution
            // 
            this.tabControlRearAirDistribution.Controls.Add(this.tabPageRearCombiMode);
            this.tabControlRearAirDistribution.Controls.Add(this.tabPageRearFourMode);
            this.tabControlRearAirDistribution.Controls.Add(this.tabPageRearCombiRoofMode);
            this.tabControlRearAirDistribution.Location = new System.Drawing.Point(10, 304);
            this.tabControlRearAirDistribution.Margin = new System.Windows.Forms.Padding(4);
            this.tabControlRearAirDistribution.Name = "tabControlRearAirDistribution";
            this.tabControlRearAirDistribution.SelectedIndex = 0;
            this.tabControlRearAirDistribution.Size = new System.Drawing.Size(692, 310);
            this.tabControlRearAirDistribution.TabIndex = 34;
            // 
            // tabPageRearCombiMode
            // 
            this.tabPageRearCombiMode.Controls.Add(this.panel8);
            this.tabPageRearCombiMode.Location = new System.Drawing.Point(4, 25);
            this.tabPageRearCombiMode.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageRearCombiMode.Name = "tabPageRearCombiMode";
            this.tabPageRearCombiMode.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageRearCombiMode.Size = new System.Drawing.Size(684, 281);
            this.tabPageRearCombiMode.TabIndex = 0;
            this.tabPageRearCombiMode.Text = "Combi Mode";
            this.tabPageRearCombiMode.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.btnRearAirDistCombiAuto);
            this.panel8.Controls.Add(this.btnRearAirDistCombiFloor);
            this.panel8.Controls.Add(this.btnRearAirDistCombiFace);
            this.panel8.Controls.Add(this.picBoxRearAutoCombi);
            this.panel8.Location = new System.Drawing.Point(4, -6);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(676, 293);
            this.panel8.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(204, 145);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 17);
            this.label7.TabIndex = 51;
            this.label7.Text = "Combi Mode: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(179, 5);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(293, 20);
            this.label8.TabIndex = 50;
            this.label8.Text = "Rear Air Distribution Combi Mode";
            // 
            // btnRearAirDistCombiAuto
            // 
            this.btnRearAirDistCombiAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistCombiAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRearAirDistCombiAuto.Location = new System.Drawing.Point(329, 174);
            this.btnRearAirDistCombiAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistCombiAuto.Name = "btnRearAirDistCombiAuto";
            this.btnRearAirDistCombiAuto.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistCombiAuto.TabIndex = 46;
            this.btnRearAirDistCombiAuto.Text = "AUTO OFF";
            this.btnRearAirDistCombiAuto.UseVisualStyleBackColor = true;
            this.btnRearAirDistCombiAuto.Click += new System.EventHandler(this.btnRearAirDistCombiAuto_Click);
            // 
            // btnRearAirDistCombiFloor
            // 
            this.btnRearAirDistCombiFloor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearAirDistCombiFloor.BackgroundImage")));
            this.btnRearAirDistCombiFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistCombiFloor.Location = new System.Drawing.Point(329, 55);
            this.btnRearAirDistCombiFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistCombiFloor.Name = "btnRearAirDistCombiFloor";
            this.btnRearAirDistCombiFloor.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistCombiFloor.TabIndex = 33;
            this.btnRearAirDistCombiFloor.UseVisualStyleBackColor = true;
            this.btnRearAirDistCombiFloor.Click += new System.EventHandler(this.btnRearAirDistCombiFloor_Click);
            // 
            // btnRearAirDistCombiFace
            // 
            this.btnRearAirDistCombiFace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearAirDistCombiFace.BackgroundImage")));
            this.btnRearAirDistCombiFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistCombiFace.Location = new System.Drawing.Point(244, 55);
            this.btnRearAirDistCombiFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistCombiFace.Name = "btnRearAirDistCombiFace";
            this.btnRearAirDistCombiFace.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistCombiFace.TabIndex = 32;
            this.btnRearAirDistCombiFace.UseVisualStyleBackColor = true;
            this.btnRearAirDistCombiFace.Click += new System.EventHandler(this.btnRearAirDistCombiFace_Click);
            // 
            // picBoxRearAutoCombi
            // 
            this.picBoxRearAutoCombi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxRearAutoCombi.Image = ((System.Drawing.Image)(resources.GetObject("picBoxRearAutoCombi.Image")));
            this.picBoxRearAutoCombi.Location = new System.Drawing.Point(240, 33);
            this.picBoxRearAutoCombi.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxRearAutoCombi.Name = "picBoxRearAutoCombi";
            this.picBoxRearAutoCombi.Size = new System.Drawing.Size(175, 42);
            this.picBoxRearAutoCombi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxRearAutoCombi.TabIndex = 48;
            this.picBoxRearAutoCombi.TabStop = false;
            // 
            // tabPageRearFourMode
            // 
            this.tabPageRearFourMode.Controls.Add(this.panel9);
            this.tabPageRearFourMode.Location = new System.Drawing.Point(4, 25);
            this.tabPageRearFourMode.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageRearFourMode.Name = "tabPageRearFourMode";
            this.tabPageRearFourMode.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageRearFourMode.Size = new System.Drawing.Size(684, 281);
            this.tabPageRearFourMode.TabIndex = 1;
            this.tabPageRearFourMode.Text = "4-Mode";
            this.tabPageRearFourMode.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel9.Controls.Add(this.btnRearAirDistFourModeBiDir);
            this.panel9.Controls.Add(this.lblFourMode);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.btnRearAirDistFourModeStatus);
            this.panel9.Controls.Add(this.btnRearAirDistFourModeAuto);
            this.panel9.Controls.Add(this.btnRearAirDistFourModeFloor);
            this.panel9.Controls.Add(this.btnRearAirDistFourModeFace);
            this.panel9.Controls.Add(this.picBoxRearAutoFourMode);
            this.panel9.Location = new System.Drawing.Point(4, -6);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(676, 293);
            this.panel9.TabIndex = 4;
            this.panel9.Visible = false;
            // 
            // btnRearAirDistFourModeBiDir
            // 
            this.btnRearAirDistFourModeBiDir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearAirDistFourModeBiDir.BackgroundImage")));
            this.btnRearAirDistFourModeBiDir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistFourModeBiDir.Location = new System.Drawing.Point(379, 57);
            this.btnRearAirDistFourModeBiDir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistFourModeBiDir.Name = "btnRearAirDistFourModeBiDir";
            this.btnRearAirDistFourModeBiDir.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistFourModeBiDir.TabIndex = 54;
            this.btnRearAirDistFourModeBiDir.UseVisualStyleBackColor = true;
            this.btnRearAirDistFourModeBiDir.Click += new System.EventHandler(this.btnRearAirDistFourModeBiDir_Click);
            // 
            // lblFourMode
            // 
            this.lblFourMode.AutoSize = true;
            this.lblFourMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFourMode.ForeColor = System.Drawing.Color.White;
            this.lblFourMode.Location = new System.Drawing.Point(204, 145);
            this.lblFourMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFourMode.Name = "lblFourMode";
            this.lblFourMode.Size = new System.Drawing.Size(95, 17);
            this.lblFourMode.TabIndex = 51;
            this.lblFourMode.Text = "Four Mode: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(179, 5);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(278, 20);
            this.label9.TabIndex = 50;
            this.label9.Text = "Rear Air Distribution Four Mode";
            // 
            // btnRearAirDistFourModeStatus
            // 
            this.btnRearAirDistFourModeStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearAirDistFourModeStatus.BackgroundImage")));
            this.btnRearAirDistFourModeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistFourModeStatus.Location = new System.Drawing.Point(244, 174);
            this.btnRearAirDistFourModeStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistFourModeStatus.Name = "btnRearAirDistFourModeStatus";
            this.btnRearAirDistFourModeStatus.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistFourModeStatus.TabIndex = 47;
            this.btnRearAirDistFourModeStatus.UseVisualStyleBackColor = true;
            this.btnRearAirDistFourModeStatus.Click += new System.EventHandler(this.btnRearAirDistFourModeStatus_Click);
            // 
            // btnRearAirDistFourModeAuto
            // 
            this.btnRearAirDistFourModeAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistFourModeAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRearAirDistFourModeAuto.Location = new System.Drawing.Point(329, 174);
            this.btnRearAirDistFourModeAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistFourModeAuto.Name = "btnRearAirDistFourModeAuto";
            this.btnRearAirDistFourModeAuto.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistFourModeAuto.TabIndex = 46;
            this.btnRearAirDistFourModeAuto.Text = "AUTO OFF";
            this.btnRearAirDistFourModeAuto.UseVisualStyleBackColor = true;
            this.btnRearAirDistFourModeAuto.Click += new System.EventHandler(this.btnRearAirDistFourModeAuto_Click);
            // 
            // btnRearAirDistFourModeFloor
            // 
            this.btnRearAirDistFourModeFloor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearAirDistFourModeFloor.BackgroundImage")));
            this.btnRearAirDistFourModeFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistFourModeFloor.Location = new System.Drawing.Point(293, 57);
            this.btnRearAirDistFourModeFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistFourModeFloor.Name = "btnRearAirDistFourModeFloor";
            this.btnRearAirDistFourModeFloor.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistFourModeFloor.TabIndex = 33;
            this.btnRearAirDistFourModeFloor.UseVisualStyleBackColor = true;
            this.btnRearAirDistFourModeFloor.Click += new System.EventHandler(this.btnRearAirDistFourModeFloor_Click);
            // 
            // btnRearAirDistFourModeFace
            // 
            this.btnRearAirDistFourModeFace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearAirDistFourModeFace.BackgroundImage")));
            this.btnRearAirDistFourModeFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearAirDistFourModeFace.Location = new System.Drawing.Point(208, 57);
            this.btnRearAirDistFourModeFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearAirDistFourModeFace.Name = "btnRearAirDistFourModeFace";
            this.btnRearAirDistFourModeFace.Size = new System.Drawing.Size(80, 74);
            this.btnRearAirDistFourModeFace.TabIndex = 32;
            this.btnRearAirDistFourModeFace.UseVisualStyleBackColor = true;
            this.btnRearAirDistFourModeFace.Click += new System.EventHandler(this.btnRearAirDistFourModeFace_Click);
            // 
            // picBoxRearAutoFourMode
            // 
            this.picBoxRearAutoFourMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxRearAutoFourMode.Image = ((System.Drawing.Image)(resources.GetObject("picBoxRearAutoFourMode.Image")));
            this.picBoxRearAutoFourMode.Location = new System.Drawing.Point(200, 33);
            this.picBoxRearAutoFourMode.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxRearAutoFourMode.Name = "picBoxRearAutoFourMode";
            this.picBoxRearAutoFourMode.Size = new System.Drawing.Size(269, 42);
            this.picBoxRearAutoFourMode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxRearAutoFourMode.TabIndex = 48;
            this.picBoxRearAutoFourMode.TabStop = false;
            // 
            // tabPageRearCombiRoofMode
            // 
            this.tabPageRearCombiRoofMode.Controls.Add(this.panel10);
            this.tabPageRearCombiRoofMode.Location = new System.Drawing.Point(4, 25);
            this.tabPageRearCombiRoofMode.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageRearCombiRoofMode.Name = "tabPageRearCombiRoofMode";
            this.tabPageRearCombiRoofMode.Size = new System.Drawing.Size(684, 281);
            this.tabPageRearCombiRoofMode.TabIndex = 2;
            this.tabPageRearCombiRoofMode.Text = "Combi-Roof Mode";
            this.tabPageRearCombiRoofMode.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel10.Controls.Add(this.btnAirCombiRoofDistModeRoof);
            this.panel10.Controls.Add(this.lblCombiRoofMode);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.btnRearCombiRoofAirDistModeStatus);
            this.panel10.Controls.Add(this.btnRearCombiRoofAirDistModeAuto);
            this.panel10.Controls.Add(this.btnRearCombiRoofAirDistModeFloor);
            this.panel10.Controls.Add(this.btnCombiRoofRearAirDistModeFace);
            this.panel10.Controls.Add(this.picBoxRearCombiRoofAutoMode);
            this.panel10.Location = new System.Drawing.Point(4, 1);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(676, 278);
            this.panel10.TabIndex = 5;
            this.panel10.Visible = false;
            // 
            // btnAirCombiRoofDistModeRoof
            // 
            this.btnAirCombiRoofDistModeRoof.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAirCombiRoofDistModeRoof.BackgroundImage")));
            this.btnAirCombiRoofDistModeRoof.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAirCombiRoofDistModeRoof.Location = new System.Drawing.Point(379, 57);
            this.btnAirCombiRoofDistModeRoof.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAirCombiRoofDistModeRoof.Name = "btnAirCombiRoofDistModeRoof";
            this.btnAirCombiRoofDistModeRoof.Size = new System.Drawing.Size(80, 74);
            this.btnAirCombiRoofDistModeRoof.TabIndex = 54;
            this.btnAirCombiRoofDistModeRoof.UseVisualStyleBackColor = true;
            this.btnAirCombiRoofDistModeRoof.Click += new System.EventHandler(this.btnAirCombiRoofDistModeRoof_Click);
            // 
            // lblCombiRoofMode
            // 
            this.lblCombiRoofMode.AutoSize = true;
            this.lblCombiRoofMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCombiRoofMode.ForeColor = System.Drawing.Color.White;
            this.lblCombiRoofMode.Location = new System.Drawing.Point(204, 145);
            this.lblCombiRoofMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCombiRoofMode.Name = "lblCombiRoofMode";
            this.lblCombiRoofMode.Size = new System.Drawing.Size(146, 17);
            this.lblCombiRoofMode.TabIndex = 51;
            this.lblCombiRoofMode.Text = "Combi-Roof Mode: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(179, 5);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(339, 20);
            this.label10.TabIndex = 50;
            this.label10.Text = "Rear Combi-Roof Air Distribution Mode";
            // 
            // btnRearCombiRoofAirDistModeStatus
            // 
            this.btnRearCombiRoofAirDistModeStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearCombiRoofAirDistModeStatus.BackgroundImage")));
            this.btnRearCombiRoofAirDistModeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearCombiRoofAirDistModeStatus.Location = new System.Drawing.Point(244, 174);
            this.btnRearCombiRoofAirDistModeStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearCombiRoofAirDistModeStatus.Name = "btnRearCombiRoofAirDistModeStatus";
            this.btnRearCombiRoofAirDistModeStatus.Size = new System.Drawing.Size(80, 74);
            this.btnRearCombiRoofAirDistModeStatus.TabIndex = 47;
            this.btnRearCombiRoofAirDistModeStatus.UseVisualStyleBackColor = true;
            this.btnRearCombiRoofAirDistModeStatus.Click += new System.EventHandler(this.btnRearCombiRoofAirDistModeStatus_Click);
            // 
            // btnRearCombiRoofAirDistModeAuto
            // 
            this.btnRearCombiRoofAirDistModeAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearCombiRoofAirDistModeAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRearCombiRoofAirDistModeAuto.Location = new System.Drawing.Point(329, 174);
            this.btnRearCombiRoofAirDistModeAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearCombiRoofAirDistModeAuto.Name = "btnRearCombiRoofAirDistModeAuto";
            this.btnRearCombiRoofAirDistModeAuto.Size = new System.Drawing.Size(80, 74);
            this.btnRearCombiRoofAirDistModeAuto.TabIndex = 46;
            this.btnRearCombiRoofAirDistModeAuto.Text = "AUTO OFF";
            this.btnRearCombiRoofAirDistModeAuto.UseVisualStyleBackColor = true;
            this.btnRearCombiRoofAirDistModeAuto.Click += new System.EventHandler(this.btnRearCombiRoofAirDistModeAuto_Click);
            // 
            // btnRearCombiRoofAirDistModeFloor
            // 
            this.btnRearCombiRoofAirDistModeFloor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRearCombiRoofAirDistModeFloor.BackgroundImage")));
            this.btnRearCombiRoofAirDistModeFloor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRearCombiRoofAirDistModeFloor.Location = new System.Drawing.Point(293, 57);
            this.btnRearCombiRoofAirDistModeFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRearCombiRoofAirDistModeFloor.Name = "btnRearCombiRoofAirDistModeFloor";
            this.btnRearCombiRoofAirDistModeFloor.Size = new System.Drawing.Size(80, 74);
            this.btnRearCombiRoofAirDistModeFloor.TabIndex = 33;
            this.btnRearCombiRoofAirDistModeFloor.UseVisualStyleBackColor = true;
            this.btnRearCombiRoofAirDistModeFloor.Click += new System.EventHandler(this.btnRearCombiRoofAirDistModeFloor_Click);
            // 
            // btnCombiRoofRearAirDistModeFace
            // 
            this.btnCombiRoofRearAirDistModeFace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCombiRoofRearAirDistModeFace.BackgroundImage")));
            this.btnCombiRoofRearAirDistModeFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCombiRoofRearAirDistModeFace.Location = new System.Drawing.Point(208, 57);
            this.btnCombiRoofRearAirDistModeFace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCombiRoofRearAirDistModeFace.Name = "btnCombiRoofRearAirDistModeFace";
            this.btnCombiRoofRearAirDistModeFace.Size = new System.Drawing.Size(80, 74);
            this.btnCombiRoofRearAirDistModeFace.TabIndex = 32;
            this.btnCombiRoofRearAirDistModeFace.UseVisualStyleBackColor = true;
            this.btnCombiRoofRearAirDistModeFace.Click += new System.EventHandler(this.btnCombiRoofRearAirDistModeFace_Click);
            // 
            // picBoxRearCombiRoofAutoMode
            // 
            this.picBoxRearCombiRoofAutoMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picBoxRearCombiRoofAutoMode.Image = ((System.Drawing.Image)(resources.GetObject("picBoxRearCombiRoofAutoMode.Image")));
            this.picBoxRearCombiRoofAutoMode.Location = new System.Drawing.Point(200, 33);
            this.picBoxRearCombiRoofAutoMode.Margin = new System.Windows.Forms.Padding(4);
            this.picBoxRearCombiRoofAutoMode.Name = "picBoxRearCombiRoofAutoMode";
            this.picBoxRearCombiRoofAutoMode.Size = new System.Drawing.Size(269, 42);
            this.picBoxRearCombiRoofAutoMode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxRearCombiRoofAutoMode.TabIndex = 48;
            this.picBoxRearCombiRoofAutoMode.TabStop = false;
            // 
            // HVAC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 722);
            this.Controls.Add(this.tabHVACControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HVAC";
            this.Text = "Global B HVAC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HVAC_FormClosing);
            this.Load += new System.EventHandler(this.HVAC_Load);
            this.tabHVACControl1.ResumeLayout(false);
            this.tabPageFrontHvac.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.tabControlAirDist.ResumeLayout(false);
            this.tabPageCombiMode.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoCombi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMaxDefrost)).EndInit();
            this.tabPageDiscreteMode.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPageFourMode.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAutoFourMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFrontFanSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFrontLeftTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFrontRightTemp)).EndInit();
            this.tabPageRearHvac.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRearLeftTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRearRightTemp)).EndInit();
            this.tabControlRearAirDistribution.ResumeLayout(false);
            this.tabPageRearCombiMode.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRearAutoCombi)).EndInit();
            this.tabPageRearFourMode.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRearAutoFourMode)).EndInit();
            this.tabPageRearCombiRoofMode.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRearCombiRoofAutoMode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabHVACControl1;
        private System.Windows.Forms.TabPage tabPageFrontHvac;
        private System.Windows.Forms.TabPage tabPageRearHvac;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button btnSync;
        public System.Windows.Forms.Button btnAuto;
        public System.Windows.Forms.Button btnRecir;
        public System.Windows.Forms.Button btnAC;
        public System.Windows.Forms.Button btnPower;
        public System.Windows.Forms.Button btnIonizerStatusOn;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnRearPanelLock;
        public System.Windows.Forms.Button btnRearAuto;
        public System.Windows.Forms.Button btnRearSync;
        public System.Windows.Forms.Button btnRearPower;
        private System.Windows.Forms.TabControl tabControlRearAirDistribution;
        private System.Windows.Forms.TabPage tabPageRearCombiMode;
        private System.Windows.Forms.TabPage tabPageRearFourMode;
        private System.Windows.Forms.TabPage tabPageRearCombiRoofMode;
        private System.Windows.Forms.Panel panel8;
        public System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Button btnRearAirDistCombiAuto;
        public System.Windows.Forms.Button btnRearAirDistCombiFloor;
        public System.Windows.Forms.Button btnRearAirDistCombiFace;
        public System.Windows.Forms.PictureBox picBoxRearAutoCombi;
        private System.Windows.Forms.Panel panel9;
        public System.Windows.Forms.Button btnRearAirDistFourModeBiDir;
        public System.Windows.Forms.Label lblFourMode;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Button btnRearAirDistFourModeStatus;
        public System.Windows.Forms.Button btnRearAirDistFourModeAuto;
        public System.Windows.Forms.Button btnRearAirDistFourModeFloor;
        public System.Windows.Forms.Button btnRearAirDistFourModeFace;
        public System.Windows.Forms.PictureBox picBoxRearAutoFourMode;
        private System.Windows.Forms.Panel panel10;
        public System.Windows.Forms.Button btnAirCombiRoofDistModeRoof;
        public System.Windows.Forms.Label lblCombiRoofMode;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Button btnRearCombiRoofAirDistModeStatus;
        public System.Windows.Forms.Button btnRearCombiRoofAirDistModeAuto;
        public System.Windows.Forms.Button btnRearCombiRoofAirDistModeFloor;
        public System.Windows.Forms.Button btnCombiRoofRearAirDistModeFace;
        public System.Windows.Forms.PictureBox picBoxRearCombiRoofAutoMode;
        private System.Windows.Forms.NumericUpDown nudFanSpeed;
        private System.Windows.Forms.NumericUpDown nudRearLeftTemp;
        private System.Windows.Forms.NumericUpDown nudRearRightTemp;
        private System.Windows.Forms.Label lblRearFanSpeed;
        private System.Windows.Forms.Label lblRearLeftTemp;
        private System.Windows.Forms.Label lblRearRightTemp;
        private System.Windows.Forms.TabControl tabControlAirDist;
        private System.Windows.Forms.TabPage tabPageDiscreteMode;
        private System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnAirDistLeftFloorWindShield;
        public System.Windows.Forms.Button btnAirDistLeftBiLevel;
        public System.Windows.Forms.Button btnAirDistRightBiLevel;
        public System.Windows.Forms.Button btnAirDistLeftStatus;
        public System.Windows.Forms.Button btnAirDistRightStatus;
        public System.Windows.Forms.Button btnHeat;
        public System.Windows.Forms.Label lblRightMode;
        public System.Windows.Forms.Label lblLeftMode;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button btnAirDistRightAuto;
        public System.Windows.Forms.Button btnAirDistLeftAuto;
        public System.Windows.Forms.Button btnAirDistMaxDefrost;
        public System.Windows.Forms.Button btnAirDistRightFloor;
        public System.Windows.Forms.Button btnAirDistRightFace;
        public System.Windows.Forms.Button btnAirDistRightWindshield;
        public System.Windows.Forms.Button btnAirDistLeftWindshield;
        public System.Windows.Forms.Button btnAirDistLeftFace;
        public System.Windows.Forms.Button btnAirDistLeftFloor;
        public System.Windows.Forms.PictureBox picBoxAutoLeft;
        public System.Windows.Forms.PictureBox picBoxAutoRight;
        public System.Windows.Forms.PictureBox picBoxAuto;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPageCombiMode;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblCombiMode;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnAirDistCombiAuto;
        public System.Windows.Forms.Button btnAirDistCombiMaxDefrost;
        public System.Windows.Forms.Button btnAirDistCombitWindshield;
        public System.Windows.Forms.Button btnAirDistCombiFloor;
        public System.Windows.Forms.Button btnAirDistCombiFace;
        public System.Windows.Forms.PictureBox picBoxAutoCombi;
        public System.Windows.Forms.PictureBox picBoxMaxDefrost;
        private System.Windows.Forms.TabPage tabPageFourMode;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Button btnAirDistFourModeBiLevel;
        public System.Windows.Forms.Button btnAirDistFourModeFloorWindShield;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button btnAirDistFourModeStatus;
        public System.Windows.Forms.Button btnAirDistFourModeAuto;
        public System.Windows.Forms.Button btnAirDistFourModeMaxDefrost;
        public System.Windows.Forms.Button btnAirDistFourModeFloor;
        public System.Windows.Forms.Button btnAirDistFourModeFace;
        public System.Windows.Forms.PictureBox picBoxAutoFourMode;
        public System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.NumericUpDown nudFrontRightTemp;
        private System.Windows.Forms.NumericUpDown nudFrontFanSpeed;
        private System.Windows.Forms.NumericUpDown nudFrontLeftTemp;
        private System.Windows.Forms.Label lblFrontFanSpeed;
        private System.Windows.Forms.Label lblFrontLeftTemp;
        private System.Windows.Forms.Label lblFrontRightTemp;
        private System.Windows.Forms.ComboBox cmbIndTmpStngStFR;
        private System.Windows.Forms.ComboBox cmbIndBlwStngStFrt;
        private System.Windows.Forms.ComboBox cmbIndTmpStngStFL;
        private System.Windows.Forms.ComboBox cmbIndBlwStngStRr;
        private System.Windows.Forms.ComboBox cmbIndTmpStngStRL;
        private System.Windows.Forms.ComboBox cmbIndTmpStngStRR;
        private System.Windows.Forms.Label lblFanSpeedSettings;
        private System.Windows.Forms.Label lblRearLeftTempSettings;
        private System.Windows.Forms.Label lblRightTempSettings;
        private System.Windows.Forms.Label lblFrontFanSpeedSettings;
        private System.Windows.Forms.Label lblFrontLeftTempSettings;
        private System.Windows.Forms.Label lblFrontRightTempSettings;
        private System.Windows.Forms.Panel panel13;
        public System.Windows.Forms.RadioButton ImperialRadBtn;
        public System.Windows.Forms.RadioButton USRadBtn;
        public System.Windows.Forms.RadioButton MetricRadBtn;
    }
}

