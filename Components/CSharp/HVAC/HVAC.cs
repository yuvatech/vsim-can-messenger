﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Extension;

namespace HVAC
{
    public partial class HVAC : Form
    {
        volatile bool HVACPowerStatus = false; // off
        volatile bool HVACACStatus = false; // off
        
        private Adapter _adapter = null;
        private dynamic _db = Adapter.Database();

        public HVAC()
        {
            InitializeComponent();
            _adapter = new Adapter();
            _adapter.Open(false);
            _adapter.OnReceived += _adapter_OnReceived;

            //// START Event Registeration for HVAC Dual Air Dist.

            //this.setClimatePowerStatus(false);

            //// END Event Registeration for HVAC Dual Air Dist.

            //this.nudFanSpeed.Enabled = false;
            //this.nudFrontFanSpeed.Enabled = false;
            this.nudFrontLeftTemp.Enabled = false;
            this.nudFrontRightTemp.Enabled = false;
            this.nudFrontFanSpeed.Enabled = false;
            this.nudRearLeftTemp.Enabled = false;
            this.nudRearRightTemp.Enabled = false;
            this.nudFanSpeed.Enabled = false;
            this.btnRearAuto.Enabled = false;
            this.btnRearPanelLock.Enabled = false;
            this.btnRearSync.Enabled = false;
            this.cmbIndBlwStngStFrt.Enabled = false;
            this.cmbIndBlwStngStRr.Enabled = false;
            this.cmbIndTmpStngStFL.Enabled = false;
            this.cmbIndTmpStngStFR.Enabled = false;
            this.cmbIndTmpStngStRL.Enabled = false;
            this.cmbIndTmpStngStRR.Enabled = false;

            this.btnAC.Enabled = false;
            this.btnAuto.Enabled = false;
            this.btnRecir.Enabled = false;
            this.btnSync.Enabled = false;
            this.btnIonizerStatusOn.Enabled = false;
        }

        private void _adapter_OnReceived(object sender, Adapter.Packet packet)
        {
            if (packet.Name == "SignalName")
                ; // testbox/lable/whatever = packet.Value;
        }
                     

        #region Front HVAC

        private void btnPower_Click(object sender, EventArgs e)
        {
            HVACPowerStatus = !HVACPowerStatus;
            this.btnPower.Text = HVACPowerStatus ? "Power On" : "Power Off";
            this.nudFanSpeed.Enabled = HVACPowerStatus;
            this.nudFrontFanSpeed.Enabled = HVACPowerStatus;
            this.nudFrontLeftTemp.Enabled = HVACPowerStatus;
            this.nudFrontRightTemp.Enabled = HVACPowerStatus;
            this.btnAC.Enabled = HVACPowerStatus;
            this.btnAuto.Enabled = HVACPowerStatus;
            this.btnRecir.Enabled = HVACPowerStatus;
            this.btnSync.Enabled = HVACPowerStatus;
            this.cmbIndTmpStngStFL.Enabled = HVACPowerStatus;
            this.cmbIndTmpStngStFR.Enabled = HVACPowerStatus;
            this.cmbIndBlwStngStFrt.Enabled = HVACPowerStatus;
            this.btnIonizerStatusOn.Enabled = HVACPowerStatus;

            SignalSend("IndFrtHVAC_On", HVACPowerStatus ? "1" : "0", "false");
        }

        private void btnAC_Click(object sender, EventArgs e)
        {
            HVACACStatus = !HVACACStatus;
            SignalSend("IndCompStng", HVACACStatus ? "2" : "1", "false");
            btnAC.Text = HVACACStatus ? "A/C On" : "A/C Off";
        }

        volatile bool Auto = false;
        private void btnAuto_Click(object sender, EventArgs e)
        {
            Auto = !Auto;
            //IndBlwStngStFrt = auto & IndAirDstFL = auto
            btnAuto.Text = Auto ? "Auto ON" : "Auto OFF";
            SignalSend("IndBlwStngStFrt", Auto ? "1" : "0", "false"); //Enum
            SignalSend("IndAirDstFL", Auto ? "1" : "0", "false"); //Enum

        }

        volatile bool FrontRecir = false;
        private void btnRecir_Click(object sender, EventArgs e) //Enum
        {
            FrontRecir = !FrontRecir;
            btnRecir.Text = FrontRecir ? "ReCir ON" : "ReCir OFF";
            SignalSend("IndRecircStng", FrontRecir ? "2" : "0", "false");
        }

        volatile bool Sync = false;
        private void btnSync_Click(object sender, EventArgs e)
        {
            Sync = !Sync;
            btnSync.Text = Sync ? "Sync On" : "Sync Off"; ;
            nudFrontRightTemp.Value = nudFrontLeftTemp.Value;
            SignalSend("IndTmpStngLvlFR", nudFrontRightTemp.Value.ToString(), "false");
            SignalSend("IndSYNC_All", Sync ? "1" : "0", "false");

            if (RearPowerStatus)
            {
                nudRearLeftTemp.Value = nudFrontLeftTemp.Value;
                nudRearRightTemp.Value = nudFrontLeftTemp.Value;
                SignalSend("IndTmpStngLvlRL", nudFrontRightTemp.Value.ToString(), "false");
                SignalSend("IndTmpStngLvlRR", nudFrontRightTemp.Value.ToString(), "false");
                SignalSend("IndSYNC_All", "1", "false");
            }

        }
        private bool FrontIonizer = false;
        private void btnIonizerStatusOn_Click(object sender, EventArgs e)
        {
            FrontIonizer = !FrontIonizer;
            this.btnIonizerStatusOn.Text = FrontIonizer ? "Ionizer On" : "Ionizer Off";
            SignalSend("IndInzrStat", FrontIonizer ? "1" : "0", "false");
        }
        private void nudFrontRightTemp_ValueChanged(object sender, EventArgs e)
        {
            SignalSend("IndTmpStngLvlFR", nudFrontRightTemp.Value.ToString(), "false");
        }

        private void nudFrontFanSpeed_ValueChanged(object sender, EventArgs e)
        {
            SignalSend("IndBlwStngLvlFrt", nudFrontFanSpeed.Value.ToString(), "false");
        }

        private void nudFrontLeftTemp_ValueChanged(object sender, EventArgs e)
        {
            SignalSend("IndTmpStngLvlFL", nudFrontLeftTemp.Value.ToString(), "false");
        }

        private void cmbIndTmpStngStFR_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cmbIndTmpStngStFR.SelectedItem.ToString().Split('=');
            if (val[0].Trim() == "2")
            {
                nudFrontRightTemp.Enabled = false;
                SignalSend("IndTmpStngLvlFR", "60", "false");
                nudFrontRightTemp.Value = 60;
                SignalSend("IndTmpStngStFR", val[0].Trim(), "false");
            }
            else if (val[0].Trim() == "3")
            {
                nudFrontRightTemp.Enabled = false;
                SignalSend("IndTmpStngLvlFR", "85", "false");
                nudFrontRightTemp.Value = 85;
                SignalSend("IndTmpStngStFR", val[0].Trim(), "false");
            }
            else if (val[0].Trim() == "4")
            {
                nudFrontRightTemp.Enabled = true;
                SignalSend("IndTmpStngStFR", val[0].Trim(), "false");
            }
            else if (val[0].Trim() == "0")
            {
                nudFrontRightTemp.Enabled = false;
                SignalSend("IndTmpStngStFR", val[0].Trim(), "false");
            }
            else
            {
                nudFrontRightTemp.Enabled = false;
                SignalSend("IndTmpStngStFR", val[0].Trim(), "false");
            }
        }

        private void cmbIndTmpStngStFL_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cmbIndTmpStngStFL.SelectedItem.ToString().Split('=');
            if (val[0].Trim() == "2")
            {
                nudFrontLeftTemp.Enabled = false;
                SignalSend("IndTmpStngLvlFL", "16", "false");
                nudFrontLeftTemp.Value = 16;
                SignalSend("IndTmpStngStFL", val[0].Trim(), "false");
            }
            else if (val[0].Trim() == "3")
            {
                nudFrontLeftTemp.Enabled = false;
                SignalSend("IndTmpStngLvlFL", "31", "false");
                nudFrontLeftTemp.Value = 31;
                SignalSend("IndTmpStngStFL", val[0].Trim(), "false");
            }
            else if (val[0].Trim() == "4")
            {
                nudFrontLeftTemp.Enabled = true;
                SignalSend("IndTmpStngStFL", val[0].Trim(), "false");
            }
            else if (val[0].Trim() == "0")
            {
                nudFrontLeftTemp.Enabled = false;
                SignalSend("IndTmpStngStFL", val[0].Trim(), "false");
            }
            else
            {
                nudFrontLeftTemp.Enabled = false;
                SignalSend("IndTmpStngStFL", val[0].Trim(), "false");
            }
        }

        private void cmbIndBlwStngStFrt_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cmbIndBlwStngStFrt.SelectedItem.ToString().Split('=');
            if (val[0].Trim() == "1")
            {
                nudFrontFanSpeed.Enabled = false;
                SignalSend("IndBlwStngLvlFrt", "8", "false");
                nudFrontFanSpeed.Value = 8;
                SignalSend("IndBlwStngStFrt", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "2")
            {
                nudFrontFanSpeed.Enabled = true;
                SignalSend("IndBlwStngStFrt", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "0")
            {
                nudFrontFanSpeed.Enabled = false;
                SignalSend("IndBlwStngStFrt", val[0].Trim(), "false");
            }
        }


        #region Front Air Distribution Combi Mode
        volatile bool FrontWindshield = false;
        volatile bool FrontFace = false;
        volatile bool FrontFloor = false;
        volatile bool FrontAuto = false;
        volatile bool FrontDefrost = false;

        private void btnAirDistCombitWindshield_Click(object sender, EventArgs e)
        {
            FrontWindshield = !FrontWindshield;
            if (FrontWindshield)
            {
                btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_active_press;
                btnAirDistCombiAuto.Text = "AUTO OFF";
                picBoxAutoCombi.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "6", "false");
                SignalSend("IndAirDstFL", "6", "false");
            }
            else
            {
                btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                SignalSend("IndAirDstFR", "1", "false");
                SignalSend("IndAirDstFL", "1", "false");
            }
        }

        private void btnAirDistCombiFace_Click(object sender, EventArgs e)
        {
            FrontFace = !FrontFace;
            if (FrontFace)
            {
                btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press;
                btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistCombiAuto.Text = "AUTO OFF";
                picBoxAutoCombi.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "4", "false");
                SignalSend("IndAirDstFL", "4", "false");
            }
            else
            {
                btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                SignalSend("IndAirDstFR", "1", "false");
                SignalSend("IndAirDstFL", "1", "false");
            }
        }

        private void btnAirDistCombiFloor_Click(object sender, EventArgs e)
        {
            FrontFloor = !FrontFloor;
            if (FrontFloor)
            {
                btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_large;
                btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistCombiAuto.Text = "AUTO OFF";
                picBoxAutoCombi.Visible = false;
                picBoxMaxDefrost.Visible = false;
                btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "2", "false");
                SignalSend("IndAirDstFL", "2", "false");
            }
            else
            {
                btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                SignalSend("IndAirDstFR", "1", "false");
                SignalSend("IndAirDstFL", "1", "false");
            }
        }

        private void btnAirDistCombiAuto_Click(object sender, EventArgs e)
        {
            FrontAuto = !FrontAuto;
            if (FrontAuto)
            {
                btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistCombiAuto.Text = "AUTO ON";
                picBoxAutoCombi.Visible = true;
                picBoxMaxDefrost.Visible = false;

                btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "1", "false");
                SignalSend("IndAirDstFL", "1", "false");
            }
            else
            {
                btnAirDistCombiAuto.Text = "AUTO OFF";
                picBoxAutoCombi.Visible = false;
                picBoxMaxDefrost.Visible = false;
                SignalSend("IndAirDstFR", "0", "false");
                SignalSend("IndAirDstFL", "0", "false");
            }
        }

        private void btnAirDistCombiMaxDefrost_Click(object sender, EventArgs e)
        {
            FrontDefrost = !FrontDefrost;
            if (FrontDefrost)
            {
                btnAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistCombitWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistCombiAuto.Text = "AUTO OFF";
                picBoxAutoCombi.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_active_press;
                SignalSend("IndAirDstFR", "9", "false");
                SignalSend("IndAirDstFL", "9", "false");
            }
            else
            {
                btnAirDistCombiMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "1", "false");
                SignalSend("IndAirDstFL", "1", "false");
            }

        }

        #endregion

        #region Front Dual Air Distribution Mode

        volatile bool DualLeftFloor = false;
        volatile bool DualLeftFace = false;
        volatile bool DualLeftBiLevel = false;
        volatile bool DualLeftWindShield = false;
        volatile bool DualRightBiLevel = false;
        volatile bool DualRightFace = false;
        volatile bool DualRightFloor = false;
        volatile bool DualLeftStatus = false;
        volatile bool DualLeftAuto = false;
        volatile bool DualDefrost = false;
        volatile bool DualHeat = false;
        volatile bool DualRightAuto = false;
        volatile bool DualRightStatus = false;
        private void btnAirDistLeftFloor_Click(object sender, EventArgs e)
        {
            DualLeftFloor = !DualLeftFloor;
            if (DualLeftFloor)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "2", "true");
            }
            else
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                SignalSend("IndAirDstFL", "0", "false");
            }
        }

        private void btnAirDistLeftFace_Click(object sender, EventArgs e)
        {
            DualLeftFace = !DualLeftFace;
            if (DualLeftFace)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "4", "true");
            }
            else
            {
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                SignalSend("IndAirDstFL", "0", "false");
            }
        }

        private void btnAirDistLeftBiLevel_Click(object sender, EventArgs e)
        {
            DualLeftBiLevel = !DualLeftBiLevel;
            if (DualLeftBiLevel)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_active_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "3", "true");
            }
            else
            {
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                SignalSend("IndAirDstFL", "0", "false");

            }
        }

        private void btnAirDistLeftFloorWindShield_Click(object sender, EventArgs e)
        {
            DualLeftWindShield = !DualLeftWindShield;
            if (DualLeftWindShield)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_active_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_active_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "7", "true");
            }
            else
            {
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                SignalSend("IndAirDstFL", "0", "false");
            }
        }

        private void btnAirDistRightBiLevel_Click(object sender, EventArgs e)
        {
            DualRightBiLevel = !DualRightBiLevel;
            if (DualRightBiLevel)
            {
                btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_large;
                btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_active_press_right;
                btnAirDistRightWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistRightAuto.Text = "AUTO OFF";
                picBoxAutoRight.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "3", "true");
            }
            else
            {
                btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                SignalSend("IndAirDstFR", "0", "false");
            }

        }

        private void btnAirDistRightFace_Click(object sender, EventArgs e)
        {
            DualRightFace = !DualRightFace;
            if (DualRightFace)
            {
                btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press_large;
                btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistRightWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistRightAuto.Text = "AUTO OFF";
                picBoxAutoRight.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "4", "true");
                SignalSend("IndAirDstFL", "4", "true");
            }
            else
            {
                btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_large;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistRightFloor_Click(object sender, EventArgs e)
        {
            DualRightFloor = !DualRightFloor;
            if (DualRightFloor)
            {
                btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_large;
                btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistRightWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistRightAuto.Text = "AUTO OFF";
                picBoxAutoRight.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "2", "true");
            }
            else
            {
                btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistLeftStatus_Click(object sender, EventArgs e)
        {
            DualLeftStatus = !DualLeftStatus;
            if (DualLeftStatus)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_active_press;
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "0", "true");
            }
            else
            {
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                SignalSend("IndAirDstFL", "0", "false");
            }
        }

        private void btnAirDistLeftAuto_Click(object sender, EventArgs e)
        {
            DualLeftAuto = !DualLeftAuto;
            if (DualLeftAuto)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistLeftAuto.Text = "AUTO ON";
                picBoxAutoLeft.Visible = true;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "1", "true");
            }
            else
            {
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = false;
                SignalSend("IndAirDstFL", "0", "false");
            }
        }

        private void btnAirDistMaxDefrost_Click(object sender, EventArgs e)
        {
            DualDefrost = !DualDefrost;
            if (DualDefrost)
            {
                btnAirDistLeftFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_down;
                btnAirDistLeftFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press_left;
                btnAirDistLeftBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_left;
                btnAirDistLeftWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press_left;
                btnAirDistLeftFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down;
                btnAirDistLeftStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistLeftAuto.Text = "AUTO OFF";
                picBoxAutoLeft.Visible = false;
                picBoxMaxDefrost.Visible = true;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_active_press;
                SignalSend("IndAirDstFL", "9", "true");
                SignalSend("IndAirDstFR", "9", "true");
            }
            else
            {
                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFL", "0", "false");
                SignalSend("IndAirDstFR", "0", "false");

            }
        }

        private void btnHeat_Click(object sender, EventArgs e)
        {
            DualHeat = !DualHeat;
            if (DualHeat)
            {
                btnHeat.BackgroundImage = global::HVAC.Properties.Resources.climate_heat_active_press;
            }
            else
            {
                btnHeat.BackgroundImage = global::HVAC.Properties.Resources.climate_heat_inactive_press;
            }
        }

        private void btnAirDistRightAuto_Click(object sender, EventArgs e)
        {
            DualRightAuto = !DualRightAuto;
            if (DualRightAuto)
            {
                btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistRightWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistRightAuto.Text = "AUTO ON";
                picBoxAutoRight.Visible = true;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "1", "true");
            }
            else
            {
                btnAirDistRightAuto.Text = "AUTO OFF";
                picBoxAutoRight.Visible = false;
                picBoxMaxDefrost.Visible = false;
                SignalSend("IndAirDstFR", "0", "false");

            }
        }

        private void btnAirDistRightStatus_Click(object sender, EventArgs e)
        {
            DualRightStatus = !DualRightStatus;
            if (DualRightStatus)
            {
                btnAirDistRightFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistRightFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistRightBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistRightWindshield.BackgroundImage = global::HVAC.Properties.Resources.climate_defrost_windshield_inactive_press;
                btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_active_press;
                btnAirDistRightAuto.Text = "AUTO OFF";
                picBoxAutoRight.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "0", "true");
            }
            else
            {
                btnAirDistRightStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }


        #endregion

        #region Front Air Distribution Four Mode

        volatile bool FrontFourFace = false;
        volatile bool FrontFourFloor = false;
        volatile bool FrontFourBiLevel = false;
        volatile bool FrontFourFloorWindShield = false;
        volatile bool FrontFourStatus = false;
        volatile bool FrontFourAuto = false;
        volatile bool FrontFourDefrost = false;
        private void btnAirDistFourModeFace_Click(object sender, EventArgs e)
        {
            FrontFourFace = !FrontFourFace;
            if (FrontFourFace)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "4", "true");
            }
            else
            {
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistFourModeFloor_Click(object sender, EventArgs e)
        {
            FrontFourFloor = !FrontFourFloor;
            if (FrontFourFloor)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "2", "true");
            }
            else
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistFourModeBiLevel_Click(object sender, EventArgs e)
        {
            FrontFourBiLevel = !FrontFourBiLevel;
            if (FrontFourBiLevel)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_active_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = false;
                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;

                SignalSend("IndAirDstFR", "3", "true");
            }
            else
            {
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistFourModeFloorWindShield_Click(object sender, EventArgs e)
        {
            FrontFourFloorWindShield = !FrontFourFloorWindShield;
            if (FrontFourFloorWindShield)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_active_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "7", "true");
            }
            else
            {
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistFourModeStatus_Click(object sender, EventArgs e)
        {
            FrontFourStatus = !FrontFourStatus;
            if (FrontFourStatus)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_active_press;
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = false;

                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "0", "true");
            }
            else
            {
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistFourModeAuto_Click(object sender, EventArgs e)
        {
            FrontFourAuto = !FrontFourAuto;
            if (FrontFourAuto)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistFourModeAuto.Text = "AUTO ON";
                picBoxAutoFourMode.Visible = true;
                picBoxMaxDefrost.Visible = false;

                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "1", "true");
            }
            else
            {
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = false;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        private void btnAirDistFourModeMaxDefrost_Click(object sender, EventArgs e)
        {
            FrontFourDefrost = !FrontFourDefrost;
            if (FrontFourDefrost)
            {
                btnAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirDistFourModeBiLevel.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnAirDistFourModeFloorWindShield.BackgroundImage = global::HVAC.Properties.Resources.climate_airfootwindshield_inactive_press_down_right;
                btnAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxAutoFourMode.Visible = false;
                picBoxMaxDefrost.Visible = true;

                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_active_press;
                SignalSend("IndAirDstFR", "9", "true");
            }
            else
            {
                btnAirDistFourModeMaxDefrost.BackgroundImage = global::HVAC.Properties.Resources.climate_air_defrost_inactive_press;
                SignalSend("IndAirDstFR", "0", "false");
            }
        }

        #endregion


        #endregion



        #region Rear HVAC

        private bool RearPowerStatus = false;
        private bool RearAuto = false;
        private bool RearPanelLock = false;
        private bool RearSync = false;
        private void btnRearPower_Click(object sender, EventArgs e)
        {
            RearPowerStatus = !RearPowerStatus;
            this.btnRearPower.Text = RearPowerStatus ? "Power On" : "Power Off";
            this.nudRearLeftTemp.Enabled = RearPowerStatus;
            this.nudRearRightTemp.Enabled = RearPowerStatus;
            this.nudFanSpeed.Enabled = RearPowerStatus;
            this.btnRearAuto.Enabled = RearPowerStatus;
            this.btnRearPanelLock.Enabled = RearPowerStatus;
            this.btnRearSync.Enabled = RearPowerStatus;
            this.cmbIndBlwStngStFrt.Enabled = RearPowerStatus;
            this.cmbIndBlwStngStRr.Enabled = RearPowerStatus;
            this.cmbIndTmpStngStFL.Enabled = RearPowerStatus;
            this.cmbIndTmpStngStFR.Enabled = RearPowerStatus;
            this.cmbIndTmpStngStRL.Enabled = RearPowerStatus;
            this.cmbIndTmpStngStRR.Enabled = RearPowerStatus;
            SignalSend("IndRrHVAC_On", RearPowerStatus ? "1" : "0", "false");
        }

        private void btnRearAuto_Click(object sender, EventArgs e)
        {
            RearAuto = !RearAuto;
            this.btnRearAuto.Text = RearAuto ? "Auto On" : "Auto Off";
            SignalSend("IndBlwStngStRr", RearAuto ? "1" : "0", "false");
            SignalSend("IndAirDstRL", RearAuto ? "1" : "0", "false");
        }

        private void btnRearPanelLock_Click(object sender, EventArgs e)
        {
            RearPanelLock = !RearPanelLock;
            this.btnRearPanelLock.Text = RearPanelLock ? "Panel Lock On" : "Panel Lock Off";
            SignalSend("IndRrHVAC_Lckd", RearPanelLock ? "1" : "0", "false");
        }

        private void btnRearSync_Click(object sender, EventArgs e)
        {
            RearSync = !RearSync;
            this.btnRearSync.Text = RearSync ? "Sync On" : "Sync Off";
            nudRearRightTemp.Value = nudRearLeftTemp.Value;
            SignalSend("IndTmpStngLvlFR", nudFrontRightTemp.Value.ToString(), "false");
            SignalSend("IndSYNC_Rr", RearSync ? "1" : "0", "false");
        }

        #region Rear Temp and Fan Speed
        private void nudRearRightTemp_ValueChanged(object sender, EventArgs e)
        {
            SignalSend("IndTmpStngLvlRL", nudRearRightTemp.Value.ToString(), "false");
        }

        private void nudRearLeftTemp_ValueChanged(object sender, EventArgs e)
        {
            SignalSend("IndTmpStngLvlRR", nudRearLeftTemp.Value.ToString(), "false");
        }

        private void nudFanSpeed_ValueChanged(object sender, EventArgs e)
        {
            SignalSend("IndBlwStngLvlRr", nudFanSpeed.Value.ToString(), "false");
        }

        private void cmbIndTmpStngStRR_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cmbIndTmpStngStRR.SelectedItem.ToString().Split('=');
            if (val[0].Trim() == "2")
            {
                nudRearRightTemp.Enabled = false;
                SignalSend("IndTmpStngLvlRR", "60", "false");
                nudRearRightTemp.Value = 60;
                SignalSend("IndTmpStngStRR", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "3")
            {
                nudRearRightTemp.Enabled = false;
                SignalSend("IndTmpStngLvlRR", "85", "false");
                nudRearRightTemp.Value = 85;
                SignalSend("IndTmpStngStRR", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "4")
            {
                nudRearRightTemp.Enabled = true;
                SignalSend("IndTmpStngStRR", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "0")
            {
                nudRearRightTemp.Enabled = false;
                SignalSend("IndTmpStngStRR", val[0].Trim(), "false");
            }
        }

        private void cmbIndTmpStngStRL_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cmbIndTmpStngStRL.SelectedItem.ToString().Split('=');
            if (val[0].Trim() == "2")
            {
                nudRearLeftTemp.Enabled = false;
                SignalSend("IndTmpStngLvlRL", "60", "false");
                nudRearLeftTemp.Value = 60;
                SignalSend("IndTmpStngStRL", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "3")
            {
                nudRearLeftTemp.Enabled = false;
                SignalSend("IndTmpStngLvlRL", "85", "false");
                nudRearLeftTemp.Value = 85;
                SignalSend("IndTmpStngStRL", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "4")
            {
                nudRearLeftTemp.Enabled = true;
                SignalSend("IndTmpStngStRL", val[0].Trim(), "false");
            }
            if (val[0].Trim() == "0")
            {
                nudRearRightTemp.Enabled = false;
                SignalSend("IndTmpStngStRR", val[0].Trim(), "false");
            }
        }

        private void cmbIndBlwStngStRr_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] val = cmbIndBlwStngStRr.SelectedItem.ToString().Split('=');
            if (val[0].Trim() == "1")
            {
                nudFanSpeed.Enabled = false;
                SignalSend("IndBlwStngLvlRr", "8", "false");
                nudFanSpeed.Value = 8;
                SignalSend("IndBlwStngStRr", val[0].Trim(), "false");
            }

            if (val[0].Trim() == "2")
            {
                nudFanSpeed.Enabled = true;
                SignalSend("IndBlwStngStRr", val[0].Trim(), "false");
            }

            if (val[0].Trim() == "0")
            {
                nudFanSpeed.Enabled = false;
                SignalSend("IndBlwStngStRr", val[0].Trim(), "false");
            }
        }
        #endregion


        #region Rear HVAC CombiAirDistributionMode

        private bool _rearHVAC = false;
        private void btnRearAirDistCombiFace_Click(object sender, EventArgs e)
        {
            _rearHVAC = !_rearHVAC;

            if (_rearHVAC)
            {
                btnRearAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnRearAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press;
                btnRearAirDistCombiAuto.Text = "AUTO OFF";
                picBoxRearAutoCombi.Visible = false;
                SignalSend("IndAirDstRR", "4", "false");
                SignalSend("IndAirDstRL", "4", "false");
            }
            else
            {
                btnRearAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                SignalSend("IndAirDstRR", "1", "false");
                SignalSend("IndAirDstRL", "1", "false");
            }
        }

        private void btnRearAirDistCombiFloor_Click(object sender, EventArgs e)
        {
            _rearHVAC = !_rearHVAC;
            if (_rearHVAC)
            {
                btnRearAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_large;
                btnRearAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnRearAirDistCombiAuto.Text = "AUTO OFF";
                picBoxRearAutoCombi.Visible = false;
                SignalSend("IndAirDstRR", "2", "false");
                SignalSend("IndAirDstRL", "2", "false");
            }
            else
            {
                btnRearAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                SignalSend("IndAirDstRR", "1", "false");
                SignalSend("IndAirDstRL", "1", "false");
            }
        }
        
        private void btnRearAirDistCombiAuto_Click(object sender, EventArgs e)
        {
            _rearHVAC = !_rearHVAC;
            if (_rearHVAC)
            {
                btnRearAirDistCombiFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnRearAirDistCombiFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnRearAirDistCombiAuto.Text = "AUTO ON";
                picBoxRearAutoCombi.Visible = true;
                SignalSend("IndAirDstRR", "1", "false");
                SignalSend("IndAirDstRL", "1", "false");
            }
            else
            {
                btnRearAirDistCombiAuto.Text = "AUTO OFF";
                picBoxRearAutoCombi.Visible = false;
                SignalSend("IndAirDstRR", "0", "false");
                SignalSend("IndAirDstRL", "0", "false");
            }

        }

        #endregion

        #region Rear Air Distrubution Four Mode

        private bool _rearFourFaceHVAC = false;
        private bool _rearFourFloorHVAC = false;
        private bool _rearFourBiDirHVAC = false;
        private bool _rearFourDisHVAC = false;
        private bool _rearFourAutoHVAC = false;

        private void btnRearAirDistFourModeFace_Click(object sender, EventArgs e)
        {
            _rearFourFaceHVAC = !_rearFourFaceHVAC;
            if (_rearFourFaceHVAC)
            {
                btnRearAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnRearAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press;
                btnRearAirDistFourModeBiDir.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnRearAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxRearAutoFourMode.Visible = false;

                SignalSend("IndAirDstRR", "4", "true");
            }
            else
            {
                btnRearAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;

                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnRearAirDistFourModeFloor_Click(object sender, EventArgs e)
        {
            _rearFourFloorHVAC = !_rearFourFloorHVAC;
            if (_rearFourFloorHVAC)
            {
                btnRearAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_large;
                btnRearAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnRearAirDistFourModeBiDir.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnRearAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxRearAutoFourMode.Visible = false;

                SignalSend("IndAirDstRR", "2", "true");

            }
            else
            {
                btnRearAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnRearAirDistFourModeBiDir_Click(object sender, EventArgs e)
        {
            _rearFourBiDirHVAC = !_rearFourBiDirHVAC;
            if (_rearFourBiDirHVAC)
            {
                btnRearAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnRearAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnRearAirDistFourModeBiDir.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_active_press_right;
                btnRearAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxRearAutoFourMode.Visible = false;

                SignalSend("IndAirDstRR", "3", "true");
            }
            else
            {
                btnRearAirDistFourModeBiDir.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnRearAirDistFourModeStatus_Click(object sender, EventArgs e)
        {
            _rearFourDisHVAC = !_rearFourDisHVAC;
            if (_rearFourDisHVAC)
            {
                btnRearAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnRearAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnRearAirDistFourModeBiDir.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnRearAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_active_press;
                btnRearAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxRearAutoFourMode.Visible = false;

                SignalSend("IndAirDstRR", "1", "true");
            }
            else
            {
                btnRearAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnRearAirDistFourModeAuto_Click(object sender, EventArgs e)
        {
            _rearFourAutoHVAC = !_rearFourAutoHVAC;
            if (_rearFourAutoHVAC)
            {
                btnRearAirDistFourModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnRearAirDistFourModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnRearAirDistFourModeBiDir.BackgroundImage = global::HVAC.Properties.Resources.climate_airbidir_inactive_press_right;
                btnRearAirDistFourModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearAirDistFourModeAuto.Text = "AUTO ON";
                picBoxRearAutoFourMode.Visible = true;
                SignalSend("IndAirDstRR", "1", "true");
            }
            else
            {
                btnRearAirDistFourModeAuto.Text = "AUTO OFF";
                picBoxRearAutoFourMode.Visible = false;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        #endregion

        #region Rear Combi-Roof Air Distribution Mode

        private bool _rearFaceHVAC = false;
        private bool _rearFloorHVAC = false;
        private bool _rearDisHVAC = false;
        private bool _rearRoofHVAC = false;
        private bool _rearAutoHVAC = false;
        private void btnCombiRoofRearAirDistModeFace_Click(object sender, EventArgs e)
        {
            _rearFaceHVAC = !_rearFaceHVAC;

            if (_rearFaceHVAC)
            {
                btnRearCombiRoofAirDistModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnCombiRoofRearAirDistModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_active_press;
                btnAirCombiRoofDistModeRoof.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_up_inactive_press;
                btnRearCombiRoofAirDistModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearCombiRoofAirDistModeAuto.Text = "AUTO OFF";
                picBoxRearCombiRoofAutoMode.Visible = false;
                SignalSend("IndAirDstRR", "4", "true");
            }
            else
            {
                btnCombiRoofRearAirDistModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }


        private void btnRearCombiRoofAirDistModeFloor_Click(object sender, EventArgs e)
        {
            _rearFloorHVAC = !_rearFloorHVAC;

            if (_rearFloorHVAC)
            {
                btnRearCombiRoofAirDistModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_active_press_large;
                btnCombiRoofRearAirDistModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirCombiRoofDistModeRoof.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_up_inactive_press;
                btnRearCombiRoofAirDistModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearCombiRoofAirDistModeAuto.Text = "AUTO OFF";
                picBoxRearCombiRoofAutoMode.Visible = false;
                SignalSend("IndAirDstRR", "2", "true");
            }
            else
            {
                btnRearCombiRoofAirDistModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnAirCombiRoofDistModeRoof_Click(object sender, EventArgs e)
        {
            _rearDisHVAC = !_rearDisHVAC;

            if (_rearDisHVAC)
            {
                btnRearCombiRoofAirDistModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnCombiRoofRearAirDistModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirCombiRoofDistModeRoof.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_up_active_press;
                btnRearCombiRoofAirDistModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearCombiRoofAirDistModeAuto.Text = "AUTO OFF";
                picBoxRearCombiRoofAutoMode.Visible = false;
                SignalSend("IndAirDstRR", "6", "true");
            }
            else
            {
                btnAirCombiRoofDistModeRoof.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_up_inactive_press;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnRearCombiRoofAirDistModeStatus_Click(object sender, EventArgs e)
        {
            _rearRoofHVAC = !_rearRoofHVAC;

            if (_rearRoofHVAC)
            {
                btnRearCombiRoofAirDistModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnCombiRoofRearAirDistModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirCombiRoofDistModeRoof.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_up_inactive_press;
                btnRearCombiRoofAirDistModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_active_press;
                btnRearCombiRoofAirDistModeAuto.Text = "AUTO OFF";
                picBoxRearCombiRoofAutoMode.Visible = false;
                SignalSend("IndAirDstRR", "0", "true");
            }
            else
            {
                btnRearCombiRoofAirDistModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                SignalSend("IndAirDstRR", "0", "false");
            }
        }

        private void btnRearCombiRoofAirDistModeAuto_Click(object sender, EventArgs e)
        {
            _rearAutoHVAC = !_rearAutoHVAC;

            if (_rearAutoHVAC)
            {
                btnRearCombiRoofAirDistModeFloor.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_inactive_press_large;
                btnCombiRoofRearAirDistModeFace.BackgroundImage = global::HVAC.Properties.Resources.climate_airface_inactive_press;
                btnAirCombiRoofDistModeRoof.BackgroundImage = global::HVAC.Properties.Resources.climate_airfoot_up_inactive_press;
                btnRearCombiRoofAirDistModeStatus.BackgroundImage = global::HVAC.Properties.Resources.climate_air_dist_inactive_press;
                btnRearCombiRoofAirDistModeAuto.Text = "AUTO ON";
                picBoxRearCombiRoofAutoMode.Visible = true;
                SignalSend("IndAirDstRR", "1", "false");
            }
            else
            {
                btnRearCombiRoofAirDistModeAuto.Text = "AUTO OFF";
                picBoxRearCombiRoofAutoMode.Visible = false;
                SignalSend("IndAirDstRR", "0", "false");
            }

        }

        #endregion

        #endregion


        #region Helper

        public void SignalSend(string name, string value, string periodic)
        {
            var packet = new Adapter.Packet() { Name = name, Value = value, Periodic = periodic };
            _adapter.Send(packet);
        }



        #endregion

        private void HVAC_Load(object sender, EventArgs e)
        {
            ComboFillEnums(cmbIndTmpStngStFR, "IndTmpStngStFR");
            ComboFillEnums(cmbIndTmpStngStFL, "IndTmpStngStFL");
            ComboFillEnums(cmbIndBlwStngStFrt, "IndBlwStngStFrt");
            ComboFillEnums(cmbIndTmpStngStRR, "IndTmpStngStRR");
            ComboFillEnums(cmbIndTmpStngStRL, "IndTmpStngStRL");
            ComboFillEnums(cmbIndBlwStngStRr, "IndBlwStngStRr");
        }

        #region Winform Helpers
        public void ComboFillEnums(ComboBox cbo, string signalName)
        {
            Utility.FormHelper.ComboFillEnums(cbo, signalName, _db);
        }
        #endregion

        decimal cel = 0, fr = 0;
        public decimal FahrenheitToCelsius(decimal val)
        {
            fr = val;
            cel = (fr - 32) * 5 / 9;

            return cel;
        }
        public decimal CelsiusToFahrenheit(decimal val)
        {
            cel = val;
            fr = (cel * 9) / 5 + 32;

            return fr;
        }
        private void MetricRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            SignalSend("DispMeasSysExtnd", "2", "false");

                nudFrontLeftTemp.Minimum = 16;
                nudFrontRightTemp.Minimum = 16;
                nudRearLeftTemp.Minimum = 16;
                nudRearRightTemp.Minimum = 16;
                nudFrontLeftTemp.Maximum = 31;
                nudFrontRightTemp.Maximum = 31;
                nudRearLeftTemp.Maximum = 31;
                nudRearRightTemp.Maximum = 31;
        }
        private void USRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            SignalSend("DispMeasSysExtnd", "0", "false");
            nudFrontLeftTemp.Minimum = 60;
            nudFrontRightTemp.Minimum = 60;
            nudRearLeftTemp.Minimum = 60;
            nudRearRightTemp.Minimum = 60;
            nudFrontLeftTemp.Maximum = 90;
            nudFrontRightTemp.Maximum = 90;
            nudRearLeftTemp.Maximum = 90;
            nudRearRightTemp.Maximum = 90;
        }
        
        private void ImperialRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            SignalSend("DispMeasSysExtnd", "1", "false");
            nudFrontLeftTemp.Minimum = 16;
            nudFrontRightTemp.Minimum = 16;
            nudRearLeftTemp.Minimum = 16;
            nudRearRightTemp.Minimum = 16;
            nudFrontLeftTemp.Maximum = 31;
            nudFrontRightTemp.Maximum = 31;
            nudRearLeftTemp.Maximum = 31;
            nudRearRightTemp.Maximum = 31;
        }

        private void HVAC_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            e.Cancel = true;
            this.Hide();
        }
    }
}
