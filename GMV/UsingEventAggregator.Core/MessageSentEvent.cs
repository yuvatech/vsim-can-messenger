﻿using Prism.Events;

namespace UsingEventAggregator.Core
{
    public class MessageSentEvent : PubSubEvent<string>
    {
    }

    public class ECommMessageEvent : PubSubEvent<string>
    {
    }

    public class ConnectEvent : PubSubEvent<string>
    {
    }

    public class DisconnectEvent : PubSubEvent<string>
    {
    }

    public class AutoReconnectNeoEvent : PubSubEvent<string>
    {
    }

    public class AutoReconnectConnectEvent : PubSubEvent<string>
    {
    }

    public class AutoReconnectFailedEvent : PubSubEvent<string>
    {
    }

    public class GBTimerEvent : PubSubEvent<string>
    {
    }

    public class ConnectWindowEvent : PubSubEvent<string>
    {
    }
}
