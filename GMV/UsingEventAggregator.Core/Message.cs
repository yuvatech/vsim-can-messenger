﻿using System;
using System.Collections.Generic;
using System.Text;
using VSIM.Core;

namespace SIM.Core
{
    [Serializable]
    public class Message
    {
        public uint Id { get; set; }

        public int Length { get; set; }

        public bool Extended { get; set; }

        public bool HighVoltage { get; set; }

        public byte[] Data { get; set; }

        public Mode Network { get; set; }

        public bool MultiFrame { get; set; }

        public uint Trigger { get; set; }

        public double TimeStamp { get; set; }

        public int Port { get; set; }

        public Message() { }
        public Message(uint id, bool extended, bool highVoltage, byte[] data, int length, Mode network, bool multiFrame = false, uint trigger = 0, int port = 0)
        {
            Id = id;
            Extended = extended;
            HighVoltage = highVoltage;
            Data = data;
            Length = length;
            Network = network;
            MultiFrame = multiFrame;
            Trigger = trigger;
            Port = port;

            if (Data == null)
            {
                var temp = new List<byte>();
                for (var i = 0; i < Length; i++)  // init w/ padding
                    temp.Add(0x00);

                Data = temp.ToArray();
            }
        }

        public Message(Message message)
        {
            Id = message.Id;
            Extended = message.Extended;
            HighVoltage = message.HighVoltage;
            Data = message.Data;
            Length = message.Length;
            Network = message.Network;
            MultiFrame = message.MultiFrame;
            Trigger = message.Trigger;
            Port = message.Port;
        }
    }
}
