﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VSIM.Core
{
    [Serializable]
    public enum Mode
    {
        SW, LIN_i, LIN_t, HS, LS, FD, MF, FD_MF, LIN
    }

    [Serializable]
    public enum Status
    {
        Connected = 0,
        DriverNotFound,
        NotConnected,
        UnknownError,
        NotAbleToConnect,
        NoSuchDevice,
        AlreadyConnected,
        InUse,
        ConnectionInProgress
    }

    class Appendix
    {
    }
}
