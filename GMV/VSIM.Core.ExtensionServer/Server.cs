﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// *** Note: is almost a copy and paste from main solution usage, keep in sync *** //

namespace ExtensionServer
{
    public class Server
    {
        private readonly IPAddress _ip;
        private readonly int _port;

        public bool IsRunning { get; private set; } = false;

        private readonly ConcurrentDictionary<Guid, Client> _clients = new ConcurrentDictionary<Guid, Client>();

        public int Count
        {
            get { return _clients.Count; }
        }

        public delegate void ReceivedHandler(object sender, byte[] buffer);
        public event ReceivedHandler OnReceived;

        public Server(int port = 55555, bool autoStart = false)
            : this(IPAddress.Any, port, autoStart)
        {

        }

        public Server(IPAddress ip, int port, bool autoStart = false)
        {
            this._ip = ip;
            this._port = port;

            if (autoStart)
                this.Run();
        }

        public bool Send(Guid id, byte[] buffer)
        {
            var sent = false;
            // block until done!
            var client = _clients.First(c => c.Key == id);
            if (!client.Equals(default(KeyValuePair<Guid, Client>)))
            {
                try
                {
                    sent = client.Value.Send(buffer);
                }
                catch (Exception e)
                {
                    ;
                }

                if (!sent)  // must be disconnected???
                {
                    Client temp = null;
                    _clients.TryRemove(client.Key, out temp); // will try again on next ping
                }
            }

            return sent;
        }

        public void Broadcast(byte[] buffer, Guid[] skip = null)  // e.g. skip self
        {
            foreach (var client in _clients)
            {
                var ignore = false;
                if (skip != null)
                {
                    if (skip.Contains(client.Value.Id))
                        ignore = true; // skip
                }

                if (!ignore)
                {
                    // Send(client.Key, buffer);
                    Task.Run(() => Send(client.Key, buffer));
                }
            }
        }

        public void Run()
        {
            this.IsRunning = true;

            // none blocking
            new Thread(() =>
            {
                // listen for connection(s)
                var listener = new TcpListener(_ip, _port);
                listener.Start();

                while (IsRunning)
                {
                    if (listener.Pending())  // want to connect ?
                    {
                        // connection incoming. accept, setup data incoming event and add to client list
                        var client = new Client(listener.AcceptSocket(), Guid.NewGuid());
                        client.OnReceived += receivedHandler_client;
                        while (!_clients.TryAdd(client.Id, client))  // keep trying!
                            Thread.Sleep(125);
                        Trace.WriteLine("Client " + client.Id + " connected.");
                    }
                    else
                    {
                        Thread.Sleep(125);  // CPU killer
                    }
                }
                Stop();

            }).Start();

            /*
            new Thread(() =>
            {
                while (running)
                {
                    var buffer = Encoding.ASCII.GetBytes("***PING***");
                    SendToAll(buffer);
                    Thread.Sleep(1000); // 1 sec
                }
            }).Start();
            */
        }

        public void Stop()
        {
            // exit listening loop
            this.IsRunning = false;

            // attempt to disconnect every client in list "client" cleanily.
            foreach (var client in _clients)
                client.Value.Close();

            _clients.Clear();
        }

        public int Connected()
        {
            return _clients.Count();
        }

        private void receivedHandler_client(object sender, byte[] buffer)
        {
            if (OnReceived != null)
            {
                var client = (Client)sender;
                /*
                string data = Encoding.ASCII.GetString(buffer.ToArray(), 0, buffer.Length);
                if (data.Contains("***PONG***"))
                    buffer = Encoding.ASCII.GetBytes(data.Replace("***PONG***", "")).ToArray();
                */

                //Debug.WriteLine("Rx'd by: " + client.Id);
                if (buffer.Length > 0)
                    OnReceived(client, buffer.ToArray());
            }
        }
    }
}
