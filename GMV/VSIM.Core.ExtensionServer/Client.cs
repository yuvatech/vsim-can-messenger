﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExtensionServer
{
    public class Client
    {
        private const int BUFFER_SIZE = 512000 * 2 * 5;  // big enough?  ~5MB
        private Socket _client;
        private bool _listen = false;

        public Guid Id { get; }

        public byte[] Terminator { get; set; }

        //  event to pass received data to the server class
        internal delegate void ReceivedHandler(object sender, byte[] data);
        internal event ReceivedHandler OnReceived;

        public Client(Socket client, Guid id)
        {
            //Assign members
            this._client = client;
            this.Id = id;

            this._client.SendTimeout = 25;
            this._client.ReceiveTimeout = -1;

            this._client.SendBufferSize = BUFFER_SIZE;
            this._client.ReceiveBufferSize = BUFFER_SIZE;

            this._client.Blocking = true;
            this._client.NoDelay = true;

            Terminator = null;

            //Init the StreamWriter
            var thread = new Thread(() => { Listen(client); }) { Priority = ThreadPriority.AboveNormal };
            thread.Start();
        }

        //Reads data from the connection and fires an event wih the received data
        private void Listen(Socket socket)
        {
            _listen = true;
            var data = new byte[BUFFER_SIZE];
            var buffer = new List<byte>();

            while (_listen)
            {
                try
                {
                    while (true)
                    {
                        var count = 0;
                        while (true)
                        {
                            try
                            {
                                count = socket.Receive(data);
                            }
                            catch (SocketException e)
                            {
                                if (socket.Connected)
                                {
                                    Thread.Sleep(1);
                                    continue;
                                }

                                throw e;
                            }

                            break;
                        }

                        if (count <= 0)
                        {
                            Thread.Sleep(25); // give CPU a breathe
                            break;
                        }

                        for (var x = 0; x <= count; x++)
                            buffer.Add(data[x]);

                        // *** Important - Hacky Mess !!! ***
                        // matching past operation (coding design...) it did not wait for an end message indicator (i.e. newline, or ...)!!!
                        // added terminator option for future because feels good                        

                        var end = buffer.Count;
                        if (Terminator != null && Terminator.Length > 0)
                        {
                            var index = Utility.Extensions.IndexOf(buffer.ToArray(), Terminator);
                            if (index >= 0)
                            {
                                end = index;
                                buffer.RemoveRange(index, Terminator.Length);
                            }
                        }

                        if (OnReceived != null)  // have a subscriber?
                        {
                            if (buffer.Count > 0) // have data?
                            {
                                if (Terminator == null)
                                {
                                    OnReceived(this, buffer.ToArray());
                                    buffer.Clear();
                                }
                                else
                                {
                                    var temp = new List<byte>();
                                    for (var i = 0; i < end; i++)
                                        temp.Add(buffer[i]);

                                    buffer.RemoveRange(0, end);

                                    OnReceived(this, temp.ToArray()); // event!                                
                                }
                                Thread.Sleep(0);

                            }
                        }
                    }
                }
                /*
                catch (System.Net.Sockets.SocketException)
                {
                    Close();
                    return;
                }
                */
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    if (!socket.Connected)
                        break;
                }
            }
            Close();
        }

        //Sends the string "data" to the client
        public bool Send(byte[] buffer)
        {
            try
            {
                if (_client == null)
                    return false;

                // write immeditaly 
                if (buffer.Length != _client.Send(buffer))
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                // cannot write then client is gone
                Close();
            }
            return false;
        }

        //Closes the connection
        public void Close()
        {
            // close streams
            if (_listen)
            {
                try
                {
                    _client.Close();
                }
                catch { }

                Trace.WriteLine("Client " + this.Id + " disconnceted.");
            }

            _client = null;
            // stop listening
            _listen = false;
        }
    }
}
