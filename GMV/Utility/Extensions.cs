﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Xml.Serialization;
using System.Numerics;

namespace Utility
{
    public static class Extensions
    {
        // [Serializable]
        public static void DeepCopy<T>(ref T object2Copy, ref T objectCopy)
        {
            using (var stream = new MemoryStream())
            {
                using (var ms = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(ms, object2Copy);
                    ms.Position = 0;

                    objectCopy = (T) formatter.Deserialize(ms);
                }
            }
        }

        public static bool TryParseJson<T>(this string item, out T result)
        {
            try
            {
                // Validate missing fields of object
                var settings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                result = JsonConvert.DeserializeObject<T>(item, settings);
                return true;
            }
            catch (JsonSerializationException ex)
            {
                result = default(T);
                return false;
            }
        }

        public static string ToHexString(this byte[] items)
        {
            if (items == null)
                return "";

            var stringBuilder = new StringBuilder();
            foreach (var item in items)
                stringBuilder.AppendFormat("{0:X2}", item);
            return stringBuilder.ToString();
        }

        public static byte[] FromHexString(this string item)
        {
            return Enumerable.Range(0, item.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(item.Substring(x, 2), 16))
                .ToArray();
        }

        /*
         * In big-endian format, whenever addressing memory or sending/storing words bytewise,
         * the most significant byte—the byte containing the most significant bit—is stored first
         * (has the lowest address) or sent first, then the following bytes are stored or sent in
         * decreasing significance order, with the least significant byte—the one containing the
         * least significant bit—stored last (having the highest address) or sent last.
         */
        public static byte[] ParseBits(this byte[] bytes, uint startBit, uint bitLength, bool littleEndian = true)
        {
            const ushort bitsInByte = 8;
            const ushort bytesInLong = 8;

            if ((bytes.Length * bitsInByte) < (startBit + bitLength))
            {
                return null;
                //throw new ArgumentOutOfRangeException("Data bytes smaller then expected. Possible multiframe message!");
            }

            // starting bit
            var offset = (int)(startBit + bitLength - 1);
            if (littleEndian)
            {
                Array.Reverse(bytes);
                var startPosition = Convert.ToInt16(startBit); // reverse start position
                // Note: 8 bits in byte
                offset = (bytes.Length * bitsInByte) - (((startPosition / bitsInByte) * bitsInByte) + ((bitsInByte-1) - (startPosition % bitsInByte)) + (int)bitLength);
            }
            var bitArray = new BitArray(bytes);
            ulong value = 0; //BitConverter.ToUInt64(bytes, 0);
            var shift = (int)(bitLength - 1);
            var traverse = (int)(offset + (bitLength - 1)); // furthest out going backwards

            //offset += 1;  // length would be max 64, index would be max 63

            // traverse backwards through the bits and perform bitwise operation from furtheset position
            for (var position = traverse; position >= offset; position--)
            {
                if (bitArray.Length > position) // Todo:  is this right??????
                {
                    value |= (Convert.ToUInt64(bitArray.Get(position)) << shift);
                    shift--;
                }
                else
                {
                    //Debug.WriteLine("REALLY?");
                }
            }

            var byteValue = BitConverter.GetBytes(value);
            byte[] specificValue = null;
            if (bitLength > bitsInByte) // if not more then 1 byte, then do nothing...
            {
                var byteValueLength = bitLength / bitsInByte;
                if (byteValueLength >= bytesInLong) // 8 bytes in long (MAX size)
                    specificValue = byteValue;
                else
                    specificValue = byteValue.SubArray(0, (int)(byteValueLength * 1.5)); // if partial byte get full byte
            }
            else
            {
                specificValue = byteValue.SubArray(0, 1);
            }

            if (littleEndian)  // put back
                Array.Reverse(bytes);

            var bitValue = new BitArray(byteValue); // to debug with break pt...
            return specificValue;
        }

        // Either Add or overwrite
        public static void AddOrUpdate<K, V>(this ConcurrentDictionary<K, V> dictionary, K key, V value)
        {
            dictionary.AddOrUpdate(key, value, (keyOld, valueOld) => value);
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            var result = new T[length];
            if (index > length)
                return null;
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static int IndexOf(byte[] data, byte[] find)
        {
            if (find.Length > data.Length)
                return -1;
            for (var i = 0; i < data.Length - find.Length; i++)
            {
                bool found = true;
                for (var j = 0; j < find.Length; j++)
                {
                    if (data[i + j] != find[j])
                    {
                        found = false;
                        break;
                    }
                }
                if (found)
                    return i;
            }
            return -1;
        }
    }
}
