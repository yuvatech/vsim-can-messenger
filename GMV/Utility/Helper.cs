﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Helper
    {
        public static long GC_Limit(long init = 15728640) // number from testing......
        {
            var bytes = init;
            while (true)
            {
                try
                {
                    GC.TryStartNoGCRegion(bytes);
                }
                catch (System.ArgumentOutOfRangeException e)
                {
                    bytes = bytes - 1048576; // 1MB
                    continue;
                }
                GC.EndNoGCRegion();
                break;
            }

            return bytes;
        }
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static bool SaveConfig(string property, string value, string section)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var sectionConfig = ((AppSettingsSection)config.GetSection(section)).Settings;
            sectionConfig.Remove(property);
            sectionConfig.Add(property, value);
            // Save the configuration file.
            config.Save(ConfigurationSaveMode.Modified);
            // Force a reload of a changed section.
            ConfigurationManager.RefreshSection(section);

            return true;
        }

        public static string GetConfig(string property, string section)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var sectionConfig = ((AppSettingsSection)config.GetSection(section)).Settings;
            var settings = sectionConfig[property].Value.ToString();
            return settings;
        }

        public static string ProcessHidden(string command, string args = "")
        {            
            ProcessStartInfo startInfo = new ProcessStartInfo();            
#if UNIX              
            startInfo.FileName = "/bin/bash";            
            string args_temp = "-c \" cd /tmp; " + command;

            if(!string.IsNullOrEmpty(args))
              args_temp += " " + args;
            args_temp += "\"";

            startInfo.Arguments = args_temp;
#else
            startInfo.FileName = command;
            startInfo.Arguments = args;
#endif
            
            Debug.WriteLine(string.Format("Terminal: {0} {1}", startInfo.FileName, startInfo.Arguments));
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            System.Diagnostics.Process processTemp = new System.Diagnostics.Process
            {
                StartInfo = startInfo, EnableRaisingEvents = true
            };
            try
            {
                processTemp.Start();
                //processTemp.WaitForExit();
                while(!processTemp.HasExited)
                    System.Threading.Thread.Sleep(25);  // rather have async await!!!                
                var output = processTemp.StandardError.ReadToEnd();
                output += Environment.NewLine;
                output += processTemp.StandardOutput.ReadToEnd();
                Trace.WriteLine(string.Format("Terminal: {0}", output));
                return output;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return string.Empty;
        }
    }
}
