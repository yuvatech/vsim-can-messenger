﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Transforms
    {
        public static MethodInfo[] GetClassMethods(System.Type type)
        {
            MethodInfo[] methodsInfo = (type).GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            return methodsInfo;            
        }
    }
}
