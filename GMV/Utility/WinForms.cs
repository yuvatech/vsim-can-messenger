﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utility
{
    public class WinForms
    {
        public static void TimerButtonFunction_Helper(Action<object, EventArgs> f1, Button button, System.Windows.Forms.Timer timer, int milliseconds = 1000)
        {
            if (!timer.Enabled)
            {
                timer.Interval = milliseconds;
                timer.Tick += (t, args) =>
                {
                    f1(null, null);
                };
                timer.Enabled = true;
                button.BackColor = Color.LawnGreen;
            }
            else
            {
                timer.Enabled = false;
                
                button.BackColor = Color.Maroon;
            }
        }
    }
}
