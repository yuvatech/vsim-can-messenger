﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using Utility;

namespace MessageDecodeManager
{
    public static class ARXML
    {
        private static List<List<Message>> _messages;
        private static List<List<SignalGroup>> _signalGroups;
        private static List<List<Signal>> _signals;

        private static bool _taken = false;
        private static object _lock = new object();

        public static bool Loaded { get; private set; }
        public static uint Channels { get; private set; }

        public static event EventHandler proc_finished = delegate { };

        public enum Hardware
        {
            CSM,
            VCU,
            None
        };
        static ARXML()
        {
            Load();
        }

        public static void Load(int channels = 2, bool overried = false)
        {
            ARXML.Channels = (uint)channels;

            var hware = Utility.Helper.GetConfig("db", "application");
            lock (_lock)
            {
                if (Loaded && !overried)
                    return;
            }

            lock (_lock)
            {
                Loaded = false;
                _taken = true;

                // Note: use "Instance" not this, do not need more then one.
                _messages = new List<List<Message>>();
                _signalGroups = new List<List<SignalGroup>>();
                _signals = new List<List<Signal>>();
                for (var i = 0; i < channels; i++)
                {
                    var channel = i;
                    _messages.Add(new List<Message>());
                    _signalGroups.Add(new List<SignalGroup>());
                    _signals.Add(new List<Signal>());

                    var filename = "arxml" + hware.ToLower() + Convert.ToString(channel + 1) + ".json";

                    var filepath = Path.Combine(Utility.Helper.AssemblyDirectory, filename);
                    if (!File.Exists(filepath))
                    {
                        Console.WriteLine("ARXML Not Found!");
                        return;
                    }

                    try
                    {

                        var fileText = File.ReadAllText(filepath);
                        //var dbTexts = (JObject)JsonConvert.DeserializeObject(fileText);                        

                        //using (var file = File.OpenText(filepath))
                        {
                            //using (var reader = new JsonTextReader(file))
                            {
                                //var obj = (JObject)JToken.ReadFrom(reader);
                                var node = JsonConvert.DeserializeXmlNode(fileText, "Root");

                                foreach (XmlNode child in node.FirstChild.ChildNodes[1].ChildNodes)
                                {
                                    var tmpList = new List<string>();

                                    foreach (XmlNode child1 in child)
                                        tmpList.Add(child1.InnerText);

                                    _signalGroups[channel].Add(new SignalGroup()
                                    {
                                        Name = child.Name,
                                        Signals = tmpList
                                    });
                                }

                                foreach (XmlNode child in node.FirstChild.ChildNodes[2].ChildNodes)
                                {
                                    var tmpDict = new Dictionary<string, string>();
                                    var enumDict = new Dictionary<string, string>();

                                    foreach (XmlNode child1 in child)
                                    {
                                        if (child1.ChildNodes.Count > 1)
                                        {
                                            enumDict = new Dictionary<string, string>();
                                            foreach (XmlNode child2 in child1)
                                                enumDict.Add(child2.Name, child2.InnerText);
                                        }
                                        else
                                        {
                                            tmpDict.Add(child1.Name, child1.InnerText);
                                        }
                                    }

                                    _signals[channel].Add(new Signal()
                                    {
                                        Name = child.Name,
                                        Properties = tmpDict,
                                        Enums = enumDict
                                    });
                                }

                                foreach (XmlNode child in node.FirstChild.ChildNodes[0].ChildNodes)
                                {
                                    var tmpDict = new Dictionary<string, string>();
                                    var signalList = new List<string>();
                                    foreach (XmlNode child1 in child)
                                    {
                                        if (child1.Name == "signals")
                                        {
                                            signalList.Add(child1.InnerText);
                                        }
                                        else
                                        {
                                            tmpDict.Add(child1.Name, child1.InnerText);
                                        }
                                    }

                                    var messageSignals = new List<MessageDecodeManager.Signal>();
                                    foreach (var signal in signalList)
                                        messageSignals.AddRange(_signals[channel].Where(s => s.Name == signal));

                                    _messages[channel].Add(new Message()
                                    {
                                        Name = child.Name,
                                        Id = (uint)Convert.ToUInt64(tmpDict["Identifier"]),
                                        Properties = tmpDict,
                                        Signals = messageSignals
                                    });
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }

                Loaded = true;
                _taken = false;
            }
            return;
        }

        private static void LockWait()
        {
            while (_taken)
                System.Threading.Thread.Sleep(30);
        }

        public static Message[] GetMessages(int channel = 0)
        {
            LockWait();
            return _messages[channel].ToArray();
        }

        public static Signal[] GetSignals(int channel = 0)
        {
            LockWait();
            return _signals[channel].ToArray();
        }

        public static Message FindMessageById(uint id, int channel = 0)
        {
            LockWait();
            var message = _messages[channel].AsParallel().FirstOrDefault(m => m.Properties["Identifier"] == id.ToString());
            return message;
        }

        public static Message FindMessageByName(string name, int channel = 0)
        {
            LockWait();
            var message = _messages[channel].AsParallel().FirstOrDefault(m => m.Name == name);
            return message;
        }

        public static Signal FindSignalByName(string name, int channel = 0)
        {
            LockWait();
            var signal = _signals[channel].AsParallel().FirstOrDefault(s => s.Name == name);
            return signal;
        }

        public static Message FindMessageBySignal(Signal signal, int channel = 0)
        {
            LockWait();
            Message result = null;
            //foreach (var item in GetMessages(channel))
            Parallel.ForEach(GetMessages(channel), (item, state0) =>
            {
                foreach (var itemSignal in item.Signals)
                {
                    if (itemSignal.Name == signal.Name)
                    {
                        result = item;
                        break;
                    }
                }

                if (result != null)
                    state0.Break();
            });
            return result;
        }

        public static dynamic SignalValue(MessageDecodeManager.Signal signal, byte[] data)
        {
            decimal min = 0;
            decimal max = 0;
            decimal enumval = 0;
            decimal offset = 0;
            decimal factor = 1;
            uint startBit = 0;
            uint bitLength = 0;
            var littleEndian = false;
            var convert = true;

            foreach (var signalAttributes in signal.Properties.ToList())
            {
                switch (signalAttributes.Key)
                {
                    /* AUTOSAR has two categories of data types: “normal” ones, which Endianness can be converted,
                       and “opaque”, for which COM does not do any conversions.
                     */
                    case "byteorder":
                        if (signalAttributes.Value.ToUpper().Contains("MOST-SIGNIFICANT"))
                            littleEndian = true;
                        else if (signalAttributes.Value.ToUpper().Contains("OPAQUE"))
                            convert = false;
                        break;
                    case "startbit":
                        startBit = Convert.ToUInt32(signalAttributes.Value);
                        break;
                    case "bitlength":
                        bitLength = Convert.ToUInt32(signalAttributes.Value);
                        break;
                }
            }

            if (signal.Enums.Count > 0) // if is an enum value type
            {
                min = 0;
                max = signal.Enums.Count == 1 ? 1 : signal.Enums.Count - 1;
            }
            else // not enum
            {
                if (signal.Properties.ContainsKey("min"))
                    min = Convert.ToDecimal(signal.Properties["min"]);
                if (signal.Properties.ContainsKey("max"))
                {
                    var temp = Convert.ToDouble("0");
                    var maxString = signal.Properties["max"];
                    var expIndex = maxString.IndexOf("+e");
                    if (expIndex >= 0)
                    {
                        // 5.2E4
                        var split = maxString.Split(new string[] { "+e" }, StringSplitOptions.None);
                        temp = Convert.ToDouble(split[0]); // 5.2
                        var raised = Convert.ToDouble(split[1]); // 4
                        temp = temp * Math.Pow(10, raised); // 5.2 * 10^4
                    }
                    else
                    {
                        temp = Convert.ToDouble(maxString);
                    }

                    max = Convert.ToDecimal(temp);
                }

                if (signal.Properties.ContainsKey("offset"))
                    offset = Convert.ToDecimal(signal.Properties["offset"]);
                if (signal.Properties.ContainsKey("factor"))
                {
                    factor = decimal.Parse(signal.Properties["factor"], NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint,
                   CultureInfo.InvariantCulture);
                }
            }

            var bits = new byte[] { };
            try
            {

                if ((startBit + bitLength) <= 64) // max long size, how can it be longer?
                {
                    bits = data.ParseBits(startBit, bitLength, littleEndian);
                    if (bits == null)
                        return null;
                }
                else  // Note: How is this possible???
                    return null;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Note: Multi frame message currently not supported, function supports but data sent in is not long enough!!!
                return null;
            }

            dynamic value;
            if (max % 1 == 0 && min % 1 == 0) // if not remainder then assume type of int (signed or not)
            {
                var temp = bits;
                Array.Resize(ref temp, 8); // 8 bytes in long, 4 bytes in int

                if (min < 0) // if less then zero then is int
                    value = (int)BitConverter.ToInt64(temp, 0);
                else // if min not less then zero is un-signed int
                    value = (uint)BitConverter.ToUInt64(temp, 0);
            }
            else
            {
                // resize to fit a single/float
                var temp = bits;
                Array.Resize(ref temp, 8); // 8 bytes in long, 4 bytes in int

                var intVal = BitConverter.ToInt64(temp, 0);
                value = Convert.ToDecimal(intVal);
            }

            //(value - OffsetVal) / FactorVal)
            if (convert)
                value = (value * factor) + offset;  // reverse change decimal to byte offset factor math :)
            else
                value = value; // break debug
            //value = (value - offset)/factor;
            return value;
        }

        #region TODO: remove copy paste code (so ugly)
        // Note: GMDeviceLibrary.cs copy and paste
        private class SenderDataType
        {
            public int dlc;
            public byte[] data;
            public uint id;
        }

        private static class LibraryMessageConvert
        {
            /// <summary>
            /// Function takes in the all of the signal information and it returns an 8 byte array of the CANMessage
            /// </summary>
            /// <param name="ByteOrder"></param>
            /// <param name="StartingBit"></param>
            /// <param name="BitLength"></param>
            /// <param name="SignalDataValue"></param>
            /// <param name="PreviousDataArray"></param>
            /// <param name="MinVal"></param>
            /// <param name="MaxVal"></param>
            /// <param name="OffsetVal"></param>
            /// <param name="FactorVal"></param>
            /// <returns></returns>
            public static byte[] setMessageSigned(string ByteOrder, uint StartingBit, uint BitLength, Int64 SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
            {
                byte[] CANDataOut;
                Int64 PreviousFullSignal;
                int SignalShiftLength;
                Int64 SignalClear;
                Int64 SignalDataConverted;
                Int64 ShiftedValue;

                if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
                {
                    throw new OverflowException("Value out of range");
                }

                PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);

                SignalDataValue = Convert.ToInt64(((SignalDataValue - OffsetVal) / FactorVal));
                BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

                SignalDataConverted = Convert.ToInt64(SignalValueConverted.ToString());

                if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
                {

                    Array.Reverse(PreviousDataArray);
                    PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);
                    SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                    Array.Reverse(PreviousDataArray);
                }
                else
                {
                    SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
                }

                if (BitLength < 64)
                {
                    BigInteger test = new BigInteger();
                    test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                    string holder = test.ToString();
                    SignalClear = Convert.ToInt64(holder);
                    SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                    SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
                }
                else
                {
                    SignalClear = 0;
                }

                PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

                ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

                ShiftedValue = ShiftedValue & ~SignalClear; //Remove leading 1's for a negative number

                PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

                CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

                if (ByteOrder == "0")
                {
                    Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent
                }

                return CANDataOut;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="ByteOrder"></param>
            /// <param name="StartingBit"></param>
            /// <param name="BitLength"></param>
            /// <param name="SignalDataValue"></param>
            /// <param name="PreviousDataArray"></param>
            /// <param name="MinVal"></param>
            /// <param name="MaxVal"></param>
            /// <param name="OffsetVal"></param>
            /// <param name="FactorVal"></param>
            /// <returns></returns>
            public static byte[] setMessageUNSigned(string ByteOrder, uint StartingBit, uint BitLength, UInt64 SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
            {
                byte[] CANDataOut;
                UInt64 PreviousFullSignal;
                int SignalShiftLength;
                UInt64 SignalClear;
                UInt64 SignalDataConverted;
                UInt64 ShiftedValue;

                if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
                {
                    //throw new OverflowException("Value out of range");
                }

                PreviousFullSignal = BitConverter.ToUInt64(PreviousDataArray, 0);

                SignalDataValue = Convert.ToUInt64((SignalDataValue - OffsetVal) / FactorVal);
                BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

                SignalDataConverted = Convert.ToUInt64(SignalValueConverted.ToString());


                if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
                {

                    Array.Reverse(PreviousDataArray);
                    PreviousFullSignal = BitConverter.ToUInt64(PreviousDataArray, 0);
                    SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                    Array.Reverse(PreviousDataArray);
                }
                else
                {
                    SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
                }

                if (BitLength < 64)
                {
                    BigInteger test = new BigInteger();
                    test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                    string holder = test.ToString();
                    SignalClear = Convert.ToUInt64(holder);
                    SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                    SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
                }
                else
                {
                    SignalClear = 0;
                }

                PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

                ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

                PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

                CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

                if (ByteOrder == "0")
                {
                    Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent
                }

                return CANDataOut;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="ByteOrder"></param>
            /// <param name="StartingBit"></param>
            /// <param name="BitLength"></param>
            /// <param name="SignalDataValue"></param>
            /// <param name="PreviousDataArray"></param>
            /// <param name="MinVal"></param>
            /// <param name="MaxVal"></param>
            /// <param name="OffsetVal"></param>
            /// <param name="FactorVal"></param>
            /// <returns></returns>
            public static byte[] setMessageDecimal(string ByteOrder, uint StartingBit, uint BitLength, decimal SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
            {
                byte[] CANDataOut;
                Int64 PreviousFullSignal;
                int SignalShiftLength;
                Int64 SignalClear;
                Int64 SignalDataConverted;
                Int64 ShiftedValue;

                if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
                    throw new OverflowException("Value out of range");

                PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);

                SignalDataValue = Convert.ToInt64((SignalDataValue - OffsetVal) / FactorVal);
                BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

                SignalDataConverted = Convert.ToInt64(SignalValueConverted.ToString());

                if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
                {

                    Array.Reverse(PreviousDataArray);
                    PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);
                    SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                    Array.Reverse(PreviousDataArray);
                }
                else
                {
                    SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
                }

                if (BitLength < 64)
                {
                    BigInteger test = new BigInteger();
                    test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                    string holder = test.ToString();
                    SignalClear = Convert.ToInt64(holder);
                    SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                    SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
                }
                else
                {
                    SignalClear = 0;
                }

                PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

                ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

                ShiftedValue = ShiftedValue & ~SignalClear; //Remove leading 1's for a negative number

                PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

                CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

                if (ByteOrder == "0")
                    Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent

                return CANDataOut;
            }

            /// <summary>
            /// Function takes in the starting bit of the signal and its length and returns the position inside the CANMessage
            /// </summary>
            /// <param name="startbit"></param>
            /// <param name="length"></param>
            /// <returns></returns>
            private static int getOffset(uint startbit, uint length)
            {
                int offset;  //offset will be used to shift value
                int byte1;
                int bit;
                int start;
                int len;

                start = Convert.ToInt16(startbit);
                len = Convert.ToInt16(length);

                bit = start % 8;
                byte1 = start / 8;

                offset = 64 - ((byte1 * 8) + (7 - bit) + len);

                return offset;
            }
        }
        #endregion

        // Signals = "<name, value>", carry over style....

        public static byte[] Encode(Signal[] signals, int channel = 0)
        {
            var signalsOldWay = new Dictionary<string, string>();
            foreach (var signal in signals)
                signalsOldWay.Add(signal.Name, Convert.ToString(signal.Value));
            return Encode(signalsOldWay, channel);
        }

        public static byte[] Encode(Dictionary<string, string> signals, int channel = 0)
        {
            // Note: similar to GlobalBDatabase Send implementation (originally a copy paste)!!!
            var items = new List<KeyValuePair<string, string>>();
            if (signals != null)
                items = signals.ToList(); // copy

            var sendMessages = new Dictionary<uint, SenderDataType>();
            foreach (var item in items)
            {
                Signal signal = null;
                Message message = null;
                UInt64 signalDataValue = 0;
                string signalDataValueString = "";
                byte[] dataOut;
                byte[] data = new byte[256]; // default, can rest to more bytes later if needed
                string byteorder = "";
                uint startBit = 0;
                uint bitLength = 0;
                decimal Min = 0;
                decimal Max = 0;
                decimal enumval = 0;
                decimal offset = 0;
                decimal factor = 1;

                if (item.Key == null)
                    continue;

                signal = ARXML.FindSignalByName(item.Key, channel);

                if (signal == null) // should not be the case, but just-incase not throw exception if not found.
                {
                    Debug.WriteLine("Signal: " + item.Key + " NOT Found!");
                    continue;
                }

                message = ARXML.FindMessageBySignal(signal);
                if (message == null) // should not be the case, but just-incase not throw exception if not found.
                {
                    Debug.WriteLine("Message: " + item.Key + " NOT Found!");
                    continue;
                }

                signalDataValueString = item.Value;
                byteorder = "1"; // if found later will change
                startBit = 0;
                bitLength = 0;
                var convert = false;

                foreach (var signalAttributes in signal.Properties.ToList())
                {
                    switch (signalAttributes.Key)
                    {
                        /* AUTOSAR has two categories of data types: “normal” ones, which Endianness can be converted,
                           and “opaque”, for which COM does not do any conversions.
                         */
                        case "byteorder":
                            if (signalAttributes.Value.ToUpper().Contains("MOST-SIGNIFICANT"))
                            {
                                var littleEndian = true;
                                byteorder = "0";
                            }
                            else if (signalAttributes.Value.ToUpper().Contains("OPAQUE"))
                                convert = false;  // if to apply factoring and offsets and so on...
                            else
                                ;
                            break;
                        case "startbit":
                            startBit = Convert.ToUInt32(signalAttributes.Value);
                            break;
                        case "bitlength":
                            bitLength = Convert.ToUInt32(signalAttributes.Value);
                            break;
                    }
                }

                var id = (uint)Convert.ToUInt32(message.Properties["Identifier"]);
                var dlc = (int)Convert.ToInt32(message.Properties["length"]);

                // prevous data
                if (sendMessages.ContainsKey(id))
                    data = sendMessages[id].data;

                if (signal.Enums.Count > 0)
                {
                    Min = 0;
                    Max = signal.Enums.Count == 1 ? 1 : signal.Enums.Count - 1;
                }
                else
                {
                    if (signal.Properties.ContainsKey("min"))
                        Min = Convert.ToDecimal(signal.Properties["min"]);
                    if (signal.Properties.ContainsKey("max"))
                    {
                        var max = Convert.ToDouble("0");
                        var max_string = signal.Properties["max"];
                        var exp_index = max_string.IndexOf("+e");
                        if (exp_index >= 0)
                        {
                            // 5.2E4
                            var split = max_string.Split(new string[] { "+e" }, StringSplitOptions.None);
                            max = Convert.ToDouble(split[0]); // 5.2
                            var raised = Convert.ToDouble(split[1]); // 4
                            max = max * Math.Pow(10, raised); // 5.2 * 10^4
                        }
                        else
                        {
                            max = Convert.ToDouble(max_string);
                        }

                        Max = Convert.ToDecimal(max);

                    }

                    if (signal.Properties.ContainsKey("offset"))
                        offset = Convert.ToDecimal(signal.Properties["offset"]);
                    if (signal.Properties.ContainsKey("factor"))
                    {
                        factor = decimal.Parse(signal.Properties["factor"],
                            NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint,
                            CultureInfo.InvariantCulture);
                    }
                }

                if (Max % 1 == 0)
                {
                    //int
                    if (Min < 0)
                    {
                        dataOut = LibraryMessageConvert.setMessageSigned(byteorder, startBit, bitLength,
                            Convert.ToInt64(signalDataValueString), data, Min, Max, offset, factor);
                    }
                    else
                    {
                        dataOut = LibraryMessageConvert.setMessageUNSigned(byteorder, startBit, bitLength,
                            Convert.ToUInt64(signalDataValueString), data, Min, Max, offset, factor);
                    }
                }
                else
                {
                    //decimal
                    dataOut = LibraryMessageConvert.setMessageDecimal(byteorder, startBit, bitLength,
                        Convert.ToDecimal(signalDataValueString), data, Min, Max, offset, factor);
                }

                // quick and dirty
                if (!sendMessages.ContainsKey(id))
                    sendMessages.Add(id, new SenderDataType() { id = id, dlc = dlc, data = dataOut });
                else
                    sendMessages[id] = new SenderDataType() { id = id, dlc = dlc, data = dataOut };
            }

            if (sendMessages.Count <= 0)
                return null;
            else if (sendMessages.Count > 1)
                throw new Exception("Signal(s) not in same message!");

            var result = sendMessages.ToArray().FirstOrDefault().Value.data;
            return result;
        }

        public static Signal[] Decode(Message message, int channel = 0)
        {
            return Decode(message.Id, message.Data);
        }

        public static Signal[] Decode(uint id, byte[] data, int channel = 0)
        {
            var signals = new List<Signal>();
            var messageLookup = ARXML.FindMessageById(id, channel);

            if (messageLookup != null)
            {
                foreach (var signal in messageLookup.Signals)
                {
                    var name = signal.Name;
                    dynamic value = null; // type is unknown at this stage
                    try
                    {
                        value = SignalValue(signal, data);
                        if (value == null) // indicator of error with multi frame message (most likely)
                            continue;
                    }
                    catch (Exception ex)
                    {
                        // Important: Exception is throw if starBit and length is greater then the 8 bytes, which means is multi frame message
                        value = null; // bad data!!!!!!!!!!!!!  
                    }

                    if (value == null)
                    {
                        signals.Add(signal);
                        continue; // could not be computed
                    }

                    signal.Value = value;
                    signals.Add(signal);
                }
            }

            return signals.ToArray();
        }

        public static bool Import(string filepath, int channel = 0)
        {
            var hType = Utility.Helper.GetConfig("db", "application");

            try
            {
#if UNIX
                Utility.Helper.ProcessHidden("python3" + " " + Path.Combine(Utility.Helper.AssemblyDirectory,"Export_ARXML_to_JSON.py"), filepath);
                Utility.Helper.ProcessHidden("/bin/mv", "-f arxml.json" + " " + Path.Combine(Utility.Helper.AssemblyDirectory,"arxml"+ (channel+1) +".json"));
#else
                var bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                bw.WorkerSupportsCancellation = true;
                bw.DoWork += (sender, args) =>
                {
                    Process proc = new Process();
                    proc.StartInfo.FileName = Path.Combine(Utility.Helper.AssemblyDirectory, "Export_ARXML_to_JSON.exe");
                    proc.StartInfo.Arguments = " " + filepath;
                    proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    proc.Start();

                    bool hasUI = proc.MainWindowHandle != IntPtr.Zero;
                    if (!proc.HasExited && hasUI)
                        proc.WaitForInputIdle();
                    proc.WaitForExit();
                    if (proc.HasExited == false)
                        if (proc.Responding)
                            proc.CloseMainWindow();
                        else
                            proc.Kill();

                };

                bw.RunWorkerAsync();

                bw.RunWorkerCompleted += (sender, args) =>
                {
                    if (args.Error != null) { }
                    proc_finished(null, new EventArgs());
                    var orginalFile = Path.Combine(Utility.Helper.AssemblyDirectory, "arxml.json");
                    var newFile = Path.Combine(Utility.Helper.AssemblyDirectory, "arxml" + hType.ToLower() + (channel + 1) + ".json");
                    if (File.Exists(newFile))
                        File.Delete(newFile);
                    File.Move(orginalFile, newFile);

                    Load(channel + 1, true);

                };

#endif
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
            return true;
        }

    }

    public class Message
    {
        public string Name { get; set; }
        public uint Id { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public List<Signal> Signals { get; set; }
        public byte[] Data { get; set; }
    }

    public class SignalGroup
    {
        public string Name { get; set; }
        public List<string> Signals { get; set; }
    }

    public class Signal
    {
        public string Name { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public Dictionary<string, string> Enums { get; set; }
        public dynamic Value;

    }
}
