﻿using DeviceLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessageDecodeManager;
using Utility;

using Peak.Can.Basic;
using TPCANHandle = System.UInt16;
using TPCANBitrateFD = System.String;
using TPCANTimestampFD = System.UInt64;

namespace DeviceLibrary
{
    public class PCanFD : IDevice
    {
        #region Hardware connection

        #region Members
        /// <summary>
        /// Saves the desired connection mode
        /// </summary>
        private bool m_IsFD;
        /// <summary>
        /// Handles of the current available PCAN-Hardware
        /// </summary>
        private TPCANHandle[] m_HandlesArray;

        /// <summary>
        /// Saves the handle of a PCAN hardware
        /// </summary>
        private static TPCANHandle m_PcanHandle;

        private readonly ISO_15765_2 _iso157652;

        private TPCANStatus pcan;

        public event ReceivedManyHandler OnReceivedMany;
        public event ReceivedHandler OnReceived;

        public event ReceivedHandler OnSent;

        public bool IsConnected
        {
            get;
            set;
        }

        #endregion


        public PCanFD()
        {
            _iso157652 = new ISO_15765_2(this);

            new Thread(() =>
            {
                while (true)
                {
                    var messages = Read();
                    if (messages.Count > 0)
                    {
                        if (OnReceived != null)
                        {
                            foreach (var message in messages)
                                OnReceived(message);
                        }
                    }
                    Thread.Sleep(1); // NEED to wait else semaphores lock waiting forever reading all the time!!!
                }
            }).Start();
        }

        /// <summary>
        /// Initialization of PCAN-Basic components
        /// </summary>
        private void InitializeBasicComponents()
        {
            // Creates an array with all possible PCAN-Channels
            //
            m_HandlesArray = new TPCANHandle[]
            {
                PCANBasic.PCAN_ISABUS1,
                PCANBasic.PCAN_ISABUS2,
                PCANBasic.PCAN_ISABUS3,
                PCANBasic.PCAN_ISABUS4,
                PCANBasic.PCAN_ISABUS5,
                PCANBasic.PCAN_ISABUS6,
                PCANBasic.PCAN_ISABUS7,
                PCANBasic.PCAN_ISABUS8,
                PCANBasic.PCAN_DNGBUS1,
                PCANBasic.PCAN_PCIBUS1,
                PCANBasic.PCAN_PCIBUS2,
                PCANBasic.PCAN_PCIBUS3,
                PCANBasic.PCAN_PCIBUS4,
                PCANBasic.PCAN_PCIBUS5,
                PCANBasic.PCAN_PCIBUS6,
                PCANBasic.PCAN_PCIBUS7,
                PCANBasic.PCAN_PCIBUS8,
                PCANBasic.PCAN_PCIBUS9,
                PCANBasic.PCAN_PCIBUS10,
                PCANBasic.PCAN_PCIBUS11,
                PCANBasic.PCAN_PCIBUS12,
                PCANBasic.PCAN_PCIBUS13,
                PCANBasic.PCAN_PCIBUS14,
                PCANBasic.PCAN_PCIBUS15,
                PCANBasic.PCAN_PCIBUS16,
                PCANBasic.PCAN_USBBUS1,
                PCANBasic.PCAN_USBBUS2,
                PCANBasic.PCAN_USBBUS3,
                PCANBasic.PCAN_USBBUS4,
                PCANBasic.PCAN_USBBUS5,
                PCANBasic.PCAN_USBBUS6,
                PCANBasic.PCAN_USBBUS7,
                PCANBasic.PCAN_USBBUS8,
                PCANBasic.PCAN_USBBUS9,
                PCANBasic.PCAN_USBBUS10,
                PCANBasic.PCAN_USBBUS11,
                PCANBasic.PCAN_USBBUS12,
                PCANBasic.PCAN_USBBUS13,
                PCANBasic.PCAN_USBBUS14,
                PCANBasic.PCAN_USBBUS15,
                PCANBasic.PCAN_USBBUS16,
                PCANBasic.PCAN_PCCBUS1,
                PCANBasic.PCAN_PCCBUS2,
                PCANBasic.PCAN_LANBUS1,
                PCANBasic.PCAN_LANBUS2,
                PCANBasic.PCAN_LANBUS3,
                PCANBasic.PCAN_LANBUS4,
                PCANBasic.PCAN_LANBUS5,
                PCANBasic.PCAN_LANBUS6,
                PCANBasic.PCAN_LANBUS7,
                PCANBasic.PCAN_LANBUS8,
                PCANBasic.PCAN_LANBUS9,
                PCANBasic.PCAN_LANBUS10,
                PCANBasic.PCAN_LANBUS11,
                PCANBasic.PCAN_LANBUS12,
                PCANBasic.PCAN_LANBUS13,
                PCANBasic.PCAN_LANBUS14,
                PCANBasic.PCAN_LANBUS15,
                PCANBasic.PCAN_LANBUS16,
            };

        }

        /// <summary>
        /// Gets the formated text for a PCAN-Basic channel handle
        /// </summary>
        /// <param name="handle">PCAN-Basic Handle to format</param>
        /// <param name="isFD">If the channel is FD capable</param>
        /// <returns>The formatted text for a channel</returns>
        private string FormatChannelName(TPCANHandle handle, bool isFD)
        {
            TPCANDevice devDevice;
            byte byChannel;

            // Gets the owner device and channel for a 
            // PCAN-Basic handle
            //
            if (handle < 0x100)
            {
                devDevice = (TPCANDevice)(handle >> 4);
                byChannel = (byte)(handle & 0xF);
            }
            else
            {
                devDevice = (TPCANDevice)(handle >> 8);
                byChannel = (byte)(handle & 0xFF);
            }

            // Constructs the PCAN-Basic Channel name and return it
            //
            if (isFD)
                return string.Format("{0}:FD {1} ({2:X2}h)", devDevice, byChannel, handle);
            else
                return string.Format("{0} {1} ({2:X2}h)", devDevice, byChannel, handle);
        }

        /// <summary>
        /// Gets the formated text for a PCAN-Basic channel handle
        /// </summary>
        /// <param name="handle">PCAN-Basic Handle to format</param>
        /// <returns>The formatted text for a channel</returns>
        private string FormatChannelName(TPCANHandle handle)
        {
            return FormatChannelName(handle, false);
        }

        /// <summary>
        /// Help Function used to get an error as text
        /// </summary>
        /// <param name="error">Error code to be translated</param>
        /// <returns>A text with the translated error</returns>
        private string GetFormatedError(TPCANStatus error)
        {
            StringBuilder strTemp;

            // Creates a buffer big enough for a error-text
            //
            strTemp = new StringBuilder(256);
            // Gets the text using the GetErrorText API function
            // If the function success, the translated error is returned. If it fails,
            // a text describing the current error is returned.
            //
            if (PCANBasic.GetErrorText(error, 0, strTemp) != TPCANStatus.PCAN_ERROR_OK)
                return string.Format("An error occurred. Error-code's text ({0:X}) couldn't be retrieved", error);
            else
                return strTemp.ToString();
        }

        public Status Connect(string port = "", bool extra = false)
        {
            if (extra)
                throw new NotImplementedException("No extra channel support");
            if (!string.IsNullOrEmpty(port))
                throw new NotImplementedException("No port assignment option");

            //Disconnect();
            //Thread.Sleep(1500);

            InitializeBasicComponents();

            UInt32 iBuffer;
            TPCANStatus stsResult;
            bool isFD;
            string cName = "";
            try
            {
                for (int i = 0; i < m_HandlesArray.Length; i++)
                {
                    // Includes all no-Plug&Play Handles
                    if (m_HandlesArray[i] <= PCANBasic.PCAN_DNGBUS1)
                    {
                        cName = FormatChannelName(m_HandlesArray[i]);
                    }
                    else
                    {
                        // Checks for a Plug&Play Handle and, according with the return value, includes it
                        // into the list of available hardware channels.
                        //
                        stsResult = PCANBasic.GetValue(m_HandlesArray[i], TPCANParameter.PCAN_CHANNEL_CONDITION, out iBuffer, sizeof(UInt32));
                        if ((stsResult == TPCANStatus.PCAN_ERROR_OK) && ((iBuffer & PCANBasic.PCAN_CHANNEL_AVAILABLE) == PCANBasic.PCAN_CHANNEL_AVAILABLE))
                        {
                            stsResult = PCANBasic.GetValue(m_HandlesArray[i], TPCANParameter.PCAN_CHANNEL_FEATURES, out iBuffer, sizeof(UInt32));
                            isFD = (stsResult == TPCANStatus.PCAN_ERROR_OK) && ((iBuffer & PCANBasic.FEATURE_FD_CAPABLE) == PCANBasic.FEATURE_FD_CAPABLE);
                            cName = FormatChannelName(m_HandlesArray[i], isFD);
                        }
                    }

                }
            }
            catch (DllNotFoundException)
            {
            }

            // Get the handle from the text being shown
            //           
            String strTemp = cName;
            strTemp = strTemp.Substring(strTemp.IndexOf('(') + 1, 3);
            strTemp = strTemp.Replace('h', ' ').Trim(' ');

            // Connects a selected PCAN-Basic channel
            //
            stsResult = PCANConnect(Convert.ToUInt16(strTemp, 16),
                            TPCANBaudrate.PCAN_BAUD_500K, TPCANType.PCAN_TYPE_ISA, 256, 3);

            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
            {
                if (stsResult != TPCANStatus.PCAN_ERROR_CAUTION)
                    GetFormatedError(stsResult);
                else
                {
                    stsResult = TPCANStatus.PCAN_ERROR_OK;
                }

            }
            IsConnected = true;
            if (strTemp != "51")
            {
                return Status.NoSuchDevice;

            }
            else
            {
                return Status.Connected;
            }
        }

        #endregion        

        #region CAN CONNECTION / DISCONNECTION

        public TPCANStatus PCANConnect(TPCANHandle handler, TPCANBaudrate baudrate, TPCANType type, UInt32 io, UInt16 interrupt)
        {
            TPCANStatus stsResult;
            m_PcanHandle = handler;

            // Connects a selected PCAN-Basic channel
            //
            if (m_IsFD)
            {
                stsResult = PCANBasic.InitializeFD(
                    m_PcanHandle,
                    "f_clock_mhz=20, nom_brp=5, nom_tseg1=2, nom_tseg2=1, nom_sjw=1, data_brp=2, data_tseg1=3, data_tseg2=1, data_sjw=1");
            }
            else
            {
                stsResult = PCANBasic.Initialize(
                    m_PcanHandle,
                    baudrate,
                    type,
                    io,
                    interrupt);
            }
            return stsResult;
        }


        public Status Disconnect()
        {
            IsConnected = false;

            // Releases a current connected PCAN-Basic channel
            PCANBasic.Uninitialize(m_PcanHandle);
            return Status.NotConnected;
        }

        #endregion      

        #region SEND / READ

        public bool Send(Message message)
        {
            var messages = new Message[] { message };
            if (message.MultiFrame)
            {
                try
                {
                    //_iso157652.FlowControl = true;
                    messages = _iso157652.Encode(message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    return false; // only while debugging multi frame...
                }
            }

            try
            {
                foreach (var frame in messages)
                    SendMessage(Convert.ToInt32(frame.Id), frame.Extended, frame.HighVoltage, frame.Data, frame.Length, frame.Network == Mode.SW);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }

            if (OnSent != null)
                OnSent(message);

            return true;  // Todo: should actually check!
        }

        //Bridge
        private void SendMessage(int id, bool extended, bool highVoltage, byte[] data, int sizeData, bool swcan)
        {
            var message = new TPCANMsg
            {
                ID = (uint)id,
                MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD,
                LEN = Convert.ToByte(sizeData),
                DATA = data
            };

            var messageFD = new TPCANMsgFD
            {
                ID = (uint)id,
                MSGTYPE = TPCANMessageType.PCAN_MESSAGE_FD,
                DLC = Convert.ToByte(sizeData),
                DATA = data
            };

            if (swcan)
            {
                this.pcan = TPCANStatus.PCAN_ERROR_BUSWARNING;
            }
            else
            {
                if (sizeData > 8)
                {
                    m_IsFD = true;
                    PCANBasic.WriteFD(m_PcanHandle, ref messageFD);
                }
                else
                {
                    PCANBasic.Write(m_PcanHandle, ref message);
                }
            }

        }

        public List<Message> Read()
        {
            var outCANMessages = new List<Message>();
            if (IsConnected)
            {
                try
                {
                    // We execute the "Read" function of the PCANBasic                
                    //

                    // Todo:  just use ReadFD
                    if (m_IsFD)
                    {
                        TPCANMsgFD CANMsg;
                        TPCANTimestampFD CANTimeStamp;
                        TPCANStatus stsResult;

                        // We execute the "Read" function of the PCANBasic                
                        //
                        stsResult = PCANBasic.ReadFD(m_PcanHandle, out CANMsg, out CANTimeStamp);
                        if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                        {
                            if (true)
                            {
                                uint canId = CANMsg.ID;
                                var receivedCANMessage =
                                    new Message
                                    {
                                        TimeStamp =
                                                DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                                                    .TotalMilliseconds / 1000,
                                        Id = (uint)canId,
                                        Extended = canId > 0x7FF
                                    };

                                var tempData = new List<byte>();
                                for (var index = 0; index < CANMsg.DATA.Length; index++)
                                {
                                    var data = CANMsg.DATA[index];
                                    tempData.Add(data);
                                }

                                receivedCANMessage.Data = tempData.ToArray();
                                receivedCANMessage.Length = receivedCANMessage.Data.Length;

                                var message = receivedCANMessage;
                                var messageLookup = ARXML.FindMessageById(message.Id);
                                if (messageLookup != null)
                                {
                                    try
                                    {
                                        var name = messageLookup.Name;
                                        var multiFrame = name.StartsWith("USDT_") || name.StartsWith("UUDT_");
                                        if (multiFrame)
                                            message = _iso157652.Decode(message);
                                    }
                                    catch (Exception ex)
                                    {
                                        if (messageLookup == null)
                                            Debug.WriteLine("Not Found in Database!!!");
                                        else
                                            Debug.WriteLine(ex);
                                    }
                                }

                                if (message != null)
                                    outCANMessages.Add(message);
                            }
                        }

                    }
                    else
                    {
                        var CANMsg = new TPCANMsg();
                        TPCANStatus stsResult;

                        // We execute the "Read" function of the PCANBasic                
                        //
                        stsResult = PCANBasic.Read(m_PcanHandle, out CANMsg);
                        if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                        {
                            if (CANMsg.DATA.Length >= 0)
                            {
                                uint canId = CANMsg.ID;
                                var receivedCANMessage =
                                    new Message
                                    {
                                        TimeStamp =
                                                DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                                                    .TotalMilliseconds / 1000,
                                        Id = (uint)canId,
                                        Extended = canId > 0x7FF
                                    };

                                var tempData = new List<byte>();
                                for (var index = 0; index < CANMsg.DATA.Length; index++)
                                {
                                    var data = CANMsg.DATA[index];
                                    tempData.Add(data);
                                }

                                receivedCANMessage.Data = tempData.ToArray();
                                receivedCANMessage.Length = receivedCANMessage.Data.Length;

                                var message = receivedCANMessage;
                                var messageLookup = ARXML.FindMessageById(message.Id);
                                if (messageLookup != null)
                                {
                                    try
                                    {
                                        var name = messageLookup.Name;
                                        var multiFrame = name.StartsWith("USDT_") || name.StartsWith("UUDT_");
                                        if (multiFrame)
                                            message = _iso157652.Decode(message);
                                    }
                                    catch (Exception ex)
                                    {
                                        if (messageLookup == null)
                                            Debug.WriteLine("Not Found in Database!!!");
                                        else
                                            Debug.WriteLine(ex);
                                    }
                                }

                                if (message != null)
                                    outCANMessages.Add(message);
                            }
                        }
                    };
                }
                catch (Exception)
                {
                    return outCANMessages;
                }


            }

            return outCANMessages;
        }

        #endregion       

    }
}
