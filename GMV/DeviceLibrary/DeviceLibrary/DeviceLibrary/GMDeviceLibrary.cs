﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Numerics;
using System.Threading;
using System.Windows;
using CSnet;
using Epiphan.FrmGrab;
//using Peak.Can.Basic;
//using TPCANHandle = System.Byte;
using System.IO.Ports;
using System.Runtime.ExceptionServices;
using System.Security;
using Utility;

namespace DeviceLibrary
{
    #region DBC File Parsing

    public static class LibraryMessageConvert
    {
        /// <summary>
        /// Function takes in the all of the signal information and it returns an 8 byte array of the CANMessage
        /// </summary>
        /// <param name="ByteOrder"></param>
        /// <param name="StartingBit"></param>
        /// <param name="BitLength"></param>
        /// <param name="SignalDataValue"></param>
        /// <param name="PreviousDataArray"></param>
        /// <param name="MinVal"></param>
        /// <param name="MaxVal"></param>
        /// <param name="OffsetVal"></param>
        /// <param name="FactorVal"></param>
        /// <returns></returns>
        public static byte[] setMessageSigned(string ByteOrder, uint StartingBit, uint BitLength, Int64 SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
        {
            byte[] CANDataOut;
            Int64 PreviousFullSignal;
            int SignalShiftLength;
            Int64 SignalClear;
            Int64 SignalDataConverted;
            Int64 ShiftedValue;

            if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
            {
                throw new OverflowException("Value out of range");
            }

            PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);

            SignalDataValue = Convert.ToInt64(((SignalDataValue - OffsetVal) / FactorVal));
            BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

            SignalDataConverted = Convert.ToInt64(SignalValueConverted.ToString());

            if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
            {

                Array.Reverse(PreviousDataArray);
                PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);
                SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                Array.Reverse(PreviousDataArray);
            }
            else
            {
                SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
            }

            if (BitLength < 64)
            {
                BigInteger test = new BigInteger();
                test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                string holder = test.ToString();
                SignalClear = Convert.ToInt64(holder);
                SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
            }
            else
            {
                SignalClear = 0;
            }

            PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

            ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

            ShiftedValue = ShiftedValue & ~SignalClear; //Remove leading 1's for a negative number

            PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

            CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

            if (ByteOrder == "0")
            {
                Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent
            }

            return CANDataOut;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByteOrder"></param>
        /// <param name="StartingBit"></param>
        /// <param name="BitLength"></param>
        /// <param name="SignalDataValue"></param>
        /// <param name="PreviousDataArray"></param>
        /// <param name="MinVal"></param>
        /// <param name="MaxVal"></param>
        /// <param name="OffsetVal"></param>
        /// <param name="FactorVal"></param>
        /// <returns></returns>
        public static byte[] setMessageUNSigned(string ByteOrder, uint StartingBit, uint BitLength, UInt64 SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
        {
            byte[] CANDataOut;
            UInt64 PreviousFullSignal;
            int SignalShiftLength;
            UInt64 SignalClear;
            UInt64 SignalDataConverted;
            UInt64 ShiftedValue;

            if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
            {
                //throw new OverflowException("Value out of range");
            }

            PreviousFullSignal = BitConverter.ToUInt64(PreviousDataArray, 0);

            SignalDataValue = Convert.ToUInt64((SignalDataValue - OffsetVal) / FactorVal);
            BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

            SignalDataConverted = Convert.ToUInt64(SignalValueConverted.ToString());


            if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
            {

                Array.Reverse(PreviousDataArray);
                PreviousFullSignal = BitConverter.ToUInt64(PreviousDataArray, 0);
                SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                Array.Reverse(PreviousDataArray);
            }
            else
            {
                SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
            }

            if (BitLength < 64)
            {
                BigInteger test = new BigInteger();
                test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                string holder = test.ToString();
                SignalClear = Convert.ToUInt64(holder);
                SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
            }
            else
            {
                SignalClear = 0;
            }

            PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

            ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

            PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

            CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

            if (ByteOrder == "0")
            {
                Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent
            }

            return CANDataOut;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByteOrder"></param>
        /// <param name="StartingBit"></param>
        /// <param name="BitLength"></param>
        /// <param name="SignalDataValue"></param>
        /// <param name="PreviousDataArray"></param>
        /// <param name="MinVal"></param>
        /// <param name="MaxVal"></param>
        /// <param name="OffsetVal"></param>
        /// <param name="FactorVal"></param>
        /// <returns></returns>
        public static byte[] setMessageDecimal(string ByteOrder, uint StartingBit, uint BitLength, decimal SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
        {
            byte[] CANDataOut;
            Int64 PreviousFullSignal;
            int SignalShiftLength;
            Int64 SignalClear;
            Int64 SignalDataConverted;
            Int64 ShiftedValue;

            if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
                throw new OverflowException("Value out of range");

            PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);

            SignalDataValue = Convert.ToInt64((SignalDataValue - OffsetVal) / FactorVal);
            BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

            SignalDataConverted = Convert.ToInt64(SignalValueConverted.ToString());

            if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
            {

                Array.Reverse(PreviousDataArray);
                PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);
                SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                Array.Reverse(PreviousDataArray);
            }
            else
            {
                SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
            }

            if (BitLength < 64)
            {
                BigInteger test = new BigInteger();
                test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                string holder = test.ToString();
                SignalClear = Convert.ToInt64(holder);
                SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
            }
            else
            {
                SignalClear = 0;
            }

            PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

            ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

            ShiftedValue = ShiftedValue & ~SignalClear; //Remove leading 1's for a negative number

            PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

            CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

            if (ByteOrder == "0")
                Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent

            return CANDataOut;
        }

        /// <summary>
        /// Function takes in the starting bit of the signal and its length and returns the position inside the CANMessage
        /// </summary>
        /// <param name="startbit"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private static int getOffset(uint startbit, uint length)
        {
            int offset;  //offset will be used to shift value
            int byte1;
            int bit;
            int start;
            int len;

            start = Convert.ToInt16(startbit);
            len = Convert.ToInt16(length);

            bit = start % 8;
            byte1 = start / 8;

            offset = 64 - ((byte1 * 8) + (7 - bit) + len);

            return offset;
        }
    }

    #endregion

    #region Devices
    public class Devices
    {
        #region Intrepid

        // ONLY LEFT HERE TO SUPPORT GLOBAL A!!!!!!!!!!!!!!

        public enum LinType : int
        {
            iLIN = 0,
            tLIN = 1,
        }

        public NeoVI_ NeoVI = new NeoVI_();
        public class NeoVI_
        {
            private readonly Intrepid _intrepid = new Intrepid();

            public bool NeoVIPortOpen;	 //tells the port status of the device

            private int neoDeviceToUse = 0;
            private bool deviceNotAvailable = false;
            /*********************************************************************/

            public void ConnectToNeoVI()
            {
                // Check if the port is already open
                if (NeoVIPortOpen == true)
                {
                    DisconnectNeoVI();
                    Thread.Sleep(3 * 1000);
                }

                _intrepid.Product = Intrepid.Type.NeoVI;
                var status = (Status)_intrepid.Connect();
                if (!_intrepid.IsConnected)
                {
                    _intrepid.Product = Intrepid.Type.ValueCAN;
                    status = (Status)_intrepid.Connect();
                }


                if (status == Status.Connected)
                    NeoVIPortOpen = true;
            }

            public void DisconnectNeoVI()
            {
                _intrepid.Disconnect();
                NeoVIPortOpen = false;
            }

            public List<CANMessage> ReadCANMessage()
            {
                var result = new List<CANMessage>();

                var messages = _intrepid.ReadCANMessage();
                foreach (var message in messages)
                {
                    var bridge = new CANMessage() { CanId = (int)message.Id, Data = message.Data };
                    bridge.Extended = message.Extended;
                    bridge.DLC = message.Length;
                    bridge.Network = message.Network == Mode.SW ? "SWCAN" : "HSCAN";
                    bridge.TimeStamp = message.TimeStamp;
                    bridge.TXRXStatus = "RX";
                    result.Add(bridge);
                }

                return result;
            }

            public void SendNeoCANMessage(int ID, bool extended, bool highVoltage, byte[] data, int sizeData, bool SWCAN)
            {
                _intrepid.Send(new Message((uint)ID, extended, highVoltage, data, sizeData, SWCAN ? Mode.SW : Mode.HS));
            }

            public bool CheckNeoErrors()
            {
                return !_intrepid.IsConnected;
            }

            public void SendNeoLINMessage(int ID, byte[] data, int lintype)
            {
                var bridge = new Message
                {
                    Id = (uint)ID,
                    Data = data,
                    Network = lintype == 0 ? Mode.LIN_i : Mode.LIN_t
                };
                _intrepid.Send(bridge);
            }
        }

        #endregion
  
        #region Device

        public Devices()
        {
            TryInit(InitNeoVI);
        }

        private void TryInit(Action func)
        {
            try
            {
                func.Invoke();
            }
            catch (Exception exception)
            {
                System.Console.WriteLine("Error init " + exception.Message);
            }
        }

        private void InitNeoVI()
        {
            NeoVI = new NeoVI_();
        }
        
        #endregion

        #region Epiphan
        public Epiphan_ Epiphan = new Epiphan_();
        public class Epiphan_
        {
            FrameGrabber _grabber;

            public unsafe string OpenGrabber()
            {
                string output = "";

                if (_grabber == null)
                {
                    FrameGrabber grabber = null;

                    try
                    {
                        grabber = new FrameGrabber();
                        V2U_Property p;
                        p.key = V2UPropertyKey.V2UKey_Version;
                        grabber.GetProperty(&p);
                        output = grabber.ToString();

                        //TextOutputAdd("Found " + grabber + "\r\nDriver version " +
                        //p.value.version.major + "." + p.value.version.minor + "." +
                        //p.value.version.micro + "." + p.value.version.nano);
                    }
                    catch (Exception x)
                    {
                        output = x.Message.ToString();
                    }

                    SetGrabber(grabber);
                }

                return output;
            }

            public bool SetGrabber(FrameGrabber grabber)
            {
                _grabber = grabber;
                if (grabber != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public Bitmap GrabFrame()
            {
                if (OpenGrabber() != null)
                {
                    VideoMode vm = _grabber.DetectVideoMode();
                    if (vm != null)
                    {
                        if (vm.IsValid())
                        {
                            Frame frame = _grabber.GrabFrame();
                            if (frame != null)
                            {
                                Bitmap bitmap = null;
                                bitmap = frame.GetBitmap();
                                //frame.Release();
                                if (bitmap != null)
                                {
                                    if ((bitmap.Size.Height > 480) || (bitmap.Size.Width > 800))
                                    {
                                        return null;
                                    }
                                    return bitmap;
                                }
                                else
                                {
                                    throw new Exception("Capture format error");
                                    return null;
                                    //TSStatus.Text = ("Capture format error");
                                    //SetGrabber(null);
                                }
                            }
                            else
                            {
                                throw new Exception("Capture failed");
                                return null;
                                //TSStatus.Text = ("Capture failed");
                                //SetGrabber(null);
                            }
                        }
                        else
                        {
                            return null;
                            //TSStatus.Text = ("No signal detected");
                            //PictureBox.Visible = false;
                        }
                    }
                    else
                    {
                        throw new Exception("Failed to detect video mode");
                        return null;
                        //TSStatus.Text = ("Failed to detect video mode");
                        //SetGrabber(null);
                    }
                }
                else
                {
                    throw new Exception("No frame grabber");
                    return null;
                }
            }
        }
        #endregion

    }

    #endregion

    #region CANMessage Receive
    public class CANMessage
    {
        /// <summary>
        /// CAN ID of the can CANSignal
        /// </summary>
        /// 
        public double TimeStamp { get; set; }

        public string TXRXStatus { get; set; }

        public string Network { get; set; }

        public string Device { get; set; }

        public bool BusError { get; set; }

        public bool Extended { get; set; }

        public int CanId { get; set; }

        public int DLC { get; set; }

        public byte[] Data
        {
            get { return new[] { Data1, Data2, Data3, Data4, Data5, Data6, Data7, Data8 }; }
            set
            {
                var data = value;
                SetData(data);
            }
        }

        public byte Data1 { get; set; }
        public byte Data2 { get; set; }
        public byte Data3 { get; set; }
        public byte Data4 { get; set; }
        public byte Data5 { get; set; }
        public byte Data6 { get; set; }
        public byte Data7 { get; set; }
        public byte Data8 { get; set; }

        public void SetData(byte[] data)
        {
            if (data.Length >= 1)
                Data1 = data[0];
            else
                Data1 = 0;
            if (data.Length >= 2)
                Data2 = data[1];
            else
                Data2 = 0;
            if (data.Length >= 3)
                Data3 = data[2];
            else
                Data3 = 0;
            if (data.Length >= 4)
                Data4 = data[3];
            else
                Data4 = 0;
            if (data.Length >= 5)
                Data5 = data[4];
            else
                Data5 = 0;
            if (data.Length >= 6)
                Data6 = data[5];
            else
                Data6 = 0;
            if (data.Length >= 7)
                Data7 = data[6];
            else
                Data7 = 0;
            if (data.Length >= 8)
                Data8 = data[7];
            else
                Data8 = 0;
        }

        public String Print()
        {
            StringBuilder hex = new StringBuilder(DLC * 2);
            if (DLC >= 1) hex.AppendFormat("{0:x2}", Data1);
            if (DLC >= 2) hex.AppendFormat(" {0:x2}", Data2);
            if (DLC >= 3) hex.AppendFormat(" {0:x2}", Data3);
            if (DLC >= 4) hex.AppendFormat(" {0:x2}", Data4);
            if (DLC >= 5) hex.AppendFormat(" {0:x2}", Data5);
            if (DLC >= 6) hex.AppendFormat(" {0:x2}", Data6);
            if (DLC >= 7) hex.AppendFormat(" {0:x2}", Data7);
            if (DLC >= 8) hex.AppendFormat(" {0:x2}", Data8);

            return hex.ToString();
        }

        public override string ToString()
        {
            var message = string.Format("{0} {1} {2} - {3} {4} -> {5}", this.TimeStamp, this.TXRXStatus, this.Network, this.CanId.ToString("X"), this.DLC.ToString("X"), this.Print());
            return message;
        }
    }
    #endregion

}
