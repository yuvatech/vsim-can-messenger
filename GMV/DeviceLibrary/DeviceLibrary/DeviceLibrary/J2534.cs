﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Diagnostics;
using Utility;

namespace DeviceLibrary
{
    using System.ComponentModel;
    
    class J2534 : IDisposable
    {
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruOpen(string pName, ref int deviceId);
        PassThruOpen m_Open;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruClose(int deviceId);
        PassThruClose m_Close;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruConnect(int deviceId, int protocolId, int flags, int baudRate, ref int channelId);
        PassThruConnect m_Connect;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruDisconnect(int channelId);
        PassThruDisconnect m_Disconnect;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruIoctl(int channelId, int ioctlID, IntPtr input, IntPtr output);
        PassThruIoctl m_IOctl;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruStartFlowControlMsgFilter
        (
            int channelid,
            int filterType,
            ref UnsafePassThruMsg maskMsg,
            ref UnsafePassThruMsg patternMsg,
            ref UnsafePassThruMsg flowControlMsg,
            ref int filterId
        );
        PassThruStartFlowControlMsgFilter m_StartFlowControlMsgFilter;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruStartPassMsgFilter
        (
            int channelid,
            int filterType,
            ref UnsafePassThruMsg maskMsg,
            ref UnsafePassThruMsg patternMsg,
            IntPtr flowControlMsg,
            ref int filterId
        );
        PassThruStartPassMsgFilter m_StartPassMsgFilter;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruGetLastError(IntPtr errorDescription);
        PassThruGetLastError m_GetLastError;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruWriteMsgs(int channelId, ref UnsafePassThruMsg msg, ref int numMsgs, int timeout);
        PassThruWriteMsgs m_WriteMsgs;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruWriteBulkMsgs(int channelId, IntPtr msgs, ref int numMsgs, int timeout);
        PassThruWriteBulkMsgs m_WriteBulkMsgs;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruReadMsgs(int channelId, IntPtr pMsgs, ref int numMsgs, int timeout);
        PassThruReadMsgs m_ReadMsgs;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruReadVersion(int deviceId, IntPtr firmwareVersion, IntPtr dllVersion, IntPtr apiVersion);
        PassThruReadVersion m_ReadVersion;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruStartPeriodicMsg(int channelId, ref UnsafePassThruMsg msg, ref int periodicMsgID, int timeInterval);
        PassThruStartPeriodicMsg m_StartPeriodicMsg;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int PassThruStopPeriodicMsg(int channelId, int periodicMsgID);
        PassThruStopPeriodicMsg m_StopPeriodicMsg;

        IntPtr pDll = IntPtr.Zero;

        System.Threading.Semaphore j2534Semaphore;

        private bool disposed = false; // to detect redundant calls
        public J2534(string dllAddress)
        {
            pDll = NativeMethods.LoadLibrary(dllAddress);

            if (this.pDll != IntPtr.Zero)
            {
                IntPtr pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruOpen");
                m_Open = (PassThruOpen)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruOpen));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruClose");
                m_Close = (PassThruClose)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruClose));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruConnect");
                m_Connect = (PassThruConnect)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruConnect));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruDisconnect");
                m_Disconnect = (PassThruDisconnect)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruDisconnect));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruIoctl");
                m_IOctl = (PassThruIoctl)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruIoctl));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruStartMsgFilter");
                m_StartFlowControlMsgFilter = (PassThruStartFlowControlMsgFilter)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruStartFlowControlMsgFilter));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruStartMsgFilter");
                m_StartPassMsgFilter = (PassThruStartPassMsgFilter)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruStartPassMsgFilter));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruGetLastError");
                m_GetLastError = (PassThruGetLastError)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruGetLastError));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruWriteMsgs");
                m_WriteMsgs = (PassThruWriteMsgs)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruWriteMsgs));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruWriteMsgs");
                m_WriteBulkMsgs = (PassThruWriteBulkMsgs)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruWriteBulkMsgs));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruReadMsgs");
                m_ReadMsgs = (PassThruReadMsgs)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruReadMsgs));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruReadVersion");
                m_ReadVersion = (PassThruReadVersion)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruReadVersion));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruStartPeriodicMsg");
                m_StartPeriodicMsg = (PassThruStartPeriodicMsg)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruStartPeriodicMsg));

                pAddressOfFunctionToCall = NativeMethods.GetProcAddress(pDll, "PassThruStopPeriodicMsg");
                m_StopPeriodicMsg = (PassThruStopPeriodicMsg)Marshal.GetDelegateForFunctionPointer(
                    pAddressOfFunctionToCall,
                    typeof(PassThruStopPeriodicMsg));

                j2534Semaphore = new System.Threading.Semaphore(1, 1);
            }
            else
            {
                throw new InvalidOperationException("Dll not found");
            }
        }

        public string GetLastError()
        {
            IntPtr errorMessage = Marshal.AllocHGlobal(250);

            int j2534error = m_GetLastError(errorMessage);

            string msg = string.Empty;
            if (j2534error == (int)J2534Err.STATUS_NOERROR)
            {
                msg = Marshal.PtrToStringAnsi(errorMessage);
            }

            return msg;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                    if (pDll != null)
                    {
                        NativeMethods.FreeLibrary(pDll);
                        pDll = IntPtr.Zero;
                        m_Open = null;
                        m_Close = null;
                        m_Connect = null;
                        m_Disconnect = null;
                        m_IOctl = null;
                        m_StartFlowControlMsgFilter = null;
                        m_StartPassMsgFilter = null;
                        m_GetLastError = null;
                        m_WriteMsgs = null;
                        m_WriteBulkMsgs = null;
                        m_ReadMsgs = null;
                        m_ReadVersion = null;
                        m_StartPeriodicMsg = null;
                        m_StopPeriodicMsg = null;
                        j2534Semaphore = null;
                    }
                }
                disposed = true;
            }
        }
        public static PassThruMsg ConvertToSafePassThruMsg(UnsafePassThruMsg uMsg)
        {
            PassThruMsg msg = new PassThruMsg();

            msg.ProtocolID = uMsg.ProtocolID;
            msg.RxStatus = uMsg.RxStatus;
            msg.Timestamp = uMsg.Timestamp;
            msg.TxFlags = uMsg.TxFlags;
            msg.Datasize = uMsg.DataSize;
            msg.ExtraDataIndex = uMsg.ExtraDataIndex;
            msg.Data = new byte[uMsg.DataSize];

            unsafe
            {
                for (int i = 0; i < uMsg.DataSize; i++)
                {
                    msg.Data[i] = uMsg.Data[i];
                }
            }

            return msg;
        }

        private UnsafePassThruMsg ConvertToUnSafePassThruMsg(PassThruMsg msg)
        {
            UnsafePassThruMsg uMsg = new UnsafePassThruMsg();

            uMsg.ProtocolID = (int)msg.ProtocolID;
            uMsg.RxStatus = (int)msg.RxStatus;
            uMsg.Timestamp = msg.Timestamp;
            uMsg.TxFlags = (int)msg.TxFlags;
            uMsg.ExtraDataIndex = msg.ExtraDataIndex;
            uMsg.DataSize = msg.Datasize;

            unsafe
            {
                for (int i = 0; i < msg.Data.Length; i++)
                {
                    uMsg.Data[i] = msg.Data[i];
                }
            }
            return uMsg;
        }

        public static string GetJ2534Hardware(string name)
        {
            string PASSTHRU_REGISTRY_PATH = "Software\\PassThruSupport.04.04";
            string PASSTHRU_REGISTRY_PATH_6432 = "Software\\Wow6432Node\\PassThruSupport.04.04";

            RegistryKey myKey = Registry.LocalMachine.OpenSubKey(PASSTHRU_REGISTRY_PATH, false);
            if (myKey == null)
            {
                myKey = Registry.LocalMachine.OpenSubKey(PASSTHRU_REGISTRY_PATH_6432, false);
                if (myKey == null)
                    return null;
            }

            string[] devices = myKey.GetSubKeyNames();

            foreach (string device in devices)
            {
                RegistryKey deviceKey = myKey.OpenSubKey(device);

                if (deviceKey == null)
                    continue;

                if (((string)deviceKey.GetValue("Name", "")).Equals(name))
                {
                    return (string)deviceKey.GetValue("FunctionLibrary", "");
                }
            }

            return null;
        }

        public int ConvertPassThruDataToCANID(PassThruMsg msg)
        {
            int CANID = 0;
            CANID = msg.Data[0] << 24;
            CANID = CANID | (msg.Data[1] << 16);
            CANID = CANID | (msg.Data[2] << 8);
            CANID = CANID | msg.Data[3];
            return CANID;
        }

        public byte[] ConvertCANIDToPassThruData(uint CANID)
        {
            byte[] PassThruCANIDData = new byte[4];

            PassThruCANIDData[0] = (byte)((CANID & 0xFF000000) >> 24);
            PassThruCANIDData[1] = (byte)((CANID & 0x00FF0000) >> 16);
            PassThruCANIDData[2] = (byte)((CANID & 0x0000FF00) >> 8);
            PassThruCANIDData[3] = (byte)((CANID & 0x000000FF));

            return PassThruCANIDData;
        }

        public byte[] ConvertPassThruDataToCANData(PassThruMsg msg)
        {
            int IDOffset = 4;
            byte[] CANData = new byte[msg.Datasize - IDOffset];

            for (int i = 0; i < CANData.Length; i++)
            {
                CANData[i] = msg.Data[i + IDOffset];
            }

            return CANData;
        }
        //added function to convert PassThruDataToCANData in 8bytes array.
        public byte[] ConvertPassThruDataToCANDataFullBytes(PassThruMsg msg)
        {
            int IDOffset = 4;
            int dataLenght = msg.Datasize - IDOffset;
            byte[] CANData = new byte[8];

            for (int i = 0; i < dataLenght; i++)
            {
                CANData[i] = msg.Data[i + IDOffset];
            }

            return CANData;
        }
        public J2534Err Open(string mpName, ref int deviceID)
        {
            j2534Semaphore.WaitOne();

            int j2534error = m_Open(mpName, ref deviceID);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err Close(int deviceId)
        {
            j2534Semaphore.WaitOne();

            int j2534error = m_Close(deviceId);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err Connect(int deviceId, ProtocolID protocolId, ConnectFlag flags, BaudRate baudRate, ref int channelId)
        {
            j2534Semaphore.WaitOne();

            int j2534error = m_Connect(deviceId, (int)protocolId, (int)flags, (int)baudRate, ref channelId);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err Disconnect(int channelId)
        {
            j2534Semaphore.WaitOne();

            int j2534error = m_Disconnect(channelId);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err WriteMsgs(int channelId, PassThruMsg msg, int numMsgs, int timeout)
        {
            j2534Semaphore.WaitOne();

            UnsafePassThruMsg uMsg = ConvertToUnSafePassThruMsg(msg);
            int j2534error = m_WriteMsgs(channelId, ref uMsg, ref numMsgs, timeout);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err WriteUnsafeMsgs(int channelId, UnsafePassThruMsg uMsg, int numMsgs, int timeout)
        {
            j2534Semaphore.WaitOne();

            int j2534error = m_WriteMsgs(channelId, ref uMsg, ref numMsgs, timeout);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err WriteBulkMsgs(int channelId, List<PassThruMsg> msgs, ref int numMsgs, int timeout)
        {
            j2534Semaphore.WaitOne();

            //Debug.WriteLine("ABOUT TO WRITE " + numMsgs.ToString() + " MESSAGES " + DateTime.Now.Ticks.ToString());

            int j2534error = 0; //status no error

            if (numMsgs > 0)
            {
                /************************************************************************************/
                int sizepntr = Marshal.SizeOf(typeof(UnsafePassThruMsg));
                IntPtr arrayuMsgs = Marshal.AllocHGlobal(sizepntr * numMsgs);

                UnsafePassThruMsg[] uMsgs = new UnsafePassThruMsg[numMsgs];

                for (int i = 0; i < numMsgs; i++)
                {
                    uMsgs[i] = (ConvertToUnSafePassThruMsg(msgs[i]));
                }

                IntPtr ptr = arrayuMsgs;

                for (int i = 0; i < numMsgs; i++)
                {
                    Marshal.StructureToPtr(uMsgs[i], ptr, false);
                    ptr += sizepntr;
                }

                j2534error = m_WriteBulkMsgs(channelId, arrayuMsgs, ref numMsgs, timeout);

                Marshal.FreeHGlobal(arrayuMsgs);
            }

            //Debug.WriteLine("WROTE " + numMsgs.ToString() + " MESSAGES " + DateTime.Now.Ticks.ToString());

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err StartCyclicMessage(int channelId, PassThruMsg msg, ref int periodicMsgID, int timeInterval)
        {
            j2534Semaphore.WaitOne();
            int j2534error = (int)J2534Err.STATUS_NOERROR; //status no error

            UnsafePassThruMsg uMsg = new UnsafePassThruMsg();
            UnsafePassThruMsg uMsg1 = new UnsafePassThruMsg();
            uMsg = ConvertToUnSafePassThruMsg(msg);
            uMsg1 = uMsg;

            j2534error = m_StartPeriodicMsg(channelId, ref uMsg1, ref periodicMsgID, timeInterval);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err StopCyclicMessage(int channelId, int periodicMsgID)
        {
            j2534Semaphore.WaitOne();
            int j2534error = (int)J2534Err.STATUS_NOERROR; //status no error

            j2534error = m_StopPeriodicMsg(channelId, periodicMsgID);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err Set_Config(int channelId, IoctlParameter Parameter, int Value)
        {
            j2534Semaphore.WaitOne();

            SConfig_List SetConfig_List;
            SConfig l_SConfig;

            unsafe
            {
                l_SConfig.Parameter = (int)Parameter;
                l_SConfig.Value = Value;
                SetConfig_List.numofparams = 1;  //set to one as array of sconfigs
                SetConfig_List.SConfig = &l_SConfig;

                IntPtr input = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(SConfig_List)));

                Marshal.StructureToPtr(SetConfig_List, input, true);

                int j2534error = m_IOctl(channelId, (int)Ioctl.SET_CONFIG, input, IntPtr.Zero);

                j2534Semaphore.Release();

                return (J2534Err)j2534error;
            }
        }

        public J2534Err StartFlowControlMsgFilter(int channelId, ProtocolID protocolID, uint mask, uint pattern, uint flowControl, ref int filterId)
        {
            j2534Semaphore.WaitOne();

            UnsafePassThruMsg unsafemaskMsg = new UnsafePassThruMsg();
            UnsafePassThruMsg unsafepatternMsg = new UnsafePassThruMsg();
            UnsafePassThruMsg unsafeflowcontrolMsg = new UnsafePassThruMsg();

            byte[] maskIDBytes = ConvertCANIDToPassThruData(mask);
            byte[] patternIDBytes = ConvertCANIDToPassThruData(pattern);
            byte[] flowControlIDBytes = ConvertCANIDToPassThruData(flowControl);

            unsafemaskMsg.ProtocolID = (int)protocolID;
            unsafemaskMsg.TxFlags = (int)TxFlag.ISO15765_FRAME_PAD;
            unsafemaskMsg.DataSize = 4;

            unsafepatternMsg.ProtocolID = (int)protocolID;
            unsafepatternMsg.TxFlags = (int)TxFlag.ISO15765_FRAME_PAD;
            unsafepatternMsg.DataSize = 4;

            unsafeflowcontrolMsg.ProtocolID = (int)protocolID;
            unsafeflowcontrolMsg.TxFlags = (int)TxFlag.ISO15765_FRAME_PAD;
            unsafeflowcontrolMsg.DataSize = 4;

            for (int i = 0; i < maskIDBytes.Length; i++)
            {
                unsafe
                {
                    unsafemaskMsg.Data[i] = maskIDBytes[i];
                    unsafepatternMsg.Data[i] = patternIDBytes[i];
                    unsafeflowcontrolMsg.Data[i] = flowControlIDBytes[i];
                }
            }

            int j2534error = m_StartFlowControlMsgFilter(channelId, (int)FilterType.FLOW_CONTROL_FILTER, ref unsafemaskMsg, ref unsafepatternMsg, ref unsafeflowcontrolMsg, ref filterId);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err StartPassMsgFilter(int channelId, ProtocolID protocolID, uint mask, uint pattern, ref int filterId)
        {
            j2534Semaphore.WaitOne();

            UnsafePassThruMsg unsafemaskMsg = new UnsafePassThruMsg();
            UnsafePassThruMsg unsafepatternMsg = new UnsafePassThruMsg();

            byte[] maskIDBytes = ConvertCANIDToPassThruData(mask);
            byte[] patternIDBytes = ConvertCANIDToPassThruData(pattern);

            unsafemaskMsg.ProtocolID = (int)protocolID;
            unsafemaskMsg.TxFlags = (int)TxFlag.ISO15765_FRAME_PAD;
            unsafemaskMsg.DataSize = 4;

            unsafepatternMsg.ProtocolID = (int)protocolID;
            unsafepatternMsg.TxFlags = (int)TxFlag.ISO15765_FRAME_PAD;
            unsafepatternMsg.DataSize = 4;

            for (int i = 0; i < maskIDBytes.Length; i++)
            {
                unsafe
                {
                    unsafemaskMsg.Data[i] = maskIDBytes[i];
                    unsafepatternMsg.Data[i] = patternIDBytes[i];
                }
            }

            int j2534error = m_StartPassMsgFilter(channelId, (int)FilterType.PASS_FILTER, ref unsafemaskMsg, ref unsafepatternMsg, IntPtr.Zero, ref filterId);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err StartExtendedPassMsgFilter(int channelId, ProtocolID protocolID, uint mask, uint pattern, ref int filterId)
        {
            j2534Semaphore.WaitOne();

            UnsafePassThruMsg unsafemaskMsg = new UnsafePassThruMsg();
            UnsafePassThruMsg unsafepatternMsg = new UnsafePassThruMsg();

            byte[] maskIDBytes = ConvertCANIDToPassThruData(mask);
            byte[] patternIDBytes = ConvertCANIDToPassThruData(pattern);

            unsafemaskMsg.ProtocolID = (int)protocolID;
            unsafemaskMsg.TxFlags = (int)TxFlag.CAN_29BIT_ID;
            unsafemaskMsg.DataSize = 4;

            unsafepatternMsg.ProtocolID = (int)protocolID;
            unsafepatternMsg.TxFlags = (int)TxFlag.CAN_29BIT_ID;
            unsafepatternMsg.DataSize = 4;

            for (int i = 0; i < maskIDBytes.Length; i++)
            {
                unsafe
                {
                    unsafemaskMsg.Data[i] = maskIDBytes[i];
                    unsafepatternMsg.Data[i] = patternIDBytes[i];
                }
            }

            int j2534error = m_StartPassMsgFilter(channelId, (int)FilterType.PASS_FILTER, ref unsafemaskMsg, ref unsafepatternMsg, IntPtr.Zero, ref filterId);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }

        public J2534Err ReadMsgs(int channelId, ref List<PassThruMsg> msgs, ref int numMsgs, int timeout)
        {
            j2534Semaphore.WaitOne();

            //Debug.WriteLine("ABOUT TO READ " + numMsgs.ToString() + " MESSAGES " + DateTime.Now.Ticks.ToString());
            /************************************************************************************/
            int sizepntr = Marshal.SizeOf(typeof(UnsafePassThruMsg)) * numMsgs;
            IntPtr arraymsgs = Marshal.AllocHGlobal(sizepntr);

            int j2534error = m_ReadMsgs(channelId, arraymsgs, ref numMsgs, timeout);

            int CurOffset = 0;

            for (int i = 0; i < numMsgs; i++)
            {
                UnsafePassThruMsg uMsg = (UnsafePassThruMsg)Marshal.PtrToStructure(new IntPtr(arraymsgs.ToInt32() + CurOffset), typeof(UnsafePassThruMsg));
                CurOffset += Marshal.SizeOf(typeof(UnsafePassThruMsg));
                msgs.Add(ConvertToSafePassThruMsg(uMsg));
            }

            Marshal.FreeHGlobal(arraymsgs);
            //Debug.WriteLine("READ " + numMsgs.ToString() + " MESSAGES " + DateTime.Now.Ticks.ToString());


            j2534Semaphore.Release();

            return (J2534Err)j2534error;


        }

        public J2534Err ReadVersion(int deviceId, ref string firmwareVersion, ref string dllVersion, ref string apiVersion)
        {
            j2534Semaphore.WaitOne();

            IntPtr pFirmwareVersion = Marshal.AllocHGlobal(120);
            IntPtr pDllVersion = Marshal.AllocHGlobal(120);
            IntPtr pApiVersion = Marshal.AllocHGlobal(120);

            int j2534error = m_ReadVersion(deviceId, pFirmwareVersion, pDllVersion, pApiVersion);

            if (j2534error == (int)J2534Err.STATUS_NOERROR)
            {
                firmwareVersion = Marshal.PtrToStringAnsi(pFirmwareVersion);
                dllVersion = Marshal.PtrToStringAnsi(pDllVersion);
                apiVersion = Marshal.PtrToStringAnsi(pApiVersion);
            }

            Marshal.FreeHGlobal(pFirmwareVersion);
            Marshal.FreeHGlobal(pDllVersion);
            Marshal.FreeHGlobal(pApiVersion);

            j2534Semaphore.Release();

            return (J2534Err)j2534error;
        }
    }

    internal unsafe struct UnsafePassThruMsg
    {
        public int ProtocolID;
        public int RxStatus;
        public int TxFlags;
        public int Timestamp;
        public int DataSize;
        public int ExtraDataIndex;
        public fixed byte Data[4128];
    }

    public class PassThruMsg
    {
        public PassThruMsg() { }
        public PassThruMsg(int myProtocolId, int myTxFlag, byte[] myByteArray, int myDataSize)
        {
            ProtocolID = myProtocolId;
            TxFlags = myTxFlag;
            Data = myByteArray;
            Datasize = myDataSize;
        }
        public int ProtocolID { get; set; }
        public int RxStatus { get; set; }
        public int TxFlags { get; set; }
        public int Timestamp { get; set; }
        public int Datasize { get; set; }
        public int ExtraDataIndex { get; set; }
        public int TimerFlag { get; set; }
        public int CycleTime { get; set; }
        public bool CyclicOn { get; set; }
        public long CurrentTick { get; set; }  //will be used if the message is cyclic
        public byte[] Data { get; set; }
    }

    public struct SConfig
    {
        public int Parameter;
        public int Value;
    }

    public unsafe struct SConfig_List
    {
        public int numofparams;
        public unsafe SConfig* SConfig;
    }

    public enum J2534Err
    {
        [Description("No Error")]
        STATUS_NOERROR = 0x00,
        [Description("Not supported")]
        ERR_NOT_SUPPORTED = 0x01,
        [Description("Invalid Channel Id")]
        ERR_INVALID_CHANNEL_ID = 0x02,
        [Description("Invalid Protocol Id")]
        ERR_INVALID_PROTOCOL_ID = 0x03,
        [Description("Null parameter")]
        ERR_NULL_PARAMETER = 0x04,
        [Description("Invalid Flags")]
        ERR_INVALID_FLAGS = 0x06,
        [Description("Failed")]
        ERR_FAILED = 0x07,
        [Description("Device not Connected")]
        ERR_DEVICE_NOT_CONNECTED = 0x08,
        [Description("Time out")]
        ERR_TIMEOUT = 0x09,
        [Description("Invalid message")]
        ERR_INVALID_MSG = 0x0A,
        [Description("Invalid Time Interval")]
        ERR_INVALID_TIME_INTERVAL = 0x0B,
        [Description("Exceeded Limit")]
        ERR_EXCEEDED_LIMIT = 0x0C,
        [Description("Invalid Msg Id")]
        ERR_INVALID_MSG_ID = 0x0D,
        [Description("Device is already in use")]
        ERR_DEVICE_IN_USE = 0x0E,
        [Description("Invalid IOControl Id")]
        ERR_INVALID_IOCTL_ID = 0x0F,
        [Description("Buffer Empty")]
        ERR_BUFFER_EMPTY = 0x10,
        [Description("Buffer Full")]
        ERR_BUFFER_FULL = 0x11,
        [Description("Buffer Overflow")]
        ERR_BUFFER_OVERFLOW = 0x12,
        [Description("Pin Invalid")]
        ERR_PIN_INVALID = 0x13,
        [Description("PCAN Channel is already in use. Close all applications which uses PCAN device")]
        ERR_CHANNEL_IN_USE = 0x14,
        [Description("Invalid Message Protocol Id")]
        ERR_MSG_PROTOCOL_ID = 0x15,
        [Description("Invalid Filter Id")]
        ERR_INVALID_FILTER_ID = 0x16,
        [Description("No Flow Control")]
        ERR_NO_FLOW_CONTROL = 0x17,
        [Description("Not unique")]
        ERR_NOT_UNIQUE = 0x18,
        [Description("Invalid Baud Rate")]
        ERR_INVALID_BAUDRATE = 0x19,
        [Description("Invalid Device ID")]
        ERR_INVALID_DEVICE_ID = 0x1A,
        [Description("PCAN passthru dll not found. Verify installation")]
        ERR_DLL_NOT_FOUND = 0x1B,
        [Description("Connection Failure. Close all applications which uses PCAN device")]
        ERR_PCAN_CONNECTION_FAILURE = 0x1C
    }

    public enum BaudRate
    {
        CAN_33333 = 33333,
        CAN_125000 = 125000,
        CAN_250000 = 250000,
        CAN_500000 = 500000,
    }

    public enum ProtocolID
    {
        J1850VPW = 0x01,
        J1850PWM = 0x02,
        ISO9141 = 0x03,
        ISO14230 = 0x04,
        CAN = 0x05,
        ISO15765 = 0x06,
        SCI_A_ENGINE = 0x07,
        SCI_A_TRANS = 0x08,
        SCI_B_ENGINE = 0x09,
        SCI_B_TRANS = 0x0A,

        SW_ISO15765_PS = 0x8007,
        SW_CAN_PS = 0x8008,  //0x10003 is intrepid sepcific sw protocol id and 0x8008 is J2534-2 sw specific protocol id

        INVALID = 0x8000,
    }

    public enum ConnectFlag
    {
        NONE = 0x0000,
        ISO9141_K_LINE_ONLY = 0x1000,
        CAN_ID_BOTH = 0x0800,
        ISO9141_NO_CHECKSUM = 0x0200,
        CAN_29BIT_ID = 0x0100
    }

    public enum Ioctl
    {
        GET_CONFIG = 0x01,
        SET_CONFIG = 0x02,
        READ_VBATT = 0x03,
        FIVE_BAUD_INIT = 0x04,
        FAST_INIT = 0x05,
        CLEAR_TX_BUFFER = 0x07,
        CLEAR_RX_BUFFER = 0x08,
        CLEAR_PERIODIC_MSGS = 0x09,
        CLEAR_MSG_FILTERS = 0x0A,
        CLEAR_FUNCT_MSG_LOOKUP_TABLE = 0x0B,
        ADD_TO_FUNCT_MSG_LOOKUP_TABLE = 0x0C,
        DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE = 0x0D,
        READ_PROG_VOLTAGE = 0x0E,

        SW_CAN_HS = 0x8000,
        SW_CAN_NS = 0x8001,
        SET_POLL_RESPONSE = 0x00008002,
        BECOME_MASTER = 0x00008003,
        START_REPEAT_MESSAGE = 0x00008004,
        QUERY_REPEAT_MESSAGE = 0x00008005,
        STOP_REPEAT_MESSAGE = 0x00008006,
        GET_DEVICE_CONFIG = 0x00008007,
        SET_DEVICE_CONFIG = 0x00008008,
        PROTECT_J1939_ADDR = 0x00008009,
        REQUEST_CONNECTION = 0x0000800A,
        TEARDOWN_CONNECTION = 0x0000800B,
        GET_DEVICE_INFO = 0x0000800C,
        GET_PROTOCOL_INFO = 0x0000800D

    }

    public enum IoctlParameter
    {
        DATA_RATE = 0x01,
        LOOPBACK = 0x03,
        NODE_ADDRESS = 0x04,
        NETWORK_LINE = 0x05,
        P1_MIN = 0x06,
        P1_MAX = 0x07,
        P2_MIN = 0x08,
        P2_MAX = 0x09,
        P3_MIN = 0x0A,
        P3_MAX = 0x0B,
        P4_MIN = 0x0C,
        P4_MAX = 0x0D,
        W0 = 0x19,
        W1 = 0x0E,
        W2 = 0x0F,
        W3 = 0x10,
        W4 = 0x11,
        W5 = 0x12,
        TIDLE = 0x13,
        TINIL = 0x14,
        TWUP = 0x15,
        PARITY = 0x16,
        BIT_SAMPLE_POINT = 0x17,
        SYNC_JUMP_WIDTH = 0x18,
        T1_MAX = 0x1A,
        T2_MAX = 0x1B,
        T3_MAX = 0x24,
        T4_MAX = 0x1C,
        T5_MAX = 0x1D,
        ISO15765_BS = 0x1E,
        ISO15765_STMIN = 0x1F,
        BS_TX = 0x22,
        STMIN_TX = 0x23,
        DATA_BITS = 0x20,
        FIVE_BAUD_MOD = 0x21,
        ISO15765_WFT_MAX = 0x25,

        CAN_MIXED_FORMAT = 0x8000,
        J1962_PINS = 0x8001,
        SW_CAN_HS_DATA_RATE = 0x8010,
        SW_CAN_SPEEDCHANGE_ENABLE = 0x8011,
        SW_CAN_RES_SWITCH = 0x8012,
        ACTIVE_CHANNELS = 0x8020,
        SAMPLE_RATE = 0x8021,
        SAMPLES_PER_READING = 0x8022,
        READINGS_PER_MSG = 0x8023,
        AVERAGING_METHOD = 0x8024,
        SAMPLE_RESOLUTION = 0x8025,
        INPUT_RANGE_LOW = 0x8026,
        INPUT_RANGE_HIGH = 0x8027,
        UEB_T0_MIN = 0x8028,
        UEB_T1_MAX = 0x8029,
        UEB_T2_MAX = 0x802A,
        UEB_T3_MAX = 0x802B,
        UEB_T4_MIN = 0x802C,
        UEB_T5_MAX = 0x802D,
        UEB_T6_MAX = 0x802E,
        UEB_T7_MIN = 0x802F,
        UEB_T7_MAX = 0x8030,
        UEB_T9_MIN = 0x8031,
        J1939_PINS = 0x803D,
        J1708_PINS = 0x803E,
        J1939_T1 = 0x803F,
        J1939_T2 = 0x8040,
        J1939_T3 = 0x8041,
        J1939_T4 = 0x8042,
        J1939_BRDCST_MIN_DELAY = 0x8043,
        TP2_0_T_BR_INT = 0x8044,
        TP2_0_T_E = 0x8045,
        TP2_0_MNTC = 0x8046,
        TP2_0_T_CTA = 0x8047,
        TP2_0_MNCT = 0x8048,
        TP2_0_MNTB = 0x8049,
        TP2_0_MNT = 0x804A,
        TP2_0_T_Wait = 0x804B,
        TP2_0_T1 = 0x804C,
        TP2_0_T3 = 0x804D,
        TP2_0_IDENTIFER = 0x804E,
        TP2_0_RXIDPASSIVE = 0x804F,
    }

    public enum FilterType
    {
        PASS_FILTER = 0x01,
        BLOCK_FILTER = 0x02,
        FLOW_CONTROL_FILTER = 0x03
    }

    public enum TxFlag
    {
        NONE = 0x00000000,
        SCI_TX_VOLTAGE = 0x00800000,
        SCI_MODE = 0x00400000,
        WAIT_P3_MIN_ONLY = 0x00000200,
        CAN_29BIT_ID = 0x00000100,
        ISO15765_ADDR_TYPE = 0x00000080,
        ISO15765_FRAME_PAD = 0x00000040,
        SW_CAN_HV_TX = 0x00000400
    }

    public enum RxStatus
    {
        NONE = 0x00000000,
        TX_MSG_TYPE = 0x00000001,
        START_OF_MESSAGE = 0x00000002,
        RX_BREAK = 0x00000004,
        TX_INDICATION = 0x00000008,
        ISO15765_PADDING_ERROR = 0x00000010,
        ISO15765_ADDR_TYPE = 0x00000080,
        CAN_29BIT_ID = 0x00000100
    }
}
