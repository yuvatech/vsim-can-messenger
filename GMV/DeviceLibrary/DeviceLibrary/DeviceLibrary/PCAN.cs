﻿using DeviceLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessageDecodeManager;
using Utility;

namespace DeviceLibrary
{
    public class PCAN : IDevice
    {
        private readonly ISO_15765_2 _iso157652;
        private readonly SecureMessage _secureMessage;

        private J2534 passthru;
        
        private J2534Err j2534Err;

        private int j2534DeviceID;

        private string dllVersion = string.Empty;

        private string apiVersion = string.Empty;

        private string firmwareVersion = string.Empty;

        private int isoFilterID;

        private int isoChannelID = 0;

        private int j2534MaxRcvdMsgs = 10;

        public event ReceivedManyHandler OnReceivedMany;
        public event ReceivedHandler OnReceived;

        public event ReceivedHandler OnSent;

        public PCAN()
        {
            _iso157652 = new ISO_15765_2(this);
            _secureMessage = new SecureMessage();

            new Thread(() =>
            {
                while (true)
                {
                    var messages = Read();
                    if (messages.Count > 0)
                    {
                        if (OnReceived != null)
                        {
                            foreach (var message in messages)
                                OnReceived(message);
                        }
                    }
                    Thread.Sleep(1); // NEED to wait else semaphores lock waiting forever reading all the time!!!
                }
            }).Start();
        }

        public Status Connect(string port = "", bool extra = false)
        {
            if(extra)
                throw new NotImplementedException("No extra channel support");
            if (!string.IsNullOrEmpty(port))
                throw new NotImplementedException("No port assignment option");

            //if (IsConnected)
            Disconnect();

            this.isoChannelID = 0; //reset
            while (true)
            {
                J2534Err errorCode = ConnectPCAN();
                switch (errorCode)
                {
                    case J2534Err.STATUS_NOERROR:
                        IsConnected = true;
                        return Status.Connected;
                    case J2534Err.ERR_DEVICE_NOT_CONNECTED:
                        return Status.NotConnected;
                    case J2534Err.ERR_DEVICE_IN_USE:
                        return Status.InUse;
                    case J2534Err.ERR_CHANNEL_IN_USE:
                        /*
                        if (this.isoChannelID < 16)
                        {
                            this.isoChannelID++;
                            continue;
                        }
                        */
                        return Status.InUse;
                    case J2534Err.ERR_DLL_NOT_FOUND:
                        return Status.DriverNotFound;
                    case J2534Err.ERR_PCAN_CONNECTION_FAILURE:
                        return Status.NotAbleToConnect;
                    default:
                        return Status.UnknownError;
                }
            }
        }

        public Status Disconnect()
        {
            IsConnected = false;

            J2534Err errorCode = DisconnectPCAN();
            switch (errorCode)
            {
                case J2534Err.STATUS_NOERROR:
                    return Status.Connected;
                case J2534Err.ERR_DEVICE_NOT_CONNECTED:
                    return Status.NotConnected;
                case J2534Err.ERR_DEVICE_IN_USE:
                    return Status.InUse;
                case J2534Err.ERR_CHANNEL_IN_USE:
                    return Status.InUse;
                case J2534Err.ERR_DLL_NOT_FOUND:
                    return Status.DriverNotFound;
                case J2534Err.ERR_PCAN_CONNECTION_FAILURE:
                    return Status.NotAbleToConnect;
                default:
                    return Status.UnknownError;
            }
        }

        public bool Send(Message message)
        {
            var messages = new System.Collections.Generic.List<Message>();

            // MACT handling
            var isSecure = false;
            messages = _secureMessage.Process(message, ref isSecure).ToList(); // comes back with encryption if found in MACT file, else original returned

            if (!isSecure)
            {
                messages.Clear();

                if (message.MultiFrame)
                {
                    try
                    {
                        //_iso157652.FlowControl = true;
                        messages.AddRange(_iso157652.Encode(message));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                        return false; // only while debugging multi frame...
                    }
                }
                else
                {
                    messages.Add(message);
                }
            }

            try
            {
                foreach (var frame in messages)
                {
                    SendMessage(Convert.ToInt32(frame.Id), frame.Extended, frame.HighVoltage, frame.Data, frame.Length,
                        frame.Network == Mode.SW);

                    if (OnSent != null)
                        OnSent(frame);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }

            return true;  // Todo: should actually check!
        }

        //Bridge
        private int SendMessage(int id, bool extended, bool highVoltage, byte[] data, int sizeData, bool swcan)
        {
            if (swcan)
            {
                this.j2534Err = J2534Err.ERR_NOT_SUPPORTED;
            }
            else
            {
                Array.Resize(ref data, sizeData);
                this.j2534Err = this.passthru.WriteMsgs(
                    this.isoChannelID,
                    this.DBCMessageToPassThruMsg(extended, highVoltage, id, data, 0, false),
                    1,
                    50);
            }

            return (int)this.j2534Err;
        }

        public List<Message> Read()
        {
            var j2534Msgs = new List<DeviceLibrary.PassThruMsg>();
            var j2534CombinedMsgs = new List<DeviceLibrary.PassThruMsg>();
            var outCANMessages = new List<Message>();
            if (IsConnected)
            {
                j2534CombinedMsgs.Clear();
                try
                {
                    j2534Msgs.Clear();
                    this.j2534MaxRcvdMsgs = 10;
                    this.j2534Err = this.passthru.ReadMsgs(
                        this.isoChannelID,
                        ref j2534Msgs,
                        ref this.j2534MaxRcvdMsgs,
                        50);
                    j2534CombinedMsgs.AddRange(j2534Msgs);
                }
                catch (Exception)
                {
                    return outCANMessages;
                }

                foreach (DeviceLibrary.PassThruMsg passThruMsg in j2534CombinedMsgs)
                {
                    if (passThruMsg.Data.Length >= 4)
                    {
                        int canId = this.passthru.ConvertPassThruDataToCANID(passThruMsg);
                        var receivedCANMessage =
                            new Message
                            {
                                TimeStamp =
                                        DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))
                                            .TotalMilliseconds / 1000,
                                //IsNeoVI = false,
                                /*
                                TXRXStatus =
                                        (passThruMsg.RxStatus & (int)RxStatus.TX_MSG_TYPE)
                                        == (int)RxStatus.TX_MSG_TYPE
                                            ? "TX"
                                            : "RX",
                                */
                                //Device = "PCAN",
                                Id = (uint)canId,
                                //Length = passThruMsg.Data.Length - 4,
                                Extended = canId > 0x7FF
                            };

                        /*
                        if (passThruMsg.ProtocolID == (int)ProtocolID.SW_CAN_PS)
                        {
                            receivedCANMessage.Network = "SWCAN";
                        }
                        else if (passThruMsg.ProtocolID == (int)ProtocolID.CAN)
                        {
                            receivedCANMessage.Network = "HSCAN";
                        }
                         */

                        var tempData = new List<byte>();
                        for (var index = 4; index < passThruMsg.Data.Length; index++)
                        {
                            var data = passThruMsg.Data[index];
                            tempData.Add(data);
                        }

                        receivedCANMessage.Data = tempData.ToArray();
                        receivedCANMessage.Length = receivedCANMessage.Data.Length;

                        var message = receivedCANMessage;

                        { // handle sync message received.
                            // Note:  we will handle if it is our MACT file
                            var isSecure = false;
                            _secureMessage.Process(message, ref isSecure);
                        }

                        /*
                        var messageLookup = ARXML.FindMessageById(message.Id);
                        if (messageLookup != null)
                        {
                            try
                            {
                                var name = messageLookup.Name;
                                var multiFrame = name.StartsWith("USDT_") || name.StartsWith("UUDT_");
                                if (multiFrame)
                                    message = _iso157652.Decode(message);
                            }
                            catch (Exception ex)
                            {
                                if (messageLookup == null)
                                    Debug.WriteLine("Not Found in Database!!!");
                                else
                                    Debug.WriteLine(ex);
                            }
                        }
                        */

                        if(message != null)
                          outCANMessages.Add(message);
                    }
                }
            }

            return outCANMessages;
        }

        public bool IsConnected
        {
            get;
            set;
        }

        private void DeleteJ2534Instance()
        {
            this.passthru.Dispose();
            this.passthru = null;
        }

        J2534Err ConnectPCAN()
        {
            if (IsConnected)
            {
                return J2534Err.ERR_DEVICE_IN_USE;
            }

            if (this.passthru != null)
            {
                // delete J2534 instance and delegates of J2534 API's
                this.DeleteJ2534Instance();
            }

            string strText = J2534.GetJ2534Hardware("PCANPT32");

            if (string.IsNullOrEmpty(strText))
            {
                return J2534Err.ERR_DLL_NOT_FOUND;
            }
            // creating J2534 istance along with delegates for the J2534DLL API's

            try
            {
                this.passthru = new J2534(strText);
            }
            catch (InvalidOperationException)
            {
                return J2534Err.ERR_DLL_NOT_FOUND;
            }

            // j2534err = passthru.Open("J2534-2:", ref j2534deviceID);
            this.j2534Err = this.passthru.Open(null, ref this.j2534DeviceID);

            if (this.j2534Err != J2534Err.STATUS_NOERROR)
            {
                return this.j2534Err;
            }
            else
            {
                // Get dll and api versions
                this.j2534Err = this.passthru.ReadVersion(this.j2534DeviceID, ref this.firmwareVersion, ref this.dllVersion, ref this.apiVersion);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                {
                    this.passthru.Close(this.j2534DeviceID);
                    return this.j2534Err;
                }

                // CAN bitrate and such...
                this.j2534Err = this.passthru.Connect(this.j2534DeviceID, ProtocolID.CAN, ConnectFlag.CAN_ID_BOTH, BaudRate.CAN_500000, ref this.isoChannelID);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                {
                    this.passthru.Close(this.j2534DeviceID);
                    if ((int)this.j2534Err == 7)
                    {
                        return J2534Err.ERR_PCAN_CONNECTION_FAILURE;
                    }

                    return this.j2534Err;
                }

                /*
                this.j2534Err = this.passthru.Set_Config(this.isoChannelID, IoctlParameter.LOOPBACK, 0x01);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                {
                    this.passthru.Close(this.j2534DeviceID);
                    return this.j2534Err;
                }
                */

                // Start CAN Pass filter
                // Note: NO filters, let ALL thru!
                this.j2534Err = this.passthru.StartPassMsgFilter(this.isoChannelID, ProtocolID.CAN, 0, 0, ref this.isoFilterID);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                {
                    this.passthru.Close(this.j2534DeviceID);
                    return this.j2534Err;
                }
                // Start CAN Pass filter
                this.j2534Err = this.passthru.StartExtendedPassMsgFilter(this.isoChannelID, ProtocolID.CAN, 0, 0, ref this.isoFilterID);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                {
                    this.passthru.Close(this.j2534DeviceID);
                    return this.j2534Err;
                }

                return J2534Err.STATUS_NOERROR;
            }
        }
        J2534Err DisconnectPCAN()
        {
            if (this.passthru != null)//this.isoChannelID != 0)
            {
                this.j2534Err = this.passthru.Disconnect(this.isoChannelID);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                    return this.j2534Err;

                //this.isoChannelID = 0;
            }

            // close the CAN hardware device
            if (this.j2534DeviceID != 0)
            {
                this.j2534Err = this.passthru.Close(this.j2534DeviceID);
                if (this.j2534Err != J2534Err.STATUS_NOERROR)
                    return this.j2534Err;

                this.j2534DeviceID = 0;
            }          
            return (int)J2534Err.STATUS_NOERROR;
        }
        private DeviceLibrary.PassThruMsg DBCMessageToPassThruMsg(bool extended, bool highVoltage, int canId, byte[] data, double cycleTime, bool cyclicOn)
        {
            DeviceLibrary.PassThruMsg passThruMessage = new DeviceLibrary.PassThruMsg();

            byte[] canIDBytes = this.passthru.ConvertCANIDToPassThruData(Convert.ToUInt32(canId));
            byte[] passThruCanData = new byte[data.Length + canIDBytes.Length];

            passThruMessage.ProtocolID = (int)ProtocolID.CAN;

            if (extended)
            {
                passThruMessage.TxFlags = (int)TxFlag.CAN_29BIT_ID | passThruMessage.TxFlags;
            }

            if (highVoltage)
            {
                passThruMessage.TxFlags = passThruMessage.TxFlags | (int)TxFlag.SW_CAN_HV_TX;
            }

            passThruMessage.Datasize = data.Length + canIDBytes.Length;
            passThruMessage.CycleTime = (int)cycleTime;
            passThruMessage.CyclicOn = cyclicOn;

            for (int i = 0; i < canIDBytes.Length; i++)
            {
                passThruCanData[i] = canIDBytes[i];
            }

            for (int l = 0; l < data.Length; l++)
            {
                passThruCanData[l + canIDBytes.Length] = data[l];
            }

            passThruMessage.Data = passThruCanData;

            return passThruMessage;
        }

    }
}
