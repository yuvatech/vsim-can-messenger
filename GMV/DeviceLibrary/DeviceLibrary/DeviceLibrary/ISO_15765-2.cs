﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Utility;

namespace DeviceLibrary
{
    // https://en.wikipedia.org/wiki/ISO_15765-2
    public class ISO_15765_2
    {
        // Note:  extended messages maybe 1 byte less in data.  6 instead of 7 type a deal, not seeing it but spec says so...
        private const ushort FRAME_DATA_LENGTH = 7;
        private const int FLOW_CONTROL_TIMEOUT = 3000; // ~3 seconds

        private readonly IDevice _device;

        private class MultiFrameMessage
        {
            public ushort Length;
            public ushort Sequence;
            public byte[] Data;
        }

        private class MultiFrameFlowControl
        {
            public byte Type;
            public ushort BlockSize;
            public ushort SeperationTime;
        }

        // internal storage        
        private readonly Dictionary<uint, List<MultiFrameMessage>> _frameMessageBuffer = new Dictionary<uint, List<MultiFrameMessage>>();
        private readonly object _lockFlowControl = new object();
        private readonly Dictionary<uint, MultiFrameFlowControl> _frameMessageFlowControlBuffer = new Dictionary<uint, MultiFrameFlowControl>();

        public ISO_15765_2(IDevice device = null)
        {
            _device = device;
        }

        // null means not full message yet, else returns full message
        public Message Decode(Message message, bool flowControl = false)  // Todo:? flow control on by default since we are the receiver we should answer or are we just the listener?
        {
            // if no device to send messages then cannot do it anyway
            if (_device == null)
                flowControl = false;

            // CAN-TP Header
            var code = (byte)((message.Data[0] & 0xF0) >> 4); // Single Frame (SF) 0, First Frame(FF) 1, Consecutive Frame (CF) 2, Flow Control (FC) 3
            var length = (ushort)(message.Data[0] & 0x0F);  // assume SF to start
            var sequence = (ushort)0x00; // index 0 on FF

            var data = new List<byte>(); // each frames data

            if (code == 0x00) // SF
            {
                message.Length = length;
                data.AddRange(message.Data.ToList().GetRange(1, message.Data.Length-1)); // 8 - 1 = 7
                message.Data = data.ToArray();

                message.MultiFrame = true;
                return message;
            }

            if (code == 0x01) // FF
            {
                // clear
                if (_frameMessageBuffer.ContainsKey(message.Id))
                    _frameMessageBuffer.Remove(message.Id);

                length = (ushort)(((message.Data[0] & 0x0F) << 8) | (message.Data[1]));
                data.AddRange(message.Data.ToList().GetRange(2, message.Data.Length-2)); // 8 data - 2 (header) = 6

                var multiFrameMessage = new MultiFrameMessage()
                {
                    Length = length,
                    Sequence = sequence,
                    Data = data.ToArray(),
                };

                var frames = new List<MultiFrameMessage> { multiFrameMessage }; // init and add
                _frameMessageBuffer.Add(message.Id, frames);

                if (flowControl)
                {
                    var responseFrame = new Message(message)
                    {
                        // [0] = code (fixed @ 3), 0 = Continue To Send
                        // [1] = Block Size - 0 = send all
                        // [2] = N/A if Block Size = 0
                        Data = new byte[] { 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, // first 3 bytes used, rest padding
                    };
                    responseFrame.Length = responseFrame.Data.Length;

                    responseFrame.Id = message.Trigger; // id to respond with!
                    responseFrame.MultiFrame = false; // stops coming back in here!  Todo: think of smarter/cleaner way!?
                    _device.Send(responseFrame);
                }

                return null; // not ready yet
            }

            if (code == 0x02)  // CF
            {
                sequence = (ushort)(message.Data[0] & 0x0F); // sequence counter starts from 1 per spec
                data.AddRange(message.Data.ToList().GetRange(1, message.Data.Length-1)); // 8 data - 1 (header) = 7

                if (!_frameMessageBuffer.ContainsKey(message.Id))
                    return null;

                var multiFrames = _frameMessageBuffer[message.Id];

                var multiFrameMessage = multiFrames.FirstOrDefault();
                if (multiFrameMessage == default(MultiFrameMessage))  // better have FF
                    return null;
                length = multiFrameMessage.Length; // total length is in First Frame (FF)

                multiFrameMessage = new MultiFrameMessage()
                {
                    Length = 0, // don't care
                    Sequence = sequence,
                    Data = data.ToArray(),
                };

                // Todo:  max sequence is 15, so care about if bigger
                // Todo:  Repeats are ignored right now, need to figure out flow control verse starting sequence over after each FC message                
                /*
                // determine if sequence is being repeated 
                if (multiFrames.Any(f => f.Sequence == sequence))
                    _frameMessageBuffer[message.Id].RemoveAll(f => f.Sequence == sequence); // should be only 1
                 */

                _frameMessageBuffer[message.Id].Add(multiFrameMessage);

                // Determine if this was the Last Frame
                data.Clear(); // re-use
                var lengths = 0;
                foreach (var frameMessage in multiFrames)
                {
                    lengths += frameMessage.Data.Length;
                    data.AddRange(frameMessage.Data);
                }

                if (length > lengths || length <= 0)
                    return null; // not all the data yet

                data.RemoveRange(length, (data.Count - length));  // join into single message with correct length (remove padding)
                return new Message(message) { Data = data.ToArray(), Length = data.Count, MultiFrame = true };
            }

            if (code == 0x03) // FC
            {
                // *** Note ***: information only, if FC message is received it will be handled in Encode as part of transmitting...

                //var code = (byte) ((message.Data[0] & 0xF0) >> 4);
                var type = (byte)(message.Data[0] & 0x0F); // same position as initial length of last 4 bits
                //0 = Continue To Send, 1 = Wait, 2 = Overflow/abort

                var blockSize = message.Data[1]; // Block Size, number that can be sent before another flow control is sent...
                // 0 = remaining "frames" to be sent without flow control or delay
                // > 0 send number of "frames" before waiting for the next flow control frame

                // Note: ignored, delay from this app to board to bus is longer then the max delay anyway!!!
                var seperationTime = (message.Data[2]);
                // block = 0, <= 127, separation time in milliseconds.
                // block > 0, 0xF1 to 0xF9 UF, 100 to 900 microseconds. 

                lock (_lockFlowControl)
                {
                    if (_frameMessageFlowControlBuffer.ContainsKey(message.Id))
                        _frameMessageFlowControlBuffer.Remove(message.Id);
                    var fc = new MultiFrameFlowControl() { Type = type, BlockSize = blockSize, SeperationTime = seperationTime }; // init and add
                    _frameMessageFlowControlBuffer.Add(message.Id, fc);
                }

                Console.WriteLine("(MF) Flow Control Decode -> Id: {2} Type: {0}, Block Size: {1}", type, blockSize, message.Id);
            }

            return null;
        }

        // only returns message(s) if did not handle internally with flow control
        public Message[] Encode(Message message, bool flowControl = false)
        {
            // if no device to send messages then cannot do it anyway
            if (_device == null)
                flowControl = false;

            var data = new List<byte>(); // each frames data

            if (message.Length <= FRAME_DATA_LENGTH) // SF
            {
                var header = (byte)((0x00 << 4) | (message.Length & 0x0F));
                data.Add(header);
                data.AddRange(message.Data);

                for (var i = data.Count; i <= FRAME_DATA_LENGTH + 1; i++)  // needs full frame of data
                    data.Add((byte)0x00); // padding

                var multiMessage = new Message(message)
                {
                    Data = data.ToArray(),
                    Length = 8, // always full frame message
                };

                return new[] { multiMessage };
            }
            else  // Multi-Frame (MF)
            {
                var multiMessages = new List<Message>();

                // First Frame
                var header = (byte)((0x01 << 4) | ((message.Length & 0xF00) >> 8)); // code and part of length
                data.Add(header);
                header = (byte)(message.Length & 0x0FF);
                data.Add(header);

                data.AddRange(message.Data);

                var multiMessage = new Message(message)
                {
                    Data = data.GetRange(0, FRAME_DATA_LENGTH + 1).ToArray(),
                    Length = 8, // always full frame message
                };
                data.RemoveRange(0, FRAME_DATA_LENGTH + 1);
                multiMessages.Add(multiMessage);

                var blocks = 0; // block size chunk..., 0 send all (per spec)
                if (flowControl)
                {
                    var response = message.Trigger;

                    var frame = multiMessages.Last();
                    frame.MultiFrame = false;  //stop recursion
                    _device.Send(frame); // send out FF to then wait for Flow Control msg

                    var type = FlowControl(_device, response); // blocking call (w/ timeout)!

                    multiMessages.RemoveAt(multiMessages.Count - 1); // last should still be the first...

                    while (type == -1)  // wait (not per spec, per method)
                        type = FlowControl(_device, response);
                    blocks = type; // if not -1 then is block size
                }

                if (blocks == 0)  // means send ALL, so no more flow control needed
                    flowControl = false;

                // Consecutive Frame(s)
                var sequence = (ushort)0x01; // index 0 on FF
                while (true)
                {
                    header = (byte)((0x02 << 4) | (sequence++ & 0x0F));  // increment sequence, max. 0x0F = 15
                    data.Insert(0, header); // insert front of list

                    for (var i = data.Count; i < FRAME_DATA_LENGTH + 1; i++)  // full message needed
                        data.Add((byte)0x00); // padding

                    multiMessage = new Message(message)
                    {
                        Data = data.GetRange(0, FRAME_DATA_LENGTH + 1).ToArray(),
                        Length = 8, // always full frame message
                    };
                    data.RemoveRange(0, FRAME_DATA_LENGTH + 1);
                    multiMessages.Add(multiMessage);

                    if (flowControl)
                    {
                        var frame = multiMessages.Last();
                        frame.MultiFrame = false;
                        _device.Send(frame);

                        multiMessages.RemoveAt(multiMessages.Count - 1); // last should still be the first...

                        if (--blocks <= 0)
                        {
                            var response = message.Trigger;

                            var status = FlowControl(_device, response); // blocking call (will timeout)!
                            while (status == -1)  // wait
                                status = FlowControl(_device, response);

                            if (status == 0)
                                flowControl = false;
                            else
                                blocks = status; // if not -1, then is block size
                        }
                    }

                    if (data.Count <= 0)  // empty
                        break;
                }

                return multiMessages.ToArray();
            }

            return new Message[] { };  // should never get here...
        }

        //  return: -1 = wait, 0 = send all, >0 = block size
        private int FlowControl(IDevice device, uint id)
        {
            // Todo: lookup <ShortName>_MSG_Triggering for response ID for control message

            var result = 0;
            var timeout = FLOW_CONTROL_TIMEOUT;

            while (timeout-- > 0)
            {                
                var response = new MultiFrameFlowControl();
                lock (_lockFlowControl)
                {
                    if (!_frameMessageFlowControlBuffer.ContainsKey(id))
                    {
                        Thread.Sleep(1);
                        continue;
                    }

                    response = _frameMessageFlowControlBuffer[id];
                }

                var type = response.Type;
                var blockSize = response.BlockSize;
                // Note: ignored, delay from this app to board to bus is longer then the max delay anyway!!!
                var seperationTime = response.SeperationTime;

                Debug.WriteLine("(MF) Flow Control Decode -> Id: {2} Type: {0}, Block Size: {1}", type, blockSize, id);

                if (type == 1) // 1 means wait
                    result = -1;
                else if (blockSize > 0)
                    result = blockSize;
                
                break;
            }

            lock (_lockFlowControl)
            {
                if (_frameMessageFlowControlBuffer.ContainsKey(id))
                    _frameMessageFlowControlBuffer.Remove(id);
            }

            return result;
        }
    }
}