﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOComm;
using MessageDecodeManager;

namespace DeviceLibrary
{
    public class EcoMate : IDevice
    {
        //private readonly ISO_15765_2 _iso157652;
        //private readonly SecureMessage _secureMessage;
        private ECOComm.EcoMate _ecoMate = null;

        public event ReceivedHandler OnSent;

        public EcoMate()
        {
            IsConnected = false;
            //_iso157652 = new ISO_15765_2(this);
            //_secureMessage = new SecureMessage();
        }

        Status Connect(string port = "", bool extra = false)
        {
            if (extra)
                throw new NotImplementedException("No extra channel support... Coming Soon!");

            var status = Status.ConnectionInProgress;
            // If call connect then close no matter what
            try
            {
                if (_ecoMate != null)
                    _ecoMate.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            IsConnected = false;  // reset

            try
            {
                if (!string.IsNullOrEmpty(port))
                    _ecoMate = new ECOComm.EcoMate(port, null);  // auto detect first board found, waits little before giving up, Todo: "await"
                if (_ecoMate == null)
                    _ecoMate = new ECOComm.EcoMate();

                if (_ecoMate != null && _ecoMate.Open())
                {
                    IsConnected = true;
                    status = Status.Connected;

                    _ecoMate.EcoMateHeartBeatNotReceived += OnEcoMateHeartBeatNotReceived;
                    _ecoMate.EcoMateAutoReconnected += OnEcoMateAutoReconnected;
                    _ecoMate.OnMessageFrameReceived += EcoMateOnMessageFrameReceived;
                }
                else
                {
                    status = Status.NotConnected;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                status = Status.UnknownError;
            }

            return status;
        }

        Status Disconnect()
        {
            if (_ecoMate.IsAutoReconnectionInProgress())
                return Status.ConnectionInProgress;

            IsConnected = false;

            if (_ecoMate == null)
                return Status.NotConnected;

            if (_ecoMate.IsOpen())
                _ecoMate.Close();
            _ecoMate = null;

            return Status.NotConnected;
        }

        public List<dynamic> ToCANMessage(Message message)
        {
            List<dynamic> collectionMessage = new List<dynamic>();
            var messages = new System.Collections.Generic.List<Message>();

            var isSecure = false;
            // messages = _secureMessage.Process(message, ref isSecure).ToList(); // comes back with encryption if found in MACT file, else original returned
            messages.Add(message);

            if (!isSecure)
            {
                messages.Clear();

                if (message.MultiFrame)
                {
                    try
                    {
                        //_iso157652.FlowControl = true;
                        //messages.AddRange(_iso157652.Encode(message));
                        messages.Add(message);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                        return collectionMessage;
                    }
                }
                else
                {
                    messages.Add(message);
                }
            }

            var status = false;
            foreach (var frame in messages)
            {
                // Bridge!!!

                dynamic sendMessage = null;
                if (message.Network == Mode.LIN)
                {
                    sendMessage = new ECOComm.LINMessage();
                    sendMessage.Id = frame.Id;
                    sendMessage.Type = ModeConvert(message.Network);
                    sendMessage.Port = (uint)message.Port;
                    sendMessage.Buffer = frame.Data;
                    sendMessage.Length = Convert.ToByte(frame.Length);
                }
                else
                {

                    //1. Message Type = 2 Bytes
                    //2. Cycle Time = 2 bytes
                    //3. High Voltage = 1 Bytes
                    //4. Extended CAN = 1 Bytes
                    //5. CAN DataLength = 1 Bytes
                    //6. CAN ID = 4 Bytes
                    //7. CAN Message
                    sendMessage = new ECOComm.CANMessage(frame.Data) // Note: is ANY message to ECO/VCUmate (Todo: change name!)
                    {
                        MsgType = ModeConvert(message.Network),
                        CycleTime = 1,
                        HighVoltage = message.HighVoltage,
                        ExtendedCAN = message.Extended,
                        DataLen = Convert.ToByte(frame.Length),
                        CanId = message.Id,
                        CanBuffer = frame.Data
                    };

                    if (message.Port == 1)
                        sendMessage.MsgType = ModeConvert(Mode.FD);
                }

                //  status |= _ecoMate.Write(sendMessage.GetBuffer()) >= 0;  // any fail then bad status
                collectionMessage.Add(sendMessage);
            }
            return collectionMessage;
        }


        public bool Send(Message message)
        {
            try
            {
                var connectStatus = Connect();
                if (connectStatus == Status.Connected)
                {
                    var messages = new System.Collections.Generic.List<Message>();

                    var isSecure = false;
                    // messages = _secureMessage.Process(message, ref isSecure).ToList(); // comes back with encryption if found in MACT file, else original returned
                    messages.Add(message);

                    if (!isSecure)
                    {
                        messages.Clear();

                        if (message.MultiFrame)
                        {
                            try
                            {
                                //_iso157652.FlowControl = true;
                                // messages.AddRange(_iso157652.Encode(message));
                                messages.Add(message);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex);
                                return false; // only while debugging multi frame...
                            }
                        }
                        else
                        {
                            messages.Add(message);
                        }
                    }

                    var status = false;
                    foreach (var frame in messages)
                    {
                        // Bridge!!!

                        dynamic sendMessage = null;
                        if (message.Network == Mode.LIN)
                        {
                            sendMessage = new ECOComm.LINMessage();
                            sendMessage.Id = frame.Id;
                            sendMessage.Type = ModeConvert(message.Network);
                            sendMessage.Port = (uint)message.Port;
                            sendMessage.Buffer = frame.Data;
                            sendMessage.Length = Convert.ToByte(frame.Length);
                        }
                        else
                        {

                            //1. Message Type = 2 Bytes
                            //2. Cycle Time = 2 bytes
                            //3. High Voltage = 1 Bytes
                            //4. Extended CAN = 1 Bytes
                            //5. CAN DataLength = 1 Bytes
                            //6. CAN ID = 4 Bytes
                            //7. CAN Message
                            sendMessage = new ECOComm.CANMessage // Note: is ANY message to ECO/VCUmate (Todo: change name!)
                            {
                                MsgType = ModeConvert(message.Network),
                                CycleTime = 1,
                                HighVoltage = message.HighVoltage,
                                ExtendedCAN = message.Extended,
                                DataLen = Convert.ToByte(frame.Length),
                                CanId = message.Id,
                                CanBuffer = frame.Data
                            };

                            if (message.Port == 1)
                                sendMessage.MsgType = ModeConvert(Mode.FD);
                        }

                        status |= _ecoMate.Write(sendMessage.GetBuffer()) >= 0;  // any fail then bad status

                        if (OnSent != null)
                            OnSent(frame);
                    }
                    return status;
                }
                else
                {
                    return false;
                }
            }
            finally
            {

            }
        }
        public bool IsConnected { get; set; }
        public event ReceivedManyHandler OnReceivedMany;

        public event ReceivedHandler OnReceived;

        private IDevice _instance = null;
        public IDevice Instance
        {
            get { return _instance ?? (_instance = new EcoMate()); }
        }

        #region ECOMAT SETTINGS

        public void SetPower()
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_HARD_RESET;
            byte[] buff = new byte[1];
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SetBootloader()
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_BOOTLOADER_ENTER;
            byte[] buff = new byte[1];
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SelectUSB(int usbDeviceNumber)
        {
            if (usbDeviceNumber != 0xFF)
            {
                ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
                ctrlMsg.Type = ECOComm.MessageTypes.CTRL_USB_ARB;
                byte[] buff = new byte[1];
                buff[0] = (byte)((usbDeviceNumber) & 0xFF);
                ctrlMsg.Buffer = buff;
                _ecoMate.Write(ctrlMsg.GetBuffer());
            }
        }

        public void SetPowerSwitch(UInt32 powerSwitch, bool powerSwitchState)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_POWER_SWITCH;
            byte[] buff = new byte[2];
            buff[0] = (byte)(powerSwitch);
            buff[1] = (byte)(powerSwitchState ? 0x01 : 0x00);
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SetComEnable(bool enableState)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_COM_ENBL;
            byte[] buff = new byte[1];
            buff[0] = (byte)(enableState ? 0x01 : 0x00);
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SetReverse(bool reverseState)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_REVERSE;
            byte[] buff = new byte[1];
            buff[0] = (byte)(reverseState ? 0x01 : 0x00);
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SelectHSCANTermination(int termination)
        {
            if (termination != 0xFF)
            {
                ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
                ctrlMsg.Type = ECOComm.MessageTypes.CTRL_CAN_TERM;
                byte[] buff = new byte[1];
                buff[0] = (byte)((termination) & 0xFF);
                ctrlMsg.Buffer = buff;
                _ecoMate.Write(ctrlMsg.GetBuffer());
            }
        }

        public void SelectEthernet(int ethernet)
        {
            if (ethernet != 0xFF)
            {
                ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
                ctrlMsg.Type = ECOComm.MessageTypes.CTRL_MEDIA_ARB;
                byte[] buff = new byte[1];
                buff[0] = (byte)((ethernet) & 0xFF);
                ctrlMsg.Buffer = buff;
                _ecoMate.Write(ctrlMsg.GetBuffer());
            }
        }

        public void SetGlowPlug(bool glowPlug)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_GlOW_PLUG;
            byte[] buff = new byte[1];
            buff[0] = (byte)(glowPlug ? 0x01 : 0x00);
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SetLSCAN(bool lscan)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CAN_LS_Ctrl;
            byte[] buff = new byte[1];
            buff[0] = (byte)(lscan ? 0x01 : 0x00);
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SetCleaSettings(UInt32 state, bool runCrank)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CLEA_SETTINGS;
            byte[] buff = new byte[8];
            buff[0] = (byte)state; //mode state
            buff[1] = (byte)(runCrank ? 0x01 : 0x00); //value buffer
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }

        public void SetCANTermSettings(UInt32 state, bool value)
        {
            ECOComm.ControlMsg ctrlMsg = new ECOComm.ControlMsg();
            ctrlMsg.Type = ECOComm.MessageTypes.CTRL_CAN_TERM;
            byte[] buff = new byte[8];
            buff[0] = (byte)state; //mode state
            buff[1] = (byte)(value ? 0x01 : 0x00); //value buffer
            ctrlMsg.Buffer = buff;
            _ecoMate.Write(ctrlMsg.GetBuffer());
        }
        #endregion

        private ushort ModeConvert(Mode mode)
        {
            ushort type_ = 0;
            switch (mode)
            {
                case Mode.LS:
                    type_ = ECOComm.MessageTypes.CAN_LS;
                    break;
                case Mode.HS:
                    type_ = ECOComm.MessageTypes.CAN_HS;
                    break;
                case Mode.FD:
                    type_ = ECOComm.MessageTypes.CAN_FD;
                    break;
                case Mode.LIN:
                    type_ = ECOComm.MessageTypes.LIN;
                    break;
            }

            return type_;
        }

        private Mode TypeConvert(ushort type)
        {
            var mode = Mode.HS;
            switch (type)
            {
                case ECOComm.MessageTypes.CAN_LS:
                    mode = Mode.LS;
                    break;
                case ECOComm.MessageTypes.CAN_HS:
                    mode = Mode.HS;
                    break;
                case ECOComm.MessageTypes.CAN_FD:
                    mode = Mode.FD;
                    break;
                case ECOComm.MessageTypes.LIN:
                    mode = Mode.LIN;
                    break;
            }

            return mode;
        }

        private void EcoMateOnMessageFrameReceived(byte[] buffer)
        {
            IsConnected = true;  // if got data must be connected
            try
            {
                dynamic message = null;
                dynamic messageEcoMate = null;

                var MsgType = BitConverter.ToUInt16(buffer, 3);

                if (MsgType == MessageTypes.CTRL_HEART_BEAT)
                    return;

                if (MsgType == MessageTypes.LIN)
                {
                    messageEcoMate = new LINMessage(buffer);
                    message = new Message()
                    {
                        Id = messageEcoMate.Id,
                        Network = TypeConvert(MsgType),
                        Port = (int)messageEcoMate.Port,
                        Data = messageEcoMate.Buffer,
                        Length = (int)messageEcoMate.Length
                    };
                }
                else // CAN Message
                {
                    messageEcoMate = new ECOComm.CANMessage(buffer);

                    var mode = TypeConvert(MsgType);
                    message = new Message(messageEcoMate.CanId, messageEcoMate.ExtendedCAN, true,
                        messageEcoMate.CanBuffer, messageEcoMate.DataLen, mode);
                    message.Network = mode;

                    // MACT handling
                    {
                        var isSecure = false;
                        // _secureMessage.Process(message, ref isSecure);
                    }

                    /*
                    if (ARXML.Loaded)
                    {
                        var messageLookup = ARXML.FindMessageById(message.Id);
                        //var messageRspLookup = ARXML.Instance.messages.FirstOrDefault(m => m.Properties["msgname"] == messageLookup.Name.Replace("_Rsp_", "_Req_"));
    
                        //var extended = messageLookup.Properties["addressingmode"].ToLower() == "extended";
                        if (messageLookup != null)
                        {
                            try
                            {
                                //var multiFrame = !messageLookup.SignalNames.Any();  // Important: Assume if NO signals then is multi-frame!!!!!!!!
                                var name = messageLookup.Name;
                                var multiFrame = name.StartsWith("USDT_") || name.StartsWith("UUDT_");
                                if (multiFrame)
                                {
                                    message = _iso157652.Decode(message);
                                    if (message != null && message.Length > 0) // temp: debug break for now....
                                        message = message;
    
                                    if(message != null) // when message is ready
                                      message.Network = Mode.MF;
                                }
                            }
                            catch (Exception ex)
                            {
                                if (messageLookup == null)
                                    Debug.WriteLine("Not Found in Database!!!");
                                else
                                    Debug.WriteLine(ex);
                            }
                        }
                    }
                    */
                }

                if (message == null)  // not full message yet so wait
                    return;

                OnReceived?.Invoke(message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                ; // Todo: look into this -> end frame was seen does not mean all the crap in the beginning was good
            }
        }

        private void OnEcoMateAutoReconnected(object sender, bool e)
        {
            IsConnected = false;
        }

        private void OnEcoMateHeartBeatNotReceived(object sender, bool e)
        {
            IsConnected = false;
        }
    }
}
