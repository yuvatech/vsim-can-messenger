﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace DeviceLibrary
{
    public class MockDevice : IDevice
    {
        private const int DELAY = 1; // ms. between messages
        private const int MESSAGES = 1; // messages before delay
        private Task _runner = null;
        private bool _active = true;
        private volatile int _count = 0;
        private readonly StreamWriter _logger = new StreamWriter(Path.Combine(Utility.Helper.AssemblyDirectory, "MockDevice.csv") );
        private readonly object _locker = new object();
        private readonly System.Collections.Generic.Queue<string>
            _logs = new System.Collections.Generic.Queue<string>();

        public MockDevice()
        {
            Task.Run(() =>
            {
                while (true)
                {                    
                    //lock (_locker)
                    {
                        try
                        {

                            if (!_logs.Any())
                            {
                                System.Threading.Thread.Sleep(25);
                            }
                            else
                            {

                                var info = _logs.Dequeue();
                                _logger.WriteLine(info);
                            }
                        }
                        catch (Exception e)
                        {
                            ;
                        }
                    }

                    System.Threading.Thread.Sleep(1);
                }
            });
        }

        public bool IsConnected
        {
            get { return true; }
            set { bool temp = value; }
        }

        public event ReceivedManyHandler OnReceivedMany;

        public event ReceivedHandler OnReceived;
        public event ReceivedHandler OnSent;

        public Status Connect(string port = "", bool extra = false)
        {
            var message = new Message(){Id=0x100};
            _runner = Task.Run(() =>
            {
                while (_active)
                {
                    for (var i=0; i < MESSAGES; i++)
                    {
                        // Debug: counter MSG

                        _count++;
                        var b = BitConverter.GetBytes(_count);
                        Array.Reverse(b); // little endian
                        message.Data = b;
                        message.Length = 8;
                        OnReceived?.Invoke(message);

                        // signal tests...
                        // SPMP_SysPwrModeAuth
                        OnReceived?.Invoke(new Message(){Id=0x284, Data = new byte[]{0x00,0x00,0x00,0x00,0x08, 0x00}, Length = 0x06});

                        // DrvDrAjrStat... so on...
                        OnReceived?.Invoke(new Message() { Id = 0x4DA, Data = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02 } });

                        // crappy id to make sure ignoring...         
                        for (var ii = 0; ii < 99; ii++)
                          OnReceived?.Invoke(new Message() { Id = (uint)ii, Data = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } });

                        InfoTimingPrint();
                    }

                    if(DELAY >= 0)
                     System.Threading.Thread.Sleep(DELAY);
                    else
                     System.Threading.Thread.Yield();
                }
            });
            return Status.Connected;
        }

        public Status Disconnect()
        {
            InfoTimingPrint();
            if (_runner != null)
                _active = false;
            return Status.UnknownError;
        }

        public bool Send(Message message)
        {            
            OnSent?.Invoke(message);
            return true;
        }

        private void InfoTimingPrint()
        {
            var info = DateTime.Now.ToString("HH:mm:ss.FFF") + "," + _count.ToString("X");
            Console.WriteLine(info);
            //_logger.WriteLine(info);

            try
            {
                //lock (_locker)
                {
                    _logs.Enqueue(info);
                }
            }
            catch (Exception e)
            {
                ;
            }                    
        }    
    }
}
