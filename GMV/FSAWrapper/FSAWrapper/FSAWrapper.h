#pragma once
#include <Windows.h>
using namespace System;
using namespace Threading;

namespace FSAWrapper
{
	public enum class ServiceEnent
	{
		InitError = 0,
		InitSuccess = 1,
		ConnectError = 2,
		Connected = 3,
		ConnectionLost = 4,
		SubscribeError = 5,
		HeartbeatSubscribed = 6,
		Subscribed = 7,
		FsaServiceReset = 8,
		Disconnected = 9,
		CleanupSuccess = 10
	};
	
	public delegate void EventCallback_dg(int service_id, FSAWrapper::ServiceEnent service_event);
	public delegate void HeartBeat_dg(unsigned int heartbeat_value);
	public delegate void reply_dg(int message_type_id, System::String ^reply_json);
	public delegate void onstar_message_dg(int message_type_id, System::String ^reply_json);

	public ref class RadioSimAPI
	{
	public:
		//Callback delegates here
		static EventCallback_dg ^init_event_callback;
		static HeartBeat_dg ^heartbeat_event_callback;
		static reply_dg ^get_callback;
		static reply_dg ^set_callback;
		static reply_dg ^request_response_callback;
		static reply_dg ^subscribe_reply_callback;
		static reply_dg ^subscribe_event_callback;
		static reply_dg ^unsubscribe_reply_callback;
	public:

		static int Init(int ServiceId, System::String ^IPAddress, int Port);
		static int Cleanup(int ServiceId);
		static int Subscribe(int ServiceId, System::String ^JSONRequest);
		static int UnSubscribe(int ServiceId, System::String ^JSONRequest);
		static int Get(int ServiceId, System::String ^JSONRequest);
		static int Set(int ServiceId, System::String ^JSONRequest);
		static int Request(int ServiceId, System::String ^JSONRequest);
		static int RequestResponse(int ServiceId, System::String ^JSONRequest);

		RadioSimAPI();
		~RadioSimAPI();
		// TODO: Add methods for this class here.
	};
}
