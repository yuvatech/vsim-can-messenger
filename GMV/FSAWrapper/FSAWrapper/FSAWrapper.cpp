#include "stdafx.h"
#include <api.hpp>
#include <malloc.h>
#include <iostream>
#include "FSAWrapper.h"
#include <Windows.h>

namespace FSAWrapper
{
	//-----------------------------------------------------------------------------------------------------
	char *__strdup(System::String ^src)
	{
		array<unsigned char>^ arr = System::Text::Encoding::GetEncoding(1251)->GetBytes(src);

		char *out = (char*)malloc(src->Length + 1);

		for (int i = 0;i < arr->GetLength(0);i++)
			out[i] = arr[i];
		out[src->Length] = 0x00;

		return out;
	}		
	//-----------------------------------------------------------------------------------------------------
	void __onstar_message_callback(int message_type_id, const char * reply_json)
	{
		std::cout << __func__ << ": message_type_id: " << message_type_id << "; reply_json: " << reply_json << std::endl;
	}
	//-----------------------------------------------------------------------------------------------------
	void __init_event_callback(int service_id, event_id_t event_id)
	{
		if (RadioSimAPI::init_event_callback != nullptr) 
			RadioSimAPI::init_event_callback(service_id, (ServiceEnent)event_id);
	}
	//-----------------------------------------------------------------------------------------------------
	void __init_heartbeat_callback(unsigned int heartbeat_value)
	{
		if (RadioSimAPI::heartbeat_event_callback != nullptr)
			RadioSimAPI::heartbeat_event_callback(heartbeat_value);
	}
	//-----------------------------------------------------------------------------------------------------
	int  RadioSimAPI::Init(int ServiceId, System::String ^ IPAddress, int Port)
	{
		char *__ip = __strdup(IPAddress);
		int result = init(ServiceId, __ip, Port, (event_cb)&__init_event_callback, (heartbeat_cb)&__init_heartbeat_callback);
		free(__ip);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	int RadioSimAPI::Cleanup(int ServiceId)
	{
		return cleanup(ServiceId);
	}
	//-----------------------------------------------------------------------------------------------------
	void __subscribe_reply_callback(int message_type_id, const char * reply_json)
	{
		if (RadioSimAPI::subscribe_reply_callback != nullptr)
			RadioSimAPI::subscribe_reply_callback(message_type_id, gcnew System::String(reply_json));
	}
	void __subscribe_event_callback(int message_type_id, const char * reply_json)
	{
		if (RadioSimAPI::subscribe_event_callback != nullptr)
			RadioSimAPI::subscribe_event_callback(message_type_id, gcnew System::String(reply_json));
	}
	int RadioSimAPI::Subscribe(int ServiceId, System::String ^JSONRequest)
	{
		char *__json_request = __strdup(JSONRequest);
		int result = subscribe(ServiceId, __json_request, (fsa_reply_cb)&__subscribe_reply_callback, (fsa_event_cb)&__subscribe_event_callback);
		free(__json_request);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	void __unsubscribe_reply_callback(int message_type_id, const char * reply_json)
	{
		if (RadioSimAPI::unsubscribe_reply_callback != nullptr)
			RadioSimAPI::unsubscribe_reply_callback(message_type_id, gcnew System::String(reply_json));
	}
	int RadioSimAPI::UnSubscribe(int ServiceId, System::String ^JSONRequest)
	{
		char *__json_request = __strdup(JSONRequest);
		int result = unsubscribe(ServiceId, __json_request, (fsa_status_cb)__unsubscribe_reply_callback);
		free(__json_request);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	void __get_reply_callback(int message_type_id, const char * reply_json)
	{
		if (RadioSimAPI::get_callback != nullptr)
			RadioSimAPI::get_callback(message_type_id, gcnew System::String(reply_json));
	}
	int RadioSimAPI::Get(int ServiceId, System::String ^JSONRequest)
	{
		char *__json_request = __strdup(JSONRequest);
		int result = get(ServiceId, __json_request, (fsa_status_cb)__get_reply_callback);
		free(__json_request);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	void __set_reply_callback(int message_type_id, const char * reply_json)
	{
		if (RadioSimAPI::set_callback != nullptr)
			RadioSimAPI::set_callback(message_type_id, gcnew System::String(reply_json));
	}
	int RadioSimAPI::Set(int ServiceId, System::String ^JSONRequest)
	{
		char *__json_request = __strdup(JSONRequest);
		int result = set(ServiceId, __json_request, (fsa_status_cb)__set_reply_callback);
		free(__json_request);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	int RadioSimAPI::Request(int ServiceId, System::String ^JSONRequest)
	{
		char *__json = __strdup(JSONRequest);
		int result = request(ServiceId, __json);
		free(__json);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	void __req_resp_reply_callback(int message_type_id, const char * reply_json)
	{
		if (RadioSimAPI::request_response_callback != nullptr)
			RadioSimAPI::request_response_callback(message_type_id, gcnew System::String(reply_json));
	}
	int RadioSimAPI::RequestResponse(int ServiceId, System::String ^JSONRequest)
	{
		char *__json_request = __strdup(JSONRequest);
		int result = request_response(ServiceId, __json_request, (fsa_response_cb)__req_resp_reply_callback);
		free(__json_request);
		return result;
	}
	//-----------------------------------------------------------------------------------------------------
	RadioSimAPI::RadioSimAPI()
	{

	}
	//-----------------------------------------------------------------------------------------------------
	RadioSimAPI::~RadioSimAPI()
	{

	}
}
//---------------------------------------------------------------------------------------------------------
