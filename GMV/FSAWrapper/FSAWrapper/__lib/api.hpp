/* Copyright 2019 GM GLOBAL TECHNOLOGY OPERATIONS LLC ALL RIGHTS RESERVED. */

#ifndef __RADIOSIM_API_HPP
#define __RADIOSIM_API_HPP

#ifdef _WIN32
#ifdef RADIOSIM_LIBRARY
#define RADIOSIM_API __declspec(dllexport)
#else
#define RADIOSIM_API __declspec(dllimport)
#endif
#else
#define RADIOSIM_API
#endif

extern "C" {

///
/// \brief Enum that defines the service event identifier
///
enum ServiceEvent {
    InitError,
    InitSuccess,
    ConnectError,
    Connected,
    ConnectionLost,
    SubscribeError,
    HeartbeatSubscribed,
    Subscribed,
    FsaServiceReset,
    Disconnected,
    CleanupSuccess
};

/// \brief UInt32 that defines the service event identifier
typedef unsigned int event_id_t;

///
/// \brief Callback type that provides the service event ID
///
/// Callback function used for calling on every service event occurrence.
/// The service events are listed within ServiceEvent enumeration. When a
/// service event happens the service invokes this callback function with
/// the corresponding event_id param
///
/// \param service_id The FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param event_id The service event identifier that corresponds to ServiceEvent enumeration
///
typedef void(__stdcall* event_cb)(int service_id, event_id_t event_id);

///
/// \brief Callback type that provides with the current heartbeat counter value
///
/// Callback function used for calling on every heartbeat event occurrence.
/// The callback is invoked every time when heartbeat property status message comes.
///
/// \param heartbeat_value The value of current heartbeat counter taken from
/// a hearbeat status message
///
typedef void(__stdcall* heartbeat_cb)(unsigned int heartbeat_value);

///
/// \brief Callback type that provides the FSA reply as a JSON string
///
/// Callback function used for notifying that an incoming FSA message
/// is received. The reply is represented as a JSON formatted string
/// and passes through `reply_json` param
///
/// \param message_type_id The FSA message type ID
/// \param reply_json The FSA reply message formatted into JSON string
///
typedef void(__stdcall* fsa_reply_cb)(int message_type_id, const char* reply_json);

typedef fsa_reply_cb fsa_event_cb;
typedef fsa_reply_cb fsa_status_cb;
typedef fsa_reply_cb fsa_response_cb;

///
/// \brief Function is intended for starting the FSA connection thread and assigning
/// the callback function for signaling about events from this thread.
///
/// This function should be invoked once on the start up for creating a corresponding
/// FSA service routine. It creates thread which initiates a TCP connection to the FSA
/// service accordingly to the `service_id`/`fsasrv_ipaddr`/`port` params. The created
/// thread invokes the `event_callback` function every time when a service event happens.
/// Service event identifiers are listed in the ServiceEvent enumeration.
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param fsasrv_ipaddr FSA service IP address
/// \param port TCP port for connection to the FSA service instance
/// \param event_callback Callback function for notification that a service event happened
/// \param heartbeat_callback Callback function for notification that a regular heartbeat
/// FSA status message received [empty ok]
///
/// \return returns 0 if the handling thread is successfully created, otherwise non-zero value
///
RADIOSIM_API int __stdcall init(
    int service_id,
    const char* fsasrv_ipaddr,
    int port,
    event_cb event_callback,
    heartbeat_cb heartbeat_callback);

///
/// \brief Function is intended for closing the connection to the FSA service,
/// cleaning up the allocated resources and stopping the corresponding FSA interacting thread.
///
/// This function forces the FSA handler thread (corresponding to `service_id` param) to close
/// the TCP connection to the FSA service, to clean up the allocated resources and finally
/// to stop its execution. The function performs synchronously, it won't return until
/// cleaning up is done.
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
///
/// \return returns 0 on success, otherwise non-zero value
///
RADIOSIM_API int __stdcall cleanup(int service_id);

///
/// \brief Function to subscribe to FSA event
///
/// Function sends the FSA RequestReply message to subscribe to the FSA event/property
/// related to `json_request` param. If subscription is successful then the service assigns
/// the `event_handler_cb` function as an event handler, it will be invoked every time when
/// FSA event comes.
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param json_request The JSON formatted string request to subscribe to an FSA event/property ID
/// \param reply_cb The callback function to be invoked once the FSA subscription response comes
/// \param event_handler_cb The callback function to be invoked every time when FSA event comes
///
/// \return return zero value on success
///
RADIOSIM_API int __stdcall subscribe(
    int service_id,
    const char* json_request,
    fsa_reply_cb reply_cb,
    fsa_event_cb event_handler_cb);

///
/// \brief Function to unsubscribe from FSA event
///
/// Function sends the FSA RequestReply message to unsubscribe from the FSA event/property
/// related to `json_request` param.
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param json_request The JSON formatted string request to unsubscribe from an FSA event/property ID
/// \param reply_cb The callback function to be invoked once the FSA unsubscription response comes
///
/// \return return zero value on success
///
RADIOSIM_API int __stdcall unsubscribe(
    int service_id,
    const char* json_request,
    fsa_reply_cb reply_cb);

///
/// \brief Function to send Get FSA request
///
/// Function sends the FSA Get message to get the FSA property Status reply
/// related to `json_request` param. FSA Status reply passes as a JSON message
/// within `status_cb` call
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param json_request JSON string for converting into FSA Get message
/// \param status_cb The callback function to be invoked once the FSA Status message comes
///
/// \return return zero value if Get message is successfully sent, non-zero value otherwise
///
RADIOSIM_API int __stdcall get(
    int service_id,
    const char* json_request,
    fsa_status_cb status_cb);

///
/// \brief Function to send Set FSA request
///
/// Function sends the FSA Set message to get the FSA property Status reply
/// related to `json_request` param. FSA Status reply passes as a JSON message
/// within `status_cb` call
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param json_request JSON string for converting into FSA Set message
/// \param status_cb The callback function to be invoked once the FSA Status message comes
///
/// \return return zero value if Set message is successfully sent, non-zero value otherwise
///
RADIOSIM_API int __stdcall set(
    int service_id,
    const char* json_request,
    fsa_status_cb status_cb);

///
/// \brief Function to send Request FSA message
///
/// \param json_request JSON string for converting into FSA Request message
///
/// \return return zero value if Request message is successfully sent, non-zero value otherwise
///
RADIOSIM_API int __stdcall request(
    int service_id,
    const char* json_request);

///
/// \brief Function to send RequestResponse FSA request
///
/// Function sends the FSA RequestResponse message for the corresponding FSA method ID.
/// FSA message payload is constructed from JSON string param `json_request`.
/// The related FSA Response passes asynchronously as a JSON formatted string
/// within `response_cb` call
///
/// \param service_id FSA service identifier: DeviceInfo, OnStarFunctions, GBRemoteReflash
/// \param json_request JSON string for converting into FSA RequestResponse message
/// \param response_cb The callback function to be invoked once the FSA Response message comes
///
/// \return return zero value if RequestResponse message is successfully sent,
/// non-zero value otherwise
///
RADIOSIM_API int __stdcall request_response(
    int service_id,
    const char* json_request,
    fsa_response_cb response_cb);
}

#endif //__RADIOSIM_API_HPP
