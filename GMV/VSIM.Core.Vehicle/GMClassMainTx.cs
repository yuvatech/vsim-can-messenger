﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using Multimedia;
using MessageDecodeManager;
using Message = MessageDecodeManager.Message;
using Extension;
using VSIM.Core.Device;
using VSIM.Core;
using Prism.Events;
using UsingEventAggregator.Core;

namespace GMVehicleMain
{
    public class GMClassMainTx
    {
        /********************Global B Timer Counters******************/
        int hundredMilliSeconds = 0;
        int twoHundredandFiftyMilliSeconds = 0;
        int fiveHundredMilliSeconds = 0;
        int oneSecond = 0;
        int fiveSeconds = 0;
        /*************************************************************/

        DeviceManager _device = null;
        //Devices Device;
        private Thread MessageReceiver = null;

        bool VehicleOn = false;
        public bool ShouldStop = false;
        public string PowerMode = "Off";
        public volatile bool NeoVIConnected = false;
        public volatile bool CSMateConnected = false;
        public volatile bool AutoReconnect = false;
        private bool csmateDeviceSelected = false;
        private bool neoVIDeviceSelected = false;
        public bool gotoSleep = false;

        public event EventHandler<bool> CSMateHeartBeatNotReceived;
        public event EventHandler<bool> NeoVIDisconnected;
        public event EventHandler<bool> NeoVIReconnected;
        public event EventHandler<bool> CSMateReconnected;

        Multimedia.Timer CSMateKeepAliveTimer = new Multimedia.Timer();
        private System.Timers.Timer NeoConnectionCheckTmr = new System.Timers.Timer(1000);
        private System.Timers.Timer AutoReconnectTmr = new System.Timers.Timer(1000);

        Multimedia.Timer GBTimer = new Multimedia.Timer();

        private Thread CSMateCheckHeartBeat = null;
        Semaphore CSMateHeartBeat = new Semaphore(0, 10);

        public int DimDspLvl = 126;
        public int DimLvl = 96;
        public int VinDig1 = 0;
        #region VIN Disable Properties

        bool bIsVINChecked = true;

        public bool IsVINChecked
        {
            set
            {
                bIsVINChecked = value;
            }
            get
            {
                return bIsVINChecked;
            }
        }

        #endregion

        public void EnableAutoReconnect()
        {
            AutoReconnect = true;
            AutoReconnectTmr.Stop();
            AutoReconnectTmr.Enabled = true;
            AutoReconnectTmr.Start();
        }
        public void DisableAutoReconnect()
        {
            AutoReconnect = false;
            AutoReconnectTmr.Stop();
            AutoReconnectTmr.Enabled = false;
        }
        IEventAggregator ea;
        public GMClassMainTx(DeviceManager device, IEventAggregator _ea)
        {
            try
            {
                _device = new DeviceManager(_ea);
                _device = device;
                ea = _ea;

                AutoReconnectTmr.Elapsed += AutoReconnectTmr_Elapsed;

                GBTimer.Mode = Multimedia.TimerMode.Periodic;
                GBTimer.Period = 25;
                GBTimer.Resolution = 1;
                GBTimer.Tick += new System.EventHandler(GBTimer_Tick);
                GBTimer.Start();

            }
            catch (Exception i)
            {
                throw new InvalidOperationException("Error", i);
            }
        }

        ~GMClassMainTx()
        {
            if (null != MessageReceiver)
            {
                MessageReceiver.Abort();
            }
            MessageReceiver = null;
        }

        #region Door Open Status

        public void DriverDoorStatus(bool open)
        {
            if (open)
            {
                if (this.GBTimer.IsRunning)
                {
                    if (this.PowerMode == "Off")
                    {
                        // stop sending power mode
                        if (this.GBTimer.IsRunning)
                        {
                            this.GBTimer.Stop();
                        }

                        this.gotoSleep = true;
                    }
                }
                else
                {
                    this.GBTimer.Start();
                }

                // Delay introduced to ensure that NM message sent first from tool.
                // Without delay, NM message not being sent first due to which CSM is not waking up
                Thread.Sleep(200);

                var GBSignalData = new GlobalBDatabase(_device);

                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("DrvDrAjrStat", "1");
                GBSignalData.sendSignals(dictionary_Signal_Val);
            }
            else
            {
                var GBSignalData = new GlobalBDatabase(_device);

                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("DrvDrAjrStat", "0");
                GBSignalData.sendSignals(dictionary_Signal_Val);

                if (this.gotoSleep)
                {
                    this.GBTimer.Stop();
                    this.VehicleOn = false;
                    this.gotoSleep = false;
                }
            }

        }

        #endregion

        #region PowerMode

        public void Off()
        {

            Syspwr("0");

            PowerMode = "Off";
            this.gotoSleep = false;
        }

        public void Acc()
        {
            if (!VehicleOn)
            {
                GBWakeUP();
                VehicleOn = true;
            }

            Syspwr("1");

            PowerMode = "Acc";
            this.gotoSleep = false;
        }

        public void Run()
        {
            if (!VehicleOn)
            {
                GBWakeUP();
                VehicleOn = true;
            }

            Syspwr("2");

            PowerMode = "Run";
            this.gotoSleep = false;
        }

        public void Crank()
        {
            if (!VehicleOn)
            {
                GBWakeUP();
                VehicleOn = true;
            }

            Syspwr("3");

            PowerMode = "Crank";
            this.gotoSleep = false;
        }

        public void Propulsion()
        {
            if (!this.VehicleOn)
            {
                this.GBWakeUP();
                this.VehicleOn = true;
            }

            Syspwr("4");
            PowerMode = "Propulsion";
            this.gotoSleep = false;
        }

        public void SetVIN(char[] VINDigs)
        {
            Properties.Settings.Default.VinDig2 = VINDigs[0];
            Properties.Settings.Default.VinDig3 = VINDigs[1];
            Properties.Settings.Default.VinDig4 = VINDigs[2];
            Properties.Settings.Default.VinDig5 = VINDigs[3];
            Properties.Settings.Default.VinDig6 = VINDigs[4];
            Properties.Settings.Default.VinDig7 = VINDigs[5];
            Properties.Settings.Default.VinDig8 = VINDigs[6];
            Properties.Settings.Default.VinDig9 = VINDigs[7];
            Properties.Settings.Default.VinDig10 = VINDigs[8];
            Properties.Settings.Default.VinDig11 = VINDigs[9];
            Properties.Settings.Default.VinDig12 = VINDigs[10];
            Properties.Settings.Default.VinDig13 = VINDigs[11];
            Properties.Settings.Default.VinDig14 = VINDigs[12];
            Properties.Settings.Default.VinDig15 = VINDigs[13];
            Properties.Settings.Default.VinDig16 = VINDigs[14];
            Properties.Settings.Default.VinDig17 = VINDigs[15];
            Properties.Settings.Default.Save();

            byte[] VIN_Digs_10to17 = new byte[8];
            VIN_Digs_10to17[0] = Convert.ToByte(Properties.Settings.Default.VinDig2);
            VIN_Digs_10to17[1] = Convert.ToByte(Properties.Settings.Default.VinDig3);
            VIN_Digs_10to17[2] = Convert.ToByte(Properties.Settings.Default.VinDig4);
            VIN_Digs_10to17[3] = Convert.ToByte(Properties.Settings.Default.VinDig5);
            VIN_Digs_10to17[4] = Convert.ToByte(Properties.Settings.Default.VinDig6);
            VIN_Digs_10to17[5] = Convert.ToByte(Properties.Settings.Default.VinDig7);
            VIN_Digs_10to17[6] = Convert.ToByte(Properties.Settings.Default.VinDig8);
            VIN_Digs_10to17[7] = Convert.ToByte(Properties.Settings.Default.VinDig9);

            byte[] VIN_Digs_2to9 = new byte[8];
            VIN_Digs_2to9[0] = Convert.ToByte(Properties.Settings.Default.VinDig10);
            VIN_Digs_2to9[1] = Convert.ToByte(Properties.Settings.Default.VinDig11);
            VIN_Digs_2to9[2] = Convert.ToByte(Properties.Settings.Default.VinDig12);
            VIN_Digs_2to9[3] = Convert.ToByte(Properties.Settings.Default.VinDig13);
            VIN_Digs_2to9[4] = Convert.ToByte(Properties.Settings.Default.VinDig14);
            VIN_Digs_2to9[5] = Convert.ToByte(Properties.Settings.Default.VinDig15);
            VIN_Digs_2to9[6] = Convert.ToByte(Properties.Settings.Default.VinDig16);
            VIN_Digs_2to9[7] = Convert.ToByte(Properties.Settings.Default.VinDig17);

            Message msg10_17 = ARXML.FindMessageByName("VehIdNmDig10_17_MSG");
            _device.SendCANMessage(Convert.ToUInt32(msg10_17.Properties["Identifier"]),
                VIN_Digs_10to17,
                Convert.ToInt32(VIN_Digs_10to17.Length),
                false,
                false,
                false);

            Message msg2_9 = ARXML.FindMessageByName("VehIdNmDig2_9_MSG");

            _device.SendCANMessage(Convert.ToUInt32(msg2_9.Properties["Identifier"]),
                VIN_Digs_2to9,
                Convert.ToInt32(VIN_Digs_2to9.Length),
                false,
                false,
                false);

            var GBSignalData = new GlobalBDatabase(_device);
            var dictionary_Signal_Val = new Dictionary<string, string>();
            dictionary_Signal_Val.Add("VehIdNmDig1", VinDig1.ToString());  // ???
            GBSignalData.sendSignals(dictionary_Signal_Val);
        }
        #endregion

        #region VIN
        public void GBSendVIN(bool Send)
        {
            if (Send)
            {
                byte[] VINdata_10_17 = new byte[8];
                VINdata_10_17[0] = 0x59;
                VINdata_10_17[1] = 0x4c;
                VINdata_10_17[2] = 0x4a;
                VINdata_10_17[3] = 0x33;
                VINdata_10_17[4] = 0x32;
                VINdata_10_17[5] = 0x38;
                VINdata_10_17[6] = 0x32;
                VINdata_10_17[7] = 0x32;
                Message msg10_17 = ARXML.FindMessageByName("VehIdNmDig10_17_MSG");

                _device.SendCANMessage(Convert.ToUInt32(msg10_17.Properties["Identifier"]),
                                       VINdata_10_17,
                                       Convert.ToInt32(msg10_17.Properties["length"]),
                                       false,
                                       false,
                                       false);

                byte[] VINdata_2_9 = new byte[8];
                VINdata_2_9[0] = 0x4a;
                VINdata_2_9[1] = 0x34;
                VINdata_2_9[2] = 0x46;
                VINdata_2_9[3] = 0x46;
                VINdata_2_9[4] = 0x34;
                VINdata_2_9[5] = 0x38;
                VINdata_2_9[6] = 0x53;
                VINdata_2_9[7] = 0x36;
                Message msg2_9 = ARXML.FindMessageByName("VehIdNmDig2_9_MSG");

                _device.SendCANMessage(Convert.ToUInt32(msg2_9.Properties["Identifier"]),
                                       VINdata_10_17,
                                       Convert.ToInt32(msg2_9.Properties["length"]),
                                       false,
                                       false,
                                       false);

                var GBSignalData = new GlobalBDatabase(_device);

                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("VehIdNmDig1", VinDig1.ToString());
                GBSignalData.sendSignals(dictionary_Signal_Val);

            }
        }
        #endregion

        #region USB Update

        bool bIsUSBChecked = false;

        public bool IsUSBChecked
        {
            set
            {
                bIsUSBChecked = value;
            }
            get
            {
                return bIsUSBChecked;
            }
        }

        public void usbupdate(bool usb)
        {
            if (usb)
            {
                var GBSignalData = new GlobalBDatabase(_device);
                var dictionary_Signal_Val = new Dictionary<string, string>();

                dictionary_Signal_Val.Add("VMMP_VehMtnMvmtStatAuth", "0");
                GBSignalData.sendSignals(dictionary_Signal_Val);

                dictionary_Signal_Val.Clear();
                dictionary_Signal_Val.Add("RmtReFlshElecPwrRdnes", "3");
                GBSignalData.sendSignals(dictionary_Signal_Val);
            }
        }

        #endregion

        public void ClearTheftLock()
        {
            //Clear VIN
#if UNIX
            Utility.Helper.ProcessHidden("python3" + " " + Path.Combine(Utility.Helper.AssemblyDirectory, "Extensions", "Python", "theft_lock.py"));            
#else
            Utility.Helper.ProcessHidden(Path.Combine(Utility.Helper.AssemblyDirectory, "Extensions", "Python", "theft_lock.exe"));
#endif
        }

        private void GBTimer_Tick(object sender, System.EventArgs e)
        {
            if (_device != null && !_device.AllowCommunication)
                return;
            //this.Run();
            hundredMilliSeconds++;
            twoHundredandFiftyMilliSeconds++;
            fiveHundredMilliSeconds++;
            oneSecond++;
            fiveSeconds++;

            if (_device == null)
                return;

            if (hundredMilliSeconds >= 4)
            {
                byte[] data_ = new byte[8];
                data_[0] = 0x45;
                data_[1] = 0x40;
                data_[2] = 0x80;
                data_[3] = 0x00;
                data_[4] = 0x00;
                data_[5] = 0x00;
                data_[6] = 0x00;
                data_[7] = 0x00;
                Message msg = ARXML.FindMessageByName("CGM_NM_Frame_On_CAN_5_Cluster");
                ea?.GetEvent<MessageSentEvent>().Publish("Sending of Signal In-Progress");
                _device.SendCANMessage(Convert.ToUInt32(msg.Properties["Identifier"]),
                                       data_,
                                       Convert.ToInt32(msg.Properties["length"]),
                                       false,
                                       false,
                                       false);
                ea?.GetEvent<GBTimerEvent>().Publish(string.Format("{0}:GB Signal Send Successfully.", DateTime.Now));
                hundredMilliSeconds = 0;
            }

            if (twoHundredandFiftyMilliSeconds >= 10)
            {
                Syspwr(DataValue);
                twoHundredandFiftyMilliSeconds = 0;
            }
            if (fiveHundredMilliSeconds >= 20)
            {
                var GBSignalData = new GlobalBDatabase(_device);
                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("IntDimDspLvl", DimDspLvl.ToString());
                dictionary_Signal_Val.Add("IntDimLvl", DimLvl.ToString());
                GBSignalData.sendSignals(dictionary_Signal_Val);
                fiveHundredMilliSeconds = 0;
            }

            if (oneSecond >= 40)
            {
                usbupdate(IsUSBChecked);
                oneSecond = 0;
            }

            if (fiveSeconds >= 200)
            {
                var GBSignalData = new GlobalBDatabase(_device);
                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("IntDimDspLvl", DimDspLvl.ToString());
                dictionary_Signal_Val.Add("IntDimLvl", DimLvl.ToString());
                GBSignalData.sendSignals(dictionary_Signal_Val);
                this.GBSendVIN(IsVINChecked);
                fiveSeconds = 0;
            }

        }

        private void GBWakeUP()
        {
            if (!GBTimer.IsRunning)
            {
                this.GBTimer.Start();
            }

            byte[] WakeupData = new byte[8];
            WakeupData[0] = 00;
            WakeupData[1] = 64;
            WakeupData[2] = 192;
            WakeupData[3] = 00;
            WakeupData[4] = 00;
            WakeupData[5] = 00;
            WakeupData[6] = 00;
            WakeupData[7] = 33;
            _device.SendCANMessage(325, WakeupData, 8, false, false, false);

            Syspwr(DataValue);

            var GBSignalData = new GlobalBDatabase(_device);
            var dictionary_Signal_Val = new Dictionary<string, string>();
            dictionary_Signal_Val.Add("IntDimDspLvl", "126");
            dictionary_Signal_Val.Add("IntDimLvl", "96");
            GBSignalData.sendSignals(dictionary_Signal_Val);
        }


        #region Auto Reconnection

        public void ConnectNeoVI()
        {
            try
            {
                if (NeoVIConnected == false)
                {
                    NeoConnectionCheckTmr.Start();
                    NeoConnectionCheckTmr.Enabled = true;
                    csmateDeviceSelected = false;
                    //ecomateDeviceSelected = false;
                    neoVIDeviceSelected = true;
                    _device.Connect(DeviceOptions.NeoVI);
                    NeoVIConnected = _device.IsConnected();

                    NeoConnectionCheckTmr.Elapsed += NeoConnectionCheckTmr_Elapsed;
                }
            }
            catch (Exception e)
            {
                NeoVIConnected = _device.IsConnected();
            }

        }

        public void DisconnectNeoVI()
        {
            if (NeoConnectionCheckTmr.Enabled == true)
            {
                NeoConnectionCheckTmr.Enabled = false;
                NeoConnectionCheckTmr.Stop();
            }

            if (AutoReconnect == false && AutoReconnectTmr.Enabled == true)
            {
                AutoReconnectTmr.Enabled = false;
                AutoReconnectTmr.Stop();
            }

            if (NeoVIConnected == true)
            {
                if (AutoReconnect == false)
                {
                    neoVIDeviceSelected = false;
                }

                _device.Disconnect();
                //LSGMLanTimer.Stop();
                //HSGMLanTimer.Stop();
                //VehicleOn = false;
                NeoVIConnected = _device.IsConnected();
            }
        }

        //public uint GetNeoErrors()
        //{
        //    uint NeoVIErrors = _device.Device.Intrepid.CheckNeoErrors();

        //    return NeoVIErrors;
        //}

        public void OnNeoVIDisConnected(bool e)
        {
            if (NeoVIDisconnected != null)
            {
                NeoVIDisconnected(this, e);
                if (AutoReconnect == true)
                {
                    AutoReconnectTmr.Enabled = true;
                    AutoReconnectTmr.Start();
                }
            }
        }

        private void OnNeoVIReconnected(bool e)
        {
            if (NeoVIReconnected != null)
            {
                System.Diagnostics.Debug.WriteLine("Intrepid has reconnected");
                AutoReconnectTmr.Enabled = false;
                AutoReconnectTmr.Stop();
                NeoVIReconnected(this, e);
            }
        }

        private void OnCSMateReconnected(bool e)
        {
            if (CSMateReconnected != null)
            {
                System.Diagnostics.Debug.WriteLine("CSMate has reconnected");
                AutoReconnectTmr.Enabled = false;
                AutoReconnectTmr.Stop();
                CSMateReconnected(this, e);
            }
        }

        private void NeoConnectionCheckTmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!_device.IsConnected())
            {
                NeoConnectionCheckTmr.Enabled = false;
                NeoConnectionCheckTmr.Stop();
                OnNeoVIDisConnected(true);
            }
        }

        private void AutoReconnectTmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ea.GetEvent<AutoReconnectNeoEvent>().Publish("Auto-Reconnection in Progress");
            AutoReconnectTmr.Enabled = false;

            if (neoVIDeviceSelected == true)
            {
                System.Diagnostics.Debug.WriteLine("Intrepid Reconnect in progress");
                ConnectNeoVI();
                if (NeoVIConnected == true)
                {
                    OnNeoVIReconnected(true);
                    return;
                }
            }

            if (AutoReconnect == false)
            {
                AutoReconnectTmr.Enabled = false;
                AutoReconnectTmr.Stop();
            }

            AutoReconnectTmr.Enabled = true;
        }


        #endregion

        #region Helpers
        public string DataValue = "0";
        public object _lockme = new object();
        public string Syspwr(string Variable)
        {
            if (_device == null)
                return string.Empty;

            lock (_lockme)
            {
                DataValue = Variable;
                if (DataValue == "1")
                    ;
                var GBSignalData = new GlobalBDatabase(_device);
                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("SPMP_SysPwrModeAuth", DataValue);
                GBSignalData.sendSignals(dictionary_Signal_Val);
            }

            return DataValue;
        }

        #endregion

        #region Passive Mode

        public void EnablePassiveMode()
        {
            try
            {
                GBTimer.Stop();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Enable Failed : " + ex);
            }
        }

        public void DisablePassiveMode()
        {
            try
            {
                GBTimer.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Disable Failed : " + ex);
            }
        }

        #endregion

        public void SendCANMessage(uint ID, byte[] Data, int sizeData, bool extended)
        {
            _device.SendCANMessage(ID, Data, sizeData, extended, false, false);
        }
    }
}
