﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfCommon.Controls;

namespace WpfWizard
{
	/// <summary>
	/// Interaction logic for WizPgSample1.xaml
	/// </summary>
	public partial class WizPgSample2 : WizardPageBase
	{
		public string SampleData 
		{ 
			get 
			{ 
				return this.GetValue("SampleData"); 
			} 
		}

		public WizPgSample2()
		{
			this.InitializeComponent();
		}

		public WizPgSample2(string pageName, string shortName, string subtitle):base(pageName, shortName, subtitle)
		{
			this.InitializeComponent();
			this.DataContext = this;
		}

		public override void ButtonClick(object sender, ref RoutedEventArgs e)
		{
			if (sender is Button )
			{
				Button button = sender as Button;
				switch (button.Name)
				{
					case "btnNext" : 
						this.SharedData.AddUpdateItem("SampleDataChanged", string.Format("{0} for a different page", this.SampleData)); 
						break;
				}
				e.Handled = false;
			}
			else
			{
			}
		}

		protected override void OnVisibleChanged()
		{
		}

		/// <summary>
		/// Just a helper to determine if the shareddata string item exists before trying to get its 
		/// value, and returning a reasonable default value to avoid exception handling nastiness.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		private string GetValue(string name)
		{
			WizardSharedDataItem obj = this.SharedData.GetItem(name);
			string result = (obj == null) ? string.Empty : (string)obj.Value;
			return result;
		}

		public override void UpdateButtons()
		{
			this.ParentWiz.nextButtonPanel.IsEnabled = (this.cbEnableNext.IsChecked == true);
		}
		private void CheckBox_Checked(object sender, RoutedEventArgs e)
		{
			if (this.IsReady)
			{
				this.UpdateButtons();
			}
		}

	}
}
