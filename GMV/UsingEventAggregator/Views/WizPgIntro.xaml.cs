﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UsingEventAggregator.ViewModels;
using WpfCommon.Controls;

namespace WpfWizard
{
	/// <summary>
	/// Interaction logic for WizPgIntro.xaml
	/// </summary>
	public partial class WizPgIntro : WizardPageBase
	{
		public WizPgIntro()
		{
			this.InitializeComponent();
		}

		public WizPgIntro(string pageName, string shortName, string subtitle, object dataContext):base(pageName, shortName, subtitle)
		{
			this.InitializeComponent();			
			this.DataContext = dataContext;
		}

		protected override void OnVisibleChanged()
		{
		}
	}
}
