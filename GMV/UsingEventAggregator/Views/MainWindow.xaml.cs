﻿using Prism.Events;
using System.Windows;
using WpfWizard;

namespace UsingEventAggregator.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ConnectMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //WndWizard form = new WndWizard(this.DataContext) { Owner = this };
            //if (form.IsValid)
            //    form.Show();

            //mdiParent.Children.Clear();
            //mdiParent.Children.Add(new WPF.MDI.MdiChild() { Title = "Wizard", Content = new WndWizard(this.DataContext) { } });
        }

        private void ConnectMenuItem_Selected(object sender, RoutedEventArgs e)
        {
            (this.DataContext as ViewModels.MainWindowViewModel)?.ConnetCommand.Execute();
            //var selectedItem = menuSelector.SelectedItem as System.Windows.Controls.TreeViewItem;
            //selectedItem.IsSelected = false;
            //menuClose.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
        }

        private void DisconnectMenuItem_Selected(object sender, RoutedEventArgs e)
        {
            (this.DataContext as ViewModels.MainWindowViewModel)?.DisconnectCommand.Execute();
            //var selectedItem = menuSelector.SelectedItem as System.Windows.Controls.TreeViewItem;
            //selectedItem.IsSelected = false;
            //menuClose.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
        }
        private void SignalTestMenuItem_Selected(object sender, RoutedEventArgs e)
        {
            stkWizardPanel.Children.Clear();
            WndWizard form = new WndWizard(this.DataContext) { };
            stkWizardPanel.Children.Add(form);
            //var selectedItem = menuSelector.SelectedItem as System.Windows.Controls.TreeViewItem;
            //selectedItem.IsSelected = false;
            //menuClose.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
        }

        private void CloseMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuToggleButton.IsChecked = false;
        }

        private void ProfilePopup_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ExitPopup_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
