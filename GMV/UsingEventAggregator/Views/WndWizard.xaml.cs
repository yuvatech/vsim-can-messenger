﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfCommon.Controls;

namespace WpfWizard
{
    /// <summary>
    /// Interaction logic for WndWizard.xaml
    /// </summary>
    public partial class WndWizard : WizardWindowBase
    {
        public WndWizard(object dataContext) : base()
        {
            this.InitializeComponent();
            // this.DataContext = this;
            this.DataContext = dataContext;
            this.InitWizard();
        }

        public WndWizard(WizardConfig config, object dataContext) : base()
        {
            this.InitializeComponent();
            // this.DataContext = this;
            this.DataContext = dataContext;
            this.InitWizard(config);
        }

        /// <summary>
        /// Initialize the wizard control with our custom values (see WpfCommon.Controls.WizardConfig.cs 
        /// file for default values)
        /// </summary>
        protected override void InitWizard(WizardConfig config = null)
        {
            if (!this.isInitialized)
            {
                string sample = "VSIM EComm";
                WizPgIntro pg0 = new WizPgIntro("Connect", "page0", string.Empty, this.DataContext);
                WizPgSignal pg1 = new WizPgSignal("Signal", "page1", "Send Signals", this.DataContext);
                //WizPgSample2 pg2    = new WizPgSample2("VIN", "page2",  "Subtitle text for Sample Page 2" );
                //WizPgSample3 pg3    = new WizPgSample3("Sample Page 3", "page3",  "Subtitle text for conditional Page 3" );
                WizPgFinish pg99 = new WizPgFinish("VIN", "page99", "Save VIN Data", this.DataContext);
                this.Pages.Add(pg0);
                this.Pages.Add(pg1);
                //this.Pages.Add(pg2);
                //this.Pages.Add(pg3);
                this.Pages.Add(pg99);

                // Add the pages to the wizard control, and setup event hooks (see 
                // WpfCommon.Controls.WndWizardBase.cs). This method vcan be overridden.
                this.ConfigureWizard(this.gridMain);

                if (this.Wizard != null)
                {
#if __DEMO__
					// you only need this if you configure  the wizard from an externally created WizardConfig object
					if (config != null)
					{
						this.WizConfig.ShowFinishButton          = config.ShowFinishButton;
						this.WizConfig.ShowCancelButton          = config.ShowCancelButton;
						this.WizConfig.ShowNextButton            = config.ShowNextButton;
						this.WizConfig.ShowPrevButton            = config.ShowPrevButton;
						this.WizConfig.ShowResetButton           = config.ShowResetButton;
															     
						// nav panel options				     
						this.WizConfig.ShowNavPanel              = config.ShowNavPanel;
						this.WizConfig.ShowPage1OfN              = config.ShowPage1OfN;
						this.WizConfig.ContentBannerHeight       = config.ContentBannerHeight;
						this.WizConfig.NavListWidth              = config.NavListWidth;
						this.WizConfig.BannerTextAlignment       = config.BannerTextAlignment;
															     
						// using a banner image				     
						this.WizConfig.ShowBannerImage           = config.ShowBannerImage;
						this.WizConfig.BannerImageAlignment      = config.BannerImageAlignment;

						// colors
						this.WizConfig.BannerBackgroundColorName = config.BannerBackgroundColorName;
						this.WizConfig.BannerBorderColorName     = config.BannerBorderColorName;
						this.WizConfig.BannerSubtitleColorName   = config.BannerSubtitleColorName;
						this.WizConfig.BannerTitleColorName      = config.BannerTitleColorName;
					}
					else
#endif
                    {
                        // at this point, you can configure the WizardConfig object to setup general UI 
                        // appeaarance regarding header panel, buttons, and the page select listbox. Here 
                        // are the available properties
                        this.WizConfig.ShowFinishButton = true;
                        this.WizConfig.ShowCancelButton = true;
                        this.WizConfig.ShowNextButton = true;
                        this.WizConfig.ShowPrevButton = true;
                        this.WizConfig.ShowResetButton = true;

                        // nav panel options				     
                        this.WizConfig.ShowNavPanel = true;
                        this.WizConfig.ShowPage1OfN = true;
                        this.WizConfig.ContentBannerHeight = 80;
                        this.WizConfig.NavListWidth = 150;
                        this.WizConfig.BannerTextAlignment = System.Windows.HorizontalAlignment.Left;

#if __DEMO__
						// you can use either WPF color names, or hex values
						this.WizConfig.BannerBackgroundColorName = "LightSteelBlue";
						this.WizConfig.BannerBorderColorName     = "SteelBlue";
						this.WizConfig.BannerSubtitleColorName   = "Black";
						this.WizConfig.BannerTitleColorName      = "#FFFFFF";
#else
                        this.WizConfig.BannerBackgroundColor = Colors.LightSteelBlue;
                        this.WizConfig.BannerBorderColor = Colors.SteelBlue;
                        this.WizConfig.BannerSubtitleColor = Colors.Black;
                        this.WizConfig.BannerTitleColor = (Color)(ColorConverter.ConvertFromString("#FFFFFF"));
#endif

                        // using a banner image
                        WizConfig.ShowBannerImage = true;
                        WizConfig.BannerImageAlignment = System.Windows.HorizontalAlignment.Right;
                    }
                    // WizConfig.BannerImageFilePath  = WpfCommon.Globals.GetImageResourceName(Assembly.GetAssembly(this.GetType()).GetName().Name, "monkeyturkey.jpg");
                    this.Wizard.UpdatedConfig();

                    // add shared data - this is essentially an observable collection of key/value pairs, 
                    // where the key is a string that represents the name, and the value is any object
                    this.Wizard.SharedData.Add(new WizardSharedDataItem("SampleData", sample));
                }
            }
        }
    }
}
