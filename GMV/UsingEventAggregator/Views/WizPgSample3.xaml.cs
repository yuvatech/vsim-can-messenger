﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfCommon.Controls;

namespace WpfWizard
{
	/// <summary>
	/// Interaction logic for WizPgSample3.xaml
	/// </summary>
	public partial class WizPgSample3 : WizardPageBase
	{
		public string SampleData { get { return this.GetValue("SampleData"); } }
		public string SampleDataChanged 
		{ 
			get 
			{ 
				string data = this.GetValue("SampleDataChanged");
				return string.IsNullOrEmpty(data) ? "SharedData item not present" : data; 
			} 
		}

		public WizPgSample3()
		{
			this.InitializeComponent();
		}
		public WizPgSample3(string pageName, string shortName, string subtitle):base(pageName, shortName, subtitle)
		{
			this.InitializeComponent();
			this.DataContext = this;
		}

		protected override void OnVisibleChanged()
		{
			this.NotifyPropertyChanged("SampleDataChanged");
		}

		public override void ButtonClick(object sender, ref RoutedEventArgs e)
		{
			if (sender is Button)
			{
				Button button = sender as Button;
				switch (button.Name)
				{
					case "btnPrev" : 
						this.SharedData.DeleteItem("SampleDataChanged");
						break;
				}
				e.Handled = false;
			}
			else
			{
			}
		}

		private string GetValue(string name)
		{
			WizardSharedDataItem obj = this.SharedData.GetItem(name);
			string result = (obj == null) ? string.Empty : (string)obj.Value;
			return result;
		}
	}
}
