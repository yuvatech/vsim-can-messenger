﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfCommon.Controls;

namespace WpfWizard
{
    /// <summary>
    /// Interaction logic for WizPgSample1.xaml
    /// </summary>
    public partial class WizPgSignal : WizardPageBase
    {
        //public override string NextPage
        //{
        //	get
        //	{
        //		return (this.cbNextPage.IsChecked == true) ? "page3" : "page99";
        //	}
        //	set
        //	{
        //		base.NextPage = "";
        //	}
        //}

        public WizPgSignal()
        {
            this.InitializeComponent();
            (this.DataContext as UsingEventAggregator.ViewModels.MainWindowViewModel)._ea.GetEvent<UsingEventAggregator.Core.ConnectWindowEvent>().Subscribe((text) =>
            {
                if (this.ParentWiz.ActivePageIndex == 0)
                {
                    this.ParentWiz.UpdateForm(1);
                }
            });
        }

        public WizPgSignal(string pageName, string shortName, string subtitle, object dataContext) : base(pageName, shortName, subtitle)
        {
            this.InitializeComponent();
            this.DataContext = dataContext;
        }

        protected override void OnVisibleChanged()
        {
        }

    }
}
