﻿using System.Windows;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Unity;
using UsingEventAggregator.Views;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Collections.Specialized;
using System.Linq;
using System;

namespace UsingEventAggregator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public partial class App : PrismApplication
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(App));
        public static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            moduleCatalog.AddModule<ModuleA.GMVSenderModule>();
            moduleCatalog.AddModule<ModuleB.GMVRecieverModule>();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo(System.Configuration.ConfigurationManager.AppSettings["log4net-config-file"]));
            base.OnStartup(e);
        }


        #region ECOMate/VCUMate Configurations
        //public void EcoMateConfigurationSetup(bool device)
        //{
        //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //    var mateConfig = (NameValueCollection)ConfigurationManager.GetSection("device");
        //    if (!mateConfig.AllKeys.Any())
        //        return;

        //    #region HSCAN/LSCAN

        //    var ECOMateLSCAN = Convert.ToBoolean(mateConfig["gb_hscan"]);
        //    if (ECOMateLSCAN == true)
        //    {
        //        GBVehicleSim.SetVCUMateLSCAN(false);
        //    }
        //    else
        //    {
        //        this.chbLSCAN.Checked = true;
        //    }

        //    #endregion


        //    #region COMM ENABLE

        //    var comEnable = Convert.ToBoolean(mateConfig["com_enable"]);
        //    if (chbComEnable.Checked != comEnable)
        //    {
        //        this.chbComEnable.Checked = true;
        //    }

        //    #endregion

        //    #region Reverse Mode
        //    var reversemode = Convert.ToBoolean(mateConfig["reverse"]);
        //    if (chbReverse.Checked != reversemode)
        //    {
        //        this.chbReverse.Checked = true;
        //    }

        //    #endregion

        //    if (device)
        //    {
        //        #region USB Mode

        //        var ecomatesettings = mateConfig["mate_usb"].Split(new char[] { ',' }).ToList();
        //        foreach (var usbmode in ecomatesettings)
        //        {
        //            switch (usbmode.ToString().ToLower())
        //            {
        //                case "p34_top":
        //                    this.rbUSB1.Checked = true;
        //                    break;
        //                case "p34_bottom":
        //                    this.rbUSB2.Checked = true;
        //                    break;
        //                case "p35_adb":
        //                    this.rbUSB3.Checked = true;
        //                    break;
        //                case "p36":
        //                    this.rbUSB4.Checked = true;
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //        #endregion

        //        #region Power Switch

        //        var powerswitch1 = Convert.ToBoolean(mateConfig["power_switch1"]);
        //        if (chbPowerSwitch1.Checked != powerswitch1)
        //            this.chbPowerSwitch1.Checked = powerswitch1;

        //        var powerswitch2 = Convert.ToBoolean(mateConfig["power_switch2"]);
        //        if (chbPowerSwitch2.Checked != powerswitch2)
        //            this.chbPowerSwitch2.Checked = powerswitch2;

        //        var powerswitch3 = Convert.ToBoolean(mateConfig["power_switch3"]);
        //        if (chbPowerSwitch3.Checked != powerswitch3)
        //            this.chbPowerSwitch3.Checked = powerswitch3;

        //        var powerswitch4 = Convert.ToBoolean(mateConfig["power_switch4"]);
        //        if (chbPowerSwitch4.Checked != powerswitch4)
        //            this.chbPowerSwitch4.Checked = powerswitch4;

        //        #endregion

        //        #region HSCAN Termination
        //        var hscanTerm = mateConfig["hscan_termination"].Split(new char[] { ',' }).ToList();
        //        foreach (var hscanTermMode in hscanTerm)
        //        {
        //            switch (hscanTermMode.ToString().ToLower())
        //            {
        //                case "full":
        //                    this.rbFull.Checked = true;
        //                    break;
        //                case "stub":
        //                    this.rbStub.Checked = true;
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //        #endregion

        //    }
        //    else
        //    {
        //        #region USB Mode
        //        var usbsettings = mateConfig["mate_usb"].ToList();
        //        foreach (var usbmode in usbsettings)
        //        {
        //            switch (usbmode.ToString().ToLower())
        //            {
        //                case "type_a":
        //                    this.rbUSB1.Checked = true;
        //                    break;
        //                case "micro":
        //                    this.rbUSB2.Checked = true;
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //        #endregion

        //        #region Ethernet
        //        var ethernet = mateConfig["ethernet"].ToList();
        //        foreach (var ethernetmode in ethernet)
        //        {
        //            switch (ethernetmode.ToString().ToLower())
        //            {
        //                case "cgm":
        //                    this.chbPowerSwitch1.Checked = true;
        //                    break;
        //                case "rsi":
        //                    this.chbPowerSwitch2.Checked = true;
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //        #endregion

        //        #region GLOW PLUG
        //        var glowPlug = Convert.ToBoolean(mateConfig["glow_plug"]);
        //        if (this.chbGlowPlug.Checked != glowPlug)
        //        {
        //            this.chbGlowPlug.Checked = glowPlug;
        //        }
        //        #endregion

        //    }
        //}

        #endregion
    }
}
