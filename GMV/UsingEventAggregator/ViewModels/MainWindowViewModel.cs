﻿using Extension;
using GMVehicleMain;
using log4net;
using MessageDecodeManager;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using SIM.Core.Device;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using UsingEventAggregator.Core;
using VSIM.Core;
using VSIM.Core.Device;

namespace UsingEventAggregator.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        public IEventAggregator _ea;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _title = "Prism Unity Application";
        public DelegateCommand ConnetCommand { get; private set; }
        public DelegateCommand DisconnectCommand { get; private set; }

        public DelegateCommand SendSignalCommand { get; private set; }

        private ObservableCollection<Signal> _signals;
        public ObservableCollection<Signal> Signals
        {
            get { return _signals; }
            set { SetProperty(ref _signals, value); }
        }

        public ObservableCollection<string> _EnumSignal;
        public ObservableCollection<string> EnumSignal
        {
            get { return _EnumSignal; }
            set { SetProperty(ref _EnumSignal, value); }
        }

        public ObservableCollection<string> _EnumSignalInv;
        public ObservableCollection<string> EnumSignalInv
        {
            get { return _EnumSignalInv; }
            set { SetProperty(ref _EnumSignalInv, value); }
        }

        private ObservableCollection<Signal> _signalsInv;
        public ObservableCollection<Signal> SignalsInv
        {
            get { return _signalsInv; }
            set { SetProperty(ref _signalsInv, value); }
        }

        private bool _FlagConnectDevice = false;
        public bool FlagConnectDevice
        {
            get { return _FlagConnectDevice; }
            set { SetProperty(ref _FlagConnectDevice, value); }
        }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private Signal _SelectedSignal;
        public Signal SelectedSignal
        {
            get { return _SelectedSignal; }
            set
            {
                SetProperty(ref _SelectedSignal, value);
                loadValues();
            }
        }

        private Signal _SelectedSignalInv;
        public Signal SelectedSignalInv
        {
            get { return _SelectedSignalInv; }
            set
            {
                SetProperty(ref _SelectedSignalInv, value);
                loadInvValues();
            }
        }

        private System.Windows.Media.SolidColorBrush _ConnectionColor = System.Windows.Media.Brushes.Gray;
        public System.Windows.Media.SolidColorBrush ConnectionColor
        {
            get { return _ConnectionColor; }
            set
            {
                SetProperty(ref _ConnectionColor, value);
            }
        }

        private KeyValuePair<string, string> _SelectedSignalEnum;
        public KeyValuePair<string, string> SelectedSignalEnum
        {
            get { return _SelectedSignalEnum; }
            set { SetProperty(ref _SelectedSignalEnum, value); }
        }

        private KeyValuePair<string, string> _SelectedSignalInvEnum;
        public KeyValuePair<string, string> SelectedSignalInvEnum
        {
            get { return _SelectedSignalInvEnum; }
            set { SetProperty(ref _SelectedSignalInvEnum, value); }
        }

        private System.Windows.Visibility _ValueTextVisibility = System.Windows.Visibility.Collapsed;
        public System.Windows.Visibility ValueTextVisibility
        {
            get { return _ValueTextVisibility; }
            set { SetProperty(ref _ValueTextVisibility, value); }
        }

        private System.Windows.Visibility _ValueComboVisibility = System.Windows.Visibility.Visible;
        public System.Windows.Visibility ValueComboVisibility
        {
            get { return _ValueComboVisibility; }
            set { SetProperty(ref _ValueComboVisibility, value); }
        }

        private System.Windows.Visibility _ValueNumericVisibility = System.Windows.Visibility.Collapsed;
        public System.Windows.Visibility ValueNumericVisibility
        {
            get { return _ValueNumericVisibility; }
            set { SetProperty(ref _ValueNumericVisibility, value); }
        }

        private System.Windows.Visibility _InvRowVisibility = System.Windows.Visibility.Collapsed;
        public System.Windows.Visibility InvRowVisibility
        {
            get { return _InvRowVisibility; }
            set { SetProperty(ref _InvRowVisibility, value); }
        }

        private string _SignalStatusText;
        public string SignalStatusText
        {
            get { return _SignalStatusText; }
            set { SetProperty(ref _SignalStatusText, value); }
        }

        private double _SelectedSignalValue;
        public double SelectedSignalValue
        {
            get { return _SelectedSignalValue; }
            set { SetProperty(ref _SelectedSignalValue, value); }
        }

        private double _SelectedSignalInvValue;
        public double SelectedSignalInvValue
        {
            get { return _SelectedSignalInvValue; }
            set { SetProperty(ref _SelectedSignalInvValue, value); }
        }

        private string _SignalInvStatusText;
        public string SignalInvStatusText
        {
            get { return _SignalInvStatusText; }
            set { SetProperty(ref _SignalInvStatusText, value); }
        }

        private double _dValueMinRange;
        public double ValueMinRange
        {
            get { return _dValueMinRange; }
            set { SetProperty(ref _dValueMinRange, value); }
        }
        private double _dCycleTime;
        public double CycleTime
        {
            get { return _dCycleTime; }
            set { SetProperty(ref _dCycleTime, value); }
        }

        private double _dValueMaxRange;
        public double ValueMaxRange
        {
            get { return _dValueMaxRange; }
            set { SetProperty(ref _dValueMaxRange, value); }
        }

        private string _ConnectionStatusText = "Waiting for connection";
        public string ConnectionStatusText
        {
            get { return _ConnectionStatusText; }
            set { SetProperty(ref _ConnectionStatusText, value); }
        }

        private string _ActivityText;
        public string ActivityText
        {
            get { return _ActivityText; }
            set { SetProperty(ref _ActivityText, value); }
        }

        private ObservableCollection<string> _signalStatusCollection;
        public ObservableCollection<string> SignalStatusCollection
        {
            get { return _signalStatusCollection; }
            set { SetProperty(ref _signalStatusCollection, value); }
        }

        private string _signalStatus;
        public string SignalStatus
        {
            get { return _signalStatus; }
            set
            {
                SetProperty(ref _signalStatus, value);
                if (gmMain != null)
                    switch ((_signalStatus + "").ToLower())
                    {
                        case "acc":
                            gmMain.Acc();
                            break;
                        case "off":
                            gmMain.Off();
                            break;
                        case "crank":
                            gmMain.Crank();
                            break;
                        case "run":
                            gmMain.Run();
                            break;
                        case "propulsion":
                            gmMain.Propulsion();
                            break;
                    }
            }
        }

        public MainWindowViewModel(IEventAggregator ea)
        {
            _ea = ea;
            _ea.GetEvent<ConnectEvent>().Subscribe(OnDeviceConnected);
            _ea.GetEvent<DisconnectEvent>().Subscribe(OnDeviceDisconnected);
            _ea.GetEvent<AutoReconnectConnectEvent>().Subscribe(OnDeviceReconnecting);
            _ea.GetEvent<AutoReconnectFailedEvent>().Subscribe(OnDeviceReconnectFailed);
            _ea.GetEvent<MessageSentEvent>().Subscribe(OnMessageSend);
            _ea.GetEvent<GBTimerEvent>().Subscribe(OnGBTimerElapsed);
            ConnetCommand = new DelegateCommand(AttemptDeviceConnect);
            DisconnectCommand = new DelegateCommand(DisconnectDevice);
            SendSignalCommand = new DelegateCommand(SendMessageSignal);
            LoadSignals();

            SignalStatusCollection = new ObservableCollection<string>() { "Off", "Acc", "Run", "Crank", "Propulsion" };
        }

        private void OnDeviceReconnecting(string obj)
        {
            ActivityText = obj;
            ConnectionColor = System.Windows.Media.Brushes.Blue;
        }

        private void OnDeviceReconnectFailed(string obj)
        {
            ActivityText = obj;
            ConnectionColor = System.Windows.Media.Brushes.Red;
            ConnectDevice();
        }

        private void OnGBTimerElapsed(string obj)
        {
            ActivityText = obj;
            ConnectionColor = System.Windows.Media.Brushes.Blue;
        }

        private void OnMessageSend(string obj)
        {
            ActivityText = obj;
            ConnectionColor = System.Windows.Media.Brushes.Green;
        }

        private void OnDeviceConnected(string obj)
        {
            ActivityText = "Device connected";
            ConnectionColor = System.Windows.Media.Brushes.Green;
        }

        private void OnDeviceDisconnected(string obj)
        {
            ActivityText = "Device Disconnected";
            ConnectionColor = System.Windows.Media.Brushes.Red;
        }

        private void DisconnectDevice()
        {
            FlagConnectDevice = false;
            if (_deviceProxy != null)
                _deviceProxy.AllowCommunication = false;
            ConnectDisconnectDevice(DeviceOptions.EcoMate, false);
        }

        private void AttemptDeviceConnect()
        {
            FlagConnectDevice = true;
            if (_deviceProxy == null)
                _deviceProxy = new DeviceManager(_ea);

            if (_deviceProxy != null)
                _deviceProxy.AllowCommunication = true;
            ConnectDevice();
        }
        private void ConnectDevice()
        {

            //if (_deviceProxy != null)
            //    EcoMateSettings.GBInstance = _deviceProxy;
            if (FlagConnectDevice)
            {
                ConnectDisconnectDevice(DeviceOptions.EcoMate, true);
            }
            InitializeCANSystem();
            // EcoMateSettings.EcoMateConfigurationSetup(true);
        }

        private void ConnectDisconnectDevice(DeviceOptions device, bool connect, string port = "")
        {
            try
            {
                Status status = Status.NotConnected;
                if (connect)
                {
                    ConnectionStatusText = "Not Connected0";
                    ConnectionColor = System.Windows.Media.Brushes.Blue;
                    switch (device)
                    {
                        case DeviceOptions.TEST:
                            _deviceProxy.Connect(DeviceOptions.TEST);
                            break;
                        case DeviceOptions.PCAN:
                            var flag = _deviceProxy.Connect(DeviceOptions.PCAN, port);
                            break;
                        case DeviceOptions.PCANFD:
                            var flagFD = _deviceProxy.Connect(DeviceOptions.PCANFD, port);
                            break;
                        case DeviceOptions.EcoMate:
                            log.Debug("Initializing the connection to the device");
                            if (_deviceProxy.Connect(DeviceOptions.EcoMate, port))
                            {
                                ConnectionStatusText = "Connected";
                                ConnectionColor = System.Windows.Media.Brushes.Green;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    var flag = _deviceProxy.Disconnect();
                    ConnectionStatusText = "Not Connected1";
                    ConnectionColor = System.Windows.Media.Brushes.Red;
                    switch (device)
                    {
                        case DeviceOptions.PCAN:
                            break;
                        case DeviceOptions.EcoMate:
                            break;
                        default:
                            break;
                    }
                }

            }
            catch (Exception i)
            {
                ConnectionColor = System.Windows.Media.Brushes.Red;
            }
        }

        private void LoadSignals()
        {
            if (Signals == null)
                Signals = new ObservableCollection<Signal>();

            Signals.Clear();
            var signals = ARXML.GetSignals();
            Signals.AddRange(signals);
        }

        private void loadInvSignals(string name)
        {
            if (SignalsInv == null)
                SignalsInv = new ObservableCollection<Signal>();
            SignalsInv.Clear();
            MessageDecodeManager.Signal signalInvData = ARXML.FindSignalByName(name);
            SignalsInv.Add(signalInvData);
        }

        private void loadValues()
        {
            if (EnumSignal == null)
                EnumSignal = new ObservableCollection<string>();
            EnumSignal.Clear();
            MessageDecodeManager.Signal signal = ARXML.FindSignalByName(SelectedSignal?.Name);
            MessageDecodeManager.Message message = ARXML.FindMessageBySignal(signal);

            StringBuilder hex = new StringBuilder();
            hex.AppendFormat("{0:x2}", (int)Convert.ToInt64(message.Properties["Identifier"]));

            if (signal.Enums.Count > 0)
            {
                foreach (var item in signal.Enums)
                {
                    EnumSignal.Add(item.Value + " = " + item.Key);
                }
                ValueTextVisibility = System.Windows.Visibility.Collapsed;
                ValueNumericVisibility = System.Windows.Visibility.Collapsed;
                ValueComboVisibility = System.Windows.Visibility.Visible;

                SelectedSignalValue = double.Parse((signal.Enums != null && !string.IsNullOrWhiteSpace(signal.Enums.FirstOrDefault().Value)) ? signal.Enums.FirstOrDefault().Value : "0");

                SignalStatusText = "Signal Name = " + signal.Properties["longname"] + "\n\n CAN ID = 0x" + hex.ToString();
            }
            else
            {
                ValueTextVisibility = System.Windows.Visibility.Collapsed;
                ValueNumericVisibility = System.Windows.Visibility.Visible;
                ValueComboVisibility = System.Windows.Visibility.Collapsed;

                // Tod: fix this!
                var min = signal.Properties["min"];
                var max = signal.Properties["max"];

                double dVal = double.MinValue;
                double dValMax = double.MaxValue;

                if (double.TryParse(min, out dVal))
                    ValueMinRange = dVal;
                if (double.TryParse(max, out dValMax))
                    ValueMaxRange = dValMax;

                SelectedSignalValue = dValMax;

                SignalStatusText = "Signal Name = " + signal.Properties["longname"] + "\n\n CAN ID = 0x" + hex.ToString() + "\n\nRange = " + signal.Properties["min"] + " to " + signal.Properties["max"];
            }

            SelectedSignalEnum = signal.Enums.FirstOrDefault();

            if (SignalsInv == null)
                SignalsInv = new ObservableCollection<Signal>();
            SignalsInv.Clear();
            if (EnumSignalInv == null)
                EnumSignalInv = new ObservableCollection<string>();
            EnumSignalInv.Clear();

            //check invalidity
            string signalinv = SelectedSignal?.Name + "_Inv";
            MessageDecodeManager.Signal signalInvData = ARXML.FindSignalByName(signalinv);
            InvRowVisibility = System.Windows.Visibility.Collapsed;
            if (signalInvData == null)
            {
                InvRowVisibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                InvRowVisibility = System.Windows.Visibility.Visible;
                SignalsInv.Clear();

                SignalsInv.Add(signalInvData);

                SelectedSignalInv = SignalsInv.FirstOrDefault();

                // CanSignal.comboBox3.Items.Add(signalInvData.Properties["name"]);                
            }
        }

        private void loadInvValues()
        {
            MessageDecodeManager.Signal signalInvData = ARXML.FindSignalByName(SelectedSignalInv?.Name);

            if (EnumSignalInv == null)
                EnumSignalInv = new ObservableCollection<string>();
            EnumSignalInv.Clear();

            if (signalInvData != null)
            {
                if (signalInvData.Enums != null)
                {
                    foreach (var item in signalInvData.Enums)
                    {
                        EnumSignalInv.Add(item.Value + " = " + item.Key);
                    }

                    SelectedSignalInvValue = double.Parse((signalInvData.Enums != null && !string.IsNullOrWhiteSpace(signalInvData.Enums.FirstOrDefault().Value)) ? signalInvData.Enums.FirstOrDefault().Value : "0");
                }
            }
        }

        DeviceManager _deviceProxy = null;
        GlobalBDatabase _gbDatabases;
        // BackgroundWorker _BackgroundWorker;
        GMClassMainTx gmMain = null;
        SockServer socketServer = null;

        private bool _sendSignalHandlerExit = false;
        private Thread _sendSignalHandler;

        private void InitializeCANSystem()
        {
            if (gmMain == null)
            {
                gmMain = new GMClassMainTx(this._deviceProxy, this._ea);
                gmMain.AutoReconnect = true;
                log.Debug("Initialized GMClassMain");
            }

            if (_gbDatabases == null)
            {
                _gbDatabases = new GlobalBDatabase(this._deviceProxy);
                log.Debug("Initialized GlobalBDatabase");
            }
            //Socket Interface
            if (socketServer == null) // was being called over and over!
            {
                var socketConfig = (NameValueCollection)ConfigurationManager.GetSection("application");
                var temp = socketConfig["port"];
                var port = (int)Convert.ToInt32(temp);

                socketServer = new SockServer(null, _gbDatabases, port);
                socketServer.OnSignalReceived += sockServer_OnSignalReceived;
                socketServer.Start();
            }
        }

        void sockServer_OnSignalReceived(Packet packet)
        {
            if (packet.Type.ToUpper() != Packet.Types[2].ToUpper()) return;
            var command = packet.Name.ToLower().Trim();
            var value = Convert.ToString(packet.Value).ToLower().Trim();
            switch (command)
            {
                case "mode":
                    if (value.Contains("passive"))
                    {
                        //this.Invoke((MethodInvoker)delegate { chbPassiveMode.Checked = true; });
                    }
                    else if (value.Contains("active"))
                    {
                        //this.Invoke((MethodInvoker)delegate { chbPassiveMode.Checked = false; });
                    }
                    if (value.Contains("off"))
                    {
                        //gmMain.Off();
                        SignalStatus = "Off";
                        //this.Invoke((MethodInvoker)delegate { this.radioButton1.Checked = true; });
                    }
                    else if (value.Contains("acc"))
                    {
                        //gmMain.Acc();
                        SignalStatus = "Acc";
                        //this.Invoke((MethodInvoker)delegate { this.radioButton2.Checked = true; });
                    }
                    else if (value.Contains("run"))
                    {
                        //gmMain.Run();
                        SignalStatus = "Run";
                        //this.Invoke((MethodInvoker)delegate { this.radioButton3.Checked = true; });
                    }
                    else if (value.Contains("crank"))
                    {
                        //gmMain.Crank();
                        SignalStatus = "Crank";
                        //this.Invoke((MethodInvoker)delegate { this.radioButton4.Checked = true; });
                    }
                    else if (value.Contains("prop"))
                    {
                        //gmMain.Propulsion();
                        SignalStatus = "Propulsion";
                        //this.Invoke((MethodInvoker)delegate { this.rbPropulsion.Checked = true; ; });
                    }
                    else if (value.Contains("reboot"))
                    {
                        //this.Invoke((MethodInvoker)delegate { this.EcoMateSettings.PowerCycle(null, null); });
                    }
                    break;
                case "ecomate_usb":
                    if (value.Contains("p34_top"))
                    {
                        //this.Invoke((MethodInvoker)delegate { this.EcoMateSettings.rbUSB1.Checked = true; });
                    }
                    else if (value.Contains("p34_bottom"))
                    {
                        //this.Invoke((MethodInvoker)delegate { this.EcoMateSettings.rbUSB2.Checked = true; });
                    }
                    else if (value.Contains("p35_adb"))
                    {
                        //this.Invoke((MethodInvoker)delegate { this.EcoMateSettings.rbUSB3.Checked = true; });
                    }
                    else if (value.Contains("p36"))
                    {
                        //this.Invoke((MethodInvoker)delegate { this.EcoMateSettings.rbUSB4.Checked = true; });
                    }
                    break;
                default:
                    break;
            }
        }

        private void SendMessageSignal()
        {
            try
            {
                log.Debug("Initiating sending of message");
                ConnectionColor = System.Windows.Media.Brushes.Blue;
                ActivityText = "Initiating sending of signal";
                var dictionary_Signal_Val = new Dictionary<string, string>();

                // This magic is taking the selected value for the signals and adding to the signal list
                //if (CanSignal.cmbValues.Visible)
                //{
                //    string[] val = CanSignal.cmbValues.SelectedItem.ToString().Split('=');
                //    dictionary_Signal_Val.Add(CanSignal.comboBox1.SelectedItem.ToString(), val[0].Trim());

                //}
                //else
                //{
                //    dictionary_Signal_Val.Add(CanSignal.comboBox1.SelectedItem.ToString(),
                //        CanSignal.nudCanRange.Value.ToString());
                //}

                // SelectedSignal
                dictionary_Signal_Val.Add(SelectedSignal.Name, SelectedSignalValue.ToString());

                if (InvRowVisibility == System.Windows.Visibility.Visible)
                {
                    dictionary_Signal_Val.Add(SelectedSignalInv.Name, SelectedSignalInvValue.ToString());
                }

                //*******************************************************************************************//
                // Special cases for timed messages
                if (SelectedSignal.Name == "IntDimDspLvl")
                {
                    int val;
                    if (int.TryParse(SelectedSignalValue.ToString(), out val))
                        gmMain.DimDspLvl = val;
                    else
                        gmMain.DimDspLvl = 126;

                }

                if (SelectedSignal.Name == "IntDimLvl")
                {
                    int val;
                    if (int.TryParse(SelectedSignalValue.ToString(), out val))
                        gmMain.DimLvl = val;
                    else
                        gmMain.DimLvl = 96;
                }

                if (SelectedSignal.Name == "VehIdNmDig1")
                {
                    int val;
                    if (int.TryParse(SelectedSignalValue.ToString(), out val))
                        gmMain.VinDig1 = val;
                    else
                        gmMain.VinDig1 = 0;
                }

                if (SelectedSignal.Name.ToUpper() == "SPMP_SysPwrModeAuth".ToUpper())
                {
                    //var vals = CanSignal.cmbValues.SelectedItem.ToString().Split('=');
                    //var val = vals[0].Trim();
                    gmMain.Syspwr(SelectedSignalValue.ToString());
                    //rbstat = (uint)Convert.ToUInt16(SelectedSignalValue.ToString());
                    //RestoreSysRadioButtonStat();
                }

                if (SelectedSignal.Name.ToString() == "VehIdNmDig1")
                {
                    int val;
                    if (int.TryParse(SelectedSignalValue.ToString(), out val))
                        gmMain.VinDig1 = val;
                    else
                        gmMain.VinDig1 = 0;
                }
                //*******************************************************************************************//

                //CanSignal.comboBox1.Enabled = false;
                //CanSignal.comboBox3.Enabled = false;
                //CanSignal.comboBox4.Enabled = false;
                //CanSignal.nudCanRange.Enabled = false;
                //CanSignal.cmbValues.Enabled = false;

                var cycleTime = CycleTime;
                var Config = (NameValueCollection)ConfigurationManager.GetSection("application");
                ActivityText = "Generated payload";
                Thread.Sleep(10);
                ActivityText = "Sending signal";
                if (_gbDatabases.sendSignals(dictionary_Signal_Val))
                {
                    ConnectionColor = System.Windows.Media.Brushes.Green;
                    ActivityText = "Device Active";
                    log.Debug("Completed sending of message");
                }
                else
                {
                    ConnectionColor = System.Windows.Media.Brushes.Red;
                    ActivityText = "Signal Failed";
                    log.Debug("Failed to send signal");
                }
            }
            catch (Exception exMsg)
            {
                ConnectionColor = System.Windows.Media.Brushes.Red;
                ActivityText = "Error sending signal";
                log.Error("Exception sending message", exMsg);
                throw;
            }
        }
    }
}
