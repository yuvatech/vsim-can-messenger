﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDLC
{
    public class Transform
    {
        public delegate void ReceviedHandler(byte[] data);
        public event ReceviedHandler OnReceived;
        private ECOMate mEcoMateHdlc = new ECOMate();
        private ushort _length = 0;

        public void Reset()
        {
            this._length = 0;
        }
        
        public ushort EncodeFrame(byte[] inData, ushort length, ref byte[] outData)
        {
            var buff = new byte[length];
            for (var i = 0; i < length; i++)
                buff[i] = inData[i];
            var outBuff = new byte[inData.Length + byte.MaxValue];

            ushort ret = mEcoMateHdlc.HdlcEncodeFrame(buff, length, ref outBuff);

            for (var i = 0; i < ret; i++)
                outData[i] = outBuff[i];

            return ret; // outData length
        }

        public ushort DecodeFrame(byte[] inData, ref byte[] outData)
        {
            ushort deframeStatus= ECOMate.FRAME_INCOMPLETE;
            byte[] outBuff = new byte[byte.MaxValue];
            ushort dataLen = (ushort)inData.Length;

            for (var i = 0; i < inData.Length; i++)
            {
                deframeStatus = mEcoMateHdlc.HdlcDecodeFrame(inData[i], ref outBuff, ref dataLen);

                if (deframeStatus == ECOMate.FRAME_COMPLETE)
                {
                    outData = new Byte[dataLen];
                    //Frame is complete. 			
                    for (var x = 0; x < dataLen; x++)
                        outData[x] = outBuff[x];

                    if (OnReceived != null)
                        OnReceived(outData);
                }
            }

            return deframeStatus;
        }
    }
}
