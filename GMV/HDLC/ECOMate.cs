﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HDLC
{
    internal class ECOMate
    {
        public const ushort HDLC_FRAME_HEADER_LEN = (3);		/**< HDLC Header Length >**/

        public const byte HDLC_FLAG = (0x7E);	/**< Hdlc frames STX    >**/
        public const byte HDLC_ESC = (0x7D);	/**< Hdlc frames ETX    >**/
        public const byte HDLC_ESC_M = (0x20);

        public const byte INVALID_SEQ = (0x07);	/**< HDLC Header Length >**/

        public const byte START_FLAG_LEN = (1);
        public const byte END_FLAG_LEN = (1);

        public const ushort IPC_BUFFER_SIZE = (125);
        public static readonly ushort RTXQ_BUFLEN = (ushort)(IPC_BUFFER_SIZE + HDLC_FRAME_HEADER_LEN);
        public static readonly ushort HDLC_FRAME_MAXLEN = (ushort)(IPC_BUFFER_SIZE + HDLC_FRAME_HEADER_LEN);	/**< max length of Hdlc frame without bytestuffing (header+data) */
        public static readonly ushort HDLC_FRAME_MINLEN = (ushort)(HDLC_FRAME_HEADER_LEN);			/**< min length of Hdlc frame */

        /* Frame Status */
        public const ushort FRAME_COMPLETE = (0x100);
        public const ushort FRAME_INCOMPLETE = (0x101);

        public const byte LLD_RX_MODE_SYNC = (0x00);
        public const byte LLD_RX_MODE_GET_DATA = (0x10);

        /*
        struct HdlcHeader{
		    uint8_t addr : 8;           //not used
		    uint8_t ctrl_sendseq : 7;   //sending sequence number
		    uint8_t ctrl_i_s : 1;       //used to determine if it's an I/S frame, not really used
		    uint8_t ctrl_recvseq : 7;   //received sequence number
		    uint8_t ctrl_p_s : 1;       //poll/final bit: not used
	    };*/

        public ushort HdlcEncodeFrame(byte[] inData, ref byte[] outData)
        {
            return HdlcEncodeFrame(inData, (ushort)inData.Length, ref outData);
        }

        private byte sendSeq = 0;
        private bool uFirstFrameSent = true;
        public ushort HdlcEncodeFrame(byte[] inData, ushort length, ref byte[] outData)
        {
            ushort wDecodeIndex = 0;
            ushort wEncodeIndex = 0;

            byte uTmpVal;
            ushort crc;
            if (length >= RTXQ_BUFLEN)
                return wEncodeIndex;

            // packed manually !!!!!!!!!!!!!!!!!!!!!
            byte[] packed = new byte[HDLC_FRAME_HEADER_LEN];
            // Add leading flag for the first frame
            if (uFirstFrameSent)
            {
                uFirstFrameSent = false;
                packed[0] = (byte)HDLC_FLAG;
                packed[1] = (byte)((sendSeq & 0x7F) | ((0 << 8) & 0x80));  // ctrl_sendseq 7-bit, ctrl_i_s 1-bit
                packed[2] = (byte)((0x00 & 0x7F) | ((0 << 8) & 0x80)); // ctrl_recvseq  7-bit, ctrl_p_s 1-bit
            }
            else
            {
                packed[0] = 0; // address 8-bit
                packed[1] = (byte)((sendSeq & 0x7F) | ((0 << 8) & 0x80));  // ctrl_sendseq 7-bit, ctrl_i_s 1-bit
                packed[2] = (byte)((0x00 & 0x7F) | ((0 << 8) & 0x80)); // ctrl_recvseq  7-bit, ctrl_p_s 1-bit
            }

            // add header to byte message
            foreach(var pack in packed)
               outData[wEncodeIndex++] = pack; 

            crc = CRC16.ccitt(0, packed, packed.Length);
            crc = CRC16.ccitt(crc, inData, length);
            // Add data, escaping when necessary (7E to 7E-7E)
            // DC3--XOFF--19-13(hex) ctrl-s
            // DC1--XON---17-11(hex) ctrl-q
            while (wDecodeIndex < length)
            {
                uTmpVal = inData[wDecodeIndex++];
                if ((uTmpVal == HDLC_FLAG) || (uTmpVal == HDLC_ESC))
                {
                    uTmpVal ^= HDLC_ESC_M;
                    outData[wEncodeIndex++] = HDLC_ESC;
                }
                outData[wEncodeIndex++] = uTmpVal;
            }

            // Add trailing flag 7E
            uTmpVal = (byte)(crc & 0x00FF);
            if ((uTmpVal == HDLC_FLAG) || (uTmpVal == HDLC_ESC))
            {
                uTmpVal ^= HDLC_ESC_M;
                outData[wEncodeIndex++] = HDLC_ESC;
            }
            outData[wEncodeIndex++] = uTmpVal;

            uTmpVal = (byte)((crc & 0xFF00) >> 8);
            if ((uTmpVal == HDLC_FLAG) || (uTmpVal == HDLC_ESC))
            {
                uTmpVal ^= HDLC_ESC_M;
                outData[wEncodeIndex++] = HDLC_ESC;
            }
            outData[wEncodeIndex++] = uTmpVal;
            outData[wEncodeIndex++] = HDLC_FLAG;
            sendSeq++;
            if (HDLC_ESC == sendSeq)
                sendSeq = 0;

            return wEncodeIndex;
        }

        private static byte uRxMode = 0;
        private static ushort wRxBuffIndex = 0;
        private static byte uEscByte = 0;
        public ushort HdlcDecodeFrame(byte inData, ref byte[] outData, ref ushort length)
        {
            ushort xMsgStat = FRAME_INCOMPLETE;
            ushort crc;
            ushort mcrc;

            switch (uRxMode)
            {
                case LLD_RX_MODE_SYNC:
                    {
                        if (HDLC_FLAG == inData)
                        {
                            uRxMode = LLD_RX_MODE_GET_DATA;
                            wRxBuffIndex = 0;
                        }
                        break;
                    }
                case LLD_RX_MODE_GET_DATA:
                    {
                        switch (inData)
                        {
                            /**< end of frame flag 0x7e */
                            case HDLC_FLAG:
                                {
                                    if (wRxBuffIndex < HDLC_FRAME_MINLEN)
                                    {
                                        /**< HDLC Frame length too small, discard the frame and look for the next start */
                                        wRxBuffIndex = 0;
                                        uEscByte = 0; // false
                                        break;
                                    }
                                    crc = CRC16.ccitt(0, outData, wRxBuffIndex - 2);
                                    // 2 bytes or 16-bit
                                    mcrc = (ushort)(((outData[wRxBuffIndex - 1] << 8) & 0xFF00) | ((outData[wRxBuffIndex - 2]) & 0x00FF));
                                    if (crc != mcrc)
                                    {
                                        //bad message
                                        wRxBuffIndex = 0;
                                        uEscByte = 0; // false
                                        break;
                                    }

                                    length = (ushort)(wRxBuffIndex - 2); //discount CRC bytes

                                    xMsgStat = FRAME_COMPLETE;
                                    wRxBuffIndex = 0;
                                    uEscByte = 0; // false
                                }
                                break;
                            /**< escape character */
                            case HDLC_ESC:
                                {
                                    uEscByte = 1;
                                    break;
                                }
                            /**< normal data */
                            default:
                                {
                                    if (wRxBuffIndex >= HDLC_FRAME_MAXLEN)
                                    {
                                        /**< Max frame length exceeded, discard the frame and look for the next start */
                                        wRxBuffIndex = 0;
                                        break;
                                    }

                                    if (1 == uEscByte) // true
                                    {
                                        inData ^= HDLC_ESC_M;
                                        uEscByte = 0; // false
                                    }
                                    outData[wRxBuffIndex++] = inData;
                                    break;
                                }
                        }
                        break;
                    }
                default:
                    {
                        uRxMode = LLD_RX_MODE_GET_DATA;
                        wRxBuffIndex = 0;
                        break;
                    }
            }

            return xMsgStat;
        }
    }
}
