﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Utility;
using VSIM.Core.Device;

namespace DeviceLibrary
{
#if UNIX
    public class SecureMessage
    {
        public SecureMessage()
        {
        }
        
        public Message[] Process(Message message, ref bool isSecure)
        {
            return new[] { message };
        }
    }
#else
    /*
     * MAC Secure Message
     * CYS2224: Message Authentication Testing Tool User Manual
     *
     * References(s):
     *
     * CYS MAC Library: https://documents.naeng.gm.com/gdm3/drl/objectId/090163bc83fe4b63/chronicleId/090163bc83f51d71 
     * MAC and Key provisioning User Guide: https://documents.naeng.gm.com/gdm3/drl/objectId/090163bc8437456a/chronicleId/090163bc83f51d70 
     * Unlock Utility Kit:  https://documents.naeng.gm.com/gdm3/drl/objectId/090163bc83f5215d/chronicleId/090163bc83f51d6c 
     * Unlock User Guide: https://documents.naeng.gm.com/gdm3/drl/objectId/090163bc83f51d8e/chronicleId/090163bc83f51d27
     *
     * The library will ONLY work with ECUs that have zero keys or known non-zero keys provisioned for message authentication.
     *
     * MACT file derived from ECU extract (found ?)
     *
     * IMPORTANT:  We not care to check if Rx is valid, we will assume is good, we only care to Tx correctly!
     */
    public class SecureMessage
    {
        private List<string> _mactList;

        private IntPtr _pointer = IntPtr.Zero;
        private const string LIBRARY_PATH = @"cysmac.dll";
        private byte[] _cntr_1 = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private byte[] _cntr_2 = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private int _counter = 0; // stored global count to know when need to re-sync or whatever

        // Function pointer prototypes
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_MA_init(string mactFile, string logTxtFile, int sspOption, int log_option);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_MA_Tx_Prepare(int MsgType, UInt32 CANID, byte[] payloadIn, Int32 payload_Inlen,
            byte[] payloadOut, ref Int32 payload_Outlen); //Prototype

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_MA_Rx_Verify(int MsgType, UInt32 CANID, byte[] payloadIn, int payload_Inlen);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_MA_SetCounter(int MsgType, UInt32 MsgID, byte[] cntrBytes, int len);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_MA_GetCounter(int MsgType, UInt32 MsgID, byte[] cntrBytes);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_MA_IncrCounterSync(int MsgType, UInt32 MsgID);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int fptr_GenerateRpt(string rptFileName);

        fptr_MA_init ptrInit = null;
        fptr_MA_Tx_Prepare ptr_MAC_Tx_Prepare = null;
        fptr_MA_Rx_Verify ptr_MAC_Rx = null;
        fptr_MA_SetCounter ptr_SetCounter = null;
        fptr_MA_GetCounter ptr_GetCounter = null;
        fptr_MA_IncrCounterSync ptr_IncrCounter = null;
        fptr_GenerateRpt ptr_GenerateRpt = null;

        public SecureMessage(string MACT_filepath="MACT.csv", bool log = false)
        {
            _pointer = NativeMethods.LoadLibrary(LIBRARY_PATH);
            if (_pointer == IntPtr.Zero)
                throw new Exception("Failed to load DLL " + LIBRARY_PATH);

            var pAddress = NativeMethods.GetProcAddress(_pointer, "MA_Init");
            ptrInit = (fptr_MA_init) Marshal.GetDelegateForFunctionPointer(pAddress, typeof(fptr_MA_init));

            pAddress = NativeMethods.GetProcAddress(_pointer, "MA_Tx_Prepare");
            ptr_MAC_Tx_Prepare =
                (fptr_MA_Tx_Prepare) Marshal.GetDelegateForFunctionPointer(pAddress, typeof(fptr_MA_Tx_Prepare));

            pAddress = NativeMethods.GetProcAddress(_pointer, "MA_Rx_Verify");
            ptr_MAC_Rx = (fptr_MA_Rx_Verify) Marshal.GetDelegateForFunctionPointer(pAddress, typeof(fptr_MA_Rx_Verify));

            pAddress = NativeMethods.GetProcAddress(_pointer, "MA_SetCounter");
            ptr_SetCounter =
                (fptr_MA_SetCounter) Marshal.GetDelegateForFunctionPointer(pAddress, typeof(fptr_MA_SetCounter));

            pAddress = NativeMethods.GetProcAddress(_pointer, "MA_GetCounter");
            ptr_GetCounter =
                (fptr_MA_GetCounter) Marshal.GetDelegateForFunctionPointer(pAddress, typeof(fptr_MA_GetCounter));

            pAddress = NativeMethods.GetProcAddress(_pointer, "MA_IncrCounterSync");
            ptr_IncrCounter =
                (fptr_MA_IncrCounterSync) Marshal.GetDelegateForFunctionPointer(pAddress,
                    typeof(fptr_MA_IncrCounterSync));

            pAddress = NativeMethods.GetProcAddress(_pointer, "MA_Generate_Report");
            ptr_GenerateRpt =
                (fptr_GenerateRpt) Marshal.GetDelegateForFunctionPointer(pAddress, typeof(fptr_GenerateRpt));

            if (ptrInit == null ||
                ptr_MAC_Tx_Prepare == null ||
                ptr_MAC_Rx == null ||
                ptr_SetCounter == null ||
                ptr_GetCounter == null ||
                ptr_IncrCounter == null ||
                ptr_GenerateRpt == null)
                throw new Exception("Procedures in DLL not fully marshalled!");

            var logFilepath =  Path.Combine(Utility.Helper.AssemblyDirectory, "secure_message.log");
            /*
            if (File.Exists(logFilepath))  // clean-up
                File.Delete(logFilepath);
            */

            var logValue = 0; // disabled
            if (log)
                logValue = 1; // enabled

            _mactList = File.ReadAllLines(MACT_filepath).ToList();
            _mactList.RemoveAt(0); // pop off header

            ptrInit(MACT_filepath, logFilepath, 0, logValue);  // mact_file, log_txt_file, ssp_option=0, log_option=0
        }
        ~SecureMessage()
        {
            try
            {
                NativeMethods.FreeLibrary(this._pointer);
            }
            catch (Exception)
            {
                // eat it!, trying to die anyway...
            }

        }

        public Message[] Process(Message message, ref bool isSecure)
        {
            var messages = new List<Message>();

            isSecure = false;

            var synchronize = false;
            var authenticate = false;

            // de-couple a little from message type (in-case shared)
            var id = message.Id;
            var extended = message.Extended;
            var data = message.Data;
            var dataLength = message.Length;

            var buffer = new byte[255 * 4]; // for sure is big enough, but memory is cheap :)
            var status = 0;

            var type = 1; // 11 bit CAN Id
            if (extended)
                type = 2; // extended id 29 bit

            uint authenticate_id = 0x00;
            uint sync_id = 0x00;

            // Step 1:  find if authentication or sync id based on MACT file
            foreach (var line in _mactList)
            {
                // decode MACT file for desired information                
                var temp = "";
                var split = line.Split(new[] {','});
                temp = split[0].Substring(2, split[0].Length - 2); //  first byte is for type of message
                authenticate_id = uint.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                temp = split[3].Substring(2, split[0].Length - 2); //  first byte is for type of message
                sync_id = uint.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);

                if (sync_id == id)
                {
                    synchronize = true;
                    break;
                }

                if (authenticate_id == id)
                {
                    authenticate = true;
                    break;
                }
            }

            // if nothing to do with secure messages then return original
            if (!authenticate && !synchronize)
                return new[] { message };
            isSecure = true;                    

            // if sync id  set new counter, else reset sync set to 0
            // this is used with RX'ing secure data, do not care unless want to verify against later....
            if (synchronize)
            {
                if (sync_id == id)
                {
                    status = ptr_SetCounter(type, id, data, dataLength);  // 64-bit counter (i.e. all 8 bytes)
                }
                else
                {
                    var temp = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // zero
                    status = ptr_SetCounter(type, id, temp, temp.Length);
                }

                if (status < 0)
                    throw new Exception("Synchronize Counter");
            }
            else
            {
                status = ptr_IncrCounter(type, id);
                if (status < 0)
                    throw new Exception("Increment Counter");
            }

            status = ptr_GetCounter(type, id, _cntr_2);
            if (status < 0)
                throw new Exception("Get Counter");
            var countBytes = _cntr_2;

            // convert counter bytes to int    
            var count = (int)BitConverter.ToInt32(countBytes, 0);
            AliveRollingCounter(message.Id, count);

            if (count == 0 || ((count % 21) == 0)) // at init and every 16 messages send counter sync
            {
                // always 64-bit
                messages.Add(new Message(){Id=sync_id, Data = countBytes, Length = countBytes.Length,Network = message.Network });
            }

            // create authenticated message to transmit
            if (authenticate)
            {
                var length = 0;
                var mactLength = MACT_Length(type, message); // get how many bytes make MAC information
                if (mactLength >= 0)
                {
                    for (var x = 0; x < mactLength; x++) // clear out old MACT data
                        message.Data[x] = 0;
                    // extract actual data trying to be sent, this is done because in ARXML MACT information already defined in message as signals
                    var data_raw = data.ToList().GetRange(mactLength, dataLength - mactLength).ToArray();
                    status = ptr_MAC_Tx_Prepare(type, id, data_raw, data_raw.Length, buffer, ref length);
                    if (status > 0)
                        throw new Exception("TX prepared message failed!");

                    // place MACT information into message (in-front)
                    for (var x = 0; x < mactLength; x++)
                        message.Data[x] = buffer[x];

                    var temp = new Message(message);
                    temp = TryManualCalculations(temp);
                    messages.Add(temp); // with MAC added
                }
                else
                {
                    throw new ArgumentOutOfRangeException(string.Format("MACT Length {0}, incorrect!", mactLength));
                }
            }
            return messages.ToArray();
        }

        private object _rollingCountersLock = new object();
        private Dictionary<uint, uint> _rollingCounters = new Dictionary<uint, uint>();
        private int AliveRollingCounter(uint id, int count=-1)
        {
            lock (_rollingCountersLock)
            {
                if (count < 0)
                {
                    if (_rollingCounters.ContainsKey(id))
                        return (int)_rollingCounters[id];
                }
                else
                {
                    if (!_rollingCounters.ContainsKey(id))
                        _rollingCounters.Add(id, (uint)count);
                    else
                        _rollingCounters[id] = (uint)count;
                }
            }
            return count;
        }
        
        public static string ToBin(uint value, int len)
        {
            string s = Convert.ToString(value, 2);
            string raw = (new string('0', len - s.Length)) + s;

            return raw;

            //return (len > 1 ? ToBin(value >> 1, len - 1) : null) + "01"[value & 1];
        }
        
        static string addBinary(string a, string b)
        {

            // Initialize result 
            string result = "";

            // Initialize digit sum 
            int s = 0;

            // Traverse both strings starting  
            // from last characters 
            int i = a.Length - 1, j = b.Length - 1;
            while (i >= 0 || j >= 0 || s == 1)
            {

                // Comput sum of last  
                // digits and carry 
                s += ((i >= 0) ? a[i] - '0' : 0);
                s += ((j >= 0) ? b[j] - '0' : 0);

                // If current digit sum is  
                // 1 or 3, add 1 to result 
                result = (char)(s % 2 + '0') + result;

                // Compute carry 
                s /= 2;

                // Move to next digits 
                i--; j--;
            }
            return result;
        }

        private Message TryManualCalculations(Message message)
        {
            var pv = 0U;

            if (message.Id == 0x306) // Too:  check if "message" has MEC signals and calculate, just look for _PV?
            {
                var signals = MessageDecodeManager.ARXML.Decode(message.Id, message.Data);

                // values needed from the IT Security DLL
                var temp = signals.FirstOrDefault(s => s.Name.ToLower() == "SPMP_SysPwrModeAuth".ToLower());
                var SPMP_SysPwrModeAuth = Convert.ToUInt32(temp.Value);

                temp = signals.FirstOrDefault(s => s.Name.ToLower() == "SPMP_SysPwrModeAuth_Inv".ToLower());
                var SPMP_SysPwrModeAuth_Inv = Convert.ToUInt32(temp.Value);

                temp = signals.FirstOrDefault(s => s.Name.ToLower() == "SPMP_ARC".ToLower());
                var SPMP_ARC = Convert.ToUInt32(temp.Value);
               
                
                //// simple rolling counter
                SPMP_ARC = (uint)AliveRollingCounter(message.Id);  // Todo: set min and max from ARXML (so far known to always be 0-3)
                temp.Value = SPMP_ARC;

                pv |= (SPMP_SysPwrModeAuth & 7) << 1;
                pv |= (SPMP_SysPwrModeAuth_Inv & 1) << 0;
                pv += (SPMP_ARC);
                if (pv < SPMP_ARC)
                    pv = 0xFFFFFFFF;

                pv = pv ^ 15;
                if (pv != 0xFFFFFFFF)
                    pv++;
                uint pv_computed = (pv & 15);

                /////////
                //var powermode = ToBin(SPMP_SysPwrModeAuth, 3);
                //var powermode_Inv = ToBin(SPMP_SysPwrModeAuth_Inv, 1);
                //var sysPwrMd = powermode + powermode_Inv;

                ////System.Diagnostics.Debug.WriteLine("ARC Value:    " + SPMP_ARC%4);
                //uint arc = SPMP_ARC % 4;
                //var ARC = ToBin(arc, 2);
                ////System.Diagnostics.Debug.WriteLine("Value:    " + arc);
                //var raw = addBinary(sysPwrMd, ARC);
                //var pv_raw = Convert.ToUInt32(raw, 2);

                //uint inverse = 15 - pv_raw;
                //uint pv_computed = inverse + 1;

                temp = signals.FirstOrDefault(s => s.Name.ToLower() == "SPMP_PV".ToLower());
                var SPMP_PV = Convert.ToUInt32(temp.Value);

                temp.Value = pv_computed; // Set calculated PV value

                message.Data = MessageDecodeManager.ARXML.Encode(signals);
            }

            return message;
        }
               
        private int MACT_Length(int type, Message message)
        {
            var buffer = new byte[255 * 4];  // for sure is big enough, but memory is cheap :)
            int length = 0;
            ptr_MAC_Tx_Prepare(type, message.Id, message.Data, message.Length, buffer, ref length);  // HACKY way to get MACT length, to just determine what comes back :)
            return length - message.Length;
        }
    }
#endif
}