﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Numerics;
using System.Threading;
using System.Windows;
//using Peak.Can.Basic;
//using TPCANHandle = System.Byte;
using System.IO.Ports;
using System.Runtime.ExceptionServices;
using System.Security;
using Utility;

namespace VSIM.Core.Device
{
    #region DBC File Parsing

    public static class LibraryMessageConvert
    {
        /// <summary>
        /// Function takes in the all of the signal information and it returns an 8 byte array of the CANMessage
        /// </summary>
        /// <param name="ByteOrder"></param>
        /// <param name="StartingBit"></param>
        /// <param name="BitLength"></param>
        /// <param name="SignalDataValue"></param>
        /// <param name="PreviousDataArray"></param>
        /// <param name="MinVal"></param>
        /// <param name="MaxVal"></param>
        /// <param name="OffsetVal"></param>
        /// <param name="FactorVal"></param>
        /// <returns></returns>
        public static byte[] setMessageSigned(string ByteOrder, uint StartingBit, uint BitLength, Int64 SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
        {
            byte[] CANDataOut;
            Int64 PreviousFullSignal;
            int SignalShiftLength;
            Int64 SignalClear;
            Int64 SignalDataConverted;
            Int64 ShiftedValue;

            if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
            {
                throw new OverflowException("Value out of range");
            }

            PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);

            SignalDataValue = Convert.ToInt64(((SignalDataValue - OffsetVal) / FactorVal));
            BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

            SignalDataConverted = Convert.ToInt64(SignalValueConverted.ToString());

            if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
            {

                Array.Reverse(PreviousDataArray);
                PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);
                SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                Array.Reverse(PreviousDataArray);
            }
            else
            {
                SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
            }

            if (BitLength < 64)
            {
                BigInteger test = new BigInteger();
                test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                string holder = test.ToString();
                SignalClear = Convert.ToInt64(holder);
                SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
            }
            else
            {
                SignalClear = 0;
            }

            PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

            ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

            ShiftedValue = ShiftedValue & ~SignalClear; //Remove leading 1's for a negative number

            PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

            CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

            if (ByteOrder == "0")
            {
                Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent
            }

            return CANDataOut;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByteOrder"></param>
        /// <param name="StartingBit"></param>
        /// <param name="BitLength"></param>
        /// <param name="SignalDataValue"></param>
        /// <param name="PreviousDataArray"></param>
        /// <param name="MinVal"></param>
        /// <param name="MaxVal"></param>
        /// <param name="OffsetVal"></param>
        /// <param name="FactorVal"></param>
        /// <returns></returns>
        public static byte[] setMessageUNSigned(string ByteOrder, uint StartingBit, uint BitLength, UInt64 SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
        {
            byte[] CANDataOut;
            UInt64 PreviousFullSignal;
            int SignalShiftLength;
            UInt64 SignalClear;
            UInt64 SignalDataConverted;
            UInt64 ShiftedValue;

            if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
            {
                //throw new OverflowException("Value out of range");
            }

            PreviousFullSignal = BitConverter.ToUInt64(PreviousDataArray, 0);

            SignalDataValue = Convert.ToUInt64((SignalDataValue - OffsetVal) / FactorVal);
            BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

            SignalDataConverted = Convert.ToUInt64(SignalValueConverted.ToString());


            if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
            {

                Array.Reverse(PreviousDataArray);
                PreviousFullSignal = BitConverter.ToUInt64(PreviousDataArray, 0);
                SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                Array.Reverse(PreviousDataArray);
            }
            else
            {
                SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
            }

            if (BitLength < 64)
            {
                BigInteger test = new BigInteger();
                test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                string holder = test.ToString();
                SignalClear = Convert.ToUInt64(holder);
                SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
            }
            else
            {
                SignalClear = 0;
            }

            PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

            ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

            PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

            CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

            if (ByteOrder == "0")
            {
                Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent
            }

            return CANDataOut;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ByteOrder"></param>
        /// <param name="StartingBit"></param>
        /// <param name="BitLength"></param>
        /// <param name="SignalDataValue"></param>
        /// <param name="PreviousDataArray"></param>
        /// <param name="MinVal"></param>
        /// <param name="MaxVal"></param>
        /// <param name="OffsetVal"></param>
        /// <param name="FactorVal"></param>
        /// <returns></returns>
        public static byte[] setMessageDecimal(string ByteOrder, uint StartingBit, uint BitLength, decimal SignalDataValue, byte[] PreviousDataArray, decimal MinVal, decimal MaxVal, decimal OffsetVal, decimal FactorVal)
        {
            byte[] CANDataOut;
            Int64 PreviousFullSignal;
            int SignalShiftLength;
            Int64 SignalClear;
            Int64 SignalDataConverted;
            Int64 ShiftedValue;

            if ((SignalDataValue > MaxVal) | (SignalDataValue < MinVal))
                throw new OverflowException("Value out of range");

            PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);

            SignalDataValue = Convert.ToInt64((SignalDataValue - OffsetVal) / FactorVal);
            BigInteger SignalValueConverted = new BigInteger(SignalDataValue);

            SignalDataConverted = Convert.ToInt64(SignalValueConverted.ToString());

            if (ByteOrder == "0")  //decide if its either intel or motorola, that will determine the shift
            {

                Array.Reverse(PreviousDataArray);
                PreviousFullSignal = BitConverter.ToInt64(PreviousDataArray, 0);
                SignalShiftLength = getOffset(StartingBit, BitLength);  //first get the offset of the signal as to get the specific location of the signal inside the message
                Array.Reverse(PreviousDataArray);
            }
            else
            {
                SignalShiftLength = Convert.ToUInt16(StartingBit);  //if intel shift by starting bit
            }

            if (BitLength < 64)
            {
                BigInteger test = new BigInteger();
                test = BigInteger.Pow(2, Convert.ToInt16(BitLength)) - 1;
                string holder = test.ToString();
                SignalClear = Convert.ToInt64(holder);
                SignalClear = SignalClear << SignalShiftLength;  //the maximum value is then shifted to the position of the signal inside the message
                SignalClear = ~SignalClear;  //take the oposite of that number effectively turning all 1's to 0's
            }
            else
            {
                SignalClear = 0;
            }

            PreviousFullSignal = PreviousFullSignal & SignalClear;  //by anding the previous signal and the newly created all 0 signal we will clear the spot in the message where that signal is located

            ShiftedValue = SignalDataConverted << SignalShiftLength;  //create the new signal with the actual value

            ShiftedValue = ShiftedValue & ~SignalClear; //Remove leading 1's for a negative number

            PreviousFullSignal = PreviousFullSignal | ShiftedValue;  //or the previous signal with the cleared out signal with the new signal

            CANDataOut = BitConverter.GetBytes(PreviousFullSignal);  //turn the 64 bit number to an 8 byte array

            if (ByteOrder == "0")
                Array.Reverse(CANDataOut);  //reverse the order message is now ready to sent

            return CANDataOut;
        }

        /// <summary>
        /// Function takes in the starting bit of the signal and its length and returns the position inside the CANMessage
        /// </summary>
        /// <param name="startbit"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private static int getOffset(uint startbit, uint length)
        {
            int offset;  //offset will be used to shift value
            int byte1;
            int bit;
            int start;
            int len;

            start = Convert.ToInt16(startbit);
            len = Convert.ToInt16(length);

            bit = start % 8;
            byte1 = start / 8;

            offset = 64 - ((byte1 * 8) + (7 - bit) + len);

            return offset;
        }
    }

    #endregion
}
