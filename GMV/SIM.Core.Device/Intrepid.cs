﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CSnet;
using VSIM.Core.Device;
using VSIM.Core;

namespace DeviceLibrary
{
    public class Intrepid : IDevice
    {
        private readonly ISO_15765_2 _iso157652;
        private readonly SecureMessage _secureMessage;
        private readonly object _rxQueueLock = new object();
        private readonly Queue<MessageDecodeManager.Message> _rxQueue = new Queue<MessageDecodeManager.Message>();

        public enum Type
        {
            NeoVI,
            ValueCAN
        }

        // keep with legacy code, Todo: make local variables 
        private IntPtr Object;
        private int SWNetworkID;
        private int HSNetworkID;
        private int HSNetworkID2;
        private int SWBitRateToUse;
        private int HSBitRateToUse;
        private int HSBitRateToUse2;
        private int TLINBitRateToUse;
        private int ILINBitRateToUse;
        private int TLINNetworkID;
        private int ILINNetworkID;
        private int errors;

        public Intrepid()
        {
            Product = Type.NeoVI; // default
            _iso157652 = new ISO_15765_2(this);
            _secureMessage = new SecureMessage();
        }

        ~Intrepid()
        {
            Disconnect();
        }

        public Type Product { get; set; }

        public VSIM.Core.Status Connect(string port = "", bool extra = false)
        {
            if (!string.IsNullOrEmpty(port))
                throw new ArgumentException("port not used, will find automatically");

            var product = Product;

            IntPtr objectvar = IntPtr.Zero;
            try
            {
                Debug.WriteLine(Convert.ToString(icsNeoDll.icsneoGetDLLVersion()));

                // Check if the port is already open
                if (IsConnected) return Status.AlreadyConnected;

                //  File NetworkID array
                var bNetwork = new byte[255];
                for (var iCount = 0; iCount < 255; iCount++)
                    bNetwork[iCount] = Convert.ToByte(iCount);

                //  Set the number of devices to find
                var iNumberOfDevices = 15;

                var ndNeoToOpen = new NeoDevice[16];
                var iResult = icsNeoDll.icsneoFindNeoDevices((uint)eHardwareTypes.NEODEVICE_ALL, ref ndNeoToOpen[0],
                    ref iNumberOfDevices);

                if (iResult == 0)
                {
                    throw new System.InvalidOperationException("Problem Finding Devices");
                    //return Status.NoSuchDevice;
                }

                if (iNumberOfDevices < 1)
                {
                    throw new System.InvalidOperationException("No Devices Found");
                    //return Status.NoSuchDevice;
                }

                var neoDeviceToUse = 0;

                while (neoDeviceToUse < ndNeoToOpen.Length)
                {
                    if (ndNeoToOpen[neoDeviceToUse].MaxAllowedClients == ndNeoToOpen[neoDeviceToUse].NumberOfClients)
                        neoDeviceToUse++;
                    else
                        break;
                }

                if (neoDeviceToUse == ndNeoToOpen.Length) // worst case we going to try to connect to first device!!!
                    neoDeviceToUse = 0;

                iResult = icsNeoDll.icsneoOpenNeoDevice(ref ndNeoToOpen[neoDeviceToUse], ref objectvar,
                    ref bNetwork[neoDeviceToUse], 1, 0);

                if (iResult == 0)
                {
                    throw new System.InvalidOperationException("Problem Opening Port");
                    //return Status.NotAbleToConnect;
                }

                /*
                byte[] bConfigBytes = new byte[1024]; //Storage for Data bytes from device
                int configBytesSize = 0; //Storage for Number of Bytes
                iResult = icsNeoDll.icsneoGetConfiguration(objectvar, ref bConfigBytes[0], ref configBytesSize);
                if (iResult == 0)
                    throw new System.InvalidOperationException("Problem Getting Configuration");
                */

                #region SW CAN baud rate setup
                //set up baud rate for 33.333k and SW CAN
                if (product == Type.NeoVI)
                {
                    //  Get the network name index to set the baud rate of 
                    SWNetworkID = Convert.ToInt32(eNETWORK_ID.NETID_SWCAN); //hardcoded to SW CAN interface
                    //neoVI_Handling.NeoVI_Info.iNetworkID = iNetworkID;

                    SWBitRateToUse = Convert.ToInt32(33333); //hardcoded to 33.333k bit rate

                    //  Set the bit rate
                    iResult = icsNeoDll.icsneoSetBitRate(objectvar, SWBitRateToUse, SWNetworkID);
                    if (iResult == 0)
                        throw new System.InvalidOperationException("Bit Rate Not Set");
                }
                else if (product == Type.ValueCAN)
                {
                    SWNetworkID = Convert.ToInt32(eNETWORK_ID.NETID_HSCAN2); //hardcoded to SW CAN interface
                    SWBitRateToUse = Convert.ToInt32(33333); //hardcoded to 33.333k bit rate
                    iResult = icsNeoDll.icsneoSetBitRate(objectvar, SWBitRateToUse, SWNetworkID);
                    if (iResult == 0)
                        throw new System.InvalidOperationException("Bit Rate Not Set");
                }
                #endregion

                #region HS CAN baud rate setup

                if (product == Type.NeoVI || product == Type.ValueCAN)
                {
                    //  Get the network name index to set the baud rate of 
                    HSNetworkID = Convert.ToInt32(eNETWORK_ID.NETID_HSCAN);

                    HSBitRateToUse = Convert.ToInt32(500000); //hardcoded to 500k bit rate

                    //  Set the bit rate
                    iResult = icsNeoDll.icsneoSetBitRate(objectvar, HSBitRateToUse, HSNetworkID);
                    if (iResult == 0)
                        throw new System.InvalidOperationException("Bit Rate Not Set");

                    // no loopback, normal mode
                    // https://cdn.intrepidcs.net/support/neoVIDLL/ConfigArray.htm
                    //bConfigBytes[Convert.ToInt32(icsConfigSetup.NEO_CFG_MPIC_HS_CAN_MODE)] = Convert.ToByte(1);
                }

                #endregion

                #region HS Bus #2
                if (extra && (product == Type.NeoVI || product == Type.ValueCAN))
                {
                    //  Get the network name index to set the baud rate of 
                    HSNetworkID2 = Convert.ToInt32(eNETWORK_ID.NETID_HSCAN2);

                    HSBitRateToUse2 = Convert.ToInt32(500000); //hardcoded to 500k bit rate

                    //  Set the bit rate
                    iResult = icsNeoDll.icsneoSetBitRate(objectvar, HSBitRateToUse2, HSNetworkID2);
                    if (iResult == 0)
                        throw new System.InvalidOperationException("Bit Rate Not Set");
                }
                #endregion

                #region TLIN baud rate setup
                if (product == Type.NeoVI)
                {
                    //  Get the network name index to set the baud rate of 
                    TLINNetworkID = Convert.ToInt32(eNETWORK_ID.NETID_LIN2); //hardcoded to SW CAN interface
                    //neoVI_Handling.NeoVI_Info.iNetworkID = iNetworkID;

                    TLINBitRateToUse = Convert.ToInt32(20000); //hardcoded to 20.000k bit rate

                    //  Set the bit rate
                    iResult = icsNeoDll.icsneoSetBitRate(objectvar, TLINBitRateToUse, TLINNetworkID);
                    if (iResult == 0)
                        throw new System.InvalidOperationException("Bit Rate Not Set");
                }
                #endregion

                #region ILIN baud rate setup
                if (product == Type.NeoVI)
                {
                    //  Get the network name index to set the baud rate of 
                    ILINNetworkID = Convert.ToInt32(eNETWORK_ID.NETID_LIN); //hardcoded to SW CAN interface
                    //neoVI_Handling.NeoVI_Info.iNetworkID = iNetworkID;

                    ILINBitRateToUse = Convert.ToInt32(10417); //hardcoded to 10.417k bit rate

                    //  Set the bit rate
                    iResult = icsNeoDll.icsneoSetBitRate(objectvar, ILINBitRateToUse, ILINNetworkID);
                    if (iResult == 0)
                        throw new System.InvalidOperationException("Bit Rate Not Set");
                }
                #endregion

                /*
                iResult = icsNeoDll.icsneoSendConfiguration(objectvar, ref bConfigBytes[0], configBytesSize);
                if (iResult == 0)
                    throw new System.InvalidOperationException("Problem Setting Configuration");
                */

                Object = objectvar;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return Status.UnknownError;
            }

            IsConnected = true;

            if (!IsConnected)
                return Status.NotConnected;

            // process received messages
            try
            {
                var thread = new Thread(() =>
                    {
                        while (IsConnected)
                        {
                            try
                            {
#if (DEBUG)
                                // var startTime = DateTime.Now;
#endif                     
                                var messages = Read();
#if (DEBUG)
                                /*
                                if (messages.Any())
                                {
                                    var stopTime = DateTime.Now;
                                    var deltaTime = stopTime.Subtract(startTime);
                                    var elapsedTime = new DateTime(deltaTime.Ticks);
                                    Console.WriteLine("Intrepid Rx Delta -> " +"(" + messages.Length + ") "+ elapsedTime.ToString("HH:mm:ss.FFF"));
                                }
                                */
#endif

                                if (messages == null || !messages.Any())
                                {
                                    Thread.Sleep(25);
                                    continue;
                                }

                                OnReceivedMany?.Invoke(messages);
                                Thread.Sleep(1); // give events time to get through the system
                            }
                            catch
                            {
                                Thread.Sleep(125); // so ugly!
                            }
                        }
                    })
                    {Priority = ThreadPriority.AboveNormal};
                thread.Start();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message.ToString());
            }

            // check for errors!!!
            try
            {
                new Thread(() =>
                {
                    while (IsConnected)
                    {
                        try
                        {
                            IsConnected = IsConnected && !CheckError();
                        }
                        catch
                        {
                            Thread.Sleep(1000); // so ugly!
                        }
                        Thread.Sleep(250*4);
                    }
                }).Start();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message.ToString());
            }

            return Status.Connected;
        }

        public Status Disconnect()
        {
            try
            {
                IsConnected = false;
                int p_errors = 0;
                icsNeoDll.icsneoClosePort(Object, ref p_errors);
                errors = p_errors;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return Status.UnknownError;
            }
            return Status.NotConnected;
        }

        public bool Send(VSIM.Core.Device.Message message)
        {
            var messages = new System.Collections.Generic.List<VSIM.Core.Device.Message>();

            var isSecure = false;
            messages = _secureMessage.Process(message, ref isSecure).ToList(); // comes back with encryption if found in MACT file, else original returned

            if (!isSecure)
            {
                messages.Clear();

                if (message.MultiFrame)
                {
                    try
                    {
                        //_iso157652.FlowControl = true;
                        messages.AddRange(_iso157652.Encode(message));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                        return false; // only while debugging multi frame...
                    }
                }
                else
                {
                    messages.Add(message);
                }
            }

            try
            {
                var i = 0;
                foreach (var frame in messages)
                {
                    if (frame.Network == Mode.LIN_i || frame.Network == Mode.LIN_t)
                    {
                        SendNeoLINMessage((int)frame.Id, frame.Data, frame.Network == Mode.LIN_i ? 0 : 1);
                    }
                    else
                    {
                        SendNeoCANMessage((int)frame.Id, frame.Extended, frame.HighVoltage, frame.Data, frame.Length,
                            frame.Network == Mode.SW, frame.Port);
                    }

                    if (OnSent != null)
                        OnSent(frame);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }

            return true;
        }

        private void SendNeoCANMessage(int ID, bool extended, bool highVoltage, byte[] data, int sizeData, bool SWCAN, int channel=0)
        {
            icsSpyMessage CANMessage = new icsSpyMessage();

            // fill 8 every time
            var temp = new List<byte>(data);
            for (var i = 0; i < 8 - data.Length; i++)
                temp.Add(0x00);
            data = temp.ToArray();

            if (SWCAN == true)
            {
                if (extended) { CANMessage.StatusBitField = Convert.ToInt16(eDATA_STATUS_BITFIELD_1.SPY_STATUS_XTD_FRAME); }
                if (highVoltage) { CANMessage.StatusBitField2 = Convert.ToInt16(eDATA_STATUS_BITFIELD_2.SPY_STATUS2_HIGH_VOLTAGE); }

                CANMessage.NetworkID = Convert.ToByte(eNETWORK_ID.NETID_SWCAN);
                if (Product == Type.ValueCAN) // uses HS at SW speed with HS -> SW adapter!
                    CANMessage.NetworkID = Convert.ToByte(eNETWORK_ID.NETID_HSCAN2);

                CANMessage.ArbIDOrHeader = ID;
                CANMessage.NumberBytesData = Convert.ToByte(sizeData);

                CANMessage.Data1 = Convert.ToByte(data[0]);
                CANMessage.Data2 = Convert.ToByte(data[1]);
                CANMessage.Data3 = Convert.ToByte(data[2]);
                CANMessage.Data4 = Convert.ToByte(data[3]);
                CANMessage.Data5 = Convert.ToByte(data[4]);
                CANMessage.Data6 = Convert.ToByte(data[5]);
                CANMessage.Data7 = Convert.ToByte(data[6]);
                CANMessage.Data8 = Convert.ToByte(data[7]);

                icsNeoDll.icsneoTxMessages(Object, ref CANMessage, SWNetworkID, 1);
            }
            else
            {
                if (extended) { CANMessage.StatusBitField = Convert.ToInt16(eDATA_STATUS_BITFIELD_1.SPY_STATUS_XTD_FRAME); }
                if (highVoltage) { CANMessage.StatusBitField2 = Convert.ToInt16(eDATA_STATUS_BITFIELD_2.SPY_STATUS2_HIGH_VOLTAGE); }

                CANMessage.NetworkID = Convert.ToByte(eNETWORK_ID.NETID_HSCAN);
                CANMessage.ArbIDOrHeader = ID;
                if (sizeData <= 8)  // classic CAN
                {
                    CANMessage.NumberBytesData = Convert.ToByte(sizeData);

                    CANMessage.Data1 = Convert.ToByte(data[0]);
                    CANMessage.Data2 = Convert.ToByte(data[1]);
                    CANMessage.Data3 = Convert.ToByte(data[2]);
                    CANMessage.Data4 = Convert.ToByte(data[3]);
                    CANMessage.Data5 = Convert.ToByte(data[4]);
                    CANMessage.Data6 = Convert.ToByte(data[5]);
                    CANMessage.Data7 = Convert.ToByte(data[6]);
                    CANMessage.Data8 = Convert.ToByte(data[7]);

                    if(channel > 0)
                      errors = icsNeoDll.icsneoTxMessages(Object, ref CANMessage, HSNetworkID2, 1);
                    else
                      errors = icsNeoDll.icsneoTxMessages(Object, ref CANMessage, HSNetworkID, 1);
                }
                else // CAN FD, is assumption made based on length
                {
                    uint iNumberTxed = 0;
                    byte[] iDataBytes = data;

                    // Get pointer to data (could also be done with Unsafe code)
                    System.Runtime.InteropServices.GCHandle gcHandle = System.Runtime.InteropServices.GCHandle.Alloc(iDataBytes, System.Runtime.InteropServices.GCHandleType.Pinned);
                    IntPtr CanFDptr = gcHandle.AddrOfPinnedObject();

                    // NOTE: CAN FD is limited to lengths of 0,1,2,3,4,5,6,7,8,12,16,20,24,32,48,64
                    CANMessage.NumberBytesData = Convert.ToByte(data.Length);
                    CANMessage.NumberBytesHeader = 0;

                    CANMessage.iExtraDataPtr = CanFDptr;
                    CANMessage.Protocol = Convert.ToByte(ePROTOCOL.SPY_PROTOCOL_CANFD);

                    // CAN FD More than 8 bytes
                    // Enable Extra Data Pointer
                    CANMessage.ExtraDataPtrEnabled = 1;
                    if(channel > 0)
                      errors = icsNeoDll.icsneoTxMessagesEx(Object, ref CANMessage, Convert.ToUInt32(HSNetworkID2), 1, ref iNumberTxed, 0);
                    else
                        errors = icsNeoDll.icsneoTxMessagesEx(Object, ref CANMessage, Convert.ToUInt32(HSNetworkID), 1, ref iNumberTxed, 0);
                    gcHandle.Free();
                }
            }
        }

        private void SendNeoLINMessage(int ID, byte[] data, int lintype)
        {
            icsSpyMessageJ1850 LINMessageBuilder = new icsSpyMessageJ1850();
            icsSpyMessage LINMessage = new icsSpyMessage();

            /*************** Build Message ****************/
            LINMessageBuilder.NumberBytesHeader = 3;

            LINMessageBuilder.Header1 = Convert.ToByte(ID);
            LINMessageBuilder.Header2 = data[0];
            LINMessageBuilder.Header3 = data[1];

            if (lintype == 0)
            {
                LINMessageBuilder.NumberBytesData = 6;

                LINMessageBuilder.Data1 = data[2];
                LINMessageBuilder.Data2 = data[3];
                LINMessageBuilder.Data3 = data[4];
                LINMessageBuilder.Data4 = data[5];
                LINMessageBuilder.Data5 = data[6];

                LINMessageBuilder.Data6 = checksum(data, 0xC4);  //checksum
            }
            else if (lintype == 1)
            {
                LINMessageBuilder.NumberBytesData = 7;

                LINMessageBuilder.Data1 = data[2];
                LINMessageBuilder.Data2 = data[3];
                LINMessageBuilder.Data3 = data[4];
                LINMessageBuilder.Data4 = data[5];
                LINMessageBuilder.Data5 = data[6];
                LINMessageBuilder.Data6 = data[7];

                LINMessageBuilder.Data7 = checksum(data, 0x9C);  //checksum
            }


            /***************************************************************************/


            icsNeoDll.ConvertJ1850toCAN(ref LINMessage, ref LINMessageBuilder);

            switch (lintype)
            {
                case 0:  //ILIN
                    icsNeoDll.icsneoTxMessages(Object, ref LINMessage, ILINNetworkID, 1);
                    break;

                case 1:  //TLIN
                    icsNeoDll.icsneoTxMessages(Object, ref LINMessage, TLINNetworkID, 1);
                    break;
            }
        }

        private byte checksum(byte[] data, byte protID)
        {
            int chk_sum = protID;  // Start with protected ID
            byte output;

            for (int i = 0; i < data.Length; i++)
            {
                chk_sum = chk_sum + data[i];

                if (chk_sum >= 256)
                {
                    chk_sum = chk_sum - 255;
                }
            }

            output = Convert.ToByte(chk_sum);

            output = Convert.ToByte(output ^ 0xff);

            return output;
        }

        private bool CheckError()
        {
            // https://cdn.intrepidcs.net/support/neoVIDLL/apiErrorMessages.htm
            var iErrors = new int[600];
            int iNumberofErrors = 0;
            icsNeoDll.icsneoGetErrorMessages(Object, ref iErrors[0], ref iNumberofErrors);

            var error = Convert.ToUInt16((iErrors[0]));

            if (error == 75 || error == 43)
                return true;
            /*
            if (Product == Type.ValueCAN)
            {
                var ignored = iErrors.Count(v => v == 259); // ignore NEOVI_ERROR_DLL_NETWORK_NOT_SUPPORTED_BY_HARDWARE = 259
                iNumberofErrors -= ignored;
            }

            if (iNumberofErrors > 0)
                return true;
            */

            return false;
        }

        private int _count = 0;
        private VSIM.Core.Device.Message[] Read()
        {
            var messages = ReadCANMessage();
            /*
            _count += messages.Count;
            Console.WriteLine(_count);
            */
            // Todo: test and turn on!
            /*
            // MACT handling
            {

                var isSecure = false;
                _secureMessage.Process(message, ref isSecure);
            }
            */

            return messages.ToArray();

            /* Todo?:  get Multiframe support back
            Message message = null;

            lock (_rxQueueLock)
            {
                foreach (var item in messages)
                    _rxQueue.Enqueue(item);

                if (!_rxQueue.Any())
                    return message;  // no message(s)
            }

            lock (_rxQueueLock)
                message = _rxQueue.Dequeue();

            { // handle sync message received.
              // Note:  we will handle if it is our MACT file
                var isSecure = false;
                _secureMessage.Process(message, ref isSecure);
            }

            if (!ARXML.Loaded) return message;

            var messageLookup = ARXML.FindMessageById(message.Id, message.Port);

            if (messageLookup == null) return message;
            try
            {
                var name = messageLookup.Name;
                var multiFrame = name.StartsWith("USDT_") || name.StartsWith("UUDT_");
                if (multiFrame)
                {
                    message = _iso157652.Decode(message);
                    if (message != null) // when message is ready
                        message.Network = Mode.MF;
                }
            }
            catch (Exception ex)
            {
                if (messageLookup == null)
                    Debug.WriteLine("Not Found in Database!!!");
                else
                    Debug.WriteLine(ex);
            }

            return message;
            */
        }

        // https://docs.microsoft.com/en-us/dotnet/api/system.runtime.exceptionservices.handleprocesscorruptedstateexceptionsattribute?redirectedfrom=MSDN&view=netframework-4.7.2
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute()]
        public List<Message> ReadCANMessage()
        {
            var Out_CAN_Message = new List<Message>();
            try
            {
                var Neo_CAN_Message = new List<icsSpyMessage>();

                try
                {
                    int R_errors = 0;
                    int R_messages = 0;
                    icsSpyMessage[] Neo_CAN_Message_temp = new icsSpyMessage[1048576 * 5];  // 5mb
                    icsNeoDll.icsneoGetMessages(Object, ref Neo_CAN_Message_temp[0], ref R_messages, ref R_errors);
                    for (var i = 0; i < R_messages; i++)
                    {

#if (DEBUG)
                        // aALL!
                        Neo_CAN_Message.Add(Neo_CAN_Message_temp[i]);
#else
                        // Rx only
                        if (!((Neo_CAN_Message_temp[i].StatusBitField & Convert.ToInt32(eDATA_STATUS_BITFIELD_1.SPY_STATUS_TX_MSG)) > 0))                        
                            Neo_CAN_Message.Add(Neo_CAN_Message_temp[i]);
#endif
                    }
                }
                catch (Win32Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                catch (System.AccessViolationException ex)
                {
                    Debug.WriteLine(ex);
                }
                catch
                {
                    Debug.WriteLine("Intrepid Driver NO Worky!");
                }

                foreach (icsSpyMessage singlemessage in Neo_CAN_Message)
                {
                    if (!(Convert.ToBoolean(singlemessage.Protocol == Convert.ToInt32(ePROTOCOL.SPY_PROTOCOL_CAN)) ||
                        Convert.ToBoolean(singlemessage.Protocol == Convert.ToInt32(ePROTOCOL.SPY_PROTOCOL_CANFD)) ||
                        Convert.ToBoolean(singlemessage.Protocol == Convert.ToInt32(ePROTOCOL.SPY_PROTOCOL_ETHERNET))))
                        continue; // not supported type of message!

                    double dTime = 0;
                    icsSpyMessage DedicatedMessage = singlemessage;

                    icsNeoDll.icsneoGetTimeStampForMsg(Object, ref DedicatedMessage, ref dTime);  //get messages timestamp

                    Message ReceivedCANMessage = new Message();
                    ReceivedCANMessage.TimeStamp = dTime;

                    if ((singlemessage.StatusBitField & Convert.ToInt32(eDATA_STATUS_BITFIELD_1.SPY_STATUS_XTD_FRAME)) > 0)
                        ReceivedCANMessage.Id = (uint)Convert.ToInt32((singlemessage.ArbIDOrHeader & 0xFFFFFFFF));
                    else
                        ReceivedCANMessage.Id = (uint)singlemessage.ArbIDOrHeader;

                    ReceivedCANMessage.Length = singlemessage.NumberBytesData;
                    ReceivedCANMessage.Data = new[]
                        {
                            singlemessage.Data1, singlemessage.Data2, singlemessage.Data3, singlemessage.Data4,
                            singlemessage.Data5, singlemessage.Data6, singlemessage.Data7, singlemessage.Data8
                        };

                    // CAN FD or Ethernet
                    var isCANFD = singlemessage.ExtraDataPtrEnabled > 0 && singlemessage.iExtraDataPtr != IntPtr.Zero;
                    if (isCANFD)
                    {
                        var dataLength = 0;
                        var dataExtra = new List<byte>();

                        if (Convert.ToBoolean(singlemessage.Protocol == Convert.ToInt32(ePROTOCOL.SPY_PROTOCOL_CANFD)))
                            dataLength = singlemessage.NumberBytesData;
                        else if (Convert.ToBoolean(singlemessage.Protocol == Convert.ToInt32(ePROTOCOL.SPY_PROTOCOL_ETHERNET)))
                            dataLength = ((singlemessage.NumberBytesHeader * 0x100) + singlemessage.NumberBytesData) - 1;

                        // More than 8 bytes of data
                        var iDataBytes = new byte[dataLength];

                        System.Runtime.InteropServices.Marshal.Copy(singlemessage.iExtraDataPtr, iDataBytes, 0, dataLength);
                        System.Runtime.InteropServices.GCHandle gcHandle = System.Runtime.InteropServices.GCHandle.Alloc(iDataBytes, System.Runtime.InteropServices.GCHandleType.Pinned);

                        for (var iByteCount = 0; iByteCount < dataLength; iByteCount++)
                            dataExtra.Add(iDataBytes[iByteCount]);

                        gcHandle.Free();

                        // add extra data to original 8 bytes
                        /*
                        var tempData = ReceivedCANMessage.Data.ToList();
                        tempData.AddRange(dataExtra);

                        ReceivedCANMessage.Data = tempData.ToArray();
                        */
                        ReceivedCANMessage.Data = dataExtra.ToArray();  // Note: holds the first 8 bytes also!!!
                        ReceivedCANMessage.Length = dataLength;
                    }

                    //if (Product == Type.ValueCAN)
                    {
                        ReceivedCANMessage.Port = 0; //default
                        if (singlemessage.NetworkID == (byte) eNETWORK_ID.NETID_HSCAN2)
                        {
                            ReceivedCANMessage.Network = Mode.HS;
                            ReceivedCANMessage.Port = 1;
                        }
                    }

                    if (singlemessage.NetworkID == (byte)eNETWORK_ID.NETID_HSCAN)
                    {
                        ReceivedCANMessage.Network = Mode.HS;
                        //if (ReceivedCANMessage.Length > 8)
                        //ReceivedCANMessage.Network = Mode.FD;
                    }
                    else if (singlemessage.NetworkID == (byte)eNETWORK_ID.NETID_SWCAN)
                        ReceivedCANMessage.Network = Mode.SW;
                    else if (singlemessage.NetworkID == (byte)eNETWORK_ID.NETID_LIN)
                        ReceivedCANMessage.Network = Mode.LIN_i;
                    else if (singlemessage.NetworkID == (byte)eNETWORK_ID.NETID_LIN2)
                        ReceivedCANMessage.Network = Mode.LIN_t;

                    if (isCANFD)  // trumps all
                        ReceivedCANMessage.Network = Mode.FD;

                    var temp = new Message();
                    Utility.Extensions.DeepCopy(ref ReceivedCANMessage, ref temp);
                    if (temp.Id > 0)
                        Out_CAN_Message.Add(temp);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return Out_CAN_Message;
        }

        public bool IsConnected
        {
            get;
            set;
        }

        public event ReceivedManyHandler OnReceivedMany;
        public event ReceivedHandler OnReceived;

        public event ReceivedHandler OnSent;
        public event DisconnectHandler OnDisconnect;

        private IDevice _instance = null;
        public IDevice Instance
        {
            get { return _instance ?? (_instance = new Intrepid()); }
        }
    }
}
