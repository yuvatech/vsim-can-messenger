﻿using DeviceLibrary;
using Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMVehicleMain
{
    public abstract class GMVBase
    {
        DeviceProxy _device = new DeviceProxy();

        #region Helpers
        public string DataValue = "0";
        public object _lockme = new object();
        public string Syspwr(string Variable)
        {
            lock (_lockme)
            {
                DataValue = Variable;
                if (DataValue == "1")
                    ;
                var GBSignalData = new GlobalBDatabase(_device);
                var dictionary_Signal_Val = new Dictionary<string, string>();
                dictionary_Signal_Val.Add("SPMP_SysPwrModeAuth", DataValue);
                GBSignalData.sendSignals(dictionary_Signal_Val);
            }

            return DataValue;
        }

        #endregion

        private void GBWakeUP()
        {
            byte[] WakeupData = new byte[8];
            WakeupData[0] = 00;
            WakeupData[1] = 64;
            WakeupData[2] = 192;
            WakeupData[3] = 00;
            WakeupData[4] = 00;
            WakeupData[5] = 00;
            WakeupData[6] = 00;
            WakeupData[7] = 33;
            _device.SendCANMessage(325, WakeupData, 8, false, false, false);

            Syspwr(DataValue);

            var GBSignalData = new GlobalBDatabase(_device);
            var dictionary_Signal_Val = new Dictionary<string, string>();
            dictionary_Signal_Val.Add("IntDimDspLvl", "126");
            dictionary_Signal_Val.Add("IntDimLvl", "96");
            GBSignalData.sendSignals(dictionary_Signal_Val);
        }
    }
}
