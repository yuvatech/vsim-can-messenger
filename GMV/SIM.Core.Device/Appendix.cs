﻿using System;
using System.Collections.Generic;
using System.Text;
using SIM.Core;

namespace VSIM.Core.Device
{
    public delegate void ReceivedHandler(Message message);
    public delegate void ReceivedManyHandler(Message[] messages);
    public delegate void DisconnectHandler();

    public enum DeviceOptions
    {
        None,
        CSMate,
        EcoMate,
        NeoVI,
        ValueCAN,
        PCAN,
        PCANFD,
        TEST
    }

    class Appendix
    {
    }
}
