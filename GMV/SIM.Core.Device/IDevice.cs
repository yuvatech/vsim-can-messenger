﻿using SIM.Core;
using System;
using System.Collections.Generic;
using System.Text;
using VSIM.Core;

namespace VSIM.Core.Device
{
    public interface IDevice
    {
        Status Connect(string port = "", bool extra = false);
        Status Disconnect();
        bool Send(Message message);
        bool IsConnected { get; set; }

        event ReceivedManyHandler OnReceivedMany;
        event ReceivedHandler OnReceived;
        event ReceivedHandler OnSent;
        event DisconnectHandler OnDisconnect;

        //IDevice Instance { get;}
    }
}
