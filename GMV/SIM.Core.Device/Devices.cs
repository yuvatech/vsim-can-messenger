﻿using DeviceLibrary;
using Epiphan.FrmGrab;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Drawing;

namespace VSIM.Core.Device
{
    #region Devices
    public class Devices
    {
        #region Intrepid

        // ONLY LEFT HERE TO SUPPORT GLOBAL A!!!!!!!!!!!!!!

        public enum LinType : int
        {
            iLIN = 0,
            tLIN = 1,
        }

        public NeoVI_ NeoVI = new NeoVI_();
        public class NeoVI_
        {
            private readonly Intrepid _intrepid = new Intrepid();

            public bool NeoVIPortOpen;	 //tells the port status of the device

            private int neoDeviceToUse = 0;
            private bool deviceNotAvailable = false;
            /*********************************************************************/

            public void ConnectToNeoVI()
            {
                // Check if the port is already open
                if (NeoVIPortOpen == true)
                {
                    DisconnectNeoVI();
                    Thread.Sleep(3 * 1000);
                }

                _intrepid.Product = Intrepid.Type.NeoVI;
                var status = (Status)_intrepid.Connect();
                if (!_intrepid.IsConnected)
                {
                    _intrepid.Product = Intrepid.Type.ValueCAN;
                    status = (Status)_intrepid.Connect();
                }


                if (status == Status.Connected)
                    NeoVIPortOpen = true;
            }

            public void DisconnectNeoVI()
            {
                _intrepid.Disconnect();
                NeoVIPortOpen = false;
            }

            public List<CANMessage> ReadCANMessage()
            {
                var result = new List<CANMessage>();

                var messages = _intrepid.ReadCANMessage();
                foreach (var message in messages)
                {
                    var bridge = new CANMessage() { CanId = (int)message.Id, Data = message.Data };
                    bridge.Extended = message.Extended;
                    bridge.DLC = message.Length;
                    bridge.Network = message.Network == Mode.SW ? "SWCAN" : "HSCAN";
                    bridge.TimeStamp = message.TimeStamp;
                    bridge.TXRXStatus = "RX";
                    result.Add(bridge);
                }

                return result;
            }

            public void SendNeoCANMessage(int ID, bool extended, bool highVoltage, byte[] data, int sizeData, bool SWCAN)
            {
                _intrepid.Send(new Message((uint)ID, extended, highVoltage, data, sizeData, SWCAN ? Mode.SW : Mode.HS));
            }

            public bool CheckNeoErrors()
            {
                return !_intrepid.IsConnected;
            }

            public void SendNeoLINMessage(int ID, byte[] data, int lintype)
            {
                var bridge = new Message
                {
                    Id = (uint)ID,
                    Data = data,
                    Network = lintype == 0 ? Mode.LIN_i : Mode.LIN_t
                };
                _intrepid.Send(bridge);
            }
        }

        #endregion

        #region Device

        public Devices()
        {
            TryInit(InitNeoVI);
        }

        private void TryInit(Action func)
        {
            try
            {
                func.Invoke();
            }
            catch (Exception exception)
            {
                System.Console.WriteLine("Error init " + exception.Message);
            }
        }

        private void InitNeoVI()
        {
            NeoVI = new NeoVI_();
        }

        #endregion

        #region Epiphan
        public Epiphan_ Epiphan = new Epiphan_();
        public class Epiphan_
        {
            FrameGrabber _grabber;

            public unsafe string OpenGrabber()
            {
                string output = "";

                if (_grabber == null)
                {
                    FrameGrabber grabber = null;

                    try
                    {
                        grabber = new FrameGrabber();
                        V2U_Property p;
                        p.key = V2UPropertyKey.V2UKey_Version;
                        grabber.GetProperty(&p);
                        output = grabber.ToString();

                        //TextOutputAdd("Found " + grabber + "\r\nDriver version " +
                        //p.value.version.major + "." + p.value.version.minor + "." +
                        //p.value.version.micro + "." + p.value.version.nano);
                    }
                    catch (Exception x)
                    {
                        output = x.Message.ToString();
                    }

                    SetGrabber(grabber);
                }

                return output;
            }

            public bool SetGrabber(FrameGrabber grabber)
            {
                _grabber = grabber;
                if (grabber != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public Bitmap GrabFrame()
            {
                if (OpenGrabber() != null)
                {
                    VideoMode vm = _grabber.DetectVideoMode();
                    if (vm != null)
                    {
                        if (vm.IsValid())
                        {
                            Frame frame = _grabber.GrabFrame();
                            if (frame != null)
                            {
                                Bitmap bitmap = null;
                                bitmap = frame.GetBitmap();
                                //frame.Release();
                                if (bitmap != null)
                                {
                                    if ((bitmap.Size.Height > 480) || (bitmap.Size.Width > 800))
                                    {
                                        return null;
                                    }
                                    return bitmap;
                                }
                                else
                                {
                                    throw new Exception("Capture format error");
                                    return null;
                                    //TSStatus.Text = ("Capture format error");
                                    //SetGrabber(null);
                                }
                            }
                            else
                            {
                                throw new Exception("Capture failed");
                                return null;
                                //TSStatus.Text = ("Capture failed");
                                //SetGrabber(null);
                            }
                        }
                        else
                        {
                            return null;
                            //TSStatus.Text = ("No signal detected");
                            //PictureBox.Visible = false;
                        }
                    }
                    else
                    {
                        throw new Exception("Failed to detect video mode");
                        return null;
                        //TSStatus.Text = ("Failed to detect video mode");
                        //SetGrabber(null);
                    }
                }
                else
                {
                    throw new Exception("No frame grabber");
                    return null;
                }
            }
        }
        #endregion

    }

    #endregion
}
