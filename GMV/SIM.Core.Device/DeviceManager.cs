﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using VSIM.Core;
using SIM.Core;
using log4net;
//using Message = MessageDecodeManager.Message;

namespace VSIM.Core.Device
{
    public class DeviceManager
    {
        public bool AllowCommunication { get; set; }
        Prism.Events.IEventAggregator ea;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DeviceManager(Prism.Events.IEventAggregator _ea)
        {
            ea = _ea;
        }
        ~DeviceManager()
        {
            Disconnect();
        }
        //// The heart of abstraction!
        //public Devices Device = new Devices();
        //// Note: is static to share very ugly with everyone until IDevice is implemented for ALL
        public static event ReceivedHandler OnReceived;

        public static event ReceivedManyHandler OnReceivedMany;

        public static event EventHandler<VSIM.Core.Device.Message> OnSent;

        public static event DisconnectHandler OnDisconnect;

        // Important:  Should be the hardware interface moving forward!!!
        private IDevice _device = null;
        private DeviceOptions _selected = DeviceOptions.None;

        // Note: IDevice Type
        public bool IsConnected()
        {
            var boolFlag = (_device != null && _selected != DeviceOptions.None && _device.IsConnected) || (_device == null);
            if (!boolFlag)
            {
                Thread.Sleep(100);
                ea.GetEvent<UsingEventAggregator.Core.AutoReconnectFailedEvent>().Publish("Attempting reconnection");
            }
            return boolFlag;
        }

        public bool Connect(DeviceOptions device, string port = "")
        {
            _selected = device;
            var status = VSIM.Core.Status.NotAbleToConnect;
            var extra = false;
            switch (device)
            {
                //case DeviceOptions.TEST:
                //    _device = new MockDevice();
                //    break;
                //case DeviceOptions.PCAN:
                //        _device = new PCAN();
                //    break;
                //case DeviceOptions.PCANFD:
                //    _device = new PCanFD();
                //    break;
                //case DeviceOptions.NeoVI:
                //    _device = new Intrepid(){ Product = Intrepid.Type.NeoVI };
                //    if(!_device.IsConnected)
                //        _device = new Intrepid() { Product = Intrepid.Type.ValueCAN };
                //    extra = true; // supports two can buses
                //    break;
                case DeviceOptions.EcoMate:
                    _device = new EcoMate();
                    log.Debug("EcoMate device is initialized");
                    break;
            }

            if (_device == null) return false;

            status = (Status)_device.Connect(port, extra);

            // re-route!
            _device.OnReceived += delegate (Message message)
            {
                OnReceived?.Invoke(message);
            };

            _device.OnReceivedMany += delegate (Message[] messages)
            {
                OnReceivedMany?.Invoke(messages);
            };

            _device.OnSent += delegate (Message message)
            {
                OnSent?.Invoke(this, message);
            };

            _device.OnDisconnect += delegate ()
            {
                OnDisconnect?.Invoke();
            };

            return status == Status.Connected;
        }

        private void _device_OnManyReceived(Message message)
        {
            throw new NotImplementedException();
        }

        public bool Disconnect()
        {
            _selected = DeviceOptions.None;
            if (_device != null)
                _device.Disconnect();
            return true;
        }

        // hook for CSMate only!
        static public void Publish(Message message)
        {
            OnReceived?.Invoke(message);
        }

        public void Send(Message message)
        {
            if (_device != null && _device.IsConnected)
            {
                _device.Send(message);
            }
        }

        public void SendCANMessage(uint id, byte[] data, int sizeData, bool extended, bool HighVoltage, bool SWCAN, bool multiFrame = false, bool fd = false, int port = 0)
        {
            var message = new Message
            {
                Id = id,
                Data = data,
                Length = sizeData,
                HighVoltage = HighVoltage,
                Extended = extended,
                MultiFrame = multiFrame,
                Port = port
            };

            message.Network = SWCAN ? Mode.SW : Mode.HS;
            /* Todo: add to ECOMate firmware to handle!!!
            if (fd && multiFrame)
                message.Network = Mode.FD_MF; 
            else if (fd)
                message.Network = Mode.FD;
            else if (multiFrame)
                message.Network = Mode.MF; 
            */

            if (_device != null && _device.IsConnected)
            {
                _device.Send(message);
            }
        }


        #region ECOMAT SETTINGS
        // Todo: figure out how to abstract better...

        public void SetEcomatePower()
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetPower();
        }

        public void SetEcomateBootloader()
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetBootloader();
        }

        public void SelectECOMateUSB(int usbDeviceNumber)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SelectUSB(usbDeviceNumber);
        }

        public void SetEcomatePowerSwitch(UInt32 powerSwitch, bool powerSwitchState)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetPowerSwitch(powerSwitch, powerSwitchState);
        }

        public void SetEcomateComEnable(bool enableState)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetComEnable(enableState);
        }

        public void SetEcomateReverse(bool reverseState)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetReverse(reverseState);
        }

        public void SelectEcoMateHSCANTermination(int termination)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SelectHSCANTermination(termination);
        }

        public void SelectVCUMateEthernet(int ethernet)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SelectEthernet(ethernet);
        }

        public void SetVCUMateGlowPlug(bool glowPlug)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetGlowPlug(glowPlug);
        }

        public void SetVCUMateLSCAN(bool lscan)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetLSCAN(lscan);
        }

        public void SetVCUMateCleaSettings(UInt32 state, bool cleaSettings)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetCleaSettings(state, cleaSettings);
        }

        public void SetVCUMateCANTermination(UInt32 state, bool value)
        {
            if (_device == null || _selected != DeviceOptions.EcoMate) return;
            ((EcoMate)_device).SetCANTermSettings(state, value);


        }
        #endregion
    }
}
