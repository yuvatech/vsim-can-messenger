﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VSIM.Core.Device
{
    #region CANMessage Receive
    public class CANMessage
    {
        /// <summary>
        /// CAN ID of the can CANSignal
        /// </summary>
        /// 
        public double TimeStamp { get; set; }

        public string TXRXStatus { get; set; }

        public string Network { get; set; }

        public string Device { get; set; }

        public bool BusError { get; set; }

        public bool Extended { get; set; }

        public int CanId { get; set; }

        public int DLC { get; set; }

        public byte[] Data
        {
            get { return new[] { Data1, Data2, Data3, Data4, Data5, Data6, Data7, Data8 }; }
            set
            {
                var data = value;
                SetData(data);
            }
        }

        public byte Data1 { get; set; }
        public byte Data2 { get; set; }
        public byte Data3 { get; set; }
        public byte Data4 { get; set; }
        public byte Data5 { get; set; }
        public byte Data6 { get; set; }
        public byte Data7 { get; set; }
        public byte Data8 { get; set; }

        public void SetData(byte[] data)
        {
            if (data.Length >= 1)
                Data1 = data[0];
            else
                Data1 = 0;
            if (data.Length >= 2)
                Data2 = data[1];
            else
                Data2 = 0;
            if (data.Length >= 3)
                Data3 = data[2];
            else
                Data3 = 0;
            if (data.Length >= 4)
                Data4 = data[3];
            else
                Data4 = 0;
            if (data.Length >= 5)
                Data5 = data[4];
            else
                Data5 = 0;
            if (data.Length >= 6)
                Data6 = data[5];
            else
                Data6 = 0;
            if (data.Length >= 7)
                Data7 = data[6];
            else
                Data7 = 0;
            if (data.Length >= 8)
                Data8 = data[7];
            else
                Data8 = 0;
        }

        public String Print()
        {
            StringBuilder hex = new StringBuilder(DLC * 2);
            if (DLC >= 1) hex.AppendFormat("{0:x2}", Data1);
            if (DLC >= 2) hex.AppendFormat(" {0:x2}", Data2);
            if (DLC >= 3) hex.AppendFormat(" {0:x2}", Data3);
            if (DLC >= 4) hex.AppendFormat(" {0:x2}", Data4);
            if (DLC >= 5) hex.AppendFormat(" {0:x2}", Data5);
            if (DLC >= 6) hex.AppendFormat(" {0:x2}", Data6);
            if (DLC >= 7) hex.AppendFormat(" {0:x2}", Data7);
            if (DLC >= 8) hex.AppendFormat(" {0:x2}", Data8);

            return hex.ToString();
        }

        public override string ToString()
        {
            var message = string.Format("{0} {1} {2} - {3} {4} -> {5}", this.TimeStamp, this.TXRXStatus, this.Network, this.CanId.ToString("X"), this.DLC.ToString("X"), this.Print());
            return message;
        }
    }
    #endregion
}
