﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VSIM.Core.Device;

namespace Extension
{
    public struct SignalData
    {
        public string conversion;
        public string datatype;
        public List<string> enumList;
        public string length;
        public string longName;
        public string message;
        public string messageDirectory;
        public string range;
        public Boolean receive;
        public string shortName;
        public string signalID;
        public Boolean transmit;
    }
    
    public class GlobalASignal
    {
        public Dictionary<string, List<SignalData>> dictionary_LS_Msg_Sig = new Dictionary<string, List<SignalData>>(); // key = Parameter IDprocess
        public Dictionary<string, List<SignalData>> dictionary_HS_Msg_Sig = new Dictionary<string, List<SignalData>>(); // key = Parameter ID

        public delegate void SendCAN(uint ID, byte[] Data, int sizeData, bool extended, bool HighVoltage, bool SWCAN);
        public SendCAN ThisCAN;

        public Dictionary<string, string> dictionary_Signal_Val = new Dictionary<string, string>();


        Devices _device = new Devices();

        Dictionary<string, object> dictionary_Obj = new Dictionary<string, object>();

        public GlobalASignal(Devices device)
        {
            _device = device;
        }

        #region sendCANMessage

        public void sendLSCANMessage()
        {
            if (_device != null)
            {
                foreach (var item in dictionary_LS_Msg_Sig)
                {
                    foreach (var data in item.Value)
                    {
                        SignalData sigData = data;

                        string className = "LSDBCMessageList." + item.Key;
                        if (dictionary_Signal_Val.ContainsKey(sigData.shortName))
                        {
                                this.processCAMMessage(className, sigData);
                        }
                    }
                }
            }            
        }

        public void sendHSCANMessage()
        {
            if (_device != null)
            {
                foreach (var item in dictionary_HS_Msg_Sig)
                {
                    foreach (var data in item.Value)
                    {
                        SignalData sigData = data;

                        string className = "HSDBCMessageList." + item.Key;
                        this.processCAMMessage(className, sigData, false);
                    }
                }
            }
        }

        #endregion
            

        #region processCAMMessage

        private void processCAMMessage(String className, SignalData sigData, bool isSwCAN = true)
        {
            if (dictionary_Signal_Val.ContainsKey(sigData.shortName))
            {
                //string message = msg;
                string signalShortName = sigData.shortName;
  
                #region Create CAN message object

                object messageObj = null;
                MethodInfo setMessageMethod = null;

                if (dictionary_Obj.ContainsKey(className))
                {
                    messageObj = dictionary_Obj[className];
                    setMessageMethod = messageObj.GetType().GetMethod("setMessage");
                }
                else
                {
                    // Create CAN message object
                    messageObj = getInstance(className);
                    if (messageObj != null)
                    {
                        dictionary_Obj[className] = messageObj;

                        setMessageMethod = messageObj.GetType().GetMethod("setMessage");

                        //Initialize
                        setMessageMethod.Invoke(messageObj, null);
                    }
                }


                if (messageObj != null)
                {
                    // Get Signal field from CAN message
                    FieldInfo sigFieldInfo = messageObj.GetType().GetField(signalShortName + "_", BindingFlags.Public
                                                                        | BindingFlags.Instance);
                    object sigObj = sigFieldInfo.GetValue(messageObj);

                    // Get Desire Method from nested signal class
                    Type type = messageObj.GetType().GetNestedType("_" + signalShortName);
                    MethodInfo sigMethod = null;

                    switch (sigData.datatype)
                    {
                        case "UNM":
                        case "ASC":
                        case "BCD":
                            {
                                if (sigData.conversion.Contains("."))
                                {
                                    decimal val = Convert.ToDecimal(dictionary_Signal_Val[sigData.shortName]);
                                    sigMethod = type.GetMethod("_Value");
                                    if (sigMethod != null)
                                    {
                                        sigMethod.Invoke(sigObj, new object[] { val });
                                    }
                                }
                                else if (sigData.conversion.Contains("-"))
                                {
                                    Int64 val = Convert.ToInt64(dictionary_Signal_Val[sigData.shortName]);
                                    sigMethod = type.GetMethod("_Value");
                                    if (sigMethod != null)
                                    {
                                        sigMethod.Invoke(sigObj, new object[] { val });
                                    }
                                }                                
                                else
                                {
                                    UInt64 val = Convert.ToUInt64(dictionary_Signal_Val[sigData.shortName]);
                                    sigMethod = type.GetMethod("_Value");
                                    if (sigMethod != null)
                                    {
                                        sigMethod.Invoke(sigObj, new object[] { val });
                                    }
                                }
                                break;
                            }
                        case "BLN":
                            {
                                if (dictionary_Signal_Val[sigData.shortName] == "0")
                                {
                                    sigMethod = type.GetMethod("_false");
                                }
                                else if (dictionary_Signal_Val[sigData.shortName] == "1")
                                {
                                    sigMethod = type.GetMethod("_true");
                                }

                                if (sigMethod != null)
                                {
                                    sigMethod.Invoke(sigObj, null);
                                }
                                break;
                            }
                        case "ENM":
                            {
                                int index = Convert.ToInt32(dictionary_Signal_Val[sigData.shortName]);
                                if (index > sigData.enumList.Count - 1 || index < 0)
                                {
                                    //System.Windows.Forms.MessageBox.Show("Insert Valid Number", "Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                                    return;
                                }
                                string enumMethod = sigData.enumList[index];
                                if (index <= 15)
                                {                                    
                                    enumMethod = "_" + enumMethod.Substring(3).Replace(" ", "");
                                    enumMethod = System.Text.RegularExpressions.Regex.Replace(enumMethod, "[& - # / ( ) ']+", String.Empty);
                                    enumMethod = enumMethod.Replace("-", "");
                                    enumMethod = enumMethod.Replace(".", "");
                                }
                                else
                                {
                                    enumMethod = "_" + enumMethod.Substring(4).Replace(" ", "");
                                    enumMethod = System.Text.RegularExpressions.Regex.Replace(enumMethod, "[& - # / ( ) ']+", String.Empty);
                                    enumMethod = enumMethod.Replace("-", "");
                                    enumMethod = enumMethod.Replace(".", "");
                                }
                                sigMethod = type.GetMethod(enumMethod);
                                if (sigMethod != null)
                                {
                                    sigMethod.Invoke(sigObj, null);
                                }
                                break;
                            }
                        case "SNM":
                            {
                                if (sigData.conversion.Contains("."))
                                {
                                    decimal val = Convert.ToDecimal(dictionary_Signal_Val[sigData.shortName]);
                                    sigMethod = type.GetMethod("_Value");
                                    if (sigMethod != null)
                                    {
                                        sigMethod.Invoke(sigObj, new object[] { val });
                                    }
                                }
                                else
                                {
                                    Int64 val = Convert.ToInt64(dictionary_Signal_Val[sigData.shortName]);
                                    sigMethod = type.GetMethod("_Value");
                                    if (sigMethod != null)
                                    {
                                        sigMethod.Invoke(sigObj, new object[] { val });
                                    }
                                }
                                break;
                            }
                        case "PKT":
                            break;
                    }


                    //  Call setMessage                            
                    setMessageMethod.Invoke(messageObj, null);

                    dictionary_Signal_Val.Remove(sigData.shortName);

                    #endregion

                    #region Send CAN message

                    if (dictionary_Signal_Val.Count() == 0)
                    {
                        // GET CAN msg ID
                        FieldInfo idFieldInfo = messageObj.GetType().GetField("ID", BindingFlags.Public
                                                                            | BindingFlags.Instance);
                        UInt32 id = (UInt32)idFieldInfo.GetValue(messageObj);

                        // GET CAN msg DataOut
                        FieldInfo dataOutFieldInfo = messageObj.GetType().GetField("DataOut", BindingFlags.Public
                                                                            | BindingFlags.Instance);
                        byte[] dataOut = (byte[])dataOutFieldInfo.GetValue(messageObj);

                        // GET CAN msg Extended
                        FieldInfo dlcFieldInfo = messageObj.GetType().GetField("DLC", BindingFlags.Public
                                                                            | BindingFlags.Instance);
                        UInt16 dlc = (UInt16)dlcFieldInfo.GetValue(messageObj);

                        // GET CAN msg Extended
                        FieldInfo extendedFieldInfo = messageObj.GetType().GetField("Extended", BindingFlags.Public
                                                                            | BindingFlags.Instance);
                        bool extended = (bool)extendedFieldInfo.GetValue(messageObj);

                        ThisCAN(id, dataOut, dlc, extended, false, isSwCAN);
                    }

                    #endregion
                }
            }
        }
        
        #endregion

        public void Publish(uint id, byte[] data, int dlc, bool extended, bool sw)
        {
            if(ThisCAN != null)
              ThisCAN(id, data, dlc, extended, false, sw); 
        }

        #region getInstance

        object getInstance(string strFullyQualifiedName)
        {
            Type type = Type.GetType(strFullyQualifiedName);
            if (type != null)
                return Activator.CreateInstance(type);
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = asm.GetType(strFullyQualifiedName);
                if (type != null)
                    return Activator.CreateInstance(type);
            }
            return null;
        }

        #endregion

        #region Persist Signal Templete

        public void loadSignalTemplate()
        {
            string fileName = Directory.GetCurrentDirectory() + "\\GA_LS_Signal_Template.json";

            if (File.Exists(fileName))
            {
                string json = File.ReadAllText(fileName);
                dictionary_LS_Msg_Sig = JsonConvert.DeserializeObject<Dictionary<string, List<SignalData>>>(json);
            }

            fileName = Directory.GetCurrentDirectory() + "\\GA_HS_Signal_Template.json";

            if (File.Exists(fileName))
            {
                string json = File.ReadAllText(fileName);
                dictionary_HS_Msg_Sig = JsonConvert.DeserializeObject<Dictionary<string, List<SignalData>>>(json);
            }
        }

        #endregion

        public SignalData getLSSignalData(string signalShortName)
        {
            SignalData LSsignalData = new SignalData();

            foreach (var items in dictionary_LS_Msg_Sig)
            {
                List<SignalData> signalList = items.Value;

                foreach (SignalData item in signalList)
                {
                    if (item.shortName.CompareTo(signalShortName) == 0)
                    {
                        LSsignalData = item;
                        break;
                    }
                }
            }

            return LSsignalData;
        }

        public SignalData getHSSignalData(string signalShortName)
        {
            SignalData HSsignalData = new SignalData();

            foreach (var items in dictionary_HS_Msg_Sig)
            {
                List<SignalData> signalList = items.Value;

                foreach (SignalData item in signalList)
                {
                    if (item.shortName.CompareTo(signalShortName) == 0)
                    {
                        HSsignalData = item;
                        break;
                    }
                }
            }

            return HSsignalData;
        }
    }
}
