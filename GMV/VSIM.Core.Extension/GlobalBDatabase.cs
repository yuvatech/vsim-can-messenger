﻿using MessageDecodeManager;
#if UNIX
#else
// using Microsoft.Office.Interop.Excel;
#endif
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Utility;
// using Message = MessageDecodeManager.Message;
using System.Globalization;
using VSIM.Core.Device;

namespace VSIM.Core
{
    public struct GBSignalData
    {
        public string id;
        public string name;
        public string shortName;
        public double startByte;
        public double startBit;
        public double lenght;
        public string dataType;
        public string range;
        public string conversion;
    }

    public struct GBMsgData
    {
        public string frameName;
        public string id;
        public double length;
        public bool extended;
    }

    public class GlobalBDatabase
    {
        static DeviceManager _device;

        public GlobalBDatabase(DeviceManager device)
        {
            if (_device == null)  // lazy and messy fix
                _device = device;
        }


        public void Send(Device.Message message)
        {
            // keep it the old way....
            if (_device != null && _device.IsConnected())
                _device.Send(message);
        }

        public void SendRAW(uint id, byte[] data, uint length, bool extended, bool multiFrame = false, bool fd = false, int port = 0)
        {
            if (_device != null && _device.IsConnected())
                _device.SendCANMessage(id, data, (int)length, extended, false, false, multiFrame, fd, port);
        }

        #region sendCANMessage_ARXML_JSON
        private class SenderDataType
        {
            public int dlc;
            public byte[] data;
            public uint id;
        }
        // Used to store previous message data
        static readonly Dictionary<string, byte[]> sentMessagesHistory = new Dictionary<string, byte[]>();
        static readonly object _sendLock = new object();
        public bool sendSignals(Dictionary<string, string> signals, int port = 0)
        {
            if (_device != null && _device.IsConnected()) // make sure have device to talk out of first
            {
                // with the static message history, feels safer to force this to be single threaded
                lock (_sendLock)
                {
                    var items = new List<KeyValuePair<string, string>>();
                    if (signals != null)
                        items = signals.ToList(); // copy

                    var sendMessages = new Dictionary<uint, SenderDataType>();
                    foreach (var item in items)
                    {
                        Signal signal = null;
                        MessageDecodeManager.Message message = null;
                        UInt64 signalDataValue = 0;
                        string signalDataValueString = "";
                        byte[] dataOut;
                        byte[] data = new byte[8]; // default, can rest to more bytes later if needed
                        string byteorder = "";
                        uint startBit = 0;
                        uint bitLength = 0;
                        decimal Min = 0;
                        decimal Max = 0;
                        decimal enumval = 0;
                        decimal offset = 0;
                        decimal factor = 1;

                        if (item.Key == null)
                            continue;

                        signal = ARXML.FindSignalByName(item.Key, port);

                        if (signal == null) // should not be the case, but just-incase not throw exception if not found.
                        {
                            Debug.WriteLine("Signal: " + item.Key + " NOT Found!");
                            continue;
                        }

                        message = ARXML.FindMessageBySignal(signal);
                        if (message == null) // should not be the case, but just-incase not throw exception if not found.
                        {
                            Debug.WriteLine("Message: " + item.Key + " NOT Found!");
                            continue;
                        }

                        if (sentMessagesHistory.ContainsKey(message.Name))
                            data = sentMessagesHistory[message.Name];

                        signalDataValueString = item.Value;
                        byteorder = "1"; // if found later will change
                        startBit = 0;
                        bitLength = 0;
                        var convert = false;

                        foreach (var signalAttributes in signal.Properties.ToList())
                        {
                            switch (signalAttributes.Key)
                            {
                                /* AUTOSAR has two categories of data types: “normal” ones, which Endianness can be converted,
                                   and “opaque”, for which COM does not do any conversions.
                                 */
                                case "byteorder":
                                    if (signalAttributes.Value.ToUpper().Contains("MOST-SIGNIFICANT"))
                                    {
                                        var littleEndian = true;
                                        byteorder = "0";
                                    }
                                    else if (signalAttributes.Value.ToUpper().Contains("OPAQUE"))
                                        convert = false;  // if to apply factoring and offsets and so on...
                                    else
                                        ;
                                    break;
                                case "startbit":
                                    startBit = Convert.ToUInt32(signalAttributes.Value);
                                    break;
                                case "bitlength":
                                    bitLength = Convert.ToUInt32(signalAttributes.Value);
                                    break;
                            }
                        }

                        if (signal.Enums.Count > 0)
                        {
                            Min = 0;
                            Max = signal.Enums.Count == 1 ? 1 : signal.Enums.Count - 1;
                        }
                        else
                        {
                            if (signal.Properties.ContainsKey("min"))
                                Min = Convert.ToDecimal(signal.Properties["min"]);
                            if (signal.Properties.ContainsKey("max"))
                            {
                                var max = Convert.ToDouble("0");
                                var max_string = signal.Properties["max"];
                                var exp_index = max_string.IndexOf("+e");
                                if (exp_index >= 0)
                                {
                                    // 5.2E4
                                    var split = max_string.Split(new string[] { "+e" }, StringSplitOptions.None);
                                    max = Convert.ToDouble(split[0]); // 5.2
                                    var raised = Convert.ToDouble(split[1]); // 4
                                    max = max * Math.Pow(10, raised); // 5.2 * 10^4
                                }
                                else
                                {
                                    max = Convert.ToDouble(max_string);
                                }

                                Max = Convert.ToDecimal(max);

                            }

                            if (signal.Properties.ContainsKey("offset"))
                                offset = Convert.ToDecimal(signal.Properties["offset"]);
                            if (signal.Properties.ContainsKey("factor"))
                            {
                                factor = decimal.Parse(signal.Properties["factor"],
                                    NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint,
                                    CultureInfo.InvariantCulture);
                            }
                        }

                        if (Max % 1 == 0)
                        {
                            //int
                            if (Min < 0)
                            {
                                dataOut = LibraryMessageConvert.setMessageSigned(byteorder, startBit, bitLength,
                                    Convert.ToInt64(signalDataValueString), data, Min, Max, offset, factor);
                            }
                            else
                            {
                                dataOut = LibraryMessageConvert.setMessageUNSigned(byteorder, startBit, bitLength,
                                    Convert.ToUInt64(signalDataValueString), data, Min, Max, offset, factor);
                            }
                        }
                        else
                        {
                            //decimal
                            dataOut = LibraryMessageConvert.setMessageDecimal(byteorder, startBit, bitLength,
                                Convert.ToDecimal(signalDataValueString), data, Min, Max, offset, factor);
                        }

                        var signalName = signal.Name;
                        if (!sentMessagesHistory.ContainsKey(message.Name)) //signal.SignalName))
                            sentMessagesHistory.Add(message.Name, dataOut);
                        else
                            sentMessagesHistory[message.Name] = dataOut;

                        var id = (uint)Convert.ToUInt32(message.Properties["Identifier"]);
                        var dlc = (int)Convert.ToInt32(message.Properties["length"]);

                        // quick and dirty
                        if (!sendMessages.ContainsKey(id))
                            sendMessages.Add(id, new SenderDataType() { id = id, dlc = dlc, data = dataOut });
                        else
                            sendMessages[id] = new SenderDataType() { id = id, dlc = dlc, data = dataOut };
                        //_device.SendCANMessage(id, dataOut, dlc, false, false, false);
                    }

                    // Used to send entire message after ALL signals in this sequence is parsed to send at once!
                    foreach (var sendMessage in sendMessages)
                        _device.SendCANMessage(sendMessage.Value.id, sendMessage.Value.data, sendMessage.Value.dlc, false, false, false, port: port);
                }
                return true;
            }
            else
                return false;
        }

        #endregion
    }
}
