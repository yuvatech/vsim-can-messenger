﻿using System;
using System.Collections.Generic;
//using System.Deployment.Internal;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using FSAWrapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utility;

namespace Extension
{
    public class FSA_Handler
    {
        public delegate void ReceivedHandler(Packet packet);
        public static event ReceivedHandler OnReceived;
        public uint _keepAlive = 0;

        public static int Id { get; private set; }

        public FSA_Handler(int id, string address, int port)
        {
            // Todo: get from app.config or "interface" setup attribute
            Id = id;

            /*
            var status = RadioSimAPI.Init(id, address, port);
            if (status != 0)
                throw new Exception("Cannot Connect!");
            */

            RadioSimAPI.init_event_callback = EventCallback_dg;
            RadioSimAPI.heartbeat_event_callback = HeartbeatEventCallback;
            RadioSimAPI.get_callback = CallbackHandler;
            RadioSimAPI.set_callback = CallbackHandler;
            RadioSimAPI.request_response_callback = CallbackHandler;
            RadioSimAPI.subscribe_event_callback = CallbackHandler;
            RadioSimAPI.subscribe_reply_callback = CallbackHandler;
            RadioSimAPI.unsubscribe_reply_callback = CallbackHandler;

            RadioSimAPI.Init(id, address, port);

           // // keep alive
           // Task.Run(() =>
           //{
           //    uint last_keepAlive = 0;
           //    while (true)
           //    {
           //        try
           //        {

           //            if (_keepAlive == 0)
           //            {
           //                var status = RadioSimAPI.Init(id, address, port);
           //            }
           //            else if (_keepAlive != 0 && _keepAlive == last_keepAlive)
           //            {
           //                var status = RadioSimAPI.Init(id, address, port);
           //                Debug.WriteLine("FSA - Reconnecting");
           //            }
           //        }
           //        catch (Exception e)
           //        {
           //            // todo: not just eat me
           //        }

           //        last_keepAlive = _keepAlive;
           //        System.Threading.Thread.Sleep(5 * 1000); // note: heart beat spec says every ~2 seconds
           //     }
           //});
        }

        //---------Connection Status------------
        static void EventCallback_dg(int service_id, FSAWrapper.ServiceEnent service_event)
        {
            OnReceived?.Invoke(new Packet()
            {
                Id = Convert.ToString(service_id),
                value = service_event.ToString()
            });
        }

        private void HeartbeatEventCallback(uint heartbeat_value)
        {
            _keepAlive = heartbeat_value;
        }
        
        private void CallbackHandler(int id, string reply)
        {
            dynamic _fsaParse;
            reply.TryParseJson(out _fsaParse);

            OnReceived?.Invoke(new Packet()
            {
                Id = Convert.ToString(id),
                Name = "Source: "+_fsaParse.From,
                value = "Interface: "+_fsaParse.Header.Interface +"Service:" + _fsaParse.Header.Service + "Status : " +_fsaParse.Status+ ", Timestamp : " + _fsaParse.Timestamp + ", Data-Param: "+_fsaParse.Data.Param + ", Data-Value: "+ _fsaParse.Data.Value + "; Param: "+_fsaParse.Param +"; Value: "+_fsaParse.Value ,
                Type = "Type: "+_fsaParse.Type,
            });

        }

        // from client to FSA
        public static void Process(Packet packet)
        {
            var dateTimeStr = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'-'HH'-'mm'-'ss'.'fff") + "Z";
            var jsonStr = "";

            if (packet.Type.ToLower().Contains("get"))
            {
                jsonStr = @"{'Type': 'Get','Header': {'Service': '" + packet.Service + @"'},'Param': " + packet.Name +
                          @", 'Value': ''}";

                RadioSimAPI.Get(Id, jsonStr);
            }

            if (packet.Type.ToLower().Contains("set"))
            {
                jsonStr = @"{'Type': 'Set','Header': {'Service': '" + packet.Service + @"'},'Param': " + packet.Name +
                          @", 'Value': " + packet.Value + @"'Timestamp':" + dateTimeStr + @" }";
                RadioSimAPI.Set(Id, jsonStr);
            }

            if (packet.Type.ToLower().Contains("subscribe"))
            {

                //        const char* subscribe_json =
                //"{"
                //"\"Param\": \"Subscribe\","
                //"\"Value\": \"OnStarMessage\","
                //"\"Timestamp\": \"2019-06-04-07-39-17.129Z\","
                //"\"Type\": \"ReqResp\","
                //"\"Header\": {"
                //"\"Service\": \"OnStarFunctions\","
                //"}"
                //"}";
                // jsonStr = "{" + "\"Param\": \"Subscribe\"," + "\"Value\": \"OnStarMessage\"," + "\"Timestamp\": \"2019-06-04-07-39-17.129Z\"," + "\"Type\": \"ReqResp\"," + "\"Header\": {" + "\"Service\": \"OnStarFunctions\"," + "}" + "}";

                jsonStr = @"{'Type': 'ReqResp','Header': {'Service': '" + packet.Service + @"'},'Param': " +
                          "Subscribe" + @", 'Value': " + packet.Value + @"'Timestamp':" + dateTimeStr + @"}";
                //jsonStr = @"{'Type': 'ReqResp','Header': {'Service': '" + packet.Service + @"'},'Param': " +
                //          "Subscribe" + @", 'Value': " + packet.Value + @"'Timestamp':" + dateTimeStr + @"}";
                RadioSimAPI.Subscribe(Id, jsonStr);
            }

            if (packet.Type.ToLower().Contains("unsubscribe"))
            {
                jsonStr = @"{'Type': 'ReqResp','Header': {'Service': '" + packet.Service + @"'},'Param': " +
                          "RemoveSubscription" + @", 'Value': " + packet.Value + @"'Timestamp':" + dateTimeStr + @"}";
                RadioSimAPI.UnSubscribe(Id, jsonStr);
            }

            if (packet.Type.ToLower().Contains("request"))
            {
                jsonStr = @"{'Type': 'Req','Header': {'Service': '" + packet.Service + @"'},'Param': " + packet.Name +
                          @", 'Value': " + packet.Value + @"}";
                RadioSimAPI.Request(Id, jsonStr);
            }

            if (packet.Type.ToLower().Contains("requestresponse"))
            {
                jsonStr = @"{'Type': 'ReqResp','Header': {'Service': '" + packet.Service + @"'},'Param': " +
                          packet.Name + @", 'Value': " + packet.Value + @"}";
                RadioSimAPI.RequestResponse(Id, jsonStr);
            }

            if (packet.Type.ToLower().Contains("clean"))
            {
                RadioSimAPI.Cleanup(Convert.ToInt32(packet.Id));
            }
        }
    }
}
