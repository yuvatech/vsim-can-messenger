﻿#if UNIX
#else
using Microsoft.Office.Interop.Excel;
#endif
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Extension
{
    public class LoadDatabase
    {

        public Dictionary<string, List<SignalData>> dictionary_LS_Msg_Sig = new Dictionary<string, List<SignalData>>(); // key = Parameter IDprocess
        public Dictionary<string, List<SignalData>> dictionary_HS_Msg_Sig = new Dictionary<string, List<SignalData>>(); // key = Parameter ID
        
        Dictionary<string, List<string>> dictionary_LS_Rx = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> dictionary_HS_Rx = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> dictionary_LS_Tx = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> dictionary_HS_Tx = new Dictionary<string, List<string>>();
#if UNIX
#else
        Worksheet workSheet_LS_Rx = null;
        Worksheet workSheet_HS_Rx = null;
        Worksheet workSheet_LS_MsgSig = null;
        Worksheet workSheet_HS_MsgSig = null;
#endif
               

        public void populateCANMessage(String excelFile)
        {
#if UNIX
#else
            Application excelApp = new Application();

            Workbook workbook = excelApp.Workbooks.Open(excelFile);

            // get worksheets
            workSheet_LS_Rx = workbook.Sheets["LS Rx"];
            workSheet_HS_Rx = workbook.Sheets["HS Rx"];
            workSheet_LS_MsgSig = workbook.Sheets["LS MsgSig"];
            workSheet_HS_MsgSig = workbook.Sheets["HS MsgSig"];

            // load data
            load_LS_Rx_data();
            load_HS_Rx_data();
            load_LS_MsgSig_data();
            load_HS_MsgSig_data();

            saveSignalTemplate(dictionary_LS_Msg_Sig, Directory.GetCurrentDirectory() + "\\GA_LS_Signal_Template.json");
            saveSignalTemplate(dictionary_HS_Msg_Sig, Directory.GetCurrentDirectory() + "\\GA_HS_Signal_Template.json");
#endif
        }
        
        #region load_LS_Rx_data

        /**
         * Load the message names and signal names from the LS_Rx sheet that are received by the CSM.
         */
        public void load_LS_Rx_data()
        {
#if UNIX
#else
            int startClassReadRow = 4;
            Range range = workSheet_LS_Rx.UsedRange;
            int rowCount = range.Rows.Count;
            string message;
            string ParameterID;
            string signalReceiver;
            string signalTransmitter;

            System.Console.WriteLine("Loading LS_Rx sheet");

            for (int rowNumber = startClassReadRow; rowNumber <= rowCount; rowNumber++)
            {
                //System.Console.WriteLine("Processing row: " + rowNumber);

                // load data from the row into the dictionary
                signalReceiver = workSheet_LS_Rx.Cells[rowNumber, 1].value;
                signalTransmitter = workSheet_LS_Rx.Cells[rowNumber, 2].value;
                if (signalReceiver != null)
                {
                    if (signalReceiver.Contains("CSM_LS"))
                    {
                        // future use : No need now
                        /*
                        if (signalTransmitter != null)
                        {
                            if (signalTransmitter.Contains(signalReceiver))
                            {
                                System.Console.WriteLine("signal is transmitted & received at row " + rowNumber);
                            }
                        }
                        */

                        message = workSheet_LS_Rx.Cells[rowNumber, 3].value;
                        if (!((message.StartsWith("VNMF_")) || (message.StartsWith("UUDT_")) || (message.StartsWith("USDT_"))))
                        {
                            ParameterID = workSheet_LS_Rx.Cells[rowNumber, 5].value;
                            ParameterID = ParameterID.Substring(1, 3);
                            List<string> supportedSignals = new List<string>();
                            supportedSignals.Add(ParameterID);
                            supportedSignals.Add(workSheet_LS_Rx.Cells[rowNumber, 6].value);
                            rowNumber++;

                            while (workSheet_LS_Rx.Cells[rowNumber, 1].value == null)
                            {
                                supportedSignals.Add(workSheet_LS_Rx.Cells[rowNumber, 6].value.Replace("\n", ""));
                                // future use : No need now
                                /*
                                if (signalTransmitter.Contains(signalReceiver))
                                {
                                    System.Console.WriteLine("signal is transmitted & received at row " + rowNumber);
                                    transmittedAndReceivedSignals.Add(workSheet_LS_Rx.Cells[rowNumber, 6].value);
                                }
                                */
                                rowNumber++;
                            }
                            dictionary_LS_Rx.Add(message, supportedSignals);
                            rowNumber--;
                        }
                    }
                }
            }
#endif
        }

        #endregion

        #region load_HS_Rx_data

        /**
         * Load the message names and signal names from the HS_Rx sheet that are received by the CSM.
         */
        public void load_HS_Rx_data()
        {
#if UNIX
#else
            string CANID;
            int startClassReadRow = 4;
            Range range = workSheet_HS_Rx.UsedRange;
            int rowCount = range.Rows.Count;
            string message;
            string signalReceiver;
            string signalTransmitter;

            System.Console.WriteLine("Loading HS_Rx sheet");

            for (int rowNumber = startClassReadRow; rowNumber <= rowCount; rowNumber++)
            {
                //System.Console.WriteLine("Processing row: " + rowNumber);

                // load data from the row into the dictionary
                signalReceiver = workSheet_HS_Rx.Cells[rowNumber, 1].value;
                signalTransmitter = workSheet_HS_Rx.Cells[rowNumber, 2].value;
                if (signalReceiver != null)
                {
                    if (signalReceiver.Contains("CSM_HS"))
                    {
                        // future use : No need now
                        /*
                        if (signalTransmitter != null)
                        {
                            if (signalTransmitter.Contains(signalReceiver))
                            {
                                System.Console.WriteLine("signal is transmitted & received at row " + rowNumber);
                                transmittedAndReceivedSignals.Add(workSheet_HS_Rx.Cells[rowNumber, 6].value);
                            }
                        }
                        */

                        message = workSheet_HS_Rx.Cells[rowNumber, 3].value;
                        if (!((message.StartsWith("VNMF_")) || (message.StartsWith("UUDT_")) || (message.StartsWith("USDT_"))))
                        {
                            CANID = workSheet_HS_Rx.Cells[rowNumber, 4].value;
                            CANID = CANID.Substring(1, 3);
                            List<string> supportedSignals = new List<string>();
                            supportedSignals.Add(CANID);
                            supportedSignals.Add(workSheet_HS_Rx.Cells[rowNumber, 6].value);
                            rowNumber++;
                            while (workSheet_HS_Rx.Cells[rowNumber, 1].value == null)
                            {
                                supportedSignals.Add(workSheet_HS_Rx.Cells[rowNumber, 6].value);
                                
                                // future use : No need now
                                /*
                                if (signalTransmitter.Contains(signalReceiver))
                                {
                                    System.Console.WriteLine("signal is transmitted & received at row " + rowNumber);
                                    transmittedAndReceivedSignals.Add(workSheet_LS_Rx.Cells[rowNumber, 6].value);
                                }
                                */

                                rowNumber++;
                            }
                            dictionary_HS_Rx.Add(message, supportedSignals);
                            rowNumber--;
                        }
                    }
                }
            }
#endif
        }

        #endregion

        #region load_LS_MsgSig_data

        /**
         * Load data from the LS_MsgSig sheet that are received or transmitted by the CSM.
         */
        public void load_LS_MsgSig_data()
        {
#if UNIX
#else
            int startClassReadRow = 3;
            Range range = workSheet_LS_MsgSig.UsedRange;
            int rowCount = range.Rows.Count;
            string message = null;
            string parameterID = null;
            string signal;
            SignalData signalData;
            List<SignalData> signalDataList = null;
            List<string> supportedSignalsRx = null;
            List<string> supportedSignalsTx = null;

            System.Console.WriteLine("Loading LS_MsgSig sheet");

            for (int rowNumber = startClassReadRow; rowNumber <= rowCount; rowNumber++)
            {
                //System.Console.WriteLine("Processing row: " + rowNumber);

                message = workSheet_LS_MsgSig.Cells[rowNumber, 1].value; // get message name from LS_MsgSig sheet                

                if (message != null)
                {
                    supportedSignalsRx = null;
                    supportedSignalsTx = null;

                    if (dictionary_LS_Rx.ContainsKey(message))
                    {
                        supportedSignalsRx = dictionary_LS_Rx[message]; // get list of signals for this message that are received by the CSM
                    }

                    if (dictionary_LS_Tx.ContainsKey(message))
                    {
                        supportedSignalsTx = dictionary_LS_Tx[message]; // get list of signals for this message that are received by the CSM
                    }

                    if (dictionary_LS_Rx.ContainsKey(message) ||
                        dictionary_LS_Tx.ContainsKey(message))
                    {
                        signalDataList = new List<SignalData>();

                        do
                        {
                            signal = workSheet_LS_MsgSig.Cells[rowNumber, 3].value; // get the long name
                            if (signal != null)
                            {
                                signal = signal.Replace("\n", "");
                            }

                            if (((supportedSignalsRx != null) && supportedSignalsRx.Contains(signal)) ||
                                ((supportedSignalsTx != null) && supportedSignalsTx.Contains(signal)))
                            {
                                signalData.message = message;
                                signalData.messageDirectory = "";
                                signalData.shortName = workSheet_LS_MsgSig.Cells[rowNumber, 4].value; // get the short name from the LS_MsgSig sheet
                                signalData.longName = signal;

                                if (supportedSignalsRx != null)
                                {
                                    parameterID = supportedSignalsRx[0];
                                }
                                else
                                {
                                    parameterID = supportedSignalsTx[0];
                                }

                                signalData.signalID = "ENM_" + signalData.shortName + "_L" + parameterID;

                                signalData.length = workSheet_LS_MsgSig.Cells[rowNumber, 7].value;  // get the length, in bits, from the LS_MsgSig sheet
                                if (signalData.length != null)
                                {
                                    signalData.length = signalData.length.Trim();
                                }

                                signalData.datatype = workSheet_LS_MsgSig.Cells[rowNumber, 8].value; // get the data type from the LS_MsgSig sheet
                                signalData.range = workSheet_LS_MsgSig.Cells[rowNumber, 9].value; // get the range from the LS_MsgSig sheet
                                signalData.conversion = workSheet_LS_MsgSig.Cells[rowNumber, 10].value; // get the conversion from the LS_MsgSig sheet

                                signalData.receive = false;
                                signalData.transmit = false;
                                if (supportedSignalsRx != null)
                                {
                                    if (supportedSignalsRx.Contains(signal))
                                    {
                                        signalData.receive = true;
                                    }
                                }
                                if (supportedSignalsTx != null)
                                {
                                    if (supportedSignalsTx.Contains(signal))
                                    {
                                        signalData.transmit = true;
                                    }
                                }

                                signalData.enumList = null;
                                if (signalData.datatype.Equals("ENM"))
                                {
                                    signalData.enumList = new List<string>();
                                    signalData.enumList.Add(workSheet_LS_MsgSig.Cells[rowNumber, 10].value);
                                    rowNumber++;

                                    // get enum list
                                    while (workSheet_LS_MsgSig.Cells[rowNumber, 4].value == null)
                                    {
                                        signalData.enumList.Add(workSheet_LS_MsgSig.Cells[rowNumber, 10].value);
                                        rowNumber++;
                                    }
                                    rowNumber--;
                                }

                                signalDataList.Add(signalData);
                            }

                            rowNumber++;

                        } while (workSheet_LS_MsgSig.Cells[rowNumber, 1].value == null);

                        //dictionary_LS_Msg_Sig.Add(parameterID, signalDataList);Substring(1, 3);                        
                        dictionary_LS_Msg_Sig.Add(signalDataList[0].message, signalDataList);
                        rowNumber--;
                    }
                }
            }
#endif
        }

        #endregion

        #region load_HS_MsgSig_data

        /**
         * Load data from the HS_MsgSig sheet that are received or transmitted by the CSM.
         */
        public void load_HS_MsgSig_data()
        {
#if UNIX
#else
            int startClassReadRow = 3;
            Range range = workSheet_HS_MsgSig.UsedRange;
            int rowCount = range.Rows.Count;
            string message = null;
            string parameterID = null;
            string signal;
            SignalData signalData;
            List<SignalData> signalDataList = null;
            List<string> supportedSignalsRx = null;
            List<string> supportedSignalsTx = null;

            //System.Console.WriteLine("Loading HS_MsgSig sheet");

            for (int rowNumber = startClassReadRow; rowNumber <= rowCount; rowNumber++)
            {
                //System.Console.WriteLine("Processing row: " + rowNumber);

                message = workSheet_HS_MsgSig.Cells[rowNumber, 1].value; // get message name from HS_MsgSig sheet

                if (message != null)
                {
                    supportedSignalsRx = null;
                    supportedSignalsTx = null;

                    if (dictionary_HS_Rx.ContainsKey(message))
                    {
                        supportedSignalsRx = dictionary_HS_Rx[message]; // get list of signals for this message that are received by the CSM
                    }

                    if (dictionary_HS_Tx.ContainsKey(message))
                    {
                        supportedSignalsTx = dictionary_HS_Tx[message]; // get list of signals for this message that are received by the CSM
                    }

                    if (dictionary_HS_Rx.ContainsKey(message) ||
                        dictionary_HS_Tx.ContainsKey(message))
                    {
                        signalDataList = new List<SignalData>();
                        do
                        {
                            signal = workSheet_HS_MsgSig.Cells[rowNumber, 3].value; // get the long name
                            if (signal != null)
                            {
                                signal = signal.Replace("\n", "");
                            }

                            if (((supportedSignalsRx != null) && supportedSignalsRx.Contains(signal)) ||
                                ((supportedSignalsTx != null) && supportedSignalsTx.Contains(signal)))
                            {
                                signalData.message = message;
                                signalData.messageDirectory = "";
                                signalData.shortName = workSheet_HS_MsgSig.Cells[rowNumber, 4].value; // get the short name from the HS_MsgSig sheet
                                signalData.longName = signal;

                                if (supportedSignalsRx != null)
                                {
                                    parameterID = supportedSignalsRx[0];
                                }
                                else
                                {
                                    parameterID = supportedSignalsTx[0];
                                }

                                signalData.signalID = "ENM_" + signalData.shortName + "_H" + parameterID;

                                signalData.length = workSheet_HS_MsgSig.Cells[rowNumber, 7].value;  // get the length, in bits, from the HS_MsgSig sheet
                                if (signalData.length != null)
                                {
                                    signalData.length = signalData.length.Trim();
                                }

                                signalData.datatype = workSheet_HS_MsgSig.Cells[rowNumber, 8].value; // get the data type from the HS_MsgSig sheet
                                signalData.range = workSheet_HS_MsgSig.Cells[rowNumber, 9].value; // get the range from the HS_MsgSig sheet
                                signalData.conversion = workSheet_HS_MsgSig.Cells[rowNumber, 10].value; // get the conversion from the HS_MsgSig sheet

                                signalData.receive = false;
                                signalData.transmit = false;
                                if (supportedSignalsRx != null)
                                {
                                    if (supportedSignalsRx.Contains(signal))
                                    {
                                        signalData.receive = true;
                                    }
                                }
                                if (supportedSignalsTx != null)
                                {
                                    if (supportedSignalsTx.Contains(signal))
                                    {
                                        signalData.transmit = true;
                                    }
                                }

                                signalData.enumList = null;
                                if (signalData.datatype.Equals("ENM"))
                                {
                                    signalData.enumList = new List<string>();
                                    signalData.enumList.Add(workSheet_HS_MsgSig.Cells[rowNumber, 10].value);
                                    rowNumber++;

                                    // get enum list
                                    while (workSheet_HS_MsgSig.Cells[rowNumber, 4].value == null)
                                    {
                                        signalData.enumList.Add(workSheet_HS_MsgSig.Cells[rowNumber, 10].value);
                                        rowNumber++;
                                    }
                                    rowNumber--;
                                }

                                signalDataList.Add(signalData);
                            }

                            rowNumber++;

                        } while (workSheet_HS_MsgSig.Cells[rowNumber, 1].value == null);

                        //dictionary_HS_Msg_Sig.Add(parameterID, signalDataList);
                        dictionary_HS_Msg_Sig.Add(signalDataList[0].message, signalDataList);
                        rowNumber--;
                    }
                }
            }
#endif
        }

        #endregion    
      
        
        #region Persist Signal Templete

        private void saveSignalTemplate(Dictionary<string, List<SignalData>> signalTemplate, string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);                
            }
            File.Create(fileName).Close();

            string json = JsonConvert.SerializeObject(signalTemplate);
            File.WriteAllText(fileName, json);
        }

        #endregion

    }
}
