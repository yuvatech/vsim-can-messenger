﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Runtime;
using System.Runtime.CompilerServices;
// using Microsoft.Office.Interop.Excel;
using Utility;
using System.Collections.Specialized;
using System.Configuration;
using VSIM.Core;
using VSIM.Core.Device;
using ExtensionServer;

namespace Extension
{
    public class SockServer
    {
        private FSA_Handler _FSA;

        private int _flushLimit = 1000; // if fall behind then clear to extension(s)
        private int _clears = 0;

        /// </summary>
        private class PeriodicPacket
        {
            public Packet Packet_ = new Packet();
            public long Count = 0;
            public long Rate = 0;
        }

        private static ExtensionServer.Server _server = null;
        private readonly List<Packet[]> _toClients = new List<Packet[]>();
        private readonly ConcurrentDictionary<string, PeriodicPacket> _subscriptionsTx = new ConcurrentDictionary<string, PeriodicPacket>();
        private readonly object _subscriptionsRxLock = new object();
        private readonly ConcurrentDictionary<int, ConcurrentBag<uint>> _subscriptionsRx = new ConcurrentDictionary<int, ConcurrentBag<uint>>();

        private readonly object _clientsLocker = new object();

        volatile bool bStopThread = false;
        readonly GlobalASignal _gaSignal;
        readonly GlobalBDatabase _gbDatabase;
        readonly bool _isGA = false;
        //readonly Label mCtrlStatus;
        Socket mHandler;

        public int Connections
        {
            get
            {
                if (_server != null)
                    return _server.Count;
                return 0;
            }
        }

        public delegate void SignalHandler(Packet packet);
        public event SignalHandler OnSignalReceived;

        public delegate void InfoHandler(dynamic info);
        public event InfoHandler OnInfo;

        private int _count = 0;
        public SockServer(GlobalASignal gaSignal, GlobalBDatabase gbDatabase, int port = 55555)
        {
            for (var i = 0; i < MessageDecodeManager.ARXML.Channels; i++)
                while (!_subscriptionsRx.TryAdd(i, new ConcurrentBag<uint>()))
                    Thread.Sleep(1);

            try
            {
                var mateConfig = (NameValueCollection)ConfigurationManager.GetSection("application");
                var limit = Convert.ToInt32(mateConfig["flush"]);
                _flushLimit = Math.Max(limit, _flushLimit);
            }
            catch { }

            _isGA = false; // default to latest 

            if (gaSignal != null)
            {
                _gaSignal = gaSignal;
                _isGA = true;
            }

            if (gbDatabase != null)
            {
                _gbDatabase = gbDatabase;
                _isGA = false;
                //mCtrlStatus = control;
            }

            if (_server == null) // can be one and ONLY one
            {
                _server = new Server(port);
                _server.OnReceived += Received_Server;
            }

            VSIM.Core.Device.DeviceManager.OnReceived += delegate (Message message)
           {
               DeviceProxy_OnMessage(message);
           };

            VSIM.Core.Device.DeviceManager.OnReceivedMany += delegate (Message[] messages)
            {
                foreach (var message in messages) { DeviceProxy_OnMessage(message); }
            };
            // will echo when sent!!!
            VSIM.Core.Device.DeviceManager.OnSent += (sender, message) => DeviceProxy_OnMessage(message);

            // info task
            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        dynamic info = new System.Dynamic.ExpandoObject();
                        info.TxCount = _toClients.Count;
                        info.Connections = _server.Count;
                        info.TxClears = _clears;
                        if (info.TxCount > 0)
                            Console.WriteLine("count: " + info.TxCount);
                        OnInfo?.Invoke(info);
                    }
                    catch (Exception e)
                    {
                        // eat me!
                    }

                    Thread.Sleep(250);
                }
            });

            try
            {
                FSA_Handler.OnReceived += FSA_Handler_OnReceived;
            }
            catch (Exception e)
            {
                // Todo: actually try again or tell someone or ?
                Console.WriteLine(e);
            }
        }

        private void FSA_Handler_OnReceived(Packet packet)
        {
            var packets = new List<Packet> { packet };
            SocketClientSend(packets);
        }

        private long PeridoicMessageProcess(long waited = 1)
        {
            var _rates = new List<long>();
            _rates.Add(long.MaxValue);  // single value
            //while (true)
            {
                try
                {
                    var checking = new List<PeriodicPacket>();
                    var lock_ = new object();
                    //foreach (var subscription in _subscriptionsTx)
                    Parallel.ForEach(_subscriptionsTx, (subscription) =>
                    {
                        var item = subscription.Value.Packet_;
                        var count = subscription.Value.Count;
                        var rate = subscription.Value.Rate;

                        lock (lock_)
                            _rates.Add(rate);

                        count += waited;
                        checking.Add(new PeriodicPacket() { Count = count, Packet_ = item, Rate = rate });
                    });

                    var sending = new List<Packet>();
                    var lock__ = new object();
                    //foreach (var item in checking)
                    Parallel.ForEach(checking, (item) =>
                    {
                        var name = item.Packet_.Name;
                        if (item.Count >= item.Rate)
                        {
                            item.Count = 0; // reset
                            lock (lock__)
                                sending.Add(item.Packet_);
                        }

                        // Note: because update not changing object value so screw it remove it.  Todo: better!
                        if (_subscriptionsTx.ContainsKey(name))
                        {
                            while (!_subscriptionsTx.TryRemove(name, out var out_))
                                Thread.Sleep(1);
                        }

                        _subscriptionsTx.AddOrUpdate(item.Packet_.Name, item); // update
                    });

                    if (sending.Any())
                    {
                        // out to CAN bus
                        Process(sending);
                        // out to socket clients
                        SocketClientSend(sending);  // Todo: is this the best place for this?
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex); // eat it!
                }
                //Thread.Sleep(wait);
            }
            return _rates.Min();
        }

        // Bus message received
        // Note: if pass in message with more data (i.e. a joined multi frame message, it can handle properly)
        void DeviceProxy_OnMessage(VSIM.Core.Device.Message message)
        {
            try
            {
                lock (_subscriptionsRxLock)
                {
                    // if have something means we are using and if so check if care for this id
                    if (_subscriptionsRx[message.Port].Count > 0 && !_subscriptionsRx[message.Port].Contains(message.Id))
                        return;  // done do not care!
                }
            }
            catch (Exception e)
            {

            }

            //Task.Run(() => PreOnMessage(message));
            PreOnMessage(message);
            /*
            var thread = new Thread(() => { PreOnMessage(message); }) { Priority = ThreadPriority.BelowNormal };
            thread.Start();
            */
        }
        private class MessageCache
        {
            public uint Key { get; set; }
            public byte[] Data { get; set; }
            public Packet[] Payload { get; set; }
        }
        private readonly ConcurrentDictionary<uint, MessageCache> _dataCache = new ConcurrentDictionary<uint, MessageCache>();
        private readonly object _dataCacheLock = new object();

        private bool ShouldDecode(Message message)
        {
            var decode = false;
            // Important:  rather error and try again knowing is being updated when checked again later!
            //lock (_dataCacheLock)
            try
            {
                if (!_dataCache.ContainsKey(message.Id))
                {
                    decode = true;
                }
                else
                {
                    var cacheData = _dataCache[message.Id].Data;
                    var isDataEqual = Enumerable.SequenceEqual(message.Data, cacheData);
                    if (!isDataEqual)
                        decode = true;
                }
            }
            catch (Exception)
            {
                return true; // on error do it!
            }
            return decode;
        }
        private void PreOnMessage(VSIM.Core.Device.Message message)
        {
            var payload = new List<Packet>();
            try
            {
#if (DEBUG)
                // var startTime = DateTime.Now;
#endif

                if (_isGA) // NOT support decoding, send RAW data
                {
                    var packet = new Packet
                    {
                        //Name = message.Id.ToString("X"),
                        Type = Packet.Types[1],
                        Value = message.Data.ToHexString(),
                        Id = message.Id.ToString("X")
                    };
                    payload.Add(packet);
                }
                else
                {
                    //var gbSignals = new GlobalBDatabase(null);
                    if (ShouldDecode(message))
                    {
                        if (true) // always send raw data
                        {
                            var temp = new Packet
                            {
                                //Name = message.Id.ToString("X"),
                                Type = Packet.Types[3], // Raw
                                Mode = Enum.GetName(typeof(Mode), message.Network),
                                Value = message.Data.ToHexString(),
                                Id = message.Id.ToString("X")
                            };
                            payload.Add(temp); // send as chunk of packet for a message at once
                        }

                        var messageLookup = MessageDecodeManager.ARXML.FindMessageById(message.Id, message.Port);
                        if (messageLookup != null) // found it
                        {
                            // lets send entire message also!
                            var temp = new Packet
                            {
                                Name = messageLookup.Name,
                                Type = Packet.Types[1],
                                Value = message.Data.ToHexString(),
                                Mode = Enum.GetName(typeof(Mode), message.Network),
                                Id = message.Id.ToString("X")
                            };
                            payload.Add(temp); // send as chunk of packet for a message at once

                            var signals = messageLookup.Signals;
                            if (signals != null)
                            {
                                foreach (var signal in signals)
                                {
                                    var name = signal.Name;
                                    dynamic value = null; // type is unknown at this stage
                                    try
                                    {
                                        value = MessageDecodeManager.ARXML.SignalValue(signal, message.Data);
                                        if (value == null) // indicator of error with multi frame message (most likely)
                                            continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        // Important: Exception is throw if starBit and length is greater then the 8 bytes, which means is multi frame message
                                        value = null; // bad data!!!!!!!!!!!!!                      
                                        Debug.WriteLine(ex);
                                    }

                                    if (value == null) continue;

                                    var packet = new Packet
                                    {
                                        Type = Packet.Types[0],
                                        Name = name,
                                        Value = Convert.ToString(value),
                                        Mode = Enum.GetName(typeof(Mode), message.Network),
                                        Id = message.Id.ToString("X")
                                    };
                                    payload.Add(packet); // send as chunk of packet for a message at once
                                }
                            }
                        }

                        // add to cache! :) (is in thread already so wait until can...)
                        //lock (_dataCacheLock)
                        {
                            if (ShouldDecode(message)) // check again may already been done (little tricky thought process here)
                            {
                                lock (_dataCacheLock)
                                {
                                    if (_dataCache.ContainsKey(message.Id))
                                    {
                                        MessageCache remove;
                                        _dataCache.TryRemove(message.Id, out remove);
                                    }

                                    _dataCache.TryAdd(message.Id,
                                        new MessageCache()
                                        { Key = message.Id, Data = message.Data, Payload = payload.ToArray() });
                                }
                            }
                        }
                    }
                    else
                    {
                        lock (_dataCacheLock)
                            payload = _dataCache[message.Id].Payload.ToList();
                    }
                }

                if (payload.Count <= 0) return;
#if (DEBUG)
                /*
                var stopTime = DateTime.Now;
                var deltaTime = stopTime.Subtract(startTime);
                var elapsedTime = new DateTime(deltaTime.Ticks);
                Console.WriteLine("Signal Process Delta -> " + elapsedTime.ToString("HH:mm:ss.FFF"));
                */
#endif

                SocketClientSend(payload);

                payload.Clear();

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void SocketClientSend(List<Packet> payload)
        {
            if (_server.Count > 0) // not sure this is really needed, but does not hurt
            {
                lock (_clientsLocker)
                    _toClients.Add(payload.ToArray());
            }
            else
            {
                lock (_clientsLocker)
                    _toClients.Clear();
            }
        }

        private int _skewRate = 1;
        public void Start()
        {
            if (!_server.IsRunning)
            {
                _server.Run();
                // Bus RX queue processor to send to ALL clients
                var threadPeriodic = new Thread(() =>
                    {
                        long wait = 5;
                        while (_server.IsRunning)
                        {
                            try
                            {
                                // thread runner...                               
                                wait = PeridoicMessageProcess(wait);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex);
                            }
                            Thread.Sleep(Convert.ToInt32(Math.Min(wait, 125))); // msec, max wait 5 msec
                        }
                    })
                { Priority = ThreadPriority.Highest };
                threadPeriodic.Start();

                var threadSend = new Thread(() =>
                    {
                        while (_server.IsRunning)
                        {
                            try
                            {
                                SocketClientSendProcess();  // send delays on its own while sending                                      
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex);
                            }
                            finally
                            {
                                if (_server.Count <= 0)
                                    Thread.Sleep(250);
                                else
                                    ; // GC.Collect();
                            }
                            Thread.Yield(); // cedes control to any ready thread associated with the current processor
                        }
                    })
                { Priority = ThreadPriority.Highest };
                threadSend.Start();
            }
        }

        public void Stop()
        {
            if (_server.IsRunning)
                _server.Stop();
        }

        private int SocketClientSendProcess()
        {
            int count = 0;
            while (true)
            {
                count = 0;
                byte[] buffer = { };
                //while (!_toClients.TryDequeue(out buffer))
                //  Thread.Sleep(1);                
                {
                    lock (_clientsLocker)
                    {
                        if (_toClients.Any())
                        {
                            // buffer = _toClients.Dequeue();
                            var amount = _toClients.Count; // Math.Min(_toClients.Count, 100);
                            var bufferAll = _toClients.GetRange(0, amount).ToArray();
                            _toClients.RemoveRange(0, amount);

                            var bufferList = bufferAll.SelectMany(buff => buff).ToList();

                            var payload = Packet.Transform(bufferList);
                            var data = JsonConvert.SerializeObject(payload); // magic decoder
                            buffer = Encoding.ASCII.GetBytes(data);
                        }
                    }
                    /*
                    count--;
                    if (count > _flushLimit) // WILL LOSS MESSAGES WITH THIS!!!!!!!!
                    {
                        _clears++;
                        _toClients.Clear();
                    }
                    */
                }

                // if have clients and data to send
                if (_server.Count > 0 && buffer.Any())
                {
                    Send(buffer);
                }
                else
                {
                    if (count <= Convert.ToInt32(_flushLimit * 0.25)) // if not have anything waiting, ~25% full...
                    {
                        Thread.Sleep(1);
                        break;
                    }
                }

                Thread.Sleep(0); // cedes control to any ready thread of equal priority or keeps going on the current thread if there is none
            }

            return count;
        }

        // to socket client(s)
        public static bool Send(byte[] data, Guid[] ignore = null)
        {
            if (!_server.IsRunning) return false;
            _server.Broadcast(data, ignore);
            return true;
        }

        // received from socket server client(s)        
        private readonly Dictionary<Guid, List<byte>> _server_buffer = new Dictionary<Guid, List<byte>>();
        private void Received_Server(object sender, byte[] buffer)
        {
            //Task.Run(() => PreProcess(sender, buffer));
            PreProcess(sender, buffer);
        }

        private object _rxLock = new Object();
        private void PreProcess(object sender, byte[] buffer)
        {
            // Todo: thread queue this?
            try
            {
                var client = (ExtensionServer.Client)sender;
                lock (client)  // sender is socket client, only lock on specific client!  may need to be concurent dictonary
                {
                    if (!_server_buffer.ContainsKey(client.Id))
                        _server_buffer.Add(client.Id, new List<byte>());

                    var server_buffer = _server_buffer[client.Id];

                    server_buffer.AddRange(buffer);

                    while (server_buffer.Any())  // has data
                    {
                        server_buffer.RemoveAll(b =>
                            Encoding.ASCII.GetString(new[] { b }).Equals("\n")); // newlines not needed

                        var startIndex = server_buffer.IndexOf(91); // [
                        // removing junk if partial message
                        if (startIndex > 1)
                        {
                            server_buffer.RemoveRange(0, startIndex);
                            startIndex = server_buffer.IndexOf(91); // find new index now...
                        }

                        var endIndex = server_buffer.IndexOf(93); // ]

                        if (startIndex == -1 || endIndex == -1) // no good data ready!!!
                            break; // not enough

                        if (startIndex > endIndex) // partial data received (first message?)
                            server_buffer.RemoveRange(0, startIndex);

                        if (endIndex + 1 < server_buffer.Count)
                            ;

                        var temp = server_buffer.GetRange(startIndex, (endIndex - startIndex) + 1);

                        var startRemove = 0;
                        // treat list in a list (in list...) as single list and process on next loop around...
                        foreach (var t in temp)  // Todo: linq style...
                        {
                            if (t == 91)
                                startRemove++;
                            else
                                break;
                        }

                        if (startRemove > 1 && temp.Count >= (startIndex + startRemove))
                            temp.RemoveRange(startIndex, startRemove - 1);

                        server_buffer.RemoveRange(startIndex, (endIndex - startIndex) + startRemove);

                        var data = Encoding.ASCII.GetString(temp.ToArray(), 0, temp.Count);

                        var payload = new List<Packet>();
                        if (!data.TryParseJson(out payload))
                            ;  // Todo: assume always works for now
                        payload = Packet.Transform(payload);  // V1 to V2 to ? magic of different payloads
                        //Console.WriteLine("+ " + payload.Count);

                        // send packet around in application and back to clients                        
                        {

                            Task.Run(() => this.Process(payload));

                            // send back to any other client connected to server
                            //var dataRevert = JsonConvert.SerializeObject(payload); // magic decoder
                            //var bufferRevert = Encoding.ASCII.GetBytes(dataRevert);

                            // NO LONGER NEEDED, we echo on send event now
                            /*
                            // send to client(s), so they know what went on the bus from any other client
                            // no echo, i.e. ignore oneself
                            // send after process in-case error and to know when onto bus   
                            Send(bufferRevert, new[] { ((ExtensionServer.Client)sender).Id });
                            */
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // bad data does anyone care?
                Debug.WriteLine(ex);
            }
        }

        private void ClientsFlush()
        {
            /*
            lock (_clientsLocker)
                _toClients.Clear();
                */
        }

        private void SubscritionAddRemoveItem(Packet item)
        {
            if (!string.IsNullOrEmpty(item.Subscribe))
            {
                var port = 0; // default
                if (!string.IsNullOrEmpty(item.Port))
                    port = Convert.ToInt32(item.Port) - 1;

                var id = 0u; // default
                if (!string.IsNullOrEmpty(item.Id))
                    id = uint.Parse(Convert.ToString(item.Id), System.Globalization.NumberStyles.HexNumber);
                else if (!string.IsNullOrEmpty(item.Name))
                {
                    var m = MessageDecodeManager.ARXML.FindMessageByName(item.Name, port);
                    if (m != null)
                        id = m.Id;
                    else
                    {
                        var s = MessageDecodeManager.ARXML.FindSignalByName(item.Name, port);
                        if (s != null)
                            id = MessageDecodeManager.ARXML.FindMessageBySignal(s, port).Id;
                    }
                }

                lock (_subscriptionsRxLock)
                {
                    if (Convert.ToBoolean(item.Subscribe)) // add
                    {
                        _subscriptionsRx[port].Add(id);
                    }
                    else // remove
                    {
                        var list = _subscriptionsRx[port].ToArray();
                        if (list.Contains(id))
                        {
                            uint take;
                            while (!_subscriptionsRx[port].IsEmpty)
                                _subscriptionsRx[port].TryTake(out take);
                        }

                        // Todo: if one client un-subscribes they all do.....
                        foreach (var l in list)
                        {
                            if (l == id)
                                continue;
                            _subscriptionsRx[port].Add(l);
                        }
                    }
                }
                return;
            }
        }

        private void PeriodicAddRemoveItem(Packet item)
        {
            if (!string.IsNullOrEmpty(item.Periodic))
            {
                var isBool = bool.TryParse(item.Periodic, out var flag);
                if (isBool && !flag) // is bool and is false
                {
                    // has entry
                    if (_subscriptionsTx.ContainsKey(item.Name))
                    {
                        while (!_subscriptionsTx.TryRemove(item.Name, out var out_))
                            Thread.Sleep(1);
                    }
                }
                else
                {
                    var port = 0; // default
                    if (!string.IsNullOrEmpty(item.Port))
                        port = Convert.ToInt32(item.Port);

                    var seconds = 1.0;
                    if (isBool)
                    {
                        var messageLookup = MessageDecodeManager.ARXML.FindMessageByName(item.Name, port);
                        if (messageLookup == null)
                        {
                            var signalLookup = MessageDecodeManager.ARXML.FindSignalByName(item.Name, port);
                            if (signalLookup != null)
                                messageLookup = MessageDecodeManager.ARXML.FindMessageBySignal(signalLookup, port);
                        }

                        if (messageLookup == null)
                            return; // todo: handle better indicate to someone...

                        if (messageLookup.Properties.ContainsKey("timeperiod"))
                            seconds = (double)Convert.ToDouble(messageLookup.Properties["timeperiod"].ToLower());
                    }
                    else
                    {
                        var isInt = int.TryParse(item.Periodic, out var msecs);
                        if (isInt)
                            seconds = msecs * 0.001;
                    }

                    var rate = (int)Convert.ToInt64(1000 * seconds);
                    _subscriptionsTx.AddOrUpdate(item.Name, new PeriodicPacket() { Packet_ = item, Rate = rate });
                }
                return;
            }
        }

        // send to bus, fire packet event
        private readonly object _lock = new object();
        private void Process(IEnumerable<Packet> payload)
        {
            //lock (_lock) // Todo: how original logic works setting entire message for each single have to process sequentially!
            {
                try
                {
                    if (_isGA)
                    {
                        var bHSCAN = false;
                        var sendReady = false;

                        _gaSignal.dictionary_Signal_Val.Clear();
                        foreach (var signal in payload)
                        {
                            if (signal.msgType != null && (signal.msgType.ToLower() == "hscan" || signal.msgType.ToLower() == "hs"))
                                bHSCAN = true;

                            if (signal.Type != null && signal.Type.ToUpper() != Packet.Types[3].ToUpper()) // SIGNAL
                            {
                                sendReady = true;
                                _gaSignal.dictionary_Signal_Val.Add(signal.signalName, signal.value);
                            }

                            // V2 support ONLY
                            var item = signal;
                            if (signal.Type != null && item.Type.ToUpper() == Packet.Types[3].ToUpper()) // RAW
                            {
                                if (item.Id != null)
                                {
                                    // hex to decimal
                                    var id = uint.Parse(Convert.ToString(item.Id), System.Globalization.NumberStyles.HexNumber);
                                    var data = Convert.ToString(item.Value).FromHexString();
                                    var extended = id > 2047; //(0x000007FF) 

                                    _gaSignal.Publish(id, data, data.Length, extended, !bHSCAN);
                                }
                            }

                            if (OnSignalReceived != null) // fire to subscribers
                                OnSignalReceived(item);
                        }

                        if (sendReady)
                        {
                            if (bHSCAN)
                                _gaSignal.sendHSCANMessage();
                            else
                                _gaSignal.sendLSCANMessage();
                        }
                    }
                    else
                    {
                        var sendReady = false;
                        var dictionary_Signal_Val = new List<Dictionary<string, string>>();
                        foreach (var channel in Packet.Ports)
                            dictionary_Signal_Val.Add(new Dictionary<string, string>());

                        foreach (var item in payload)
                        {
                            try
                            {
                                // handler for FSA type message
                                if (!string.IsNullOrEmpty(item.Service))
                                {
                                    if (item.Service.ToLower().Contains("init"))
                                    {
                                        _FSA = new FSA_Handler(Convert.ToInt32(item.Id), item.Value.ToString().Split(':')[0], Convert.ToInt32(item.Value.ToString().Split(':')[1]));
                                    }
                                    FSA_Handler.Process(item);
                                    //OnSignalReceived?.Invoke(item); // note echos back to sending client....                                    
                                    continue;
                                }

                                // hook for LIN type message
                                if (!string.IsNullOrEmpty(item.Mode) && item.Mode.ToUpper().Contains(Packet.Modes[5])) // LIN
                                {
                                    // LIN_Hook.Process(item);
                                    var data = new byte[] { }; // empty!
                                    if (item.Value != null)
                                        data = Convert.ToString(item.Value).FromHexString();
                                    _gbDatabase.Send(new Message() { Id = Convert.ToUInt32(item.Id), Data = data, Length = data.Length, Network = Mode.LIN, Port = (int)Convert.ToInt32(item.Port) });
                                    OnSignalReceived?.Invoke(item); // note echos back to sending client....                                    
                                    continue;
                                }

                                // if periodic defined, setup and send
                                if (!string.IsNullOrEmpty(item.Periodic))
                                {
                                    PeriodicAddRemoveItem(item);
                                    continue;
                                }
                                // subscription defined
                                if (!string.IsNullOrEmpty(item.Subscribe))
                                {
                                    SubscritionAddRemoveItem(item);
                                    continue;
                                }

                                if (String.Equals(item.Type, Packet.Types[1], StringComparison.InvariantCultureIgnoreCase))
                                {
                                    var messageLookup = MessageDecodeManager.ARXML.FindMessageByName(item.Name, (int)Convert.ToInt32(item.Port));

                                    if (messageLookup == default(MessageDecodeManager.Message))
                                    {
                                        Debug.WriteLine("Message not found in Database!");
                                        continue;
                                    }

                                    var id = (uint)messageLookup.Id;
                                    var extended = messageLookup.Properties["addressingmode"].ToLower() == "extended";
                                    //var multiFrame = !messageLookup.SignalNames.Any(); // Important: Assume if NO signals then is multi-frame!!!!!!!!
                                    var name = messageLookup.Name;
                                    var multiFrame = name.StartsWith("USDT_") || name.StartsWith("UUDT_");

                                    var data = Convert.ToString(item.Value).FromHexString();

                                    _gbDatabase.SendRAW(id, data, (uint)data.Length, extended, multiFrame);
                                }
                                // vehicle sim message
                                else if (String.Equals(item.Type, Packet.Types[2], StringComparison.InvariantCultureIgnoreCase))
                                {
                                    if (Convert.ToString(item.Name).ToLower().Contains("Rx") &&
                                        Convert.ToString(item.Value).ToLower().Contains("flush"))
                                        ClientsFlush();
                                    if (Convert.ToString(item.Name).ToLower().Contains("subscriptions") &&
                                        Convert.ToString(item.Value).ToLower().Contains("clear"))
                                    {
                                        var port = -1; // ignore
                                        if (!string.IsNullOrEmpty(item.Port))
                                            port = Convert.ToInt32(item.Port) - 1;

                                        lock (_subscriptionsRxLock)
                                        {
                                            uint take;
                                            for (var i = 0; i < MessageDecodeManager.ARXML.Channels; i++)
                                            {
                                                if (port < 0 || i == port)
                                                {
                                                    while (!_subscriptionsRx[i].IsEmpty)
                                                        _subscriptionsRx[i].TryTake(out take);
                                                }
                                            }
                                        }
                                    }

                                    if (Convert.ToString(item.Name).ToLower().Contains("periodic") &&
                                        Convert.ToString(item.Value).ToLower().Contains("clear"))
                                    {
                                        _subscriptionsTx.Clear();
                                    }

                                    if (Convert.ToString(item.Name).ToLower().Contains("subscriptions") &&
                                        Convert.ToString(item.Value).ToLower().Contains("echo"))
                                    {
                                        lock (_subscriptionsRxLock)
                                        {
                                            var items = new List<Packet>();
                                            foreach (var s in _subscriptionsRx)
                                            {
                                                var ids = s.Value.ToArray();
                                                var unique_items = new HashSet<uint>(ids);

                                                foreach (var uniqueItem in unique_items)
                                                {
                                                    items.Add(new Packet()
                                                    {
                                                        Id = uniqueItem.ToString("X"),
                                                        Port = Convert.ToString(s.Key),
                                                        Type = Packet.Types[1],
                                                        Subscribe = "true"
                                                    });
                                                }
                                            }
                                            SocketClientSend(items);
                                        }
                                    }

                                    if (Convert.ToString(item.Name).ToLower().Contains("periodic") &&
                                        Convert.ToString(item.Value).ToLower().Contains("echo"))
                                    {
                                        var items = new List<Packet>();
                                        foreach (var entry in _subscriptionsTx)
                                            items.Add(entry.Value.Packet_);
                                        SocketClientSend(items);
                                    }
                                    // continue;
                                }
                                else if (String.Equals(item.Type, Packet.Types[3], StringComparison.InvariantCultureIgnoreCase)) // Raw
                                {
                                    var id = uint.Parse(Convert.ToString(item.Id),
                                        System.Globalization.NumberStyles.HexNumber);
                                    // 0x ... int intValue = Convert.ToInt32(Convert.ToString(item.Name) , 16);
                                    var data = Convert.ToString(item.Value).FromHexString();
                                    var extended = id > 2047; //(0x000007FF) 

                                    var multi = false;
                                    var fd = false;
                                    if (string.IsNullOrEmpty(item.Mode))
                                        item.Mode = "HS";  //default if nothing

                                    if (item.Mode.ToUpper() == Packet.Modes[0])
                                        multi = data.Length > 8;
                                    else if (item.Mode.ToUpper() == Packet.Modes[2])
                                        multi = true;
                                    else if (item.Mode.ToUpper() == Packet.Modes[3])
                                        fd = true;
                                    else if (item.Mode.ToUpper() == Packet.Modes[4])
                                    {
                                        multi = true;
                                        fd = true;
                                    }

                                    var port = Convert.ToInt32(item.Port) - 1;

                                    _gbDatabase.SendRAW(id, data, (uint)data.Length, extended, multi, fd, port);
                                }
                                // Signal(s)
                                else
                                {
                                    sendReady = true;
                                    var channel = Convert.ToInt32(item.Port) - 1;

                                    if (dictionary_Signal_Val[channel].ContainsKey(item.Name))
                                        dictionary_Signal_Val[channel][item.Name] = Convert.ToString(item.Value);
                                    else
                                        dictionary_Signal_Val[channel].Add(item.Name, Convert.ToString(item.Value));
                                }

                                OnSignalReceived?.Invoke(item); // note echos back to sending client....

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }

                        if (sendReady)
                        {
                            for (var i = 0; i < dictionary_Signal_Val.Count; i++)
                            {
                                if (dictionary_Signal_Val[i].Any())
                                    _gbDatabase.sendSignals(dictionary_Signal_Val[i], i);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
