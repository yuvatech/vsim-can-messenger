﻿using System;
using System.Collections.Generic;
using System.Net;


namespace Extension
{
    public class Packet
    {
        public static readonly string[] Types = { "Signal", "Message", "VehicleSim", "Raw" };
        public static readonly string[] Modes = { "HS", "LS", "MF", "FD", "FD_MF", "LIN" };
        public static readonly string[] Ports = { "1", "2" };

        //v1
        public string msgType;
        public string signalName;        
        public string value;

        //v2
        public string Id;
        public string Type;
        public string Mode;
        public string Name;
        public object Value;
        public string Port; // BUS1, BUS2, ...
        public string Periodic; // true / false
        public string Subscribe; // true / false
        public object Extension;  // first time being used

        //v3
        public string Service; // FSA, service name


        public static List<Packet> Transform(List<Packet> packet)
        {
            var transform = new List<Packet>();
            foreach (var item in packet)
            {
                // ### V2 -> V1 ###
                if (!string.IsNullOrEmpty(item.Name))
                {
                    if (string.IsNullOrEmpty(item.signalName))
                        item.signalName = item.Name;
                }

                if (item.Value != null)
                {
                    if (string.IsNullOrEmpty(item.value))
                      item.value = Convert.ToString(item.Value);
                }

                if (!string.IsNullOrEmpty(item.Mode))
                {
                    if (string.IsNullOrEmpty(item.msgType))
                      item.msgType = item.Mode;
                }

                // ### V1 -> V2 ###
                // Type
                if (string.IsNullOrEmpty(item.Type))
                {
                    if (item.msgType == "vehicle_sim") // old way
                        item.Type = Packet.Types[2];
                    else
                    {
                        item.Type = Types[0]; // v1 only sent signal types
                        item.Mode = item.msgType;
                    }
                }

                // Name
                if (string.IsNullOrEmpty(item.Name))
                {
                    if (!string.IsNullOrEmpty(item.signalName))
                        item.Name = item.signalName;
                }

                // Value
                if (item.Value == null)
                    item.Value = item.value;

                // Port
                if (string.IsNullOrEmpty(item.Port))
                    item.Port = Ports[0]; // default

                // Periodic
                /*
                if (string.IsNullOrEmpty(item.Periodic))
                    item.Periodic = Convert.ToString(false); // default
                */
                transform.Add(item);
            }

            return transform;
        }
    }
}
