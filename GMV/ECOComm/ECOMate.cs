﻿#define POLLING
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Linq.Expressions;
using System.Threading;
using System.Timers;
using HDLC;
using Prism.Events;
using log4net;

namespace ECOComm
{
    public class EcoMate : IDisposable
    {
        private const int AUTO_CONNECT_ATTEMPT = 10;
        private const int HEART_BEAT_TIMER = 1000;
        private const int HEART_BEAT_EXPIRED = 3000;
        private const int AUTO_CONNECT_TIMER = 2000;

        public const int HEART_BEAT_TIMEOUT = HEART_BEAT_TIMER + HEART_BEAT_EXPIRED + AUTO_CONNECT_TIMER;

        private static EcoMate mEcoMate = null;
        private SerialPort _serialPort = null;
        private HDLC.Transform mHDLCWrapper;

        // EcoMate Heartbeat
        private System.Threading.Thread _receiveHandler = null;
        private System.Timers.Timer mHeartBeatTimer = new System.Timers.Timer(HEART_BEAT_TIMER);
        private System.Timers.Timer mHeartBeatExpired = new System.Timers.Timer(HEART_BEAT_EXPIRED);
        private System.Timers.Timer mAutoReconnectTimer = new System.Timers.Timer(AUTO_CONNECT_TIMER);
        private int count = 0;
        public event EventHandler<bool> EcoMateHeartBeatNotReceived;
        public event EventHandler<bool> EcoMateAutoReconnected;
        private bool mIsAutoReconnectionInProgress = false;

        public delegate void MessageRawHandler(byte[] buffer);
        public event MessageRawHandler OnMessageFrameReceived;
        IEventAggregator eventAggregator;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EcoMate(String port = "", IEventAggregator ea = null)
        {
            eventAggregator = ea;
            mEcoMate = this;
            CommunicationSetup(port);

            mHeartBeatTimer.Elapsed += HeartBeatTimer_Elapsed;
            mHeartBeatExpired.Elapsed += HeartBeatExpired_Elapsed;
            mAutoReconnectTimer.Elapsed += AutoReconnectTimer_Elapsed;

            mAutoReconnectTimer.Enabled = false;
            mHeartBeatExpired.Enabled = false;
            mHeartBeatTimer.Enabled = false;

            // Load Encoder Decoder Comm : Native
            mHDLCWrapper = new Transform();
            mHDLCWrapper.OnReceived += MsgHandler_DataReceived;
        }

        ~EcoMate()
        {
            if (mAutoReconnectTimer != null)
                mAutoReconnectTimer.Enabled = false;
            if (mHeartBeatExpired != null)
                mHeartBeatExpired.Enabled = false;
            if (mHeartBeatTimer != null)
                mHeartBeatTimer.Enabled = false;

            if (mHeartBeatTimer != null)
                mHeartBeatTimer.Elapsed -= HeartBeatTimer_Elapsed;
            if (mHeartBeatExpired != null)
                mHeartBeatExpired.Elapsed -= HeartBeatExpired_Elapsed;
            if (mAutoReconnectTimer != null)
                mAutoReconnectTimer.Elapsed -= AutoReconnectTimer_Elapsed;
            if (mHDLCWrapper != null)
                mHDLCWrapper.OnReceived += MsgHandler_DataReceived;

            WindupCommunication();
        }

        private void MsgHandler_DataReceived(byte[] hdlc)
        {
            mHeartBeatExpired.Enabled = false;
            if (OnMessageFrameReceived != null)  // send ALL messages, skip that CANMessage Receiver logic!
                OnMessageFrameReceived(hdlc);
        }

        private void CommunicationSetup(string port = "")
        {
            // always close, just in-case!
            if (this._serialPort != null)
                Close();

            if (string.IsNullOrEmpty(port))
            {
                string[] ecomates = null;

                ecomates = ECOComm.ECOMateFinder.Find();

                // if none found keep current to try
                if (ecomates == null || !ecomates.Any())
                {
                    if (this._serialPort != null)
                        port = this._serialPort.PortName;
                }
                else
                {
                    // only care about 1 (so first)
                    port = ecomates.LastOrDefault(); // Note: sometimes last one holds on hence goes to bigger number
                }
            }


            //configure communication device
            if (!string.IsNullOrEmpty(port))
            {
                _serialPort = new SerialPort
                {
                    PortName = port,
                    //BaudRate = 115200,  // default
                    Parity = Parity.None,
                    DataBits = 8,
                    StopBits = StopBits.One,
                    Handshake = Handshake.XOnXOff,
                    ReadTimeout = 125, // -1, no time-out 
                    WriteTimeout = 125 // -1, no time-out 
                };
                // Need to timeout else may not keep up and fall behind to far!                
            }

            if (_serialPort == null)
                Console.WriteLine("Serial Port: Not Found " + port);
        }

        private void WindupCommunication()
        {
            // always close, just in-case!
            if (this._serialPort != null)
                Close();
        }

        private void AutoReconnectTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            mAutoReconnectTimer.Enabled = false;
            mIsAutoReconnectionInProgress = true;

            CommunicationSetup(); // find board again!!!

            if (Open())
            {
                System.Diagnostics.Debug.WriteLine("Done AutoReconnectTimer!");
                mIsAutoReconnectionInProgress = false;
                if (EcoMateAutoReconnected != null)
                    EcoMateAutoReconnected(this, true);
                return;
            }

            System.Diagnostics.Debug.WriteLine("Keep AutoReconnectTimer.");

            mAutoReconnectTimer.Enabled = true;
        }

        private void HeartBeatExpired_Elapsed(object sender, ElapsedEventArgs e)
        {
            mHeartBeatExpired.Enabled = false;
            mIsAutoReconnectionInProgress = true;
            mHDLCWrapper.Reset();
            System.Diagnostics.Debug.WriteLine("No Heart Beat Received!!!");
            if (EcoMateHeartBeatNotReceived != null)
                EcoMateHeartBeatNotReceived(this, false);
            Close();
            Thread.Sleep(2000);
            mAutoReconnectTimer.Enabled = true;
            System.Diagnostics.Debug.WriteLine("Start AutoReconnectTimer.");
        }

        public bool IsAutoReconnectionInProgress()
        {
            return mIsAutoReconnectionInProgress;
        }

        private void HeartBeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ControlMsg ctrlMsg = new ControlMsg();
            ctrlMsg.Type = MessageTypes.CTRL_HEART_BEAT;
            byte[] buff = new byte[4];
            ctrlMsg.Buffer = buff;
            Write(ctrlMsg.GetBuffer());
            mHeartBeatExpired.Enabled = true;
            //System.Diagnostics.Debug.WriteLine("Heart Beat: " + ++count);
        }

        // https://www.mono-project.com/archived/howtosystemioports/
        private void Serial_Poller()
        {
            while (true)
            {
                try
                {
                    if (_serialPort != null && _serialPort.IsOpen)
                    {
                        var buffer = new List<byte>();

                        while (_serialPort.BytesToRead <= 0) // wait for data
                            System.Threading.Thread.Sleep(30);

                        if (_serialPort.BytesToRead > 0)
                        {
                            var temp = new byte[_serialPort.BytesToRead];
                            _serialPort.Read(temp, 0, temp.Length);
                            buffer.AddRange(temp);
                        }

                        if (buffer.Count > 0)
                        {
                            byte[] outBuff = new byte[byte.MaxValue];
                            mHDLCWrapper.DecodeFrame(buffer.ToArray(), ref outBuff);
                        }
                    }
                    else
                    {
                        break;  // no serial port no reading
                    }
                }
                catch (System.TimeoutException ex)
                {
                    if (_serialPort != null)
                        _serialPort.DiscardInBuffer();
                    System.Diagnostics.Debug.WriteLine("Rx: Timeout, LOST message(s)!!!");
                }
                catch (System.IO.IOException ex)
                {
                    break;  // serial port needs to be re-connected
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    System.Threading.Thread.Sleep(25);
                }
            }
        }

        // Data Received On Serial Bus
        private void Serial_ReceivedEvent(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort serialPort = (SerialPort)sender;
            if (serialPort != null && !serialPort.IsOpen)
                return;
            Int32 bytesToRead = serialPort.BytesToRead;
            byte[] buffer = null;

            if (bytesToRead > 0 && _serialPort.IsOpen)
            {
                buffer = new byte[bytesToRead];
                serialPort.Read(buffer, 0, bytesToRead);

                // Decode Received CAN Message
                byte[] outBuff = new byte[byte.MaxValue];
                mHDLCWrapper.DecodeFrame(buffer, ref outBuff);
                // Notify Received CAN message 
                //System.Diagnostics.Debug.WriteLine("Serial Data Received: " + BitConverter.ToString(buffer));
            }
        }

        public bool IsOpen()
        {
            if (_serialPort == null)
                return false;
            return _serialPort.IsOpen;
        }

        public bool IsActive()
        {
            return !mHeartBeatExpired.Enabled;
        }

        public bool Open()
        {
            if (_serialPort == null)
                return false;

            int connectionAttemptCounter = 0;

            var attempts = 3; // 3 strikes out
            while (attempts-- > 0)
            {
                try
                {
                    Close();
                    if (_serialPort != null)
                    {
                        var baudrates = new[] { 115200, 128000, 153600, 230400, 256000, 460800, 921600, 268435456 };
                        baudrates = baudrates.Reverse().ToArray();  // highest first

                        foreach (var baudrate in baudrates)
                        {
                            try
                            {
                                log.DebugFormat("Attempting connection for baudrate {0} on serial port {1}", baudrate, _serialPort?.PortName);
                                _serialPort.BaudRate = baudrate;
                                if (_serialPort != null && !_serialPort.IsOpen)
                                    _serialPort.Open();

                                connectionAttemptCounter++;
                                while (!_serialPort.IsOpen && connectionAttemptCounter <= AUTO_CONNECT_ATTEMPT)
                                {
                                    connectionAttemptCounter++;
                                    System.Threading.Thread.Sleep(125);
                                    log.DebugFormat("Attempting connection Initialization for baudrate {0} on serial port {1}. Attempt [{2}/{3}]", baudrate, _serialPort?.PortName, connectionAttemptCounter, AUTO_CONNECT_ATTEMPT);
                                }
                                _serialPort.DiscardInBuffer();
                                _serialPort.DiscardOutBuffer();
                                Console.WriteLine("BaudRate: " + baudrate);
                                break;
                            }
                            catch (Exception exx)//(ArgumentOutOfRangeException ex), Linux not through this one :(
                            {
                                log.Error(string.Format("Error connecting for baudrate {0} on serial port {1}", baudrate, _serialPort?.PortName), exx);
                                //Debug.WriteLine(ex.Message);  // will tell max baud rate
                                if (_serialPort != null)
                                    _serialPort.Close();
                                Thread.Sleep(250);
                                continue;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Error connecting for serial port {0}", _serialPort?.PortName), ex);
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    if (attempts <= 0)
                        return false;
                }
                System.Threading.Thread.Sleep(350);
            }

#if POLLING
            if (_receiveHandler == null)
                _receiveHandler = new System.Threading.Thread(Serial_Poller);

            if (_receiveHandler != null && !_receiveHandler.IsAlive)
                _receiveHandler.Start();
#else
            _serialPort.DataReceived += Serial_ReceivedEvent;
#endif

            // TODO : Enable post testing                
            if (mHeartBeatTimer != null)
                mHeartBeatTimer.Enabled = true;

            return true;
        }

        public bool Close()
        {
            var status = false;

            log.DebugFormat("Disconnecting threads serial port {0}", _serialPort?.PortName);

            if (mHeartBeatTimer != null)
                mHeartBeatTimer.Enabled = false;
            if (mHeartBeatExpired != null)
                mHeartBeatExpired.Enabled = false;
            if (mAutoReconnectTimer != null)
                mAutoReconnectTimer.Enabled = false;

            try
            {
                if (_receiveHandler != null && _receiveHandler.IsAlive)
                    _receiveHandler.Abort();
            }
            catch (Exception)
            {
            }

            if (_serialPort != null && _serialPort.IsOpen)
            {
                try
                {
                    _serialPort.DiscardOutBuffer();
                    _serialPort.DiscardInBuffer();
                    log.DebugFormat("Discarded serial port buffers on serial port {0}", _serialPort?.PortName);
#if POLLING
#else
                    _serialPort.DataReceived -= Serial_ReceivedEvent;
#endif
                    // wait until can close device!  handles windows not letting go of device on close fast enough
                    Task.Run(() =>
                    {
                        int connectionAttemptCounter = 0;
                        while (_serialPort != null && _serialPort.IsOpen && ++connectionAttemptCounter <= AUTO_CONNECT_ATTEMPT)
                        {
                            try
                            {
                                log.DebugFormat("Attempting closing of serial port {0}", _serialPort?.PortName);
                                _serialPort.Close();
                            }
                            catch (Exception exClose)
                            {
                                log.Error(string.Format("Exception closing of serial port {0}", _serialPort?.PortName), exClose);
                                // if already closed
                                Thread.Sleep(new TimeSpan(0, 0, 0, 1)); // 1 sec.
                            }
                        }
                        status = true;
                    }).Wait(new TimeSpan(0, 0, 0, 12));  // ~ seconds
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Exception closing of serial port {0}", _serialPort?.PortName), ex);
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }

#if POLLING
            _receiveHandler = null; // reset
#endif
            return status;
        }

        private readonly object _writeLock = new object();
        public int Write(byte[] buffer)
        {
            // HDLC Encode 
            //lock (_writeLock)
            {
                // Todo: something that reacts to missed sends!
                //while (true)
                {
                    try
                    {
                        byte[] send = new byte[ushort.MaxValue];
                        int encodeFrameLength = mHDLCWrapper.EncodeFrame(buffer, (ushort)buffer.Length, ref send);

                        // Send to Serial
                        //if (_serialPort != null && !_serialPort.IsOpen)
                        //    _serialPort.Open();
                        if (_serialPort != null && _serialPort.IsOpen)
                        {
                            log.DebugFormat("Established connection on {0}", _serialPort?.PortName);
                            lock (_writeLock)  // only one can write at time to make sure is full message that got out not a mix!
                            {
                                try
                                {
                                    log.DebugFormat("Writing payload to {0}, Length: {1}", _serialPort?.PortName, encodeFrameLength);
                                    _serialPort.Write(send, 0, encodeFrameLength);
                                    log.DebugFormat("Completed writing payload on {0}", _serialPort?.PortName);
                                }
                                catch (System.TimeoutException ex)
                                {
                                    log.Error(string.Format("Exception writing payload to {0}", _serialPort?.PortName), ex);
                                    _serialPort.DiscardOutBuffer();
                                    System.Diagnostics.Debug.WriteLine("Tx: Timeout, LOST message(s)!!!");
                                    //    System.Threading.Thread.Sleep(25);
                                    //    continue; // on timeout try again!
                                    return -1;
                                }
                                //finally
                                //{
                                //    if (_serialPort != null && _serialPort.IsOpen)
                                //        _serialPort.Close();
                                //    log.DebugFormat("Closed connection on {0}", _serialPort.PortName);
                                //}
                            }
                        }
                        // break;
                    }
                    catch (Exception exWrite)
                    {
                        log.Error(string.Format("Exception writing payload to {0}", _serialPort?.PortName), exWrite);
                        System.Diagnostics.Debug.WriteLine(exWrite.ToString());
                        //     break;
                        return -2;  // unknown
                    }
                    //finally
                    //{
                    //    if (_serialPort != null && _serialPort.IsOpen)
                    //        _serialPort.Close();
                    //}
                }
                return 1;
            }
            return 0;
        }

        public void Dispose()
        {
            this.Close();
        }
    }

}
