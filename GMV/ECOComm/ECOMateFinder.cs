﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ECOComm
{
    public class ECOMateFinder
    {
        public static string[] Find()
        {
            const ushort UDEV_VID = 0xc251;
            const ushort UDEV_PID = 0x4705;
            
            var ecomates = new List<string>();
            try
            {
                ecomates = FindSerialPorts(UDEV_VID, UDEV_PID);
            }
            catch { }
            return ecomates.ToArray();
        }
               
        #region EcoMate Port Detection on Windows and Linux

        private static List<string> FindSerialPortsOnWindows(ushort vid, ushort pid)
        {
            var comports = new List<string>();

            var rx = new Regex(string.Format("^VID_{0:X04}.PID_{1:X04}", vid, pid), RegexOptions.IgnoreCase);

            var rk2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum", false);
            foreach (var s3 in rk2.GetSubKeyNames())
            {
                var rk3 = rk2.OpenSubKey(s3);
                foreach (var s in rk3.GetSubKeyNames())
                {
                    if (rx.Match(s).Success)
                    {
                        var rk4 = rk3.OpenSubKey(s);
                        foreach (var s2 in rk4.GetSubKeyNames())
                        {
                            var rk5 = rk4.OpenSubKey(s2);
                            var rk6 = rk5.OpenSubKey("Device Parameters");

                            try
                            {
                                comports.Add((string)rk6.GetValue("PortName"));
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }
                    }
                }
            }

            var result = new List<string>(comports.Count);
            foreach (var str in SerialPort.GetPortNames())
            {
                if (comports.Contains(str)) result.Add(str);
            }

            return result;
        }

        private static List<string> FindSerialPortsOnUnix(ushort vid, ushort pid)
        {
            //        FileName = "lsusb",
            //        Arguments = string.Format("-d {0:04x}:{1:04x}", vid, pid),

            var comports = new List<string>();
            var rxPath = new Regex(@"^P\:\s+", RegexOptions.Compiled);
            var rxDevPath = new Regex(@"^E\:\s+DEVNAME\s*=\s*(.+)$", RegexOptions.Compiled);
            var rxVendor = new Regex(@"^E\:\s+ID_VENDOR_ID\s*=\s*" + vid.ToString("x04") + @"\s*$", RegexOptions.Compiled);
            var rxProduct = new Regex(@"^E\:\s+ID_MODEL_ID\s*=\s*" + pid.ToString("x04") + @"\s*$", RegexOptions.Compiled);

            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "udevadm",
                    Arguments = "info --export-db",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            proc.Start();

            string path = null;
            bool vidFound = false, pidFound = false;
            while (true)
            {
                var line = proc.StandardOutput.ReadLine();
                if (null == line) break;

                if (rxPath.IsMatch(line))
                {
                    path = null;
                    vidFound = false;
                    pidFound = false;
                }
                else if (rxVendor.IsMatch(line))
                {
                    vidFound = true;
                }
                else if (rxProduct.IsMatch(line))
                {
                    pidFound = true;
                }
                else
                {
                    var m = rxDevPath.Match(line);
                    if (m.Success) path = m.Groups[1].Value;
                }

                if (vidFound && pidFound && path != null)
                {
                    if (path.Contains("tty")) comports.Add(path);

                    path = null;
                    vidFound = false;
                    pidFound = false;
                }
            }
            proc.WaitForExit();

            return comports;
        }

        public static List<string> FindSerialPorts(ushort vid, ushort pid)
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                    return FindSerialPortsOnWindows(vid, pid);

                case PlatformID.Unix:
                    return FindSerialPortsOnUnix(vid, pid);

                default:
                    return new List<string>(0);
            }
        }

        #endregion
    }
}
