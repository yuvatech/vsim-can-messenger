﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOComm
{
    public class ControlMsg
    {
        public byte[] Buffer = null;
        public UInt16 Type = 0;

        public ControlMsg()
        {

        }

        public ControlMsg(byte[] buff)
        {
            Type = BitConverter.ToUInt16(buff, 0);
            Buffer = new byte[buff.Length - 2];
            Array.Copy(buff, 2, Buffer, 0, buff.Length - 2);
        }

        public byte[] GetBuffer()
        {
            byte[] buffer = null;

            if (Buffer != null)
            {
                UInt16 length = (UInt16)(Buffer.Length + 2);
                buffer = new byte[length];                
                Array.Copy(BitConverter.GetBytes(Type), 0, buffer, 0, sizeof(UInt16));
                Array.Copy(Buffer, 0, buffer, 2, Buffer.Length);

            }
            else
            {
                UInt16 length = 2;
                buffer = new byte[length];                
                Array.Copy(BitConverter.GetBytes(Type), 0, buffer, 0, sizeof(UInt16));                
            }

            return buffer;
        } 
    }
}
