﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOComm
{
    public class CANMessage
    {
        private byte[] mCanBuffer;
        private UInt32 mCanId;
        private Byte mDataLen;
        private bool mExtendedCAN;
        private bool mHighVoltage;
        private UInt16 mCanType;
        private UInt16 mCycleTime;

        public byte[] CanBuffer
        {
            get
            {
                return mCanBuffer;
            }

            set
            {
                mCanBuffer = value;
            }
        }

        public uint CanId
        {
            get
            {
                return mCanId;
            }

            set
            {
                mCanId = value;
            }
        }

        public Byte DataLen
        {
            get
            {
                return mDataLen;
            }

            set
            {
                mDataLen = value;
            }
        }

        public bool ExtendedCAN
        {
            get
            {
                return mExtendedCAN;
            }

            set
            {
                mExtendedCAN = value;
            }
        }

        public bool HighVoltage
        {
            get
            {
                return mHighVoltage;
            }

            set
            {
                mHighVoltage = value;
            }
        }

        public ushort MsgType
        {
            get
            {
                return mCanType;
            }

            set
            {
                mCanType = value;
            }
        }

        public ushort CycleTime
        {
            get
            {
                return mCycleTime;
            }

            set
            {
                mCycleTime = value;
            }
        }

        public CANMessage()
        {

        }

        public CANMessage(byte[] buffer)
        {
            //1. Message Type = 2 Bytes
            //2. Cycle Time = 2 bytes
            //3. High Voltage = 1 Bytes
            //4. Extended CAN = 1 Bytes
            //5. CAN DataLength = 1 Bytes
            //6. CAN ID = 4 Bytes
            //7. CAN Message
            MsgType = BitConverter.ToUInt16(buffer, 3);
            mCycleTime = BitConverter.ToUInt16(buffer, 5);
            HighVoltage = (buffer[7] == 1) ? true : false;
            ExtendedCAN = (buffer[8] == 1) ? true : false;

            DataLen = buffer[9];

            CanId = BitConverter.ToUInt32(buffer, 10) & (0x000007FF);
			if (ExtendedCAN == true)
                CanId = BitConverter.ToUInt32(buffer, 10) & (0x1FFFFFFF);

            if (DataLen == 0 || (buffer.Length - 14) != DataLen)  // lets make sure we got the amount of data we are supposed to
            {
                Debug.WriteLine("Ecomate Bad Data Length!!!");
                return; // No data sent
            }

            mCanBuffer = new byte[DataLen];
            Array.Copy(buffer, 14, CanBuffer, 0, DataLen);
            return;
        }

        private byte[] GetBuffer(UInt32 canId, bool extendedCAN, bool highVoltage, byte[] buffer, UInt16 buffSize)
        {
            UInt16 packetType = (ushort)mCanType;
            UInt16 cycleTime = (ushort)mCycleTime;
            Byte size = (Byte)buffSize;
            Byte highVolt = (Byte)(highVoltage ? 1 : 0);
            Byte extenCAN = (Byte)(extendedCAN ? 1 : 0);

            //1. Message Type = 2 Bytes
            //2. Cycle Time = 2 bytes
            //3. High Voltage = 1 Bytes
            //4. Extended CAN = 1 Bytes
            //5. CAN DataLength = 1 Bytes
            //6. CAN ID = 4 Bytes
            //7. CAN Message

            byte[] buff = new byte[buffSize + 11];

            byte[] tempArr = BitConverter.GetBytes(packetType);
            Array.Copy(tempArr, 0, buff, 0, 2);

            tempArr = BitConverter.GetBytes(cycleTime);
            Array.Copy(tempArr, 0, buff, 2, 2);

            tempArr = BitConverter.GetBytes(highVolt);
            Array.Copy(tempArr, 0, buff, 4, 1);

            tempArr = BitConverter.GetBytes(extenCAN);
            Array.Copy(tempArr, 0, buff, 5, 1);

            tempArr = BitConverter.GetBytes(size);
            Array.Copy(tempArr, 0, buff, 6, 1);

            tempArr = BitConverter.GetBytes(canId);
            Array.Copy(tempArr, 0, buff, 7, 4);

            Array.Copy(buffer, 0, buff, 11, size);

            return buff;
        }

        public byte[] GetBuffer()
        {
            return GetBuffer(mCanId, mExtendedCAN, mHighVoltage, mCanBuffer, mDataLen);
        }
    }
}
