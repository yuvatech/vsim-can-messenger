﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOComm
{
    public class LINMessage
    {
        public byte[] Buffer { get; set; }

        public uint Id { get; set; }
        public uint Port { get; set; }

        public byte Length { get; set; }

        public ushort Type { get; set; }


        public LINMessage()
        {
        }

        public LINMessage(byte[] buffer)
        {
            Type = BitConverter.ToUInt16(buffer, 3);
            var NOT_USED_0 = BitConverter.ToUInt16(buffer, 5);
            var NOT_USED_1 = buffer[7];
            Port = buffer[8];

            Length = buffer[9];

            Id = BitConverter.ToUInt32(buffer, 10) ;

            Buffer = new byte[Length];
            if(Length > 0)
              Array.Copy(buffer, 14, Buffer, 0, Length);
            return;
        }

        private byte[] GetBuffer(UInt32 id, byte[] buffer, UInt16 buffSize)
        {
            UInt16 packetType = (ushort)Type;
            Byte size = (Byte)buffSize;

            byte[] buff = new byte[buffSize + 11];

            byte[] tempArr = BitConverter.GetBytes(packetType);
            Array.Copy(tempArr, 0, buff, 0, 2); // type, 16 bit

            tempArr = BitConverter.GetBytes(0);
            Array.Copy(tempArr, 0, buff, 2, 2); // not used, 16 bit

            tempArr = BitConverter.GetBytes(0);
            Array.Copy(tempArr, 0, buff, 4, 1); // not used, 8 bit

            tempArr = BitConverter.GetBytes(Port);
            Array.Copy(tempArr, 0, buff, 5, 1);  // port, 8 bit

            tempArr = BitConverter.GetBytes(size);
            Array.Copy(tempArr, 0, buff, 6, 1); // length, 8 bit

            tempArr = BitConverter.GetBytes(id);
            Array.Copy(tempArr, 0, buff, 7, 4); // id, 32 bit

            Array.Copy(buffer, 0, buff, 11, size); // buffer data

            return buff;
        }

        public byte[] GetBuffer()
        {
            return GetBuffer(Id, Buffer, Length);
        }
    }
}
