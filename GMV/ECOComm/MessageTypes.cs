﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOComm
{
    public class MessageTypes
    {
        public const UInt16 CAN_HS = 1;
        public const UInt16 CAN_LS = 2;

        public const UInt16 CTRL_HEART_BEAT = 3;

        public const UInt16 CTRL_USB_ARB = 20;
        public const UInt16 CTRL_HARD_RESET = 17;
        public const UInt16 CTRL_BOOTLOADER_ENTER = 13;
        public const UInt16 CTRL_POWER_SWITCH = 21;
        public const UInt16 CTRL_COM_ENBL = 22;
        public const UInt16 CTRL_REVERSE = 23;
        public const UInt16 CTRL_CAN_TERM = 24;

        public const UInt16 CAN_FD = 25;
        public const UInt16 CTRL_MEDIA_ARB = 26;
        public const UInt16 CTRL_GlOW_PLUG = 27;
        public const UInt16 CAN_LS_Ctrl = 28;
        public const UInt16 LIN = 29;
        public const UInt16 CLEA_SETTINGS = 30;

        // TODO : Add Appropriate Message Type
    }
}
