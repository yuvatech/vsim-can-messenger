﻿using log4net;
using Prism.Events;
using Prism.Mvvm;
using SIM.Core;
using SIM.Core.Device;
using System;
using System.Collections.ObjectModel;
using UsingEventAggregator.Core;
using VSIM.Core;
using VSIM.Core.Device;

namespace ModuleB.ViewModels
{
    public class MessageListViewModel : BindableBase
    {
        IEventAggregator _ea;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ObservableCollection<string> _messages;
        public ObservableCollection<string> Messages
        {
            get { return _messages; }
            set { SetProperty(ref _messages, value); }
        }

        private string _port = "COM1";
        public string Port
        {
            get { return _port; }
            set { SetProperty(ref _port, value); }
        }

        public MessageListViewModel(IEventAggregator ea)
        {
            _ea = ea;
            Messages = new ObservableCollection<string>();
            //_ea.GetEvent<MessageSentEvent>().Subscribe(MessageReceived);
            //_ea.GetEvent<ECommMessageEvent>().Subscribe(ECommMessageReceived);
        }

        private void ECommMessageReceived(string message)
        {
            Messages.Add(message);
        }

        private void MessageReceived(string message)
        {
            MessageListViewModel.log.DebugFormat("Test Message Received: {0}", message);
            //EcoMate objMessageEComm = new EcoMate();
            //ECOComm.EcoMate deviceECOM = new ECOComm.EcoMate(Port, this._ea);
            //var messageReceived = new SIM.Core.Device.Message()
            //{
            //    Data = System.Text.ASCIIEncoding.ASCII.GetBytes(message),
            //    HighVoltage = true,
            //    Id = 101,
            //    MultiFrame = false,
            //    Network = Mode.FD,
            //    Extended = false
            //};

            //var messages = objMessageEComm.ToCANMessage(messageReceived);            
            //foreach(dynamic sendMessage in messages)
            //{
            //    deviceECOM.Write(sendMessage.GetBuffer());
            //}
            
            //System.IO.Ports.SerialPort endPort = new System.IO.Ports.SerialPort("COM2");            
            //try
            //{
            //    endPort.Open();
            //    MessageListViewModel.log.DebugFormat("Received the Message on Serail Port: {0}", endPort.ReadExisting());
            //}
            //finally
            //{
            //    endPort.Close();
            //}
            Messages.Add(message);
        }
    }
}
